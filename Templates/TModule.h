#pragma once

#include "Templates/TCommon.h"

#include <list>
#include "Templates/TSingleton.h"
#include "Templates/TFactory.h"
#include "ThreadModule/IThreadMessageCB.h"
#include "ThreadModule/ThreadFactory.h"
#include "ThreadModule/IThread.h"
#include "FrameworkUtilities/FwkString.h"
#include "Utilities/Tracing.h"

template <class InterfaceImpl, class Interface = InterfaceImpl> 
class CNekObjectMod : public IThreadMessageCB
{
public:
	CNekObjectMod()
	{
		m_pThread = 0;
	}
	virtual ~CNekObjectMod(){}

	inline IThread * GetThread() const { return m_pThread; }
	inline virtual void SetThread(IThread * val) { m_pThread = val;	}

	virtual bool SendThreadMessage(int nMsg,NK_OBJPOINTER nParam = 0)
	{
		if(m_pThread)
			return m_pThread->SendThreadMessage(nMsg, this, nParam);
		return false;
	}
	virtual void SetFactory(ITFactory<Interface> * pFactory)
	{
		m_pFactory = pFactory;
	}
	virtual void ReleaseObjMod(Interface * pObj)
	{
		if(m_pFactory)
			m_pFactory->FreeResource(pObj);
	}

protected:
	IThread * m_pThread;
	ITFactory<Interface> * m_pFactory;
};

template <class InterfaceImpl, class Interface = InterfaceImpl> 
class TRealImplementation :	public CNekObjectMod<InterfaceImpl, Interface>
{
public:
	inline void SetThread(IThread * val)
	{
		CNekObjectMod<InterfaceImpl,Interface>::SetThread(val);
		m_Implementation.SetThread(val);
	}
	inline InterfaceImpl & GetImplementation()
	{
		return m_Implementation;
	}
	virtual bool SendThreadMessage(int nMsg,NK_OBJPOINTER nParam = 0)
	{
		return m_Implementation.SendThreadMessage(nMsg, nParam);
	}
protected:
	InterfaceImpl m_Implementation;
};

template <class InterfaceImpl, class Interface = InterfaceImpl> 
class TFactoryAssignThread : public TFactory<InterfaceImpl, Interface>
{
public:

	bool SetNumThreads(int nNumThreads);
	void SetModuleName(const CFwkString & fstrName)
	{
		this->m_fstrModuleName = fstrName;
	}

	virtual int InitThreads();
	virtual int TerminateThreads();

	virtual Interface* GetResource();
protected:
	enum MODE_THREADS
	{
		NO_ASSIGN_THREADS,
		ASSIGN_FROM_POOL,
		ASSIGN_ONE_EACH
	};
	MUTEXCLASS m_ModMutex;
	std::list<IThread*> m_ListThreads;
	CFwkString m_fstrModuleName;
	int m_nMode;
};

template <class InterfaceImpl, class Interface>
bool TFactoryAssignThread<InterfaceImpl, Interface>::SetNumThreads( int nNumThreads )
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, this->m_ModMutex, 0);
	if(nNumThreads == 0)
	{
		m_nMode = NO_ASSIGN_THREADS;
	}
	else if(nNumThreads > 0)
	{
		m_nMode = ASSIGN_FROM_POOL;
		size_t nNumRealThreads = nNumThreads - m_ListThreads.size();
		for (size_t i = 0; i < nNumRealThreads; i++)
		{
			CFwkString fstrNameReal = this->m_fstrModuleName;
			fstrNameReal.sprintfc("_%d",m_ListThreads.size());
			IThread * pThread = CThreadFactory::St_GetInstance()->GetResource();
			SIX_ASSERT(pThread);
			if(pThread)
			{
				m_ListThreads.push_back(pThread);
				pThread->SetThreadName(fstrNameReal);
				pThread->Start();
			}
		}
	}
	else
	{
		m_nMode = ASSIGN_ONE_EACH;
		TerminateThreads();
	}
	return true;
}

template <class InterfaceImpl, class Interface>
Interface* TFactoryAssignThread< InterfaceImpl, Interface>::GetResource()
{
	Interface *pObj = TFactory<InterfaceImpl,Interface>::GetResource();
	switch(m_nMode)
	{
	case NO_ASSIGN_THREADS:
		break;
	case ASSIGN_FROM_POOL:
		{
			ACE_GUARD_RETURN(MUTEXCLASS, guard,this->m_ModMutex, 0);
			InterfaceImpl * pRealObj = dynamic_cast < InterfaceImpl* > (pObj);
			SIX_ASSERT(pRealObj);
			if (pRealObj)
			{
				if (pRealObj->GetThread() == NULL)
				{
					IThread * pThread = m_ListThreads.front();
					m_ListThreads.pop_front();
					m_ListThreads.push_back(pThread);
					pRealObj->SetFactory(this);
					pRealObj->SetThread(pThread);
				}
			}
		}		
		break;
	case ASSIGN_ONE_EACH:
		{
			ACE_GUARD_RETURN(MUTEXCLASS, guard,this->m_ModMutex, 0);
			InterfaceImpl * pRealObj = dynamic_cast < InterfaceImpl* > (pObj);
			if (pRealObj->GetThread() == NULL)
			{
				CFwkString fstrNameReal = this->m_fstrModuleName;
				fstrNameReal.sprintfc("_%p",pRealObj);
				IThread * pThread = CThreadFactory::St_GetInstance()->GetResource();
				SIX_ASSERT(pThread);
				if(pThread)
				{
					pThread->SetThreadName(fstrNameReal);
					pThread->Start();
					pRealObj->SetFactory(this);
					pRealObj->SetThread(pThread);
				}				
			}			
		}		
		break;
	default:
		break;
	}		
	return pObj;
}

template <class InterfaceImpl, class Interface>
int TFactoryAssignThread<InterfaceImpl, Interface>::InitThreads()
{
	if (m_ListThreads.size() == 0)
	{
		SetNumThreads(1);
	}
	return 0;
}

template <class InterfaceImpl, class Interface>
int TFactoryAssignThread<InterfaceImpl, Interface>::TerminateThreads()
{
	std::list<IThread*>::iterator threadIterator = m_ListThreads.begin();
	while (threadIterator != m_ListThreads.end())
	{
		IThread * pThread = *threadIterator;
		if(pThread)
		{
			pThread->Stop();
			CThreadFactory::St_GetInstance()->FreeResource(pThread);
		}
		threadIterator++;
	}
	m_ListThreads.clear();
	return 0;
}

template <class Module,class InterfaceImpl, class Interface = InterfaceImpl> 
class TModule : public TFactoryAssignThread<InterfaceImpl, Interface>
				, public TSingleton<Module>
{
public:
	typedef TModule<Module,InterfaceImpl,Interface> SUPERMOD;

	virtual int Init();
	virtual int Exit();
};


template <class Module,class InterfaceImpl, class Interface>
int TModule<Module,InterfaceImpl, Interface>::Init()
{
	return TFactoryAssignThread<InterfaceImpl, Interface>::InitThreads();
}

template <class Module,class InterfaceImpl, class Interface>
int TModule<Module,InterfaceImpl, Interface>::Exit()
{
	return TFactoryAssignThread<InterfaceImpl, Interface>::TerminateThreads();
}
