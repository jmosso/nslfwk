#pragma once

#include "FrameworkUtilities/FwkString.h"
#include "Utilities/Tracing.h"
#include "Templates/TFactory.h"

template <class InterfaceImpl, class Interface = InterfaceImpl> 
class TFactoryNavigator : public TFactory<InterfaceImpl,Interface>
{
public:

	bool IsHere(Interface* pObjectToFind) ;
	bool IsBusy(Interface* pObjectToFind) ;
	bool setBusy(Interface* pObjectToFind,bool busy = true);
};

template <class InterfaceImpl, class Interface> 
bool TFactoryNavigator<InterfaceImpl,Interface>::IsHere(Interface* pObjectToFind) 
{
	bool bIsHere = false;

	ACE_GUARD_RETURN(MUTEXCLASS, guard, this->m_mutex_Factory, false);

	typename TFactory<InterfaceImpl,Interface>::OBJECTMAP::iterator i = this->m_BusyMap.find(pObjectToFind);

	if(i == this->m_BusyMap.end())
	{
		i = this->m_FreeMap.find(pObjectToFind);

		if(i == this->m_FreeMap.end())
		{
			return false;
		}
	}
	
	return true;
}

template <class InterfaceImpl, class Interface> 
bool TFactoryNavigator<InterfaceImpl,Interface>::setBusy(Interface* pObjectToFind,bool busy)
{
	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard, this->m_mutex_Factory, false);
	
	if(busy)
	{
		typename TFactory<InterfaceImpl,Interface>::OBJECTMAP::iterator i = this->m_FreeMap.find(pObjectToFind);

		if(i == this->m_FreeMap.end())
		{
			return false;
		}
		this->m_BusyMap.insert(std::pair <Interface *, int>(pObjectToFind, 0));
		this->m_FreeMap.erase(i);
	}
	else
	{
		typename TFactory<InterfaceImpl,Interface>::OBJECTMAP::iterator i = this->m_BusyMap.find(pObjectToFind);

		if(i == this->m_BusyMap.end())
		{
			SIX_ASSERT(!"This object is not busy");
			return false;
		}
		this->m_FreeMap.insert(std::pair <Interface *, int>(pObjectToFind, 0));
		this->m_BusyMap.erase(i);
	}
	return true;
}

template <class InterfaceImpl, class Interface> 
bool TFactoryNavigator<InterfaceImpl,Interface>::IsBusy(Interface* pObjectToFind) 
{
	bool bIsHere = false;
	ACE_GUARD_RETURN(MUTEXCLASS, guard, this->m_mutex_Factory, false);
	typename TFactory<InterfaceImpl,Interface>::OBJECTMAP::iterator i = this->m_BusyMap.find(pObjectToFind);

	if(i == this->m_BusyMap.end())
	{
		return false;
	}
	return true;
}
