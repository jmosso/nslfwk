#pragma once

template <class ObjectToStore> 
class TDrawer 
{
public:
	
    TDrawer(ObjectToStore* pObjToStore);
    virtual ~TDrawer();
	
    virtual short Attach(TDrawer* pTDrawerPartner);
    virtual short RemoveDrawer();
    virtual short FreeDrawer();
	
    bool 			m_bInUse;
	ObjectToStore*	m_pObjStored;
    TDrawer*		m_pDrawerNext;
    TDrawer* 		m_pDrawerPrev;

	int				m_iDrawerSeq;
};

template <class ObjectToStore> 
TDrawer<ObjectToStore>::TDrawer(ObjectToStore * pObjToStore)
{
	m_pObjStored = pObjToStore;
	m_bInUse = false;
    m_pDrawerNext = (TDrawer*)0;
    m_pDrawerPrev = (TDrawer*)0;
	m_iDrawerSeq = 0;
}

template <class ObjectToStore> 
TDrawer<ObjectToStore>::~TDrawer()
{
}

template <class ObjectToStore> 
short TDrawer<ObjectToStore>::Attach(TDrawer* pDrawerPartner)
{
	m_pDrawerPrev = pDrawerPartner;
	m_pDrawerNext = pDrawerPartner->m_pDrawerNext;
	pDrawerPartner->m_pDrawerNext = this;

	if(m_pDrawerNext != 0)
	{
		m_pDrawerNext->m_pDrawerPrev = this;
	}

    return 0;
}

template <class ObjectToStore> 
short TDrawer<ObjectToStore>::RemoveDrawer()
{
	if(m_pDrawerPrev)
		m_pDrawerPrev->m_pDrawerNext = m_pDrawerNext;
	if (m_pDrawerNext)
		m_pDrawerNext->m_pDrawerPrev = m_pDrawerPrev;

	m_bInUse = false;
    m_pDrawerNext = (TDrawer*)0;
    m_pDrawerPrev = (TDrawer*)0;
	m_iDrawerSeq = 0;

	return 0;
}

template <class ObjectToStore> 
short TDrawer<ObjectToStore>::FreeDrawer()
{
	m_bInUse = false;	
    return 0;	
}

