#pragma once

template <class Interface> 
class ITFactory
{
public:
	virtual Interface* GetResource() = 0;	
	virtual bool FreeResource(Interface* pObj) = 0;
};

