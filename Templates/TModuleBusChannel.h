#pragma once

#include <map>
#include "Templates/TModuleBase.h"
#include "Templates/TIdObjectMap.h"
#include "FrameworkUtilities/DynamicStruct.h"
#include "CommChannel/ISixCommChannel.h"
#include "CommChannel/SixCommChannelModule.h"
#include "TimerModule/TimerFactory.h"
#include "TimerModule/ITimerCB.h"
#include "SmallConfiguration/SmallConfiguration.h"
#include "CommChannel/SGenericParamFullNames.hpp"

using namespace std;

template <class InterfaceImpl, class Interface = InterfaceImpl>
class CNekObjectModBusChannel : public CNekObjectModBase<InterfaceImpl, Interface>
							  , public ITimerCB
{
public:
	CNekObjectModBusChannel()
	{
		ReinitObject();
	}
	virtual ~CNekObjectModBusChannel(){;}

	virtual void ReinitObject()
	{
		m_DynRemoteInfo.Clean();
		int iTimerId;
		for (itTimerMap = m_TimerMap.begin(); itTimerMap != m_TimerMap.end(); itTimerMap++)
		{
			iTimerId = itTimerMap->second;
			CTimerFactory::St_GetInstance()->KillTimer(iTimerId);
		}
		m_TimerMap.clear();
		m_fstrSession.Clear();
	}

	void SetObjectId(const CFwkString & val) {
		m_DynObjectInfo.AddString(_six_kHeader_ResId,val);
	}
	CFwkString GetObjectId() {
		CFwkString fstrRet;
		m_DynObjectInfo.GetString(_six_kHeader_ResId,fstrRet);
		return fstrRet;
	}

	void ExtractRemoteInfo( CDynamicStruct *pDynData )
	{
		if( m_DynRemoteInfo.GetSize() != 0 )
		{
			/// en el mensaje que llega el local es nuestro remoto.
			CDynamicStruct * pOrigenData = pDynData->GetGroupParams( _six_kHeader_Org );
			if ( pOrigenData )
			{
				SetRemoteObjectId(pOrigenData);
				DYNFACTFREERES(pOrigenData);
			}
		}
		if( m_fstrSession.IsEmpty() )
		{
			pDynData->GetString( _six_kHeader_Session , m_fstrSession);
			if( m_fstrSession.IsEmpty() )
			{
				m_fstrSession = "NoSession";
			}
		}
	}
	void SetRemoteModuleName(const CFwkString &val) {
		m_DynRemoteInfo.AddString(_six_kHeader_Dest_Id,val);
	}
	void SetRemoteObjectId(CDynamicStruct * pOrigenData) {
		m_DynRemoteInfo.SetGroupParams(_six_kHeader_Dest,pOrigenData);
	}

	virtual bool SendMessage(int nMsg,CDynamicStruct *pDynData = 0)
	{
		CDynamicStruct *pToSend = DYNFACTGETRES;
		if (pDynData == 0)
		{
			pDynData = DYNFACTGETRES;
		}

		pToSend->SetGroupParams( _six_kOperGroup, pDynData );
		pToSend->AddInt(_six_kOper_Type, nMsg);

		DYNFACTFREERES(pDynData);
		pDynData = 0;

		*pToSend += m_DynObjectInfo;
		*pToSend += m_DynRemoteInfo;

		return CNekObjectModBase<InterfaceImpl,Interface>::SendMessage( 0xffff , pToSend );//0xffff  porque ya esta adicionado el method
	}

	virtual void ProcessMessageCallBack(CDynamicStruct * pDynData) = 0;

	virtual int KillTimer(CDynamicStruct * pDynData)
	{
		int iRet = 1;
		return iRet;
	}

	enum TIMERS
	{
		TM_NONE,
		TM_RESPONSE
	};

	// ITimerCB hierarchy
	void TimeOut(size_t iEventId, size_t iTimerId){}

	void setSession( const CFwkString & fstrSession)
	{
		m_fstrSession = fstrSession;
	}

	CFwkString & getSession( )
	{
		return m_fstrSession;
	}

protected:

	CDynamicStruct m_DynObjectInfo;
	CDynamicStruct m_DynRemoteInfo;

	CFwkString m_fstrSession;

	map<int, int> m_TimerMap;
	map<int, int>::iterator itTimerMap;
};

template <class InterfaceImpl, class Interface = InterfaceImpl> 
class TModuleBusChannel : public TModuleBase<InterfaceImpl, Interface>
						  , public ISixCommChannelEvent
{
public:

	typedef TModuleBase<InterfaceImpl,Interface> SUPERMODBASE;

	virtual Interface* GetResource( CDynamicStruct * pDynData = 0 );
	virtual Interface* GetResource( CFwkString& fstrQueue, CFwkString& fstrResourceId);
	virtual bool FreeResource(Interface* pObject);

	void SetRemoteModuleName(CFwkString fstrName)
	{
		m_fstrRemoteModuleName = fstrName;
	}

	virtual int InitChannels();
	virtual int StopChannels();

	virtual void ProcessCommChannelEvent(CDynamicStruct* pDynEvent);

private:
	TUIDObjectMap<InterfaceImpl> m_IdObjImplMap;
	ISixCommChannel* m_pBusChannel;

	CFwkString m_fstrRemoteModuleName;

};

template <class InterfaceImpl, class Interface>
Interface* TModuleBusChannel< InterfaceImpl, Interface>::GetResource( CDynamicStruct * pDynData )
{
	CFwkString fstrObjectId;
	InterfaceImpl * pRealObj = 0;

	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard,this->m_ModMutex, 0);
	if( pDynData )
	{
		pDynData->GetString( _six_kHeader_ResId,fstrObjectId );
		SIX_ASSERT(!fstrObjectId.IsEmpty());
		pRealObj = m_IdObjImplMap.GetObjectFromId(fstrObjectId);
		if(!pRealObj)
		{
			Interface *pObj = 0;
			pObj = SUPERMODBASE::GetResource();
			pRealObj = dynamic_cast < InterfaceImpl* > (pObj);
			m_IdObjImplMap.setIdFromObject( fstrObjectId, pRealObj );
			SIX_ASSERT(pRealObj);
			if (pRealObj)
			{
				pRealObj->SetRemoteModuleName(m_fstrRemoteModuleName);
				pRealObj->SetObjectId(fstrObjectId);
			}
		}
	}
	else
	{
		Interface *pObj = 0;
		pObj = SUPERMODBASE::GetResource();
		pRealObj = dynamic_cast < InterfaceImpl* > (pObj);
		fstrObjectId = m_IdObjImplMap.GetNewId(pRealObj);

		SIX_ASSERT(pRealObj);
		if (pRealObj)
		{
			pRealObj->SetRemoteModuleName(m_fstrRemoteModuleName);
			pRealObj->SetObjectId( fstrObjectId );
		}
	}
	return pRealObj;
}

template <class InterfaceImpl, class Interface>
Interface* TModuleBusChannel< InterfaceImpl, Interface>::GetResource(CFwkString& fstrQueue, CFwkString& fstrResourceId)
{
	CFwkString fstrObjectId;
	InterfaceImpl * pRealObj = 0;

	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard,this->m_ModMutex, 0);
	Interface *pObj = 0;
	pObj = SUPERMODBASE::GetResource();
	pRealObj = dynamic_cast < InterfaceImpl* > (pObj);
	if(fstrResourceId.IsEmpty() == true)
		fstrObjectId = m_IdObjImplMap.GetNewId(pRealObj);
	else
	{
		fstrObjectId = fstrResourceId;
		m_IdObjImplMap.setIdFromObject( fstrObjectId, pRealObj );
	}

	SIX_ASSERT(pRealObj);
	if (pRealObj)
	{
		if(fstrQueue.IsEmpty() == false)
			pRealObj->SetRemoteModuleName(fstrQueue);
		else
			pRealObj->SetRemoteModuleName(m_fstrRemoteModuleName);

		pRealObj->SetObjectId( fstrObjectId );
	}

	return pRealObj;
}

template <class InterfaceImpl, class Interface> 
bool TModuleBusChannel< InterfaceImpl, Interface>::FreeResource( Interface* pObject )
{
	InterfaceImpl * pRealObj = dynamic_cast < InterfaceImpl* > (pObject);
	if (pRealObj)
	{
		m_IdObjImplMap.DeleteUID( pRealObj->GetObjectId() );
		pRealObj->ReinitObject();
	}
	return SUPERMODBASE::FreeResource(pObject);
}

template <class InterfaceImpl, class Interface>
void TModuleBusChannel< InterfaceImpl, Interface>::ProcessCommChannelEvent( CDynamicStruct * pDynData )
{
	Interface *pObj = 0;
	pObj = GetResource( pDynData );
	InterfaceImpl * pRealObj = dynamic_cast < InterfaceImpl* > (pObj);
	CFwkString fstrOrId;

	SIX_ASSERT(pRealObj);
	if (pRealObj)
	{
		int iTypeMsg = 0;
		int iMethodRef = 0;
		if(pDynData->GetInt(_six_kOper_Type, iTypeMsg) == false)
		{
			DYNFACTFREERES(pDynData);
			return;
		}
		pDynData->GetInt(_six_kOper_TypeRef, iMethodRef);

		if((iTypeMsg == _six_kDone || iTypeMsg == _six_kError) && iMethodRef == _six_kGetResource)
		{
			CDynamicStruct * pOrigenData = pDynData->GetGroupParams(_six_kHeader_Org);
			if(pOrigenData != 0)
			{
				pRealObj->SetRemoteObjectId(pOrigenData);
				DYNFACTFREERES(pOrigenData);
		}
	}

		CDynamicStruct * pOperation = pDynData->GetGroupParams( _six_kOperGroup );
		pDynData->GetString(_six_kHeader_ResId, fstrOrId);
		pOperation->AddString(_six_kHeader_ResId, fstrOrId);
		pRealObj->ProcessMessageCallBack( pOperation, iTypeMsg, iMethodRef);
	}
	DYNFACTFREERES(pDynData);
}

template <class InterfaceImpl, class Interface>
int TModuleBusChannel< InterfaceImpl, Interface>::InitChannels()
{
	m_pBusChannel = CSixCommChannelModule::St_GetInstance()->GetCommChannel(this->m_fstrURLCx, this->m_fstrModuleName, this->m_fstrModuleIdentifier);
	m_pBusChannel->SetEventCallback(this);
	
	CDynamicStruct dynStartInfo;
	dynStartInfo.AddString("URL", this->m_fstrURLCx);
	dynStartInfo.AddString("NAME_GROUP", this->m_fstrModuleName);
	dynStartInfo.AddString("NAME_UNIQUE", this->m_fstrModuleIdentifier);
	dynStartInfo.AddInt("MODULE_TYPE", this->m_nType);
	dynStartInfo.AddString("DESCRIPTION", this->m_fstrDescription);

	m_pBusChannel->Start(&dynStartInfo);

	SUPERMODBASE::setChannelFactory(m_pBusChannel);
	SUPERMODBASE::InitChannels();
	return 0;
}

template <class InterfaceImpl, class Interface > int TModuleBusChannel< InterfaceImpl, Interface>::StopChannels()
{
	SUPERMODBASE::StopChannels();
	if (m_pBusChannel)
	{
		m_pBusChannel->ReleaseChannel();
		m_pBusChannel = 0;
	}
	return 0;
}


