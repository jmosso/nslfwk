#pragma once

#include "Templates/TDrawer.h"

template <class ObjectToStore> 
class TStore
{
public:
	TStore();
	virtual ~TStore();

	short AddObject(ObjectToStore* pObjectToAdd);
	short RemoveObject(ObjectToStore* pObjectToRemove);
	virtual short FreeObject(ObjectToStore* pObjectToFree);
	bool  IsHere(ObjectToStore* pObjectToFind);
	inline int GetCount() { return m_iCount; }
	bool IsBusy(ObjectToStore* pObjectToFind);
	void Clear();

	ObjectToStore* GetFreeObject();
	ObjectToStore* GetFirstObject();
	ObjectToStore* GetNextObject();
	ObjectToStore* GetPrevObject();
	ObjectToStore* GetRandObject();
	
	ObjectToStore* GetFirstObject(void*& hNavigator);
	ObjectToStore* GetNextObject(void*& hNavigator);
	ObjectToStore* GetPrevObject(void*& hNavigator);
	ObjectToStore* GetLastObject(void*& hNavigator);

	virtual int GetCountBusy(){return m_iBusy;}

	void GoInit();
	void setBusy(ObjectToStore* pObjectToFind,bool busy = true);
	int m_iCount;
	int m_iBusy;

	TDrawer<ObjectToStore>* FindObjectDrawer(ObjectToStore* pObjectToFind);
	
	bool AddDrawer(TDrawer<ObjectToStore> * pDrawer);
	bool RemoveDrawer(TDrawer<ObjectToStore> * pDrawer);
	void setBusyDrawer(TDrawer<ObjectToStore> * pDrawer,bool busy = true);
	bool IsHereDrawer(TDrawer<ObjectToStore> * drTempParam);

protected:


private:	
	enum EnLimit
	{
		LIM_,
		LIM_HEAD,
		LIM_TAIL
	};
	
	EnLimit	a_enLimit;

	TDrawer<ObjectToStore>* m_pDrawerTail;
	TDrawer<ObjectToStore>* m_pDrawerHead;
	TDrawer<ObjectToStore>* m_pDrawerActual;
	TDrawer<ObjectToStore>* m_pDrawerGhostNext;
	TDrawer<ObjectToStore>* m_pDrawerGhostPrev;

	bool m_bIsGhostHere;
};



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
template <class ObjectToStore> 
TStore<ObjectToStore>::TStore()
{
	a_enLimit = LIM_;
	m_pDrawerTail = 0;
	m_pDrawerHead = 0;
	m_pDrawerActual = 0;
	m_pDrawerGhostNext = 0;
	m_pDrawerGhostPrev = 0;
	m_iCount = 0;
	m_iBusy = 0;
	m_bIsGhostHere = false;
}

template <class ObjectToStore> 
TStore<ObjectToStore>::~TStore()
{
	Clear();
}

template <class ObjectToStore> 
void TStore<ObjectToStore>::Clear()
{
	if(m_pDrawerHead == 0)
		return;

	TDrawer<ObjectToStore>* drTemp = m_pDrawerHead;
	TDrawer<ObjectToStore>* drRegi = 0;
	while(drTemp != 0)
	{
		drRegi = drTemp->m_pDrawerNext;
		drTemp->RemoveDrawer();
		delete drTemp;
		drTemp = drRegi;
	}

	a_enLimit = LIM_;
	m_pDrawerTail = 0;
	m_pDrawerHead = 0;
	m_pDrawerActual	= 0;
	m_iCount = 0;

}

template <class ObjectToStore> 
short TStore<ObjectToStore>::AddObject(ObjectToStore* pObjectToAdd)
{
	TDrawer<ObjectToStore>* drTemp;

	drTemp = new TDrawer<ObjectToStore>(pObjectToAdd);
	AddDrawer(drTemp);
    return 0;
}

template <class ObjectToStore> 
short TStore<ObjectToStore>::RemoveObject(ObjectToStore* pObjectToRemove)
{
	TDrawer<ObjectToStore>* drTemp = FindObjectDrawer (pObjectToRemove);
	if(drTemp != 0)
	{
		RemoveDrawer(drTemp);
		delete drTemp;
		return 0;
	}
    return -1;
}

template <class ObjectToStore> 
short TStore<ObjectToStore>::FreeObject(ObjectToStore* pObjectToFree)
{
	TDrawer<ObjectToStore>* drTemp = FindObjectDrawer(pObjectToFree);
	if (drTemp != 0)
		drTemp->FreeDrawer();

	m_iBusy--;
    return 0;
}

template <class ObjectToStore> 
ObjectToStore* TStore<ObjectToStore>::GetFreeObject()
{
	if(m_pDrawerHead == 0)
		return 0;

	TDrawer<ObjectToStore>* drTemp = m_pDrawerHead;
	while(drTemp != 0)
	{
		if(drTemp->m_bInUse == false)
		{
			m_iBusy++;
			drTemp->m_bInUse = true;
			return drTemp->m_pObjStored;
		}
		drTemp = drTemp->m_pDrawerNext;
	}
	return 0;
}

template <class ObjectToStore> 
ObjectToStore* TStore<ObjectToStore>::GetFirstObject()
{
	a_enLimit = LIM_;
	m_pDrawerActual = m_pDrawerHead;

	if(m_pDrawerActual != 0)
		return (m_pDrawerActual->m_pObjStored);

	return 0;
}

template <class ObjectToStore>
ObjectToStore* TStore<ObjectToStore>::GetNextObject()
{
	if(m_pDrawerActual == 0)
	{
		if(m_bIsGhostHere == true)
		{
			m_pDrawerActual = m_pDrawerGhostNext;
			m_bIsGhostHere = false;

			if(m_pDrawerActual == 0)
			{
				a_enLimit = LIM_TAIL;
				m_pDrawerTail = m_pDrawerGhostPrev;
				return 0;
			}
			return m_pDrawerActual->m_pObjStored;
		}
		else if(a_enLimit == LIM_HEAD)
			return GetFirstObject();
		return 0;
	}
	m_pDrawerActual = m_pDrawerActual->m_pDrawerNext;

	if(m_pDrawerActual != 0)
		return m_pDrawerActual->m_pObjStored;

	a_enLimit = LIM_TAIL;
	return 0;
}

template <class ObjectToStore>
ObjectToStore * TStore<ObjectToStore>::GetPrevObject()
{
	if(m_pDrawerActual == 0)
	{
		if(m_bIsGhostHere == true)
		{
			m_pDrawerActual = m_pDrawerGhostPrev;
			m_bIsGhostHere = false;
			if(m_pDrawerActual == 0)
			{
				a_enLimit = LIM_HEAD;
				m_pDrawerHead = m_pDrawerGhostNext;
				return 0;
			}
			return m_pDrawerActual->m_pObjStored;
		}
		else if(a_enLimit == LIM_TAIL)
		{
			m_pDrawerActual = m_pDrawerTail;
			if(m_pDrawerActual != 0)
				return m_pDrawerActual->m_pObjStored;
		}
		return 0;
	}
	m_pDrawerActual = m_pDrawerActual->m_pDrawerPrev;
	if(m_pDrawerActual != 0)
		return m_pDrawerActual->m_pObjStored;
	a_enLimit = LIM_HEAD;
	return 0;
}

template <class ObjectToStore> 
void TStore<ObjectToStore>::GoInit()
{
	m_pDrawerActual = m_pDrawerHead;
}

template <class ObjectToStore> 
TDrawer<ObjectToStore>* TStore<ObjectToStore>::FindObjectDrawer(ObjectToStore * pObjectToFind)
{
	TDrawer<ObjectToStore> * drTemp = m_pDrawerHead;
	while (drTemp != 0)
	{
		if (drTemp->m_pObjStored == pObjectToFind)
			return drTemp;
		drTemp = drTemp->m_pDrawerNext;
	}
	return 0;
}

template <class ObjectToStore> 
bool TStore<ObjectToStore>::IsHere(ObjectToStore * pObjectToFind)
{
	if (FindObjectDrawer(pObjectToFind) )
		return true;
	return false;
}

template <class ObjectToStore> 
bool TStore<ObjectToStore>::IsBusy(ObjectToStore * pObjectToFind)
{

	TDrawer<ObjectToStore>* drTemp = FindObjectDrawer(pObjectToFind);
	if(drTemp != 0)
		return drTemp->m_bInUse;
	return false;
}

template <class ObjectToStore> 
void TStore<ObjectToStore>::setBusy(ObjectToStore * pObjectToFind,bool busy)
{
	TDrawer<ObjectToStore>* drTemp = FindObjectDrawer(pObjectToFind);
	setBusyDrawer(drTemp,busy);
}

template <class ObjectToStore> 
void TStore<ObjectToStore>::setBusyDrawer(TDrawer<ObjectToStore> * drTemp,bool busy)
{
	if(drTemp != 0)
	{
		if(drTemp->m_bInUse == busy) //si ya esta en busy y se quiere setear a busy entonces no haga nada
			return;
		drTemp->m_bInUse = busy;
	}
	if(busy)
		m_iBusy++;
	else
		m_iBusy--;
}


template <class ObjectToStore> 
ObjectToStore* TStore<ObjectToStore>::GetRandObject()
{
	if(m_iCount == 0)
		return 0;

	int count = rand()%(m_iCount - 1);

	TDrawer<ObjectToStore> * drTemp = m_pDrawerHead;
	while (drTemp)
	{
		for (int i = 0; i< count; i++)
			drTemp = drTemp->m_pDrawerNext;
		return drTemp->m_pObjStored;
	}
	return 0;
}

template <class ObjectToStore> 
ObjectToStore* TStore<ObjectToStore>::GetFirstObject(void*& hNavigator)
{
	if(m_pDrawerHead != 0)
	{
		hNavigator = m_pDrawerHead;
		return (m_pDrawerHead->m_pObjStored);
	}

	hNavigator = 0;
	return 0;
}

template <class ObjectToStore> 
ObjectToStore* TStore<ObjectToStore>::GetLastObject(void*& hNavigator)
{
	if(m_pDrawerTail != 0)
	{
		hNavigator = m_pDrawerTail;
		return (m_pDrawerTail->m_pObjStored);
	}

	hNavigator = 0;
	return 0;
}

template <class ObjectToStore> 
ObjectToStore* TStore<ObjectToStore>::GetNextObject(void*& hNavigator)
{
	TDrawer<ObjectToStore>* tmpDrawer;
	TDrawer<ObjectToStore>* tmpNextDrawer;

	tmpDrawer = (TDrawer<ObjectToStore>*)hNavigator;

	if(tmpDrawer == 0)
	{
		hNavigator = 0;
		return 0;
	}

	tmpNextDrawer = tmpDrawer->m_pDrawerNext;

	if(tmpNextDrawer != 0)
	{
		hNavigator = tmpNextDrawer;
		return tmpNextDrawer->m_pObjStored;
	}

	hNavigator = 0;
	return 0;
}

template <class ObjectToStore> 
ObjectToStore* TStore<ObjectToStore>::GetPrevObject(void*& hNavigator)
{
	TDrawer<ObjectToStore>* tmpDrawer;
	TDrawer<ObjectToStore>* tmpNextDrawer;

	tmpDrawer = (TDrawer<ObjectToStore>*)hNavigator;

	if(tmpDrawer == 0)
	{
		hNavigator = 0;
		return 0;
	}

	tmpNextDrawer = tmpDrawer->m_pDrawerPrev;

	if(tmpNextDrawer != 0)
	{
		hNavigator = tmpNextDrawer;
		return tmpNextDrawer->m_pObjStored;
	}

	hNavigator = 0;
	return 0;
}


template <class ObjectToStore> 
bool TStore<ObjectToStore>::AddDrawer(TDrawer<ObjectToStore> * drTemp)
{
	if(m_pDrawerHead == 0)
	{
		m_iCount++;
		m_pDrawerHead = m_pDrawerTail = drTemp;
		return true;
	}

	drTemp->Attach(m_pDrawerTail);
	drTemp->m_iDrawerSeq = m_iCount;
	m_pDrawerTail = drTemp;
	m_iCount++;
    return true;
}


template <class ObjectToStore> 
bool TStore<ObjectToStore>::RemoveDrawer(TDrawer<ObjectToStore> * drTemp)
{
	if(drTemp != 0)
	{
		if(drTemp == m_pDrawerHead)
			m_pDrawerHead = drTemp->m_pDrawerNext;
		if(drTemp == m_pDrawerTail)
			m_pDrawerTail = drTemp->m_pDrawerPrev;
		if(drTemp == m_pDrawerActual)
		{
			m_bIsGhostHere = true;
			m_pDrawerGhostNext = drTemp->m_pDrawerNext;
			m_pDrawerGhostPrev	= drTemp->m_pDrawerPrev;
			m_pDrawerActual = 0;
		}
		drTemp->RemoveDrawer();
		m_iCount--;
		return true;
	}
    return false;
}

template <class ObjectToStore> 
bool TStore<ObjectToStore>::IsHereDrawer(TDrawer<ObjectToStore> * drTempParam)
{
	TDrawer<ObjectToStore> * drTemp = m_pDrawerHead;
	while (drTemp != 0)
	{
		if (drTemp == drTempParam)
			return true;
		drTemp = drTemp->m_pDrawerNext;
	}
	return false;
}
