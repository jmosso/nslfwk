#pragma once
#include "Templates/TModuleBusChannel.h"

template <class Module,class InterfaceImpl, class Interface = InterfaceImpl> 
class TModuleBusChannelSingleton : public TModuleBusChannel<InterfaceImpl,Interface>
								   , public TSingleton<Module>
{
public:

	typedef TModuleBusChannelSingleton<Module,InterfaceImpl,Interface> SUPERMODBUSCHANSING;
	typedef TModuleBusChannel<InterfaceImpl,Interface> SUPERMODBUSCHAN;

	virtual int Init();
	virtual int Exit();
};

template <class Module,class InterfaceImpl, class Interface>
int TModuleBusChannelSingleton<Module, InterfaceImpl, Interface>::Init()
{
	SUPERMODBUSCHAN::InitChannels();
	return 0;
}

template <class Module,class InterfaceImpl, class Interface >
int TModuleBusChannelSingleton<Module, InterfaceImpl, Interface>::Exit()
{
	SUPERMODBUSCHAN::StopChannels();
	return 0;
}


