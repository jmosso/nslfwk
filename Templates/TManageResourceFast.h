#pragma once

#include "Templates/TFactoryNavigator.h"
#include "Utilities/ThreadUtilities.h"
#include "FrameworkUtilities/FwkString.h"


template <class Interface> 
class TManageResourceFast : public TFactoryNavigator<Interface,Interface>
{
public:
	TManageResourceFast();
	virtual ~TManageResourceFast();

	typedef TFactory<Interface,Interface> SUPER;

	virtual void AddResource(Interface * pObject);
	virtual Interface* GetResource();
	virtual Interface* GetResource(int iResourceInfo);

private:

	virtual bool IsResourceAvailable(Interface* pObjectToFind,int iResourceInfo)
	{
		return true;
	}

	virtual bool IsResourceAvailable(Interface* pObjectToFind)
	{
		return true;
	}
};

template <class Interface> 
TManageResourceFast<Interface>::TManageResourceFast()
{

}

template <class Interface> 
TManageResourceFast<Interface>::~TManageResourceFast()
{
//	Clear();
}

template <class Interface> 
void TManageResourceFast<Interface>::AddResource(Interface * pObject)
{
	ACE_GUARD(MUTEXCLASS, guard, this->m_mutex_Factory);
	this->m_BusyMap.insert(std::pair <Interface *, int>(pObject, 0));
	this->m_StoredQueue.push_back(pObject);
}

template <class Interface> 
Interface* TManageResourceFast<Interface>::GetResource()
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, this->m_mutex_Factory, 0);

	Interface *tmp = 0;

	typename TFactory<Interface,Interface>::OBJECTMAP::iterator it;

	for(it = this->m_FreeMap.begin(); this->m_FreeMap.end() != it ; it++)
	{
		tmp = it->first;
		if (tmp)
		{
			if (IsResourceAvailable(tmp))
			{				
				break;
			}
		}
	}
	if (tmp != 0)
	{
		TFactoryNavigator<Interface,Interface>::setBusy(tmp);
	}
	return tmp;
}

template <class Interface> 
Interface* TManageResourceFast<Interface>::GetResource(int iResourceInfo)
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, this->m_mutex_Factory, 0);

	Interface *tmpRet = 0;

	typename TFactory<Interface,Interface>::OBJECTMAP::iterator it;

	for(it = this->m_FreeMap.begin(); this->m_FreeMap.end() != it ; it++)
	{
		Interface *tmp = 0;
		tmp = it->first;
		if (tmp)
		{
			if (IsResourceAvailable(tmp,iResourceInfo)) {
				tmpRet = tmp;				
				break;
			}
		}
	}

	if (tmpRet != 0)
	{
		TFactoryNavigator<Interface,Interface>::setBusy(tmpRet);
	}
	return tmpRet;	
}
