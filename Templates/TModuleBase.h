#pragma once

#include <list>
#include "Templates/TSingleton.h"
#include "Templates/TFactory.h"
#include "ThreadModule/IThreadMessageCB.h"
#include "CommChannel/ISixCommChannel.h"
#include "FrameworkUtilities/FwkString.h"
#include "FrameworkUtilities/DynamicStruct.h"

template <class InterfaceImpl, class Interface = InterfaceImpl>
class CNekObjectModBase : public IThreadMessageCB
{
public:
	CNekObjectModBase(void)
	{
		m_pChannel = 0;
		m_pFactory = 0;
	}
	~CNekObjectModBase(void)
	{
	}

	inline ISixCommChannel* GetChannel() const { return m_pChannel; }
	inline void SetChannel(ISixCommChannel* val) { m_pChannel = val; }

	virtual bool SendMessage(int nMsg,CDynamicStruct * pParam = 0)
	{
		SIX_ASSERT(m_pChannel);

		if(m_pChannel)
		{
			return m_pChannel->SendChannelMessage(pParam);
		}
		else
		{
			if(pParam)
				DYNFACTFREERES(pParam);
		}
		return false;
	}

	virtual void SetFactory(ITFactory<Interface> * pFactory)
	{
		m_pFactory = pFactory;
	}
	virtual void ReleaseObjMod(Interface * pObj)
	{
		SIX_ASSERT(m_pFactory);
		if(m_pFactory)
			m_pFactory->FreeResource(pObj);
	}
protected:

	ISixCommChannel* m_pChannel;
	ITFactory<Interface> * m_pFactory;
};

template <class InterfaceImpl, class Interface = InterfaceImpl> 
class TModuleBase : public TFactory<InterfaceImpl,Interface>
{
public:
	TModuleBase()
	{
		m_pTheChannel = 0;
	}
	~TModuleBase(void)
	{
		m_pTheChannel = 0;
	}

	bool SetNumChannels(int nNumChannels);

	void SetModuleName(const CFwkString & fstrName)
	{
		this->m_fstrModuleName = fstrName;
		TFactory<InterfaceImpl,Interface>::SetFactName(fstrName);
	}

	void SetModuleIdentifier(const CFwkString & fstrIdentifier,int nType,const CFwkString & fstrDesc, const char* strURLCx)
	{
		this->m_fstrModuleIdentifier = fstrIdentifier;
		this->m_nType = nType;
		this->m_fstrDescription = fstrDesc;
		this->m_fstrURLCx = strURLCx;
		TFactory<InterfaceImpl,Interface>::SetFactName(fstrIdentifier);
	}

	virtual int InitChannels();
	virtual int StopChannels();

	void setChannelFactory(ISixCommChannel* val) { m_pTheChannel = val; }

	virtual Interface* GetResource();
protected:
	MUTEXCLASS m_ModMutex;
	std::list<ISixCommChannel*> m_ListChannels;
	CFwkString m_fstrModuleName;
	CFwkString m_fstrModuleIdentifier;
	CFwkString m_fstrURLCx;
	int m_nType;
	CFwkString m_fstrDescription;
	ISixCommChannel* m_pTheChannel;
};

template <class InterfaceImpl, class Interface>
bool TModuleBase< InterfaceImpl, Interface>::SetNumChannels( int nNumChannels )
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, this->m_ModMutex, 0);
	size_t nNumRealChannels = nNumChannels - m_ListChannels.size();
	for (size_t i = 0; i < nNumRealChannels; i++)
	{
		m_ListChannels.push_back(m_pTheChannel);
	}
	return true;
}

template <class InterfaceImpl, class Interface>
Interface* TModuleBase< InterfaceImpl, Interface>::GetResource()
{
	Interface *pObj = TFactory<InterfaceImpl,Interface>::GetResource();
	ACE_GUARD_RETURN(MUTEXCLASS, guard,this->m_ModMutex, 0);
	InterfaceImpl * pRealObj = dynamic_cast < InterfaceImpl* > (pObj);
	SIX_ASSERT(pRealObj);
	if (pRealObj)
	{
		if (pRealObj->GetChannel() == NULL)
		{
			pRealObj->SetChannel(m_pTheChannel);
			pRealObj->SetFactory(this);
		}
	}
	return pObj;
}

template <class InterfaceImpl, class Interface>
int TModuleBase< InterfaceImpl, Interface>::InitChannels()
{
	if (m_ListChannels.size() == 0)
	{
		SetNumChannels(1);
	}
	return 0;
}

template <class InterfaceImpl, class Interface>
int TModuleBase< InterfaceImpl, Interface>::StopChannels()
{
	std::list<ISixCommChannel*>::iterator threadIterator = m_ListChannels.begin();
	while (threadIterator != m_ListChannels.end())
	{
		ISixCommChannel* pChannel = *threadIterator;
		if(pChannel)
			pChannel->ReleaseChannel();
		threadIterator++;
	}
	m_ListChannels.clear();
	return 0;
}
