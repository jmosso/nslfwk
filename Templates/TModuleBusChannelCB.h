#pragma once

#include "Templates/TModuleBusChannel.h"

#include "ThreadModule/ThreadFactory.h"
#include "ThreadModule/IThread.h"

template <class RemoteInterfaceImpl,class InterfaceImpl, class Interface = InterfaceImpl>
class CNekObjectModBusChannelCB : public CNekObjectModBusChannel<InterfaceImpl, InterfaceImpl>
{
public:
	CNekObjectModBusChannelCB()
	{
		m_pThread = 0;
		m_pRemoteImplementation = new RemoteInterfaceImpl;
	}

	virtual ~CNekObjectModBusChannelCB()
	{
		delete m_pRemoteImplementation;
	}

	inline IThread * GetThread() const { return m_pThread; }
	inline void SetThread(IThread * val)
	{
		m_pThread = val;
		m_pRemoteImplementation->SetThread(val);
	}
	virtual bool SendThreadMessage(int nMsg,NK_OBJPOINTER nParam = 0)
	{
		SIX_ASSERT(m_pThread);
		if(m_pThread)
			return m_pThread->SendThreadMessage(nMsg, m_pRemoteImplementation, nParam);
		return false;
	}
	RemoteInterfaceImpl * GetRemoteImplementation() const { return m_pRemoteImplementation; }


	void ProcessMessageCallBack( CDynamicStruct * pDynData )
	{
		int nMsg = -1;
		if (pDynData->GetInt(_six_kType,nMsg))
		{
			SendThreadMessage(nMsg,(NK_OBJPOINTER)pDynData);
		}
	}

protected:

	IThread * m_pThread;
	RemoteInterfaceImpl * m_pRemoteImplementation;
};

template <class InterfaceImpl, class Interface = InterfaceImpl> 
class TModuleBusChannelCB : public TModuleBusChannel<InterfaceImpl, Interface>
{
public:

	typedef TModuleBusChannel<InterfaceImpl,Interface> SUPERMODBUSCHAN;
	virtual Interface* GetResource(  CDynamicStruct * pDynData = 0 );
	bool SetNumThreads( int nNumThreads );

	virtual int InitChannels();
	virtual int StopChannels();

protected:

	std::list<IThread*> m_ListThreads;
};

template <class InterfaceImpl, class Interface>
bool TModuleBusChannelCB<InterfaceImpl, Interface>::SetNumThreads( int nNumThreads )
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, this->m_ModMutex, 0);
	size_t nNumRealThreads = nNumThreads - m_ListThreads.size();
	for (size_t i = 0; i < nNumRealThreads; i++)
	{
		CFwkString fstrNameReal = this->m_fstrModuleName;
		fstrNameReal.sprintfc("_%d",m_ListThreads.size());
		IThread * pThread = CThreadFactory::St_GetInstance()->GetResource();
		SIX_ASSERT(pThread);
		if(pThread)
		{
			m_ListThreads.push_back(pThread);
			pThread->SetThreadName(fstrNameReal);
			pThread->Start();
		}
	}
	return true;
}

template <class InterfaceImpl, class Interface>
Interface* TModuleBusChannelCB< InterfaceImpl, Interface>::GetResource( CDynamicStruct * pDynData )
{
	Interface *pObj = SUPERMODBUSCHAN::GetResource( pDynData );
	ACE_GUARD_RETURN(MUTEXCLASS, guard,this->m_ModMutex, 0);
	InterfaceImpl * pRealObj = dynamic_cast < InterfaceImpl* > (pObj);
	SIX_ASSERT(pRealObj);
	if (pRealObj)
	{
		pObj->GetRemoteImplementation()->SetCallBack(pRealObj);
		if (pRealObj->GetThread() == NULL)
		{
			IThread * pThread = m_ListThreads.front();
			m_ListThreads.pop_front();
			m_ListThreads.push_back(pThread);
			pRealObj->SetThread(pThread);
		}
	}
	return pObj;
}

template <class InterfaceImpl, class Interface>
int TModuleBusChannelCB< InterfaceImpl, Interface>::InitChannels()
{
	if (m_ListThreads.size() == 0)
	{
		SetNumThreads(1);
	}
	SUPERMODBUSCHAN::InitChannels();
	return 0;
}

template <class InterfaceImpl, class Interface >
int TModuleBusChannelCB< InterfaceImpl, Interface>::StopChannels()
{
	SUPERMODBUSCHAN::StopChannels();
	std::list<IThread*>::iterator threadIterator = m_ListThreads.begin();
	while (threadIterator != m_ListThreads.end())
	{
		IThread * pThread = *threadIterator;
		if(pThread)
		{
			pThread->Stop();
			CThreadFactory::St_GetInstance()->FreeResource(pThread);
		}
		threadIterator++;
	}
	m_ListThreads.clear();
	return 0;
}


