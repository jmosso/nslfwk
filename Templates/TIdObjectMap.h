#pragma once

#include <map>
#include "Utilities/utilities.h"

template <class InterfaceImpl> 
class TIdObjectMap
{
public:
	TIdObjectMap(void){ m_nNextId = 1000; }
	~TIdObjectMap(void){;}

	typedef TIdObjectMap<InterfaceImpl> SUPER;
	
	typedef typename std::map< int , InterfaceImpl *>  IDPOINTERMAP;


	inline InterfaceImpl * GetObjectFromId(int id)
	{
		ACE_GUARD_RETURN(MUTEXCLASS, guard, m_mutex_IdObMap, 0);
		typename IDPOINTERMAP::iterator i = m_IdObjMap.find(id);
		SIX_ASSERT( i != m_IdObjMap.end() );
		if ( i == m_IdObjMap.end() )
		{
			return 0;
		}
		return i->second;
	}

	inline int GetIdFromObject(InterfaceImpl * pContext)
	{
		ACE_GUARD_RETURN(MUTEXCLASS, guard, m_mutex_IdObMap, 0);
		int id = m_nNextId++;
		m_IdObjMap[id] = pContext;
		return id;
	}

protected:
	MUTEXCLASS m_mutex_IdObMap;

	int m_nNextId;
	IDPOINTERMAP m_IdObjMap;

};


template <class InterfaceImpl> 
class TUIDObjectMap
{
public:
	TUIDObjectMap(void){
		//m_nNextId = 1000;
	}
	~TUIDObjectMap(void){;}

	typedef TIdObjectMap<InterfaceImpl> SUPER;

	inline InterfaceImpl * GetObjectFromId(const CFwkString & id)
	{
		ACE_GUARD_RETURN(MUTEXCLASS, guard, m_mutex_IdObMap, 0);

		typename UIDPOINTERMAP::iterator it = m_UIDObjMap.find(id);
		
		if (it == m_UIDObjMap.end())
		{
			return 0;
		}
		return it->second;
	}

	inline bool DeleteUID(const CFwkString & id)
	{
		ACE_GUARD_RETURN(MUTEXCLASS, guard, m_mutex_IdObMap, 0);
		
		typename UIDPOINTERMAP::iterator it = m_UIDObjMap.find(id);
		SIX_ASSERT(it != m_UIDObjMap.end());
		if (it == m_UIDObjMap.end())
		{
			SIX_ASSERT(!"UID Does Not exist");
			return false;
		}
		m_UIDObjMap.erase(id);
		return true;
	}

	inline void setIdFromObject(const CFwkString & id,InterfaceImpl * pContext)
	{
		ACE_GUARD( MUTEXCLASS, guard, m_mutex_IdObMap );
		m_UIDObjMap[id] = pContext;
	}

	inline CFwkString GetNewId(InterfaceImpl * pContext)
	{
		ACE_GUARD_RETURN(MUTEXCLASS, guard, m_mutex_IdObMap, "");
		CFwkString fstrUUID;
		//id.sprintf("%d", m_nNextId++ );
		FwkGetUUID(fstrUUID);
		fstrUUID.ToUpperThis();
		m_UIDObjMap[fstrUUID] = pContext;
		return fstrUUID;
	}

protected:
	MUTEXCLASS m_mutex_IdObMap;

	typedef std::map<CFwkString,InterfaceImpl *>  UIDPOINTERMAP;
	//int m_nNextId;
	UIDPOINTERMAP m_UIDObjMap;
};



