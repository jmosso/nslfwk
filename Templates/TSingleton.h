#pragma once

#include "Templates/TCommon.h"

template <class Module> class TSingleton
{
public:
	static short St_CreateInstance();
	static short St_TerminateInstance();
	static Module * St_GetInstance();
	virtual short ReloadConfiguration(){return 0;}

protected:
	typedef TSingleton<Module> SUPERSING;

	virtual int Init() { return 0; }
	virtual int Exit() { return 0; }
	
	static MUTEXCLASS mutex_;
	static Module* ms_pModuleReference;
};

template <class Module> MUTEXCLASS TSingleton<Module>::mutex_;
template <class Module> Module * TSingleton<Module>::ms_pModuleReference = 0;

template <class Module> 
short TSingleton<Module>::St_CreateInstance()
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, mutex_, -1);

	if(ms_pModuleReference == 0)
	{
		ms_pModuleReference = new Module();
		if(ms_pModuleReference->Init() != 0)
			return -1;
	}
	return 0;
}

template <class Module> 
short TSingleton<Module>::St_TerminateInstance()
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, mutex_, -1);

	if(ms_pModuleReference)
	{
		ms_pModuleReference->Exit();
		delete ms_pModuleReference;
		ms_pModuleReference = 0;
	}

	return 0;
}

template <class Module> 
Module* TSingleton<Module>::St_GetInstance()
{
	return ms_pModuleReference;
}

