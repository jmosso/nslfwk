# Create a library called "Hello" which includes the source file "hello.cxx".
# The extension is already found.  Any number of sources could be listed here.

include_directories (
	${FRAMEWORK_ROOT}
	${ACE_INCLUDE_DIR}
	)
	
#add_library (Templates 
#		ITFactory.h
#		TDrawer.h
#		TFactory.h
#		TFactoryNavigator.h
#		TIdObjectMap.h
#		TManageResourceFast.h
#		TModuleBase.h
#		TModuleBusChannelCB.h
#		TModuleBusChannel.h
#		TModuleBusChannelSingletonCB.h
#		TModuleBusChannelSingleton.h
#		TModule.h
#		TSingleton.h
#		TStore.h
#		)

INSTALL( FILES
        TCommon.h
        ITFactory.h
        TDrawer.h
        TFactory.h
        TFactoryNavigator.h
        TIdObjectMap.h
        TManageResourceFast.h
        TModuleBase.h
        TModuleBusChannelCB.h
        TModuleBusChannel.h
        TModuleBusChannelSingletonCB.h
        TModuleBusChannelSingleton.h
        TModule.h
        TSingleton.h
        TStore.h

        DESTINATION ${SIXLABS_DIR}/include/nslFwk/ThreadModule )

