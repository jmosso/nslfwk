#pragma once

#include "Templates/TModuleBusChannelCB.h"

template <class Module,class InterfaceImpl, class Interface = InterfaceImpl> 
class TModuleBusChannelSingletonCB : public TModuleBusChannelCB<InterfaceImpl,Interface>
									 , public TSingleton<Module>
{
public:

	typedef TModuleBusChannelCB<InterfaceImpl,Interface> SUPERMODBUSCHANCB;
	typedef TModuleBusChannelSingletonCB<Module,InterfaceImpl,Interface> SUPERMODBUSCHANSINGCB;

	virtual int Init();
	virtual int Exit();
	
};

template <class Module,class InterfaceImpl, class Interface>
int TModuleBusChannelSingletonCB<Module, InterfaceImpl, Interface>::Init()
{
	SUPERMODBUSCHANCB::InitChannels();
	return 0;
}

template <class Module,class InterfaceImpl, class Interface >
int TModuleBusChannelSingletonCB<Module, InterfaceImpl, Interface>::Exit()
{
	SUPERMODBUSCHANCB::StopChannels();
	return 0;
}


