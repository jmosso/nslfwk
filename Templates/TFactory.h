#pragma once

#include "Templates/TCommon.h"

#include "Templates/ITFactory.h"
#include <string>
#include <deque>
#include <map>
#include <typeinfo>

#ifdef _DEBUG
//#include "FactoryTrace.h"
//extern CFactoryTrace g_factTrace;
#endif

template <class InterfaceImpl, class Interface = InterfaceImpl> 
class TFactory : public ITFactory<Interface>
{
public:
	TFactory(const char* name = 0);
	virtual ~TFactory(void);

	virtual Interface* GetResource();
	virtual bool FreeResource(Interface* pObject);
	virtual bool RemoveResource(Interface* pObject);
	
	size_t GetCount();
	size_t GetCountFree();
	size_t GetCountBusy();
	void SetMaxObjects(size_t iMaxObjects);

	void SetFactName(const char* fstrName){ m_fstrFactoryName = fstrName; }

protected:
	
	virtual Interface* CreateInstance();

	std::string m_fstrFactoryName;
	MUTEXCLASS m_mutex_Factory;

	typedef typename std::map<Interface*, int> OBJECTMAP;
	OBJECTMAP m_BusyMap;
	OBJECTMAP m_FreeMap;

	typedef std::deque<Interface*> OBJECTQUEUE;
	OBJECTQUEUE m_StoredQueue;
};

template <class InterfaceImpl, class Interface> 
TFactory<InterfaceImpl, Interface>::TFactory(const char* name)
{
	if (m_fstrFactoryName.empty())
	{
		if(name != 0)
			m_fstrFactoryName = name;
		else
		{
			char buff[255];
#ifndef WIN32
			snprintf(buff, sizeof(buff), "%s<%p>", typeid(InterfaceImpl).name(), this);
#else
			_snprintf_s(buff, sizeof(buff), 255, "%s<%p>", typeid(InterfaceImpl).name(), this);
#endif
			m_fstrFactoryName = buff;
		}
	}
}

template <class InterfaceImpl, class Interface> 
TFactory<InterfaceImpl, Interface>::~TFactory()
{
	typename std::map<Interface*, int>::iterator it = m_BusyMap.begin();
	while(it != m_BusyMap.end())
{
		Interface* obj = it->first;
		delete(obj);
		it++;
	}
	m_BusyMap.clear();

	it = m_FreeMap.begin();
	while(it != m_FreeMap.end())
	{
		Interface* obj = it->first;
		delete(obj);
		it++;
	}
	m_FreeMap.clear();

	m_StoredQueue.clear();
}

template <class InterfaceImpl, class Interface> 
Interface* TFactory<InterfaceImpl, Interface>::CreateInstance()
{
	return new InterfaceImpl;
}

template <class InterfaceImpl, class Interface> 
Interface* TFactory<InterfaceImpl, Interface>::GetResource()
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, m_mutex_Factory, 0);

	Interface *pObject = 0;

	if(m_FreeMap.empty())
	{
		pObject = CreateInstance();
		m_BusyMap.insert(std::pair <Interface *, int>(pObject, 0));
		m_StoredQueue.push_back(pObject);
	}
	else
	{
		typename std::map<Interface*, int>::iterator i = m_FreeMap.begin();
		pObject = i->first;
		m_BusyMap.insert(std::pair <Interface *, int>(pObject, 0));
		m_FreeMap.erase(i);
	}

#ifdef _DEBUG
	//g_factTrace.Write(m_fstrFactoryName.c_str(), "GetResource", pObject, GetCountBusy(), GetCountFree(), GetCount());
#endif //_DEBUG

	return pObject;
}

template <class InterfaceImpl, class Interface> 
bool TFactory<InterfaceImpl, Interface>::FreeResource(Interface* pObject)
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, m_mutex_Factory, false);

	typename std::map<Interface *, int>::iterator i = m_BusyMap.find(pObject);
	if(i != m_BusyMap.end())
	{
		m_FreeMap.insert(std::pair <Interface *, int>(i->first, 0));
		m_BusyMap.erase(i);
	}
	else
	{
		printf("TFactory<%s> trying to free NOT BUSY object<0x%x>!!!\n", m_fstrFactoryName.c_str(), pObject);
		return false;
	}

#ifdef _DEBUG
	//g_factTrace.Write(m_fstrFactoryName.c_str(), "FreeResource", pObject, GetCountBusy(), GetCountFree(), GetCount());
#endif //_DEBUG
	return true;
}

template <class InterfaceImpl, class Interface> 
bool TFactory<InterfaceImpl, Interface>::RemoveResource(Interface* pObject)
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, m_mutex_Factory, false);

	typename std::map<Interface *, int>::iterator i;

	i = m_BusyMap.find(pObject);
	if(i != m_BusyMap.end())
	{
		m_BusyMap.erase(i);
		return true;
	}

	i = m_FreeMap.find(pObject);
	if(i != m_FreeMap.end())
	{
		m_FreeMap.erase(i);
		return true;
	}

	typename std::deque<Interface *>::iterator Iter;
	for(Iter = m_StoredQueue.begin(); Iter != m_StoredQueue.end(); Iter++)
	{
		if(*Iter == pObject)
		{
			m_StoredQueue.erase(Iter);
			break;
		}
	}

	return false;
}

template <class InterfaceImpl, class Interface> 
size_t TFactory<InterfaceImpl, Interface>::GetCount()
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, m_mutex_Factory, 0);
	return m_FreeMap.size() + m_BusyMap.size();
}

template <class InterfaceImpl, class Interface> 
size_t TFactory<InterfaceImpl, Interface>::GetCountFree()
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, m_mutex_Factory, 0);
	return m_FreeMap.size();
}

template <class InterfaceImpl, class Interface> 
size_t TFactory<InterfaceImpl, Interface>::GetCountBusy()
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, m_mutex_Factory, 0);
	return m_BusyMap.size();
}

template <class InterfaceImpl, class Interface> 
void TFactory<InterfaceImpl, Interface>::SetMaxObjects(size_t iMaxObjects)
{

}
