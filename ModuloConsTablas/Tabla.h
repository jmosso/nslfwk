// Tabla.h: interface for the Tabla class.
//

//////////////////////////////////////////////////////////////////////
/*


  */
#if !defined(AFX_TABLA_H__2138A077_8D6E_11D4_A7D3_00805FF7227D__INCLUDED_)
#define AFX_TABLA_H__2138A077_8D6E_11D4_A7D3_00805FF7227D__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "ModuloConsTablas/InterfazTab.h"

class Tabla : public InterfazTab 

{

public:

	Tabla();
	virtual ~Tabla();

	const char* searchservice(const char* NumAbonadoB);
	const char* SearchGwDirIp(const char *NumAbonadoB);		
	
	short GetScriptFile(const char* strPhoneNumber, char* strFilePath,size_t buffersize);
	short GetBusTarget(const char* strPhoneNumber, char* strBusTarget, size_t buffersize);
	short GetGroupId(const char* strPhoneNumber, char* strGroupId, size_t buffersize);

	short GetParamBoardChar(const char * board, const char *item, size_t size, char *param);
	short GetParamBoardInt(const char * board,const char *item, int & OutVariable);
	short GetParamChannelChar(char *param,const char *item, const char * channel, ...);
	short GetParamChannelInt(int & OutVariable,const char *item, const char * channel, ...);

	short FinalizarConsTabla();
	short Reinicie();
	
private:

};

#endif // !defined(AFX_TABLA_H__2138A077_8D6E_11D4_A7D3_00805FF7227D__INCLUDED_)


