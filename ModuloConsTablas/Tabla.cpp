
#include "Tabla.h"
#include "GwTabla.h"
#include "ModConsTabla.h"
#include "Utilities/Tracing.h"

#include "ace/OS.h"

#define MAX_BOARDS_LINE_LEN 2000
#define MAX_PHONE_LEN 100

Tabla::Tabla()
{
//	pCanalList = new CChannelList;
}

Tabla::~Tabla()
{
//	delete pCanalList;
}


short Tabla::Reinicie()
{
//	ipj[0] = '\0';
//	carpr[0] = '\0';
//	Ident[0] = '\0';
//	Estserv[0] = '\0';
//	ipr[0] = '\0';
//	Val[0] = '\0';

//	cantidadNumeros = 0;
//	Eval = 0;
//	resultado = 0;

//	ivrpr = 0;				//--apuntador para tabla IVRnoIVR
//	ptrIp = 0;	

	return 0;
}

short Tabla::FinalizarConsTabla( )
{
	Reinicie();
	ModConsTabla::St_GetInstance()->FreeResource(this);
	return 0;
}

const char* Tabla::searchservice(const char* NumAbonadoB)  //--metodo para discriminar dato pstn-ip-ivr
{
	char NumeroB[MAX_PHONE_LEN];

	size_t	length = ACE_OS::strlen(NumAbonadoB);
	if(length > MAX_PHONE_LEN-1)
	{
		ACE_OS::memcpy(NumeroB, NumAbonadoB, MAX_PHONE_LEN-2);
		NumeroB[MAX_PHONE_LEN-1] = '\0';
	}
	else
	{
		ACE_OS::strcpy(NumeroB, NumAbonadoB);
	}

 	const char * ipr = ModConsTabla::St_GetInstance()->m_pTablaServicios->getPar(NumeroB); //---Par�metros Tabla1--//


	if (ipr == 0)	
		TRACEPF_DEBUG("", "Tabla::searchservice No encontrado %s", NumeroB);

	return ipr;
}


const char * Tabla::SearchGwDirIp (const char *NumAbonadoB)
{
	char NumeroB[MAX_PHONE_LEN];

	size_t length = ACE_OS::strlen(NumAbonadoB);
	if(length > MAX_PHONE_LEN-1)
	{
		ACE_OS::memcpy(NumeroB, NumAbonadoB, MAX_PHONE_LEN-2);
		NumeroB[MAX_PHONE_LEN-1] = '\0';
	}
	else
	{
		ACE_OS::strcpy(NumeroB, NumAbonadoB);
	}

	const char *ptrIp = ModConsTabla::St_GetInstance()->m_pTablaGws->getPar(NumeroB); 
	
	if (ptrIp == 0)	
	{
		TRACEPF_ERROR("", "Error No hay elemento de la tabla gws asociado al numero %s", NumeroB);
	}
	return ptrIp;
}

short Tabla::GetScriptFile(const char* strPhoneNumber, char* strFilePath,size_t buffersize)
{
	char NumeroB[MAX_PHONE_LEN];

	size_t length = ACE_OS::strlen(strPhoneNumber);
	if(length > MAX_PHONE_LEN-1)
	{
		ACE_OS::memcpy(NumeroB, strPhoneNumber, MAX_PHONE_LEN-2);
		NumeroB[MAX_PHONE_LEN-1] = '\0';
	}
	else
	{
		ACE_OS::strcpy(NumeroB, strPhoneNumber);
	}

	const char * teldespr = ModConsTabla::St_GetInstance()->m_pTablaScriptChoice->getPar(NumeroB); 

	if(teldespr == 0)
	{
		ACE_OS::strcpy(strFilePath, "");
		return -1;	
	}
	size_t len = ACE_OS::strlen(teldespr);
	if(len <= buffersize)
		ACE_OS::strcpy(strFilePath, teldespr);
	return 0;
}

short Tabla::GetBusTarget(const char* strPhoneNumber, char* strBusTarget, size_t buffersize)
{
	char NumeroB[MAX_PHONE_LEN];
	size_t length = ACE_OS::strlen(strPhoneNumber);
	if(length > MAX_PHONE_LEN-1)
	{
		ACE_OS::memcpy(NumeroB, strPhoneNumber, MAX_PHONE_LEN-2);
		NumeroB[MAX_PHONE_LEN-1] = '\0';
	}
	else
	{
		ACE_OS::strcpy(NumeroB, strPhoneNumber);
	}

	const char * teldespr = ModConsTabla::St_GetInstance()->m_pTablaBusTarget->getPar(NumeroB); 
	if(teldespr == 0)
	{
		ACE_OS::strcpy(strBusTarget, "");
		return -1;	
	}

	size_t len = ACE_OS::strlen(teldespr);
	if(len < buffersize)
		ACE_OS::strcpy(strBusTarget, teldespr);

	return 0;
}

short Tabla::GetGroupId(const char* strPhoneNumber, char* strGroupId, size_t buffersize)
{
	char NumeroB[MAX_PHONE_LEN];
	size_t length = ACE_OS::strlen(strPhoneNumber);
	if(length > MAX_PHONE_LEN-1)
	{
		ACE_OS::memcpy(NumeroB, strPhoneNumber, MAX_PHONE_LEN-2);
		NumeroB[MAX_PHONE_LEN-1] = '\0';
	}
	else
	{
		ACE_OS::strcpy(NumeroB, strPhoneNumber);
	}

	const char * teldespr = ModConsTabla::St_GetInstance()->m_pTablaGroupId->getPar(NumeroB); 
	if(teldespr == 0)
	{
		ACE_OS::strcpy(strGroupId, "");
		return -1;	
	}

	size_t len = ACE_OS::strlen(teldespr);
	if(len < buffersize)
		ACE_OS::strcpy(strGroupId, teldespr);

	return 0;
}

short Tabla::GetParamBoardChar(const char * board,const char *item, size_t size, char *param)
{
	char valResult[MAX_BOARDS_LINE_LEN];
	ACE_OS::memset(valResult,0,MAX_BOARDS_LINE_LEN);
	char itemex[20];
	ACE_OS::strcpy(itemex,item);
	ACE_OS::strcat(itemex,":");
	const char * pAux = ModConsTabla::St_GetInstance()->gw_apuntTab18->getPar(board);
	if(pAux == NULL)
		return 2; // error en la tabla 0 board no existe

	size_t len = ACE_OS::strlen(pAux);
	if(len > MAX_BOARDS_LINE_LEN)
		return 2; // error en la tabla 0 board no existe

	ACE_OS::strcpy(valResult,pAux);
	char * pAux0 = ACE_OS::strstr(valResult,itemex);

	if(pAux0 == NULL)
		return 0; // parametro no existe

	len = ACE_OS::strlen(itemex);

	pAux0 = pAux0 + len;
	char * pAux1 = ACE_OS::strchr(pAux0,'$');

	if(pAux1 != NULL)
	{
		*pAux1 = '\0'; // en pAux0 queda el valor del paramentro
	}

	if(ACE_OS::strlen(pAux0) < size)
		ACE_OS::strcpy(param,pAux0);
	else
	{
		return 2;
	}	
	return 1; // paramentro existe
}

short Tabla::GetParamBoardInt(const char * board,const char *item, int & OutVariable)
{
	char valResult[MAX_BOARDS_LINE_LEN];
	ACE_OS::memset(valResult,0,MAX_BOARDS_LINE_LEN);
	char itemex[20];
	ACE_OS::strcpy(itemex,item);
	ACE_OS::strcat(itemex,":");
	const char * pAux = ModConsTabla::St_GetInstance()->gw_apuntTab18->getPar(board);
	if(pAux == NULL)
		return 2; // error en la tabla 0 board no existe

	size_t len = ACE_OS::strlen(pAux);
	if(len > MAX_BOARDS_LINE_LEN)
		return 2; // error en la tabla 0 board no existe

	ACE_OS::strcpy(valResult,pAux);
	char * pAux0 = ACE_OS::strstr(valResult,itemex);

	if(pAux0 == NULL)
		return 0; // parametro no existe

	len = ACE_OS::strlen(itemex);

	pAux0 = pAux0 + len;
	char * pAux1 = ACE_OS::strchr(pAux0,'$');

	if(pAux1 != NULL)
	{
		*pAux1 = '\0'; // en pAux0 queda el valor del paramentro
	}

	//OutVariable = ACE_OS::atoi(pAux0);
	if(ACE_OS::strchr(pAux0,'x'))
	{
		::sscanf( pAux0, "0x%x", &OutVariable );
	}
	else
		OutVariable = ACE_OS::atoi(pAux0);


	return 1; // paramentro existe
}

short Tabla::GetParamChannelChar(char *param, const char *item, const char * channel,...)
{
    char szBuff[50];
	char valResult[400];
	ACE_OS::memset(valResult,0,400);

    int retValue;

	va_list lista;
	va_start( lista, channel );     // Initialize variable arguments. 
	//Trace::sC.Lock();	retValue = wACE_OS::vsprintf(szBuff,format, lista); 	Trace::sC.Unlock();
	retValue = ACE_OS::vsprintf(szBuff,channel, lista); 
	va_end( lista ); 		// Reset variable arguments.      


	const char * pAux = 0;
	ModConsTabla* ref = ModConsTabla::St_GetInstance();
	if(ref)
	{
		if(ref->gw_apuntTab19)
		{
			pAux = ModConsTabla::St_GetInstance()->gw_apuntTab19->getPar(szBuff);
		}
	}
	
	if (pAux == NULL)	
	{
		return 0;
	}

	char itemex[20];
	ACE_OS::strcpy(itemex,item);
	ACE_OS::strcat(itemex,":");

	ACE_OS::strcpy(valResult,pAux);

	char * pAux0 = ACE_OS::strstr(valResult,itemex);

	if(pAux0 == NULL)
		return 0; // parametro no existe

	size_t len = ACE_OS::strlen(itemex);

	pAux0 = pAux0 + len;
	char * pAux1 = ACE_OS::strchr(pAux0,'$');

	if(pAux1 != NULL)
	{
		*pAux1 = '\0'; // en pAux0 queda el valor del paramentro
	}

	ACE_OS::strcpy(param,pAux0);
	return 1;
}

short Tabla::GetParamChannelInt(int & OutVariable, const char *item, const char * channel,...)
{
    char szBuff[50];
	char valResult[400];
	ACE_OS::memset(valResult,0,400);

    int retValue;

	va_list lista;
	va_start( lista, channel );     // Initialize variable arguments. 
	//Trace::sC.Lock();	retValue = wACE_OS::vsprintf(szBuff,format, lista); 	Trace::sC.Unlock();
	retValue = ACE_OS::vsprintf(szBuff,channel, lista); 
	va_end( lista ); 		// Reset variable arguments.      


	const char * pAux = ModConsTabla::St_GetInstance()->gw_apuntTab19->getPar(szBuff); 
	
	if (pAux == NULL)	
	{
		return 0;
	}

	char itemex[20];
	ACE_OS::strcpy(itemex,item);
	ACE_OS::strcat(itemex,":");

	ACE_OS::strcpy(valResult,pAux);

	char * pAux0 = ACE_OS::strstr(valResult,itemex);

	if(pAux0 == NULL)
		return 0; // parametro no existe

	size_t len = ACE_OS::strlen(itemex);

	pAux0 = pAux0 + len;
	char * pAux1 = ACE_OS::strchr(pAux0,'$');

	if(pAux1 != NULL)
	{
		*pAux1 = '\0'; // en pAux0 queda el valor del paramentro
	}
	//OutVariable = ACE_OS::atoi(pAux0);
	if(ACE_OS::strchr(pAux0,'x'))
	{
		::sscanf( pAux0, "0x%x", &OutVariable );
	}
	else
		OutVariable = ACE_OS::atoi(pAux0);

	return 1;
}

