#pragma once

#include <cstddef>

class InterfazTab  
{
public:

	virtual const char * searchservice (const char* NumAbonadoB) = 0;
	virtual const char * SearchGwDirIp (const char *NumAbonadoB) = 0;
	virtual short FinalizarConsTabla()=0;

	virtual short GetScriptFile(const char* strPhoneNumber, char* strFilePath, size_t buffersize) = 0;
	virtual short GetBusTarget(const char* strPhoneNumber, char* strBusTarget, size_t buffersize) = 0;
	virtual short GetGroupId(const char* strPhoneNumber, char* strGroupId, size_t buffersize) = 0;

	virtual short GetParamBoardChar(const char * board, const char *item, size_t size, char *param) = 0;
	virtual short GetParamBoardInt(const char * board,const char *item, int & OutVariable) = 0;
	virtual short GetParamChannelChar(char *param,const char *item, const char * channel, ...) = 0;
	virtual short GetParamChannelInt(int & OutVariable,const char *item, const char * channel, ...) = 0;
};

