// ModConsTabla.h: interface for the ModConsTabla class.
//
//////////////////////////////////////////////////////////////////////
#pragma once

#include "ModuloConsTablas/InterfazTab.h"
#include "FrameworkUtilities/DynamicStruct.h"
#include "Templates/TFactory.h"
#include "Templates/TSingleton.h"

#include <map>

class GwTabla;

class ModConsTabla : public TSingleton<ModConsTabla>
{
	class ModConsTablaDP;
	friend class Tabla;

public:

	ModConsTabla();
	virtual ~ModConsTabla();

	InterfazTab* GetResource();
	void FreeResource(InterfazTab* obj);

	int Init();
	int Exit();

	bool GetLineData(const CFwkString & strTableName, const char* strKey, CDynamicStruct& dynData);
	bool SetLineData(const CFwkString & strTableName, const char* strKey, const char* strData);
	GwTabla* TableByName(const CFwkString & strTableName);

	void ReloadFiles();

	bool GetTableValue(const CFwkString & strMnemonic, CFwkString& strKey, CFwkString& strValue);
	bool GetTableValue(const CFwkString & strMnemonic, CFwkString& strKey, SIX_FWK_INT64& iValue);
	bool GetTableValueInv(const CFwkString & strMnemonic, CFwkString& strKey, CFwkString& strValue);
	bool GetTableValueInv(const CFwkString & strMnemonic, CFwkString& strKey, SIX_FWK_INT64& iValue);

private:

	GwTabla* m_pTablaServicios;	
	GwTabla* m_pTablaGws;
	GwTabla* m_pTablaNumberMap;
	GwTabla* m_pTablaScriptChoice;
	GwTabla* m_pTablaBusTarget;
	GwTabla* m_pTablaGroupId;
	GwTabla *gw_apuntTab18;
	GwTabla *gw_apuntTab19;

	ModConsTablaDP* m_factoryDP;

	typedef std::map<CFwkString, GwTabla*> MAPSTR2GWTBL;
	MAPSTR2GWTBL m_mapServiceTables;
	MAPSTR2GWTBL m_mapServiceTablesInv;
};

