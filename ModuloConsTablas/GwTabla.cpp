
#include "Utilities/Tracing.h"
#include "Utilities/ThreadUtilities.h"
#include "GwTabla.h"

#include "ace/OS.h"

NodoGw1::NodoGw1()
{
	isAsterisco = false;
	isEnd = false;
	for (int i=0;i<95;i++)
	{
		pNext[i]=0;
	}
}

NodoGw1::~NodoGw1()
{
	for (int i=0;i<95;i++)
	{
		if (pNext[i]!=0)
		{
			delete pNext[i];
			pNext[i] = 0;
		}
	}

}


GwTabla::GwTabla()
{
	uiLongTableGW = 0;
	ArchOk = false;
	actTabla = true;
	actTablaWrite = true;
	inverso = false;
	blockUpdate = 0;
	blockUpdateTabla = 0;
	blockUpdate = new CSectionCritical;
	blockUpdateTabla = new CSectionCritical;
	ACE_OS::memset(nameTabla,0,200);
	uiLongTableReal = 0;
}


GwTabla::~GwTabla()
{
	if(blockUpdate)
		delete blockUpdate;
	if(blockUpdateTabla)
		delete blockUpdateTabla;
}

const char * GwTabla::
getNumDestino(const char *pTelDestino)
{
	if (!ArchOk)
		return 0;
	int i = 0;
	char index;
	size_t lengthInd = ACE_OS::strlen(pTelDestino);
	NodoGw1 *pB ;
	NodoGw1 *pBAnt=0; 

	
	
	if  (actTabla)		 			///------cargar tabla auxiliar------///

	 pB = &(TabGW); 
		else 
	 pB = &(TabGW1);

	while ( pTelDestino [i] != '\0')	
	{
		index = pTelDestino[i] - ' ';
		
		/*if(pTelDestino[i] == '*') // se hace la conversi�n de * a 10
			index = 10;
		if(pTelDestino[i] == '#') // se hace la conversi�n de # a 11
			index = 11;
		if(pTelDestino[i] == '.') // se hace la conversi�n de . a 12
			index = 12;*/

		if (pB->pNext[index] != 0)
		{
			pB = pB->pNext[index];
			i++;
			if (pB->isAsterisco||(pB->isEnd & (i==lengthInd)))	// (pB->isEnd & (i==lengthInd)) significa que 
			{													//  el indicativo solicitado corresponde exactamente
				pBAnt = pB;										//  con uno que est� en la tabla
			}
			
		}
		else
		{
			break;
		}
		
		
	}
	char aux[BUFFER_SIZE];
	ACE_OS::strcpy(valret,pTelDestino);
	if(pBAnt->ip.GetBuffer())
	{
		ACE_OS::strcpy(aux,pBAnt->ip.GetBuffer());
		char * aux0;
		aux0 = ACE_OS::strchr(aux,':');
		if(!aux0)
			return 0;
		*aux0 = 0;
		
		int numeroDigitos = ACE_OS::atoi(aux);
		if(numeroDigitos)
		{
			size_t lengh = ACE_OS::strlen(valret);
			if(numeroDigitos == lengh)
			{
				aux0++;
				int numeroDigitosMarcar = ACE_OS::atoi(aux0);
				return &valret[numeroDigitos-numeroDigitosMarcar];
			}
			else
			{
				return 0;
			}

		}
		else
		{
			return 0;
		}
		

	}
	else
	{
		return 0;
	}
	return pBAnt->ip.GetBuffer();
}


int GwTabla::
analisisDigitos(const char *pTelDigitos, int * pTotalDigitos)
{
	char numAcumulado[50];
	ACE_OS::strcpy(numAcumulado,pTelDigitos);
	bool numval = false;


	if (!ArchOk)
		return 0;
	int i = 0;
	char index;
	size_t lengthInd = ACE_OS::strlen(numAcumulado);
	NodoGw1 *pB ;
	NodoGw1 *pBAnt=0; 

	if  (actTabla)		 			///------cargar tabla auxiliar------///
	 pB = &(TabGW); 
	
	else 
	 pB = &(TabGW1);

	while ( numAcumulado [i] != '\0')	
	{
		index = numAcumulado[i] - ' ';
		/*if(numAcumulado[i] == '*') // se hace la conversi�n de * a 10
			index = 10;
		if(numAcumulado[i] == '#') // se hace la conversi�n de * a 10
			index = 11;
		if(numAcumulado[i] == '.') // se hace la conversi�n de * a 10
			index = 12;
		*/
		if (pB->pNext[index] != 0)
		{
			numval = true;
			pB = pB->pNext[index];
			i++;
			if (pB->isAsterisco||(pB->isEnd & (i==lengthInd)))	// (pB->isEnd & (i==lengthInd)) significa que 
			{													//  el numero acumulado a analizar corresponde exactamente
				pBAnt = pB;										//  con uno que est� en la tabla
			}
			
		}
		else
		{
			numval = false;
			break;
		}				
	}
	if((!numval)&(pBAnt==0))
	{
		numAcumulado [0] = 0;
		*pTotalDigitos = -1;
		return 0;
	}
	else
	{
		if(pBAnt)
		{
			char aux[20];
			ACE_OS::strcpy(aux,pBAnt->ip.GetBuffer());
			int numeroDigitos = ACE_OS::atoi(aux);
			/*if(!numeroDigitos)
			{
				numAcumulado [0] = 0;
				*pTotalDigitos = -1;
				return 0;
			}*/
			*pTotalDigitos = numeroDigitos;
			numAcumulado [0] = 0;
			return 2;
		}
		else
		{
			*pTotalDigitos = -1;
			return 1;
		}
	}
}

const char * GwTabla::
getPar(const char *pIndicativo)
{

	if (!ArchOk)
	{
		TRACEPF_DEBUG("", "GwTabla::getPar No existe archivo asociado");
		return 0;
	}

	int i = 0;
	char index;
	size_t lengthInd = ACE_OS::strlen(pIndicativo);//-----N�mero entero de la longitud de la cadena.

	NodoGw1 *pB ;
	NodoGw1 *pBAnt=0; 

//-----------------++++++++++++++++----------------+++++++++++++++++---//
	if  (actTabla)		 			///------cargar tabla auxiliar------///
	 pB = &(TabGW); 
	else 
	 pB = &(TabGW1);
//----------------++++++++++++++++---------------+++++++++++++++++++------////

	while ( pIndicativo [i] != '\0')	
	{

		index = pIndicativo[i] - ' ';
		/*if(pIndicativo[i] == '*') // se hace la conversi�n de * a 10
			index = 10;
		if(pIndicativo[i] == '#') // se hace la conversi�n de * a 10
			index = 11;
		if(pIndicativo[i] == '.') // se hace la conversi�n de * a 10
			index = 12;
		*/
		if((index < 0) || (index > 94))
		{
			if(pBAnt == 0)
				return 0;

			return pBAnt->ip.GetBuffer();
		}

		if (pB->pNext[index] != 0)

		{
			pB = pB->pNext[index];
			i++;
			if (pB->isAsterisco||(pB->isEnd & (i==lengthInd)))	// (pB->isEnd & (i==lengthInd)) significa que 
			{													//  el indicativo solicitado corresponde exactamente
				pBAnt = pB;										//  con uno que est� en la tabla
			}			
		}
		else
		{
			break;
		}				
	}
	if(pBAnt == 0)
	{
		if(actTabla)
			pB = &(TabGW); 
		else 
			pB = &(TabGW1);

		if(pB->isAsterisco == true)
			return pB->ip.GetBuffer();

		return 0;
	}

	return pBAnt->ip.GetBuffer();
}



bool GwTabla::
init(
	const char* fileName		//----ruta de la tabla
	)
{
	// cargar los datos del archivo ...
	if(fileName[0] == '\0')
		return false;
	ACE_OS::strcpy(nameTabla,fileName);
	uiLongTableGW = longFile(fileName);
	if (uiLongTableGW == -1)
	{
		return false;
	}
	if (!loadGWs(fileName))
	{
		return false;
	}
	ArchOk = true;
	return true;
}



bool GwTabla::
loadGWs(
	const char* fileName
	)
{
	char Buffer[BUFFER_SIZE];
	CFwkString ind;              //--arreglo para � de Tel..16 digitos..
	CFwkString dirIp;					   //--arreglo para Dir IP 64 digitos	
	size_t count=0;				   //--entero sin signo	
	int i=0,
		ch;
	FILE *f;
	bool comentario = false;
	bool coma_dospuntos = false;
	uiLongTableReal = 0;
	
	Buffer[0]= '\0';		// Inicializaci�n
	
	if ( (f = ACE_OS::fopen (fileName, "r")) == 0 )
	{
		return (false);
	}

	while ( (!feof( f )) && (count < uiLongTableGW) )
	{
		ch = ACE_OS::fgetc ( f );

		switch (ch)
		{
		case ',':
				if(comentario)
					break;
				if(!coma_dospuntos)
				{
					ind = Buffer;
					Buffer [0] = '\0';
					i=0;
					coma_dospuntos = true;
				}
				else
				{
					Buffer[i] = (char) ch;
					Buffer[i+1] = '\0';
					i++;
				}
				break;
		//case ':':
		//		if(comentario)
		//			break;
		//		if(!coma_dospuntos)
		//		{
		//			ind = Buffer;
		//			Buffer [0] = '\0';
		//			i=0;
		//			coma_dospuntos = true;
		//		}
		//		else
		//		{
		//			Buffer[i] = (char) ch;
		//			Buffer[i+1] = '\0';
		//			i++;
		//		}
		//		break;
		case ';':							// caracter de terminaci�n					
				if(comentario)
					break;
				{
				dirIp = Buffer;
				Buffer [0] = '\0';
				bool bRet = true;
				if(!inverso)
					bRet = Insertar(ind,dirIp);
				else
					bRet = Insertar(dirIp,ind);
				ind = Buffer;
				dirIp = Buffer;
				i=0;
				coma_dospuntos = false;
				if(bRet)
				{
					count++;
					uiLongTableReal++;
				}
				if (count == uiLongTableGW)
				{
					ACE_OS::fclose (f);
					return true;
				}
				}
				comentario = false;
				break;
		case '\x0a':						// Fin de linea
				/*Buffer [0] = '\0';
				ind = Buffer;
				dirIp = Buffer;
				i=0;
				coma_dospuntos = false;*/
				Buffer [0] = '\0';
				i=0;
				comentario = false;
				break;

		case '\r':						// Fin de linea
				comentario = false;
				break;

		case -1:							// Fin de Archivo.
				ACE_OS::fclose (f);
				return true;
		//case ' ':
		//		break;
		case '?':
			    //if((ind[(size_t)0] == 0)&&(dirIp[(size_t)0]==0))
			    if((ind.IsEmpty() == true)&&(dirIp.IsEmpty() == true))
					comentario = true;
				else
				{
					Buffer[i] = (char) ch;
					Buffer[i+1] = '\0';
					i++;
				}
				break;

		case '\\': // usado como caracter de escape, leer el siguiente caracter como el valido
			ch = ACE_OS::fgetc ( f );
			Buffer[i] = (char) ch;
			Buffer[i+1] = '\0';
			i++;
			break;
				
		default:
				if(!comentario)
				{
					Buffer[i] = (char) ch;
					Buffer[i+1] = '\0';
					i++;
				}
				break;
		}
	}
	ACE_OS::fclose (f);
	TRACEPF_DEBUG("", "tabla = %s est� mal construida",fileName);
	return false;
}


//--------retorna el n�mero de filas de la tabla------------///


int GwTabla::
longFile(
	const char *pArch
	)
{
	char Buffer[BUFFER_SIZE];
	int iLong = 0;

	FILE *f = ACE_OS::fopen (pArch, "r");
	if ( f == 0 )
	{
		return (-1);
	}

	while ( !feof( f ) )
	{
		ACE_OS::fgets(Buffer, BUFFER_SIZE, f );
		char * pAuxendLine = ACE_OS::strchr(Buffer,'\x0a');
		if(pAuxendLine == 0) // la linea tiene mas de BUFFER_SIZE caracteres
		{
			pAuxendLine = ACE_OS::strchr(Buffer,-1);
			if(pAuxendLine == 0)
			{
				// se lee el archivo hasta encontrar el fin de la linea
				char ch = ACE_OS::fgetc ( f );
				while((ch != '\x0a')&(ch != -1))
				{
					ch = ACE_OS::fgetc ( f );
				}
			}
		}

		iLong++;
	}
	ACE_OS::fclose (f);
	return (iLong);
}
	
int GwTabla::
longFile()
{
	char Buffer[BUFFER_SIZE];
	int iLong = 0;

	FILE *f = ACE_OS::fopen (nameTabla, "r");
	if ( f == 0 )
	{
		return (-1);
	}

	while ( !feof( f ) )
	{
		ACE_OS::fgets(Buffer, BUFFER_SIZE, f );
		char * pAuxendLine = ACE_OS::strchr(Buffer,'\x0a');
		if(pAuxendLine == 0) // la linea tiene mas de BUFFER_SIZE caracteres
		{
			pAuxendLine = ACE_OS::strchr(Buffer,-1);
			if(pAuxendLine == 0)
			{
				// se lee el archivo hasta encontrar el fin de la linea
				char ch = ACE_OS::fgetc ( f );
				while((ch != '\x0a')&(ch != -1))
				{
					ch = ACE_OS::fgetc ( f );
				}
			}
		}

		iLong++;
	}
	ACE_OS::fclose (f);
	return (iLong);
}

bool GwTabla::Insertar(CFwkString & Ind, CFwkString & IpGw)
{
	size_t i=0,		
	index;

	//if ((Ind[(size_t)0] == 0)||(IpGw[(size_t)0]== 0))
	//	return false;
	
	if(Ind.IsEmpty() == true || IpGw.IsEmpty() == true)
		return false;

	size_t iIndLen = Ind.GetMaxSize();
	if  (actTablaWrite)
	{

		NodoGw1 *pB = &(TabGW);

		//while ( Ind[i] != '\0')
		while ( i < iIndLen)
		{
			index = Ind[i] - ' ';  // espacio es el primer caracter de la tabla ascii que vale la pena incluir
			/*if(Ind[i] == '*') // se hace la conversi�n de * a 10
				index = 10;
			if(Ind[i] == '#') // se hace la conversi�n de * a 10
				index = 11;
			if(Ind[i] == '.') // se hace la conversi�n de * a 10
				index = 12;*/
			if (Ind[i]=='!')		// anteriomente este caracter era *, pero fue necesario cambiarlo, ya que * es un caracter del teclado telefonico
			{

				pB->isAsterisco = true;
				pB->ip = IpGw;

				break;
			}
			else
			{
				if (index<0||index>94)
					return false;
				if (pB->pNext[index] == 0)
				{
					pB->pNext[index] = new NodoGw1;
				}
				pB = pB->pNext[index];
				
			}
			i++;
		}
		pB->ip = IpGw;
		pB->isEnd = true;
	}
	else 
	{
		NodoGw1 *pC = &(TabGW1);

		//while ( Ind[i] != '\0')
		while ( i < iIndLen )
		{
			index = Ind[i] - ' ';  // espacio es el primer caracter de la tabla ascii que vale la pena incluir
			
			/*if(Ind[i] == '*') // se hace la conversi�n de * a 10
				index = 10;
			if(Ind[i] == '#') // se hace la conversi�n de * a 10
				index = 11;
			if(Ind[i] == '.') // se hace la conversi�n de * a 10
				index = 12;*/
			if (Ind[i]=='!')	// anteriomente este caracter era *, pero fue necesario cambiarlo, ya que * es un caracter del teclado telefonico
			{

				pC->isAsterisco = true;
				pC->ip = IpGw; 
				
				break;
			}
			else
			{
				if (index<0||index>94)
					return false;
				if (pC->pNext[index] == 0)
				{
					pC->pNext[index] = new NodoGw1;
				}
				
				pC = pC->pNext[index];
			}
			i++;
		}
		
		pC->ip = IpGw;
		pC->isEnd = true;	
		
	}
	return true;
}

bool GwTabla::setPar(const char *pKeyTabla, char *pParam)
{
	blockUpdate->Lock();
	char Buffer[BUFFER_SIZE],BufferAux[BUFFER_SIZE];
	bool keyFind = false;
	FILE * f = 0;	
	//f = ACE_OS::fopen (nameTabla, "r+"); // abre para lectura y escritura el archivo
	f = ACE_OS::fopen (nameTabla, "r"); // abre para lectura el archivo

	if ( f == 0 )
	{
		blockUpdate->Unlock();
		return false;
	}

	FILE *stream = 0;
	if( (stream = tmpfile()) == 0 )
	{
		ACE_OS::fclose(f);
		blockUpdate->Unlock();
		return false;

	}

  	while ( !feof(f) )
	{
		//long posInit = ftell( f );
		if (ACE_OS::fgets(Buffer, BUFFER_SIZE, f ) != 0)
		{
			char * pAuxendLine = ACE_OS::strchr(Buffer,'\x0a');
			if(pAuxendLine == 0) // la linea tiene mas de BUFFER_SIZE caracteres
			{
				pAuxendLine = ACE_OS::strchr(Buffer,-1);
				if(pAuxendLine == 0)
				{
					// se lee el archivo hasta encontrar el fin de la linea
					char ch = ACE_OS::fgetc ( f );
					while((ch != '\x0a')&(ch != -1))
					{
						ch = ACE_OS::fgetc ( f );
					}
					Buffer[BUFFER_SIZE-4] = 32;
					Buffer[BUFFER_SIZE-3] = 32;
					Buffer[BUFFER_SIZE-2] = 10;
					Buffer[BUFFER_SIZE-1] = 0;
				}
			}
			ACE_OS::strcpy(BufferAux,Buffer);
			char * pAux = ACE_OS::strchr(Buffer,',');
			if (pAux != 0)
			{
				* pAux = 0;
				if(strcmp(Buffer,pKeyTabla)==0) // la clave ha sido encontrada
				{
					keyFind = true;
					if(pParam[0] != 0) // el else significa que se quiere borrar la clave
					{
						if(ACE_OS::fprintf(stream,"%s,%s\n",pKeyTabla,pParam)<0)  // hubo error
						{
							ACE_OS::fclose(f);
							ACE_OS::fclose(stream);
							blockUpdate->Unlock();
							return false;
						}
					}
				}
				else	// la clave leida del arhivo no corresponde con la clave buscada
				{
					if(ACE_OS::fputs(BufferAux,stream)==EOF)
					{
						ACE_OS::fclose(f);
						ACE_OS::fclose(stream);
						blockUpdate->Unlock();
						return false;
					}
				}
			}
			else
			{
				TRACEPF_DEBUG("",  "La tabla %s tiene un error o posiblemente lineas en blanco",nameTabla);
			}
		}
		else // posiblemente fin de archivo
		{
			if(feof(f)!= 0) // fin de archivo
			{
				break;
			}
			else // hubo error en el archivo
			{
				ACE_OS::fclose(f);
				ACE_OS::fclose(stream);
				blockUpdate->Unlock();
				return false;
			}
			
		}
	} // end While


	if(ACE_OS::fclose(f))
		TRACEPF_DEBUG("", "el archivo de tablas %s no se pudo cerrar",nameTabla);
	if(!keyFind) // la clave no existe, de debe adicionar la clave solicitada
	{
		if(pParam[0]!=0)
		{
			if(ACE_OS::fprintf(stream,"\n%s,%s\n",pKeyTabla,pParam)<0)  // hubo error
			{
				ACE_OS::fclose(stream);
				blockUpdate->Unlock();
				return false;
			}
		}
	}
	f = 0;
	f = ACE_OS::fopen (nameTabla, "w"); // se abre para escritura la tabla y se copia desde el archivo temporal.
	if ( f == 0 )
	{
		ACE_OS::fclose(stream);
		blockUpdate->Unlock();
		return false;
	}
	if(ACE_OS::fseek( stream, 0, SEEK_SET)!=0) // hubo un error
	{
		ACE_OS::fclose(f);
		ACE_OS::fclose(stream);
		TRACEPF_DEBUG("", "Error grave en La tabla %s, posiblemente fue destruida",nameTabla);
		blockUpdate->Unlock();
		return false;
	}
	else
	{
		while ( !feof(stream) )
		{
			if (ACE_OS::fgets(Buffer, 80, stream ) != 0)
			{
				ACE_OS::fputs(Buffer,f);
			}
		}
		ACE_OS::fclose(f);
		ACE_OS::fclose(stream);
		AuctualizarTabla(nameTabla);
		blockUpdate->Unlock();
		return true;

	}
	if(f)
		ACE_OS::fclose(f);
	if(stream)
		ACE_OS::fclose(stream);
	blockUpdate->Unlock();		
	return false;
}
bool GwTabla::AuctualizarTabla(const char * name)
{
	blockUpdateTabla->Lock();
	if(actTabla) // se recarga la tabla auxiliar
	{
		for (int i=0;i<95;i++)
		{
			if (TabGW1.pNext[i]!=0)
			{
				delete TabGW1.pNext[i];
				TabGW1.pNext[i] = 0;
			}
		}
	}
	else
	{
		for (int i=0;i<95;i++)
		{
			if (TabGW.pNext[i]!=0)
			{
				delete TabGW.pNext[i];
				TabGW.pNext[i] = 0;
			}
		}
	}
	actTablaWrite = !actTablaWrite;
	if(!init(name))
	{
		actTablaWrite = !actTablaWrite;
		blockUpdateTabla->Unlock();
		return false;
	}
	actTabla = !actTabla;
	blockUpdateTabla->Unlock();
	return true;
}
char * GwTabla::getNameFile()
{
	return nameTabla;
}

bool GwTabla::File2Dynamic(CDynamicStruct& dynData)
{
	FILE * f = 0;	
	int countLine = 0;
	char Buffer[BUFFER_SIZE];
	f = ACE_OS::fopen (nameTabla, "r"); // abre para lectura el archivo

	if( f == 0 )
		return 0;

	dynData.AddString("Table.Name", nameTabla);
	dynData.AddInt("Table.iCount", countLine);
	CFwkString str;

	while (!feof(f))
	{
		if (ACE_OS::fgets(Buffer, BUFFER_SIZE, f ) != 0)
		{
			char * pAuxendLine = ACE_OS::strchr(Buffer,'\x0a');
			if(pAuxendLine != 0)
				*pAuxendLine = 0;

			char * pAux2 = ACE_OS::strchr(Buffer,'?');	
			if(pAux2)		// es un comentario
				continue;
			char * pAux1 = ACE_OS::strchr(Buffer,';');
			if(pAux1)
			{
				str.sprintf("Table.Data[%d]", countLine);
				dynData.AddString(str, Buffer);
				countLine ++;
			}
		}
		else
		{
			dynData.AddInt("Table.iCount", countLine);
			ACE_OS::fclose(f);
			return false;
		}
	}
	if(f)
		ACE_OS::fclose(f);

	dynData.AddInt("Table.iCount", countLine);
	return true;	
}

	
bool GwTabla::getLine(size_t line, char * pBuffer)
{
	FILE * f = 0;	
	size_t countLine = 0;
	char Buffer[BUFFER_SIZE];
	f = ACE_OS::fopen (nameTabla, "r"); // abre para lectura el archivo

	if ( f == 0 )
	{
		return 0;
	}
	while ( !feof(f) )
	{
		if (ACE_OS::fgets(Buffer, BUFFER_SIZE, f ) != 0)
		{
			char * pAuxendLine = ACE_OS::strchr(Buffer,'\x0a');
			if(pAuxendLine == 0) // la linea tiene mas de BUFFER_SIZE caracteres
			{
				pAuxendLine = ACE_OS::strchr(Buffer,-1);
				if(pAuxendLine == 0)
				{
					// se lee el archivo hasta encontrar el fin de la linea
					char ch = ACE_OS::fgetc ( f );
					while((ch != '\x0a')&(ch != -1))
					{
						ch = ACE_OS::fgetc ( f );
					}
					Buffer[BUFFER_SIZE-4] = 32;
					Buffer[BUFFER_SIZE-3] = 32;
					Buffer[BUFFER_SIZE-2] = 10;
					Buffer[BUFFER_SIZE-1] = 0;
				}
			}
			char * pAux2 = ACE_OS::strchr(Buffer,'?');	
			if(pAux2)		// es un comentario
				continue;
			char * pAux1 = ACE_OS::strchr(Buffer,';');
			if(pAux1)
			{

				countLine ++;
				if(countLine == line)
				{
					ACE_OS::strcpy(pBuffer,Buffer);
					ACE_OS::fclose(f);
					return true;
				}
			}
		}
		else
		{
			ACE_OS::fclose(f);
			pBuffer = 0;
			return false;
		}
	}
	if(f)
		ACE_OS::fclose(f);
	return false;	
}

void GwTabla::SetInverso(bool invertido)
{
	inverso = invertido;
}
int GwTabla::longFileReal()
{
	return uiLongTableReal;
}
