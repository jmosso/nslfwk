// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#include <bitset>
#include <iostream>
#include <queue>
#include <map>
#include "ace/ACE.h"
#include "ace/Reactor.h"
#include "ace/OS_NS_stdio.h"
#include "ace/OS.h"
#include "Utilities/Tracing.h"
#include "FrameworkUtilities/FwkString.h"

#define UINT unsigned int

// TODO: reference additional headers your program requires here
