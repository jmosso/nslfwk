// GwTabla.h: interface for the GwTabla class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GWTABLA_H__2138A079_8D6E_11D4_A7D3_00805FF7227D__INCLUDED_)
#define AFX_GWTABLA_H__2138A079_8D6E_11D4_A7D3_00805FF7227D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FrameworkUtilities/DynamicStruct.h"

#define BUFFER_SIZE	1200

class NodoGw1
{

public:	
	//CPhoneNumber indicativo;
	CFwkString ip;
	NodoGw1 *pNext[95]; // hay 95 elementos en la tabla ascii que vale la pena incluir
	bool isAsterisco;
	bool isEnd;
	NodoGw1();
	~NodoGw1();
	
};

class CSectionCritical;

class GwTabla  
{
public:
	GwTabla();
	virtual ~GwTabla();

	char * 
	getNameFile();

	bool
	getLine(
		size_t line,		// inicia en 1
		char * pBuffer);

	const char * 
	getNumDestino(
		const char *pTelDestino
				);							// funci�n utilizada para obtener el n�mero que 
											// se debe marcar en el destino
											// La tabla es de la forma: #tel,#total cifras:#de cifras a marcar
											// #tel puede ser de la forma 091!, indicando todos los numeros que
											// inician con 091 (el * se cambio por !)

	int								// 0 = mala seceuncia, 1 = va bien, pero faltan digitos, 2 = seceuncia correcta
	analisisDigitos(
		const char *pTelDigitos,	//	secuencia numerica que se desea revisar si es correcta
		int * pTotalDigitos			//  si la secuencia es correcta se llena con la totalidad de numeros de la secuencia, en caso contrario = -1
		);									// Funci�n empleada para verificar si una secuencai marcada
											// por el usuario es correcta, la verificaci�n se puede realizar digito
											// a digito. la tabla es de la forma:
											// secuencia,numero total de digitos
											// 091!,10

	const char *
	getPar(const char *pIndicativo   // indicativo solicitado
		);									// funci�n utilizada para obteenr el par�metro 
											// que acompa�a al numero telefonico.
											// en una tabla con el formato #tel,parametro;
										    // retorna parametro

	bool						// verdadero si pudo hacer exitosamente la actualizaci�n de una clave
	setPar(
		const char *pKeyTabla,			// clave a ser modificada o adicionada, la clave es el valor que esta antes de la coma (,) dentro de las tablas
		char *pParam			// parametro a cambiar, si es nulo, significa que se quiere borrar la clave
		);									// funci�n utilizada para fijar un par�metro asociado a una clave
											// se encarga de escribir en el archivo asociado a la tabla y
											// refrescar la memoria


	bool 
	init(
		const char* fileName		// nombre de la tabla
		);									// Lee la tabla referenciada por filename y
											// la almacena en memoria, en una estructura arbol

	 
	bool
	AuctualizarTabla(
		const char * name
		);
	
	void
	SetInverso(
		bool invertido
		);

	bool File2Dynamic(CDynamicStruct& dynData);

	int  
	longFileReal();

	int 
	longFile();

private:
	
	int  
	longFile(
		const char *pArch
		);
	bool 
	loadGWs(
		const char* fileName
		);
	bool 
	Insertar(
		CFwkString & Ind, 
		CFwkString & IpGw
		);

	NodoGw1	TabGW;
	NodoGw1 TabGW1;				//----duplicar TABLA en memoria-----������
	bool actTablaWrite;
	bool actTabla;

	unsigned uiLongTableGW;
	unsigned uiLongTableReal;
	int indLonger;
	bool ArchOk;
	bool inverso;	// determina el oreden de la busqueda. Las tablas son del tipo key,par; si invertido = true, este orden se invierte
	char valret[200];
	char nameTabla[200];
	//HANDLE	m_sincAlmacen;
	CSectionCritical *blockUpdate;
	CSectionCritical *blockUpdateTabla;
};

#endif // !defined(AFX_GWTABLA_H__2138A079_8D6E_11D4_A7D3_00805FF7227D__INCLUDED_)
