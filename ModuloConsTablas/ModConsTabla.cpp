
#include "Tabla.h"
#include "ModConsTabla.h"
#include "TablaMap.h"
#include "GwTabla.h"

#include "Utilities/Tracing.h"
#include "SmallConfiguration/SmallConfiguration.h"


#define  ubicacion  6  //desplazamiento del apuntador en la tabla para despertador...

#define MODULE_CONF_NAME "Constabla"
#ifdef WIN32
#define STR_TO_LL ACE_OS::strtoll
#else
#define STR_TO_LL strtoll
#endif
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

class ModConsTabla::ModConsTablaDP
{
public:
	ModConsTablaDP(){};
	~ModConsTablaDP(){};

	 TFactory<Tabla, InterfazTab> m_factory;
};

ModConsTabla::ModConsTabla(): m_factoryDP(new ModConsTablaDP)
{
	m_factoryDP->m_factory.SetFactName("ModConsTabla");

	m_pTablaServicios = new GwTabla;
	m_pTablaGws = new GwTabla;
	m_pTablaNumberMap = new GwTabla;
	m_pTablaScriptChoice =  new GwTabla;
	m_pTablaBusTarget =  new GwTabla;
	m_pTablaGroupId =  new GwTabla;

	gw_apuntTab18 = new GwTabla;
	gw_apuntTab19 = new GwTabla;
}

ModConsTabla::~ModConsTabla()
{
	delete m_pTablaServicios;
	delete m_pTablaGws;
	delete m_pTablaNumberMap;
	delete m_pTablaScriptChoice;
	delete m_pTablaBusTarget;
	delete m_pTablaGroupId;

	delete gw_apuntTab18;
	delete gw_apuntTab19;
}

InterfazTab* ModConsTabla::GetResource()
{
	return m_factoryDP->m_factory.GetResource();
}

void ModConsTabla::FreeResource(InterfazTab* obj)
{
	m_factoryDP->m_factory.FreeResource(obj);
}


bool ModConsTabla::GetLineData(const CFwkString & strTableName, const char* strKey, CDynamicStruct& dynData)
{
	GwTabla* pTable = TableByName(strTableName);
	if(pTable == 0)
		return false;

	const char* pData = pTable->getPar(strKey);
	if(pData == 0)
		return false;

	dynData.AddString("ACK.Data", pData);
	return true;
}

bool ModConsTabla::SetLineData(const CFwkString & strTableName, const char* strKey, const char* strData)
{
	GwTabla* pTable = TableByName(strTableName);
	if(pTable == 0)
		return false;

	pTable->setPar(strKey, (char*)strData);
	return true;
}

GwTabla* ModConsTabla::TableByName(const CFwkString & strTableName)
{
	if(strTableName *= "SCRIPT_SELECTOR")
		return m_pTablaScriptChoice;
	if(strTableName *= "SERVICES")
		return m_pTablaServicios;
	if(strTableName *= "GATEWAYS")
		return m_pTablaGws;
	if(strTableName *= "NUMBERATION_MAP")
		return m_pTablaNumberMap;
	return 0;
}



void ModConsTabla::ReloadFiles()
{
	ModConsTabla::St_GetInstance()->m_pTablaScriptChoice->AuctualizarTabla(ModConsTabla::St_GetInstance()->m_pTablaScriptChoice->getNameFile());
	ModConsTabla::St_GetInstance()->m_pTablaGroupId->AuctualizarTabla(ModConsTabla::St_GetInstance()->m_pTablaGroupId->getNameFile());
	ModConsTabla::St_GetInstance()->m_pTablaBusTarget->AuctualizarTabla(ModConsTabla::St_GetInstance()->m_pTablaBusTarget->getNameFile());
	ModConsTabla::St_GetInstance()->m_pTablaServicios->AuctualizarTabla(ModConsTabla::St_GetInstance()->m_pTablaServicios->getNameFile());
	ModConsTabla::St_GetInstance()->m_pTablaGws->AuctualizarTabla(ModConsTabla::St_GetInstance()->m_pTablaGws->getNameFile());
	ModConsTabla::St_GetInstance()->m_pTablaNumberMap->AuctualizarTabla(ModConsTabla::St_GetInstance()->m_pTablaNumberMap->getNameFile());

	ModConsTabla::St_GetInstance()->gw_apuntTab18->AuctualizarTabla(ModConsTabla::St_GetInstance()->gw_apuntTab18->getNameFile());
	ModConsTabla::St_GetInstance()->gw_apuntTab19->AuctualizarTabla(ModConsTabla::St_GetInstance()->gw_apuntTab19->getNameFile());
}

int ModConsTabla::Init()
{
	CFwkString fstrPath;

	bool bResult = false;
	/////////////////////////////////////////////////////////////////////
	if(VARIABLECHAR(fstrPath, MODULE_CONF_NAME, "ScriptSelectorFile") == true)
	{
		if(m_pTablaScriptChoice->init(fstrPath) == true)
			bResult = true;
	}
	if(bResult == false)
		TRACEPF_WARNING("", "Error loading config file ScriptSelectorFile [%s]", fstrPath.GetBuffer());
	bResult = false;
	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////
	if(VARIABLECHAR(fstrPath, MODULE_CONF_NAME, "servicios") == true)
	{
		if(m_pTablaServicios->init(fstrPath) == true)
			bResult = true;
	}
	if(bResult == false)
		TRACEPF_WARNING("", "Error loading config file servicios [%s]", fstrPath.GetBuffer());
	bResult = false;
	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////
	if(VARIABLECHAR(fstrPath, MODULE_CONF_NAME, "GwsTxtPath") == true)
	{
		if(m_pTablaGws->init(fstrPath) == true)
			bResult = true;
	}
	if(bResult == false)
		TRACEPF_DEBUG("",  "Error loading config file GwsTxtPath [%s]", fstrPath.GetBuffer());
	bResult = false;
	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////
	if(VARIABLECHAR(fstrPath, MODULE_CONF_NAME, "TabTeldes") == true)
	{
		if(m_pTablaNumberMap->init(fstrPath) == true)
			bResult = true;
	}
	if(bResult == false)
		TRACEPF_DEBUG("",  "Error loading config file TabTeldes [%s]", fstrPath.GetBuffer());
	bResult = false;
	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////
	fstrPath = "";
	if(VARIABLECHAR(fstrPath, MODULE_CONF_NAME, "BusTargetFile") == true)
	{
		if(m_pTablaBusTarget->init(fstrPath) == true)
			bResult = true;
	}
	if(bResult == false)
		TRACEPF_WARNING("", "Error loading config file BusTargetFile [%s]", fstrPath.GetBuffer());
	bResult = false;

	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////
	fstrPath = "";
	if(VARIABLECHAR(fstrPath, MODULE_CONF_NAME, "GroupIdFile") == true)
	{
		if(m_pTablaGroupId->init(fstrPath) == true)
			bResult = true;
	}
	if(bResult == false)
		TRACEPF_DEBUG("",  "Error loading config file GroupIdFile [%s]", fstrPath.GetBuffer());
	bResult = false;


	if(VARIABLECHAR(fstrPath, MODULE_CONF_NAME, "Boards") == true)
	{
		if(gw_apuntTab18->init(fstrPath) == true)
			bResult = true;
	}
	if(bResult == false)
		TRACEPF_DEBUG("", "Existence, location or some syntax error in boards" );
	bResult = false;

	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////
	if(VARIABLECHAR(fstrPath, MODULE_CONF_NAME, "Channels") == true)
	{
		if(gw_apuntTab19->init(fstrPath) == true)
			bResult = true;
	}
	if(bResult == false)
		TRACEPF_DEBUG("", "Existence, location or some syntax error in channels" );
	bResult = false;



	std::map<CFwkString, CFwkString> mapVar;
	ENUMERATEVALUES("TABLE_MAPPING", mapVar);

	std::map<CFwkString, CFwkString>::iterator iter;
	iter = mapVar.begin();
	CFwkString strKey;
	CFwkString strValue;
	GwTabla* pTableFile = 0;
	while(iter != mapVar.end())
	{
		strKey = (*iter).first;
		strValue = (*iter).second;
		pTableFile = new GwTabla();
		if(pTableFile->init(strValue) == true)
		{
			m_mapServiceTables[strKey] = pTableFile;
		}
		else
		{
			TRACEPF_ERROR("", "Error loading TABLE_MAPPING FORWARD for key %s file[%s]",  (const char*)strKey, (const char*)strValue);
		}

		pTableFile = new GwTabla();
		pTableFile->SetInverso(true);
		if(pTableFile->init(strValue) == true)
		{
			m_mapServiceTablesInv[strKey] = pTableFile;
		}
		else
		{
			TRACEPF_ERROR("", "Error loading TABLE_MAPPING REVERSE for key %s file[%s]",  (const char*)strKey, (const char*)strValue);
		}

		iter++;
	}

	return 0;
}

int ModConsTabla::Exit()
{
	return 0;
}

bool ModConsTabla::GetTableValue(const CFwkString & strMnemonic, CFwkString& strKey, CFwkString& strValue)
{
	MAPSTR2GWTBL::iterator iter = m_mapServiceTables.find(strMnemonic);
	if(iter == m_mapServiceTables.end())
		return false;

	GwTabla* theTable = (*iter).second;
	const char* pResult = theTable->getPar(strKey);
	if(pResult == 0)
		return false;

	strValue = pResult;
	return true;
}

bool ModConsTabla::GetTableValue(const CFwkString & strMnemonic, CFwkString& strKey, SIX_FWK_INT64& iValue)
{
	MAPSTR2GWTBL::iterator iter = m_mapServiceTables.find(strMnemonic);
	if(iter == m_mapServiceTables.end())
		return false;

	GwTabla* theTable = (*iter).second;
	const char* pResult = theTable->getPar(strKey);
	if(pResult == 0)
		return false;

	iValue = STR_TO_LL(pResult, 0, 10);
	return true;
}

bool ModConsTabla::GetTableValueInv(const CFwkString & strMnemonic, CFwkString& strKey, CFwkString& strValue)
{
	MAPSTR2GWTBL::iterator iter = m_mapServiceTablesInv.find(strMnemonic);
	if(iter == m_mapServiceTablesInv.end())
		return false;

	GwTabla* theTable = (*iter).second;
	const char* pResult = theTable->getPar(strKey);
	if(pResult == 0)
		return false;

	strValue = pResult;
	return true;
}

bool ModConsTabla::GetTableValueInv(const CFwkString & strMnemonic, CFwkString& strKey, SIX_FWK_INT64& iValue)
{
	MAPSTR2GWTBL::iterator iter = m_mapServiceTablesInv.find(strMnemonic);
	if(iter == m_mapServiceTablesInv.end())
		return false;

	GwTabla* theTable = (*iter).second;
	const char* pResult = theTable->getPar(strKey);
	if(pResult == 0)
		return false;

	iValue = STR_TO_LL(pResult, 0, 10);
	return true;
}