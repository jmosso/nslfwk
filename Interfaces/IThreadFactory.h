// IThreadFactory.h: interface for the IThreadFactory class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ITHREADFACTORY_H__D239BC02_15D0_4561_A8D1_4EE6AF0CCBCE__INCLUDED_)
#define AFX_ITHREADFACTORY_H__D239BC02_15D0_4561_A8D1_4EE6AF0CCBCE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Templates/TStore.h"

class IThread;

class IThreadFactory  
{
public:

	///  Get a list of all busy threads
	virtual void GetListThreads(TStore<IThread> & ThreadList) = 0;

};

#endif // !defined(AFX_ITHREADFACTORY_H__D239BC02_15D0_4561_A8D1_4EE6AF0CCBCE__INCLUDED_)
