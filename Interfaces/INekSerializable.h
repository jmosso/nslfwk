#pragma once

class CFwkBuffer;

class INekSerializable
{
public:
//	ISerializable();
//	virtual ~ISerializable();

	virtual void writeObject(CFwkBuffer & out, int nSleepTime = 0) = 0;
	virtual void readObject(const CFwkBuffer & in, int nSleepTime = 0) = 0;
};
