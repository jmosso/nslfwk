#pragma once

#include "ComunHeader.h"

class IThreadMessageCB;
class CDynamicStruct;

/**
* Interfaz para envio y recepción de mensajes.
* 
*/

class INekCommChannel
{
public:
	// IChannel hierarchy
	//************************************
	// Method:    Start
	// FullName:  CChannel::Start
	// Access:    public 
	// Returns:   bool
	// Descript:  Start the thread to process messages.
	//************************************
	virtual bool Start() = 0;

	//************************************
	// Method:    Stop
	// FullName:  CChannel::Stop
	// Access:    public 
	// Returns:   bool
	// Descript:  Stop thread to process messages.
	//************************************	
	virtual bool Stop() = 0;

	//************************************
	// Method:    SendChannelMessage
	// FullName:  CChannel::SendChannelMessage
	// Access:    public 
	// Returns:   bool
	// Descript:  Send a message to this thread
	// Parameter: unsigned short usMessage
	// Parameter: IChannelMessageCB* pObjeSender
	// Parameter: NK_OBJPOINTER pPointer 
	//************************************	
	virtual bool SendMessage(unsigned short usMessage, IThreadMessageCB* pObjeSender, CDynamicStruct * pParam = 0) = 0;

};
