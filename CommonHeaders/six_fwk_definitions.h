#pragma once

#define NK_OBJPOINTER		void*
#define SIX_FWK_MESSAGE		unsigned short
#define SIX_FWK_INT64		long long

#ifdef WIN32
#define STR_TO_LL ACE_OS::strtoll
#else
#define STR_TO_LL strtoll
#endif

/*****************************************************************************
 *                 Constants Definitions                                     *
 *****************************************************************************/

typedef short SAS_Status;
/* Error Codes  */

#define SAS_Success				(0)     /* Success - no error, operation finished successfully. */
#define SAS_Timeout				(-9)    /* Request Timeout		*/
#define SAS_Failure				(-10)    /* General failure (more details can be found on		*/
#define SAS_ThrMsgFail			(-11)    /* Sending Thread Message failure						*/
#define SAS_IllegalAction		(-12)    /* The requested action is illegal (usually illegal 
                                         action in the current state)							*/
#define SAS_InvalidParameter	(-13)    /* A given parameter to function is illegal defected	*/
#define SAS_NetworkProblem		(-14)    /* Action failed due to network problems				*/
#define SAS_ResourceFail		(-15)    /* GetResource from Factory Failed						*/
#define SAS_WaitForResource		(-16)    /* GetResource action performed by callback			*/
#define SAS_FxErrorReceived		(-17)    /* Function was called but received error callback		*/

#define SAS_ResIdNotFound		(6)		 /* No such device or address */
//  *****************************************************************************/
/* Causas principales de terminación de llamada*/
#define SAS_Sgnl_Error			(-100)
#define SAS_Service_Rejected	(-2)
#define SAS_Cause_NotDefined	(-1)
#define SAS_Normal_Release		(16)
#define SAS_User_Busy			(17)
#define SAS_No_User_Responding	(18)
#define SAS_No_Answer_From_User	(19)
#define SAS_Subscriber_Absent 	(20)
#define SAS_Call_Rejected	 	(21)
#define SAS_Normal_Unspecified 	(31)
#define SAS_Temporary_Failure 	(41)
#define SAS_Resource_Unavailable (47)

/* Banderas de validacion del CDR*/
#define SAS_Flag_OK				(0)
#define SAS_Flag_RLNC			(1)		/* Release Not Completed: DONE/ERROR to END not received*/
#define SAS_Flag_ACNC			(2)		/* Accept Not Completed*/
#define SAS_Flag_ANNC			(3)		/* Answer Not Completed*/
#define SAS_Flag_MCNC			(4)		/* MakeCall Not Completed*/
#define SAS_Flag_ACNCPT			(5)		/* Accept Not Completed, PRACK TIMEOUT*/

//  *****************************************************************************/

/* Mensajería principal */

#define SAS_MSG_SGNL_END				(101)
#define SAS_MSG_SGNL_ACCEPT				(102)
#define SAS_MSG_SGNL_CONNECT			(103)
#define SAS_MSG_SGNL_OPEN_SESSION		(104)
#define SAS_MSG_SGNL_NOTIFY				(105)

#define SAS_MSG_SGNL_END_ACK			(121)
#define SAS_MSG_SGNL_ACCEPT_ACK			(122)
#define SAS_MSG_SGNL_CONNECT_ACK		(123)
#define SAS_MSG_SGNL_OPEN_SESSION_ACK	(124)
#define SAS_MSG_SGNL_NOTIFY_ACK			(125)
#define SAS_MSG_SGNL_INNER_EVENT		(126)
#define SAS_MSG_SGNL_GET_RESOURCE_DONE	(127)

#define SAS_MSG_BAS_REQUEST				(200)
#define SAS_MSG_DIAMETER_REQUEST		(900)

#define SAS_DBE_EXECUTE_SIMPLE_QUERY			(210)
#define SAS_DBE_EXECUTE_QUERY_WITH_NAVIGATION	(211)
#define SAS_DBE_EXECUTE_UPDATE					(212)
#define SAS_DBE_EXECUTE_PROCEDURE				(213)
#define SAS_DBE_OPEN							(214)
#define SAS_DBE_OPEN_DATA_SOURCE				(215)
#define SAS_DBE_CLOSE							(216)

#define SAS_STYPE_SIP_REINVITE		(1)
#define SAS_STYPE_SIP_REGISTER 		(2)
#define SAS_STYPE_SIP_PRACK  		(3)
#define SAS_STYPE_SIP_INFO  		(4)
#define SAS_STYPE_SIP_SUBSCRIBE		(5)
#define SAS_STYPE_SIP_NOTIFY 		(6)
#define SAS_STYPE_SIP_REFER 		(7)
#define SAS_STYPE_SIP_MESSAGE 		(8)
#define SAS_STYPE_SIP_OPTIONS 		(9)
#define SAS_STYPE_SIP_ONHOLD 		(10)
#define SAS_STYPE_SIP_UNHOLD 		(11)
#define SAS_STYPE_SIP_ACK	 		(12)
#define SAS_STYPE_SIP_UPDATE 		(13)
#define SAS_STYPE_SIP_UPDATE_ACK	(14)

#define SAS_BEARER_TYPE_PSTN		(1)
#define SAS_BEARER_TYPE_IP			(2)

#define SAS_UID_MAX_LENGHT			(36)

#define MSG_TYPE_DEBUGGER					666 
#define MSG_SUBTYPE_DBG_START				1 
#define MSG_SUBTYPE_DBG_END					3 
#define MSG_SUBTYPE_DBG_INTERPRET			2
#define MSG_SUBTYPE_DBG_WAIT4STEP			4
#define MSG_SUBTYPE_DBG_NEXTSTEP			10
#define MSG_SUBTYPE_DBG_CONTINUE			11
#define MSG_SUBTYPE_DBG_REFRESHBP			12
#define MSG_SUBTYPE_DBG_REFRESHWATCH		13

#define MSG_TYPE_DEBUGGER_EVENT				2003 
#define MSG_TYPE_DEBUGGER_START				2000 
#define MSG_TYPE_DEBUGGER_END				2002 
#define MSG_TYPE_DEBUGGER_NEXT_STEP			2001 
#define MSG_TYPE_DEBUGGER_WAITING_NEXT_STEP	2004
#define MSG_TYPE_DEBUGGER_CONTINUE			2005 
#define MSG_TYPE_DEBUGGER_REFRESH_BP		2006
#define MSG_TYPE_DEBUGGER_REFRESH_WATCH		2007
#define MSG_TYPE_DEBUGGER_SERVICE_LOADED	2008
#define MSG_TYPE_DEBUGGER_SERVICE_ERROR		2010
#define MSG_TYPE_DEBUGGER_SERVICE_WARN		2011
#define MSG_TYPE_DEBUGGER_OPERATION			2020

#define OPER_DBG_STEPLOCATION	"oper.param.steplocation"
#define OPER_DBG_STEPNAME		"oper.param.stepname"
#define OPER_DBG_STEPINFO		"oper.param.stepinfo"
#define OPER_DBG_STEPSOURCEFILE	"oper.param.stepsourcefile"
#define OPER_DBG_BREAKPOINTS	"oper.param.breakpoints"
#define OPER_DBG_WATCHNAMES		"oper.param.watchnames"
#define OPER_DBG_WATCH_COUNT	"oper.param.watch.count"
#define OPER_DBG_WATCH_NAME_F	"oper.param.watch.v_%d.name"
#define OPER_DBG_WATCH_VALUE_F	"oper.param.watch.v_%d.value"
#define OPER_DBG_WATCH_TYPE_F	"oper.param.watch.v_%d.type"
