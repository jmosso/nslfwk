
#include "ThreadFactory.h"
#include "Thread.h"
#include "IThreadParams.h"
#include "Utilities/Tracing.h"
#include "Templates/TFactory.h"

class CThreadFactory::CThreadFactoryDP:public TFactory<CThread, IThread>
{
public:
	CThreadFactoryDP()
	{
		m_fstrFactoryName = "CThreadFactoryDP";
	};
	~CThreadFactoryDP(){};

	int PingThreads();
};


CThreadFactory::CThreadFactory(void): m_factorieDP(new CThreadFactoryDP)
{
}

CThreadFactory::~CThreadFactory(void)
{
}

IThread* CThreadFactory::GetResource()
{
	return m_factorieDP->GetResource();
}

bool CThreadFactory::FreeResource(IThread* pObj)
{
	TRACEPF_WARNING("", "CThreadFactory::FreeResource<0x%x> Threads are self released calling Stop!!!", this);
	return false;
}

void CThreadFactory::PrivateFreeResource(IThread* pObj)
{
	m_factorieDP->FreeResource(pObj);
}

int CThreadFactory::PingThreads()
{
	m_factorieDP->PingThreads();

	return 0;
}

int CThreadFactory::CThreadFactoryDP::PingThreads()
{
	ACE_GUARD_RETURN(MUTEXCLASS, guard, m_mutex_Factory, 0);

	TFactory<CThread, IThread>::OBJECTMAP::iterator it;
	int iCounter = 0;

	CThread* pThread = 0;
	CThread::ms_iPingProcessed = 0;
	for(it = m_BusyMap.begin(); m_BusyMap.end() != it ; it++)
	{
		iCounter++;
		pThread = (CThread*)it->first;

		TRACEPF_ALWAYS("", "PING_MESSAGE (%d) to [%s] 0x%x Queued %d", iCounter, pThread->GetThreadName(), pThread->GetId(), pThread->GetQueueSize());
		pThread->Ping();
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////