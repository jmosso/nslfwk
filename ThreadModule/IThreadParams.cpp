
#include "ThreadModule/IThreadParams.h"

TFactory<IThreadParams, IThreadParams> IThreadParams::ms_factMsgParams;

void IThreadParams::Clear()
{
	uMessage = 0;
	ipIServiceOr = 0;
	iLParam1 = 0;
	iLParam2 = 0;
	iLParam3 = 0;
	iLParam4 = 0;
	iLParam5 = 0;
	lData = 0;

 	m_fstrString1.Clear();
 	m_fstrString2.Clear();
 	m_fstrString3.Clear();
 	m_fstrString4.Clear();
 	m_sDynamicStruct.Clean();
}

void IThreadParams::Free()
{
	Clear();
	ms_factMsgParams.FreeResource(this);
}
