
#include "ace/Synch.h"
#include "ThreadModule/Thread.h"
#include "ThreadModule/IThreadMessageCB.h"
#include "ThreadModule/ThreadFactory.h"
#include "Utilities/Tracing.h"

const SIX_FWK_MESSAGE CThread::END_MESSAGE = ACE_Message_Block::MB_USER + 1;
const SIX_FWK_MESSAGE CThread::PING_MESSAGE = ACE_Message_Block::MB_USER + 2;
const SIX_FWK_MESSAGE CThread::LOWEST_MESSAGE = ACE_Message_Block::MB_USER + 10;

int CThread::ms_iPingProcessed = 0;

CThread::CThread(void) : BufferNotEmpty (this->BufferLock)
{
	m_bRunnig = false;
	m_pSemaphore = new ACE_Thread_Semaphore(0);
	m_dThreadID = 0;

	QueueSize = 0;
	QueueStartOffset = 0;
}

CThread::~CThread(void)
{
	if (m_bRunnig)
	{
		printf("CThread::~CThread activate IThread::Error [%s] ThreadId 0x%x\n", m_fstrThreadName.c_str(), m_dThreadID);
		Stop();
	}

	delete m_pSemaphore;
}

// IThread hierarchy
bool CThread::Start()
{
	if(m_fstrThreadName.empty())
	{
		char buff[32];
		sprintf(buff, "%p", this);
		m_fstrThreadName = buff;
	}

	if(this->activate () == -1)
	{
		TRACEPF_ERROR("", "CThread::Start<0x%x> activate ERROR [%s] ThreadId 0x%x", this, m_fstrThreadName.c_str(), m_dThreadID);
		return false;
	}

	m_pSemaphore->acquire();

	TRACEPF_ALWAYS("", "CThread::Start<0x%x> [%s] 0x%x. started...", this, m_fstrThreadName.c_str(), m_dThreadID);

	return true;
}

bool CThread::Stop()
{
	this->putmq(END_MESSAGE, 0, 0);
	return true;
}

void CThread::Ping()
{
	this->putmq(PING_MESSAGE, 0, 0);
	return;
}

bool CThread::SendThreadMessage(SIX_FWK_MESSAGE usMessage, IThreadMessageCB* pObjeSender, NK_OBJPOINTER pPointer)
{
	if(!m_bRunnig)
	{
		TRACEPF_ERROR("", "CThread::SendThreadMessage<0x%x> NOT RUNNING [%s] ThreadId 0x%x", this, m_fstrThreadName.c_str(), m_dThreadID);
		return false;
	}

	usMessage = usMessage + LOWEST_MESSAGE;
	return putmq(usMessage, pObjeSender, pPointer);
}

int CThread::svc( void )
{
	m_dThreadID = ACE_Thread::self();

 	m_bRunnig = true;
	m_pSemaphore->release();
	CaptureMessage();

	TRACEPF_DEBUG("", "CThread::svc<0x%x> [%s] 0x%x. Leaving infinite loop!!", this, m_fstrThreadName.c_str(), m_dThreadID);

	CThreadFactory::St_GetInstance()->PrivateFreeResource(this);
	return 0;
}

void CThread::CaptureMessage()
{
	while(true)
	{
		SIX_FWK_MESSAGE usMessage = 0;
		IThreadMessageCB* pObjeSender = 0;
		NK_OBJPOINTER pPointer = 0;

		if (getmq(usMessage, pObjeSender, pPointer))
		{
			switch(usMessage)
			{
				case PING_MESSAGE:
				{
					ms_iPingProcessed++;
					TRACEPF_ALWAYS("", "PING_MESSAGE processed (%d) [%s] 0x%x Queued %d", ms_iPingProcessed, m_fstrThreadName.c_str(), m_dThreadID, QueueSize);
				}
				break;

				case END_MESSAGE:
				{
					End();
					return;
				}
				break;

				default:
				{
					if(pObjeSender)
						pObjeSender->ProcessMessage(usMessage - LOWEST_MESSAGE, pPointer);
				}
				break;
			}
		}
	}
}

void CThread::End()
{
	m_bRunnig = false;
}

int CThread::GetId()
{
	return m_dThreadID;
}

bool CThread::putmq(SIX_FWK_MESSAGE usMessage, IThreadMessageCB* pObjeSender, NK_OBJPOINTER pPointer )
{
	BufferLock.acquire();

	if(QueueSize == BUFFER_MESSAGE_SIZE)
	{
		TRACEPF_ERROR("", "CThread::putmq<0x%x> BUFFER FULL!!! [%s] ThreadId 0x%x pObjeSender <0x%x>. Message [%d] discarded",
			this, m_fstrThreadName.c_str(), m_dThreadID, pObjeSender, usMessage);

		BufferLock.release();
		return false;
	}


	// Insert the item at the end of the queue and increment size.

	Buffer[(QueueStartOffset + QueueSize) % BUFFER_MESSAGE_SIZE].nType = usMessage;
	Buffer[(QueueStartOffset + QueueSize) % BUFFER_MESSAGE_SIZE].pPointer = pPointer;
	Buffer[(QueueStartOffset + QueueSize) % BUFFER_MESSAGE_SIZE].pObjeSender = pObjeSender;
	QueueSize++;


	BufferLock.release();

	// If a consumer is waiting, wake it.
	BufferNotEmpty.signal();
	return true;
}

bool CThread::getmq(SIX_FWK_MESSAGE &usMessage, IThreadMessageCB*& pObjeSender, NK_OBJPOINTER &pPointer)
{
	BufferLock.acquire();

	while(QueueSize == 0)
		BufferNotEmpty.wait();

	// Consume the first available item.

	usMessage = Buffer[QueueStartOffset].nType;
	pPointer = Buffer[QueueStartOffset].pPointer;
	pObjeSender = Buffer[QueueStartOffset].pObjeSender;

	QueueSize--;
	QueueStartOffset++;

	if (QueueStartOffset == BUFFER_MESSAGE_SIZE)
	{
		QueueStartOffset = 0;
	}

	BufferLock.release();

	return true;
}
