#pragma once

#include "CommonHeaders/six_fwk_definitions.h"

#define ON_NKMESSAGE(id, memberFxn) \
case id:\
	memberFxn();\
	break;\

#define ON_NKMESSAGE_INT(id, memberFxn) \
case id:\
	memberFxn((intptr_t)pParam);\
	break;\

#define ON_NKMESSAGE_UINT(id, memberFxn) \
case id:\
	memberFxn((size_t)pParam);\
	break;\

#define ON_NKMESSAGE_POINTER(id, memberFxn) \
case id:\
	memberFxn(pParam);\
	break;\

#define ON_NKMESSAGE_DYN(id, memberFxn) \
case id:\
	memberFxn((CDynamicStruct *)pParam);\
	break;\

#define ON_NKMESSAGE_DYN_FREE(id, memberFxn) \
case id:\
	memberFxn((CDynamicStruct *)pParam);\
	DYNFACTFREERES((CDynamicStruct *)pParam);\
	break;\

#define NK_BEGIN_MESSAGE_MAP(theClass, baseClass) \
bool theClass::ProcessMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER pParam)\
{\
	baseClass::ProcessMessage(usMessage, pParam);\
	switch(usMessage)\
	{\

#define NK_BEGIN_MESSAGE_MAP_COND(theClass, condition) \
bool theClass::ProcessMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER pParam)\
{\
	if(condition)\
		return true;\
	switch(usMessage)\
	{\

#define NK_END_MESSAGE_MAP() \
	}\
	return true;\
}\

#define NK_DECLARE_MESSAGE_MAP() \
public: \
	virtual bool ProcessMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER pParam = 0); \


class IThreadMessageCB
{
public:
	virtual bool ProcessMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER pParam = 0)
	{
		return false;
	}
};
