#pragma once

#include "CommonHeaders/six_fwk_definitions.h"
#include <string>

class IThreadMessageCB;

class IThread
{
public:
	IThread(){;}
	virtual ~IThread(){;}

	virtual bool Start() = 0;
	virtual bool Stop() = 0;
	virtual int GetId() = 0;
	virtual void Ping() = 0;
	virtual size_t GetQueueSize() = 0;

	virtual bool SendThreadMessage(SIX_FWK_MESSAGE usMessage, IThreadMessageCB* pObjeSender, NK_OBJPOINTER iParam = 0) = 0;
	inline void SetThreadName(const char* strThreadName){m_fstrThreadName = strThreadName;}
	inline const char* GetThreadName(){return m_fstrThreadName.c_str();}

protected:

	std::string m_fstrThreadName;
};
