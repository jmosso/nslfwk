#pragma once

#include "ThreadModule/IThread.h"
#include "ace/Task.h"

#define BUFFER_MESSAGE_SIZE 10000

class IThreadMessageCB;

class CThread : public IThread
			  , public ACE_Task<ACE_MT_SYNCH>
{
	friend class CThreadFactory;
public:
	CThread(void);
	virtual ~CThread(void);

	struct sMSG_OBJ
	{
		IThreadMessageCB* pObjeSender;
		SIX_FWK_MESSAGE nType;
		NK_OBJPOINTER pPointer;
	};


	bool Start();
	bool Stop();
	void Ping();
	bool SendThreadMessage(SIX_FWK_MESSAGE usMessage, IThreadMessageCB* pObjeSender, NK_OBJPOINTER pPointer = 0);
	int GetId();
	inline size_t GetQueueSize(){ return QueueSize; }

	static const SIX_FWK_MESSAGE END_MESSAGE;
	static const SIX_FWK_MESSAGE PING_MESSAGE;
	static const SIX_FWK_MESSAGE LOWEST_MESSAGE;


protected:

	virtual void CaptureMessage();
	virtual void End();

	bool m_bRunnig;

	virtual bool putmq(SIX_FWK_MESSAGE usMessage, IThreadMessageCB* pObjeSender, NK_OBJPOINTER pPointer = 0);
	virtual bool getmq(SIX_FWK_MESSAGE &usMessage, IThreadMessageCB*& pObjeSender, NK_OBJPOINTER &pPointer);

private:
	ACE_thread_t m_dThreadID;
	int svc(void);
	ACE_Thread_Semaphore *m_pSemaphore;

	sMSG_OBJ Buffer[BUFFER_MESSAGE_SIZE];
	unsigned long QueueSize;
	unsigned long QueueStartOffset;

	static int ms_iPingProcessed;

	ACE_Condition_Thread_Mutex BufferNotEmpty;//ACE_Condition_Recursive_Thread_Mutex
	ACE_Thread_Mutex BufferLock;//ACE_Recursive_Thread_Mutex
};

