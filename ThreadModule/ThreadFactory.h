#pragma once

#include "Templates/TSingleton.h"
#include "ThreadModule/IThread.h"
#include "ThreadModule/IThreadParams.h"

class CThreadFactory : public TSingleton<CThreadFactory>
{
	friend class CThread;
	class CThreadFactoryDP;

public:
	CThreadFactory(void);
	virtual ~CThreadFactory(void);

	int PingThreads();

	bool FreeResource(IThread* pObj);
	IThread* GetResource();

private:

	CThreadFactoryDP* m_factorieDP;
	void PrivateFreeResource(IThread* pObj);
};
