#pragma once

#include "CommonHeaders/six_fwk_definitions.h"
#include "FrameworkUtilities/DynamicStruct.h"
#include "Templates/TFactory.h"

#define THRPARAMSGETRES	IThreadParams::GetThreadParams();

class IThreadParams
{
public:
	IThreadParams(){Clear();}

	int				uMessage;
	int				iLParam1;
	int				iLParam2;
	int				iLParam3;
	SIX_FWK_INT64	lData;
	NK_OBJPOINTER	ipIServiceOr;
	NK_OBJPOINTER	iLParam4;
	NK_OBJPOINTER	iLParam5;
 	CFwkString		m_fstrString1;
 	CFwkString		m_fstrString2;
 	CFwkString		m_fstrString3;
 	CFwkString		m_fstrString4;

 	CDynamicStruct m_sDynamicStruct;

	void Free();
	static inline IThreadParams* GetThreadParams(){return ms_factMsgParams.GetResource();}

private:

	void Clear();
	static TFactory<IThreadParams> ms_factMsgParams;
};
