# dist target
# from cmake wiki, feel free to modify

INCLUDE(Version)

SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Media Resource Distribution")
SET(CPACK_PACKAGE_VENDOR "Sixbell Nekotec Solutions")
SET(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_CURRENT_SOURCE_DIR}/README")
SET(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/COPYING")

SET(CPACK_PACKAGE_FILE_NAME "MR${CMAKE_DEBUG_POSTFIX}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}-Linux-EL4-i686")
SET(CPACK_PACKAGE_INSTALL_DIRECTORY "sixlabs/bin/MRconsole${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")

IF(WIN32 AND NOT UNIX)
  SET(CPACK_GENERATOR NSIS)     # can be NSIS, STGZ, TBZ2, TGZ, TZ and ZIP
  SET(CPACK_NSIS_COMPRESSOR "/SOLID lzma")
  # There is a bug in NSI that does not handle full unix paths properly. Make
  # sure there is at least one set of four (4) backlasshes.
  SET(CPACK_PACKAGE_ICON "${CMAKE_SOURCE_DIR}/install_win32\\\\mr.bmp")
  SET(CPACK_NSIS_INSTALLED_ICON_NAME "MRConsole.exe")
  SET(CPACK_NSIS_DISPLAY_NAME "${CPACK_PACKAGE_INSTALL_DIRECTORY}")
  SET(CPACK_NSIS_HELP_LINK "http:\\\\\\\\www.sixbell.cl")
  SET(CPACK_NSIS_URL_INFO_ABOUT "http:\\\\\\\\www.sixbell.cl")
  SET(CPACK_NSIS_CONTACT "leonardo.bolanos@nekotectelecom.com")

  SET(CPACK_SOURCE_GENERATOR "NSIS")
  SET(CPACK_SOURCE_IGNORE_FILES "/\\\\.git/")
ELSE(WIN32 AND NOT UNIX)
  SET(CPACK_GENERATOR TBZ2)     # can be STGZ, TBZ2, TGZ, TZ and ZIP

  SET(CPACK_SOURCE_GENERATOR "TBZ2;TGZ")
  SET(CPACK_SOURCE_IGNORE_FILES "/install_win32/" "/sim/win32/" "/\\\\.git/")
ENDIF(WIN32 AND NOT UNIX)
SET(CPACK_PACKAGE_EXECUTABLES "MRConsole" "MRConsole")

INCLUDE(CPack)
