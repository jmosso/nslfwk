# - Find AMQ
# Find the native AMQ headers and libraries.
#
#  AMQ_INCLUDE_DIR - where to find cms/Session.h , etc.
#  AMQ_LIBRARY    - List of libraries when using AMQ.
#  AMQ_FOUND        - True if AMQ found.

# Look for the header file.

MESSAGE(STATUS "AMQ ------------------------------------------------")

FIND_PATH(AMQ_ROOT_DIR cms/Session.h PATHS
				/usr/include/activemq-cpp
				/usr/include/activemq-cpp-3.8.2
				 $ENV{AMQ_ROOT}/include
				 $ENV{AMQ_ROOT}/include/*/*
				 $ENV{AMQ_ROOT}
				 $ENV{AMQ_ROOT}/*/*
				${FRAMEWORK_ROOT}
				${FRAMEWORK_ROOT}/*
				${FRAMEWORK_ROOT}/*/*
				${FRAMEWORK_ROOT}/*/*/*
				${FRAMEWORK_ROOT}/*/*/*/*
				${FRAMEWORK_ROOT}/../*
				${FRAMEWORK_ROOT}/../*/*
				${FRAMEWORK_ROOT}/../*/*/*
				${FRAMEWORK_ROOT}/../*/*/*/*
				${FRAMEWORK_ROOT}/../../*
				${FRAMEWORK_ROOT}/../../*/*
				${FRAMEWORK_ROOT}/../../*/*/*
				${FRAMEWORK_ROOT}/../../*/*/*/*
				 /usr/local/include
				 /usr/local/include/*
				 /usr/local/include/*/*/*
				 /usr/include
				 /usr/include/*/*/*
				 /usr/*/*/*/*
				 DOC "directory containing src/main/cms/*.h for activemq-cpp  library")

MARK_AS_ADVANCED( AMQ_ROOT_DIR )

IF( WIN32 )
	set (AMQCLIENT_LIB libactivemq-cpp)
	IF (DEBUG_COMPILATION)
		SET(SN_DEBUG_POSTFIX "")
		set (AMQCLIENT_LIB libactivemq-cppd)
	ELSE (DEBUG_COMPILATION)	
		SET(SN_DEBUG_POSTFIX "d")
	ENDIF (DEBUG_COMPILATION)
ELSE ( WIN32 )
	set (AMQCLIENT_LIB activemq-cpp) 
ENDIF( WIN32 )

# Look for the library.
IF (DEBUG_COMPILATION)
FIND_LIBRARY(AMQ_LIBRARY NAMES ${AMQCLIENT_LIB} PATHS 
										${AMQ_ROOT_DIR}/../../vs2005-build/Debug
										${AMQ_ROOT_DIR}/../../src/main/.libs
										${AMQ_ROOT_DIR}/../../lib
										$ENV{AMQ_ROOT}/vs2005-build/Debug
										$ENV{AMQ_ROOT}/src/main/.libs
										${CMAKE_SOURCE_DIR}/../*
										/usr/lib/*/*
										/usr/local/lib/*/*
										$ENV{AMQ_ROOT}/lib/*/*
										$ENV{AMQ_ROOT}/*/*
										DOC "AMQ Debug library file")
ELSE (DEBUG_COMPILATION)
FIND_LIBRARY(AMQ_LIBRARY NAMES ${AMQCLIENT_LIB} PATHS 
										${AMQ_ROOT_DIR}/../../vs2005-build/Release
										${AMQ_ROOT_DIR}/../../src/main/.libs
										${AMQ_ROOT_DIR}/../../lib
										$ENV{AMQ_ROOT}/debug
										$ENV{AMQ_ROOT}/src/main/.libs
										$ENV{AMQ_ROOT}/lib/*
										$ENV{AMQ_ROOT}/*
										${CMAKE_SOURCE_DIR}/../*
										/usr/lib/*
										/usr/local/lib/*
										DOC "AMQ library file")
										
ENDIF (DEBUG_COMPILATION)

	 
IF (WIN32 AND NOT CYGWIN)
	FIND_LIBRARY(AMQ_DEBUG_LIBRARY NAMES ${AMQCLIENT_LIB}${SN_DEBUG_POSTFIX}	PATHS
										${AMQ_ROOT_DIR}/../../vs2005-build/Debug
										${AMQ_ROOT_DIR}/../../src/main/.libs
										${AMQ_ROOT_DIR}/../../lib
										$ENV{AMQ_ROOT}/vs2005-build/Debug
										$ENV{AMQ_ROOT}/src/main/.libs
										${CMAKE_SOURCE_DIR}/../*
										/usr/lib/*
										/usr/local/lib/*
										$ENV{AMQ_ROOT}/lib/*
										$ENV{AMQ_ROOT}/*
		DOC "AMQ library file (debug version)")
ENDIF (WIN32 AND NOT CYGWIN)

IF ( AMQ_DEBUG_LIBRARY AND NOT AMQ_LIBRARY )
	SET(AMQ_LIBRARY ${AMQ_DEBUG_LIBRARY})
ENDIF  ( AMQ_DEBUG_LIBRARY AND NOT AMQ_LIBRARY )

MARK_AS_ADVANCED( AMQ_LIBRARY )

########################################################################
## OS-specific extra linkage

# Solaris needs some extra libraries that may not have been found already
IF(CMAKE_SYSTEM_NAME STREQUAL "SunOS")
  MESSAGE(STATUS "need to link solaris-specific libraries")
  #  LINK_LIBRARIES(socket rt)
  #SET(AMQ_LIBRARY ${AMQ_LIBRARY})
ENDIF(CMAKE_SYSTEM_NAME STREQUAL "SunOS")

# Windows needs some extra libraries
IF (WIN32 AND NOT CYGWIN)
  MESSAGE(STATUS "need to link windows-specific libraries")
  # if AMQ found, add winmm dependency
  IF(AMQ_LIBRARY)
    SET(AMQ_LIBRARY ${AMQ_LIBRARY} ws2_32
		rpcrt4)
  ENDIF(AMQ_LIBRARY)
ENDIF (WIN32 AND NOT CYGWIN)

IF (MINGW)
  MESSAGE(STATUS "need to link windows-specific libraries")
  #LINK_LIBRARIES(winmm wsock32)
  SET(AMQ_LIBRARY ${AMQ_LIBRARY} rpcrt4 wsock32)
ENDIF(MINGW)

# Copy the results to the output variables.
IF(AMQ_ROOT_DIR AND AMQ_LIBRARY)
  SET(AMQ_FOUND 1)
  SET(AMQ_INCLUDE_DIR ${AMQ_ROOT_DIR})
  SET(AMQ_INCLUDE_DIRS ${AMQ_INCLUDE_DIR} )
 ELSE(AMQ_ROOT_DIR AND AMQ_LIBRARY)
  SET(AMQ_FOUND 0)
  SET(AMQ_LIBRARIES)
  SET(AMQ_INCLUDE_DIRS)
ENDIF(AMQ_ROOT_DIR AND AMQ_LIBRARY)

IF (AMQ_DEBUG_LIBRARY)
	SET(AMQ_DEBUG_FOUND TRUE)
ELSE(AMQ_DEBUG_LIBRARY)
	SET(AMQ_DEBUG_LIBRARY ${AMQ_LIBRARY})
ENDIF (AMQ_DEBUG_LIBRARY)

IF (AMQ_FOUND)
	IF (NOT AMQ_FIND_QUIETLY)
		MESSAGE(STATUS "Found AMQ_LIBRARY: ${AMQ_LIBRARY}")	
		MESSAGE(STATUS "Found AMQ_INCLUDE_DIR: ${AMQ_INCLUDE_DIR}")
		MESSAGE(STATUS "Found AMQ_DEBUG_LIBRARY: ${AMQ_DEBUG_LIBRARY}")	
	ENDIF (NOT AMQ_FIND_QUIETLY)
ELSE (AMQ_FOUND)
	IF (AMQ_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find AMQ")
	ENDIF (AMQ_FIND_REQUIRED)
ENDIF (AMQ_FOUND)

# Report the results.
IF(NOT AMQ_FOUND)
  SET(AMQ_DIR_MESSAGE
    "AMQ was not found. Make sure AMQ is compiled and installed AMQ_ROOT_DIR=${AMQ_ROOT_DIR}  AMQ_LIBRARY=${AMQ_LIBRARY}.")
  IF(NOT AMQ_FIND_QUIETLY)
    MESSAGE(STATUS "${AMQ_DIR_MESSAGE}")
  ELSE(NOT AMQ_FIND_QUIETLY)
    IF(AMQ_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "${AMQ_DIR_MESSAGE}")
    ENDIF(AMQ_FIND_REQUIRED)
  ENDIF(NOT AMQ_FIND_QUIETLY)
ENDIF(NOT AMQ_FOUND)
