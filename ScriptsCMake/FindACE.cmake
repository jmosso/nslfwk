# - Find ACE
# Find the native ACE headers and libraries.
#
#  ACE_INCLUDE_DIRS - where to find ACE/ACE.h, etc.
#  ACE_LIBRARIES    - List of libraries when using ACE.
#  ACE_FOUND        - True if ACE found.

# Look for the header file.

MESSAGE(STATUS "ACE ------------------------------------------------")

FIND_PATH(ACE_INCLUDE_DIR ace/ACE.h 
				 $ENV{ACE_ROOT}
				 $ENV{ACE_ROOT}/*
				 $ENV{ACE_ROOT}/include
				 $ENV{ACE_ROOT}/include/*
				 ${FRAMEWORK_ROOT}
				 ${FRAMEWORK_ROOT}/*/*
				 ${FRAMEWORK_ROOT}/../*/*
				 ${FRAMEWORK_ROOT}/../../*/*
 				 ${FRAMEWORK_ROOT}/../../common-libs/ACE_wrappers
 				 ${FRAMEWORK_ROOT}/../common-libs/ACE_wrappers
				 ${CMAKE_SOURCE_DIR}/../*/*
				 /usr/local/include/*/*
				 /usr/include/*/*
				 /usr/*/*/*
				 DOC "directory containing ace/*.h for ACE library")
				 
MARK_AS_ADVANCED(ACE_INCLUDE_DIR)

# Look for the library.
IF (DEBUG_COMPILATION)
	FIND_LIBRARY(ACE_LIBRARY NAMES ACEd aced ACE ace PATHS 
					$ENV{ACE_ROOT}/lib
					${ACE_INCLUDE_DIR}/lib
					${CMAKE_SOURCE_DIR}/../*
					/usr/lib/*/*/*
					/usr/local/lib/*/*/*
					$ENV{ACE_ROOT}/lib/*/*
					$ENV{ACE_ROOT}/*/*
					DOC "ACE library file")
ELSE (DEBUG_COMPILATION)
	FIND_LIBRARY(ACE_LIBRARY NAMES ACE ace PATHS 
						$ENV{ACE_ROOT}/lib
						${ACE_INCLUDE_DIR}/lib
						${CMAKE_SOURCE_DIR}/../*
						/usr/lib/*/*
						/usr/local/lib/*/*
						$ENV{ACE_ROOT}/lib/*
						$ENV{ACE_ROOT}/*
						DOC "ACE library file")
ENDIF (DEBUG_COMPILATION)

IF (WIN32 AND NOT CYGWIN)
	SET(SN_DEBUG_POSTFIX "d")
	FIND_LIBRARY(ACE_DEBUG_LIBRARY NAMES ACE${SN_DEBUG_POSTFIX} ace${SN_DEBUG_POSTFIX} PATHS 
									$ENV{ACE_ROOT}/lib
									${ACE_INCLUDE_DIR}/lib
									${CMAKE_SOURCE_DIR}/../*
									/usr/lib/*/*/*
									/usr/local/lib/*/*/*
									$ENV{ACE_ROOT}/lib/*/*
									$ENV{ACE_ROOT}/*/*
									DOC "ACE library file (debug version)")
ENDIF (WIN32 AND NOT CYGWIN)

IF ( ACE_DEBUG_LIBRARY AND NOT ACE_LIBRARY )
	SET(ACE_LIBRARY ${ACE_DEBUG_LIBRARY})
ENDIF  ( ACE_DEBUG_LIBRARY AND NOT ACE_LIBRARY )

MARK_AS_ADVANCED(ACE_LIBRARY)

########################################################################
## OS-specific extra linkage

# Solaris needs some extra libraries that may not have been found already
IF(CMAKE_SYSTEM_NAME STREQUAL "SunOS")
  MESSAGE(STATUS "need to link solaris-specific libraries")
  #  LINK_LIBRARIES(socket rt)
  SET(ACE_LIBRARY ${ACE_LIBRARY} socket rt nsl)
ENDIF(CMAKE_SYSTEM_NAME STREQUAL "SunOS")

# Windows needs some extra libraries
IF (WIN32 AND NOT CYGWIN)
  MESSAGE(STATUS "need to link windows-specific libraries")
  # if ACE found, add winmm dependency
  IF(ACE_LIBRARY)
    SET(ACE_LIBRARY ${ACE_LIBRARY} winmm)
  ENDIF(ACE_LIBRARY)
ENDIF (WIN32 AND NOT CYGWIN)

IF (MINGW)
  MESSAGE(STATUS "need to link windows-specific libraries")
  #LINK_LIBRARIES(winmm wsock32)
  SET(ACE_LIBRARY ${ACE_LIBRARY} winmm wsock32)
ENDIF(MINGW)

# Copy the results to the output variables.
IF(ACE_INCLUDE_DIR AND ACE_LIBRARY)
  SET(ACE_FOUND 1)
  SET(ACE_LIBRARIES ${ACE_INCLUDE_DIR}/lib)
  SET(ACE_INCLUDE_DIRS ${ACE_INCLUDE_DIR})
ELSE(ACE_INCLUDE_DIR AND ACE_LIBRARY)
  SET(ACE_FOUND 0)
  SET(ACE_LIBRARIES)
  SET(ACE_INCLUDE_DIRS)
ENDIF(ACE_INCLUDE_DIR AND ACE_LIBRARY)

IF (ACE_DEBUG_LIBRARY)
	SET(ACE_DEBUG_FOUND TRUE)
ELSE(ACE_DEBUG_LIBRARY)
	SET(ACE_DEBUG_LIBRARY ${ACE_LIBRARY})
ENDIF (ACE_DEBUG_LIBRARY)

IF (ACE_FOUND)
	IF (NOT Ace_FIND_QUIETLY)
		MESSAGE(STATUS "Found ACE_LIBRARY: ${ACE_LIBRARY}")
		MESSAGE(STATUS "Found ACE_INCLUDE_DIR: ${ACE_INCLUDE_DIR}")
		MESSAGE(STATUS "Found ACE_LIBRARIES: ${ACE_LIBRARIES}")
		MESSAGE(STATUS "Found ACE_DEBUG_LIBRARY: ${ACE_DEBUG_LIBRARY}")	
	ENDIF (NOT Ace_FIND_QUIETLY)
ELSE (ACE_FOUND)
	IF (Ace_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find ACE")
	ENDIF (Ace_FIND_REQUIRED)
ENDIF (ACE_FOUND)

# Report the results.
IF(NOT ACE_FOUND)
  SET(ACE_DIR_MESSAGE
    "ACE was not found. Make sure is compiled and installed. ACE_INCLUDE_DIR=${ACE_INCLUDE_DIR}  ACE_LIBRARY=${ACE_LIBRARY}")
  IF(NOT ACE_FIND_QUIETLY)
    MESSAGE(STATUS "${ACE_DIR_MESSAGE}")
  ELSE(NOT ACE_FIND_QUIETLY)
    IF(ACE_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "${ACE_DIR_MESSAGE}")
    ENDIF(ACE_FIND_REQUIRED)
  ENDIF(NOT ACE_FIND_QUIETLY)
ENDIF(NOT ACE_FOUND)
