MACRO( SIXVERSION libname )

  #Get version from directory
  EXEC_PROGRAM( "echo ${CMAKE_CURRENT_SOURCE_DIR} | awk -F '/${libname}/' '{print $2}'" OUTPUT_VARIABLE VERSION_DIR )

  EXEC_PROGRAM( "echo ${VERSION_DIR} | awk -F/ '{print $2}'" OUTPUT_VARIABLE VERSION_NUMBER )


  IF(NOT ${VERSION_NUMBER} )
    SET( VERSION_NUMBER ".")
  ENDIF(NOT ${VERSION_NUMBER} )

  EXEC_PROGRAM( "echo ${VERSION_NUMBER} | awk -F. '{print $1}'" OUTPUT_VARIABLE MAJOR_VERSION )
  EXEC_PROGRAM( "echo ${VERSION_NUMBER} | awk -F. '{print $2}'" OUTPUT_VARIABLE MINOR_VERSION )
  EXEC_PROGRAM( "echo ${VERSION_NUMBER} | awk -F. '{print $3}'" OUTPUT_VARIABLE RELEASE_VERSION )


  IF( ${RELEASE_VERSION} GREATER -1 )
    MESSAGE( "")
    MESSAGE( "\t========================")
    MESSAGE( "\t  MAJOR_VERSION   : " ${MAJOR_VERSION})
    MESSAGE( "\t  MINOR_VERSION   : " ${MINOR_VERSION})
    MESSAGE( "\t  RELEASE_VERSION : " ${RELEASE_VERSION})
    MESSAGE( "\t========================")
    MESSAGE( "")

    SET( TARGET_VERSION  "${MAJOR_VERSION}.${MINOR_VERSION}.${RELEASE_VERSION}" )
  
  ELSEIF( ${MINOR_VERSION} GREATER -1 )
    MESSAGE( "")
    MESSAGE( "\t========================")
    MESSAGE( "\t  MAJOR_VERSION : " ${MAJOR_VERSION})
    MESSAGE( "\t  MINOR_VERSION : " ${MINOR_VERSION})
    MESSAGE( "\t========================")
    MESSAGE( "")

    SET( TARGET_VERSION  "${MAJOR_VERSION}.${MINOR_VERSION}" )
  
  ELSE( ${RELEASE_VERSION} GREATER -1 )
    MESSAGE( "")
    MESSAGE( "\t========================")
    MESSAGE( "\t  MAJOR_VERSION   : 0" )
    MESSAGE( "\t  MINOR_VERSION   : 0")
    MESSAGE( "\t  RELEASE_VERSION : 0" )
    MESSAGE( "\t========================")
    MESSAGE( "")

    SET( TARGET_VERSION "0.0.0" )
    SET( MAJOR_VERSION "0" )

  ENDIF( ${RELEASE_VERSION} GREATER -1 )

  SET_TARGET_PROPERTIES( ${LIBNAME} PROPERTIES VERSION ${TARGET_VERSION} SOVERSION ${MAJOR_VERSION} )

ENDMACRO( SIXVERSION )
