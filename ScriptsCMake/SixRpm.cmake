#
# Sixlabs CMake macro definitions for RPM building.
#

MACRO( SIX_CREATE_RPM rpm_name
                      cmake_args
                      rpm_bits )

   SET( RPM_CMAKE_ARGS ${cmake_args} )

   IF( ${rpm_bits} STREQUAL "64" )
      SET( RPM_PROCESSOR_BITS "x86_64" )
      SET( RPM_LIB_DIRECTORY_NAME "lib64" )
   ELSE( ${rpm_bits} STREQUAL "64" )
      SET( RPM_PROCESSOR_BITS "i386" )
      SET( RPM_LIB_DIRECTORY_NAME "lib" )
   ENDIF( ${rpm_bits} STREQUAL "64" )

   SET( LIB ${RPM_LIB_DIRECTORY_NAME} )

   CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/rpm/SpecTemplate.spec
                   ${PROJECT_BINARY_DIR}/spec/${rpm_name}.spec
                   @ONLY 
		   @LIB@
                   @RPM_PROCESSOR_BITS@ )

   ADD_CUSTOM_TARGET( rpm
                      # Making directories
                      COMMAND mkdir -p ${PROJECT_BINARY_DIR}/RPM/{BUILD,RPMS,S{OURCE,PEC,RPM}S}
                      COMMAND rm -rf ${PROJECT_BINARY_DIR}/RPM/SOURCES/${RPM_NAME}
                      COMMAND mkdir -p ${PROJECT_BINARY_DIR}/RPM/SOURCES/${RPM_NAME}
                      # Copying necesary files to create RPM
                      COMMAND cp -r
                              ${SOURCE_BASE_DIRCTORIES}
                              ${PROJECT_BINARY_DIR}/RPM/SOURCES/${RPM_NAME}
                      COMMAND cp
                              ${PROJECT_BINARY_DIR}/spec/${RPM_NAME}.spec
                              ${PROJECT_BINARY_DIR}/RPM/SPECS
                      # Crating RPM file
                      COMMAND rpmbuild --define '_topdir ${PROJECT_BINARY_DIR}/RPM'
                              -ba RPM/SPECS/${RPM_NAME}.spec )

ENDMACRO( SIX_CREATE_RPM )

