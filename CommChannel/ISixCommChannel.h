#pragma once

class CDynamicStruct;

class ISixCommChannelEvent
{
public:
	virtual void ProcessCommChannelEvent(CDynamicStruct* pDynEvent) = 0;
};

class ISixCommChannel
{
public:
	ISixCommChannel(){m_pCallback = 0;}
	virtual ~ISixCommChannel(){};

	enum CHANNEL_TYPE
	{
		SIX_CHANNEL_AMQ,
		SIX_CHANNEL_UNIXQUEUE,
		SIX_CHANNEL_ZMQ,
		SIX_CHANNEL_ZMQ_MANAGER,
		SIX_CHANNEL_ZMQ_SENDER,
		SIX_CHANNEL_RAW_SOCKET,
	};

	virtual void SetEventCallback(ISixCommChannelEvent* p){m_pCallback = p;}
	virtual bool Start(CDynamicStruct* pDynStartInfo) = 0;
	virtual bool ReleaseChannel() = 0;
	virtual bool SendChannelMessage(CDynamicStruct* pDynData) = 0;

	CHANNEL_TYPE m_eChannelType;

protected:
	ISixCommChannelEvent* m_pCallback;
};
