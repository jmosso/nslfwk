
#include "SixChannelUnixQ.h"
#include "SixCommChannelModule.h"
#include "TimerModule/TimerFactory.h"
#include "ThreadModule/ThreadFactory.h"
#include "FrameworkUtilities/DynamicStruct.h"
#include "SmallConfiguration/SmallConfiguration.h"
#include "Utilities/Tracing.h"

#include "SGenericParamFullNames.hpp"
#include <paramsCodec.hpp>

NK_BEGIN_MESSAGE_MAP(CSixChannelUnixQ, IThreadMessageCB)
	ON_NKMESSAGE(MSG_START, ThrStart);
	ON_NKMESSAGE(MSG_TIME_OUT, ThrTimeOut);
	ON_NKMESSAGE_POINTER(MSG_SEND_CHANNEL_MESSAGE, ThrSendMessage);
	ON_NKMESSAGE_DYN(MSG_ON_MESSAGE, ThrOnMessage);
NK_END_MESSAGE_MAP()

extern const char * sSTATE[];

int CSixChannelUnixQ::m_iTimerReconnect = 10000;
void CSixChannelUnixQ::Initialize()
{
	m_iTimerReconnect = 10000;
	VARIABLEINT(m_iTimerReconnect, "Configuracion", "BusTimeMonitor");
}

CSixChannelUnixQ::CSixChannelUnixQ(void)
{
	Clean();
}

CSixChannelUnixQ::~CSixChannelUnixQ(void)
{

}

void CSixChannelUnixQ::Clean()
{
	m_fstrGroupName.Clear();
	m_fstrUniqueName.Clear();
	m_fstrDescription.Clear();
	m_iModuleType = -1;
	m_eSTATE = STATE_NONE;
}

bool CSixChannelUnixQ::Start(CDynamicStruct* pDynStartInfo)
{
	pDynStartInfo->GetString("NAME_GROUP", m_fstrGroupName);
	pDynStartInfo->GetString("NAME_UNIQUE", m_fstrUniqueName);
	pDynStartInfo->GetInt("MODULE_TYPE", m_iModuleType);
	pDynStartInfo->GetString("DESCRIPTION", m_fstrDescription);

	m_pThreadTx = CThreadFactory::St_GetInstance()->GetResource();
	m_pThreadTx->SetThreadName(CFwkString(1, "CSixChannelUnixQTx<0x%x>", this));
	m_pThreadTx->Start();

	m_pThreadRx = CThreadFactory::St_GetInstance()->GetResource();
	m_pThreadRx->SetThreadName(CFwkString(1, "CSixChannelUnixQRx<0x%x>", this));
	m_pThreadRx->Start();

	if(m_fstrGroupName.IsEmpty())
	{
		TRACEPF_ERROR("", "CSixChannelUnixQ::Start<0x%x> Can not create a CommChannel UNIXQUEUE without NAME_GROUP", this);
		return false;
	}

	if(m_fstrGroupName.CheckIsNumeric() == false || m_fstrUniqueName.CheckIsNumeric() == false)
	{
		TRACEPF_ERROR("", "CSixChannelUnixQ::Start<0x%x> Can not create a CommChannel UNIXQUEUE with NON NUMERIC queue name", this);
		return false;
	}



	m_pThreadTx->SendThreadMessage(MSG_START, this);

	m_eSTATE = STATE_OPENING;
	return true;
}

void CSixChannelUnixQ::ThrStart()
{
	try
	{
		CRouterUnixQueue* pConsumer = RegisterConsumer(m_fstrGroupName);
		if (pConsumer == 0)
		{
			TRACEPF_ERROR("", "CSixChannelUnixQ::ThrStart<0x%x> Can not register consumer group: %s", this, (const char*)m_fstrGroupName);
			HandleCxError();
			return;
		}

		if (m_fstrUniqueName.IsEmpty() == false)
		{
			CRouterUnixQueue* pConsumerIdentifier = RegisterConsumer(m_fstrUniqueName);
			if (pConsumerIdentifier == 0)
			{
				TRACEPF_ERROR("", "CSixChannelUnixQ::ThrStart<0x%x> Can not register consumer uniqueName: %s", this, (const char*)m_fstrUniqueName);
				HandleCxError();
				return;
			}
		}

		m_eSTATE = STATE_OPENED;

		TRACEPF_DEBUG("", "CSixChannelUnixQ::ThrStart<0x%x> channel OPENED queues:[%s, %s]", this, 
			(const char*)m_fstrGroupName, (const char*)m_fstrUniqueName);

	}
	catch (...)
	{
		TRACEPF_ERROR("", "CSixChannelAMQ::ThrStart<0x%x> Can not start connection queues:[%s, %s]", this, (const char*)m_fstrGroupName, (const char*)m_fstrUniqueName);
		HandleCxError();
	}

	return;
}

void CSixChannelUnixQ::TimeOut(size_t iEventId, size_t iTimerId)
{
	m_pThreadTx->SendThreadMessage(MSG_TIME_OUT, this);
}

void CSixChannelUnixQ::ThrTimeOut()
{
	TRACEPF_WARNING("", "CSixChannelUnixQ::ThrTimeOut<0x%x> Trying connection with ", this);
	ThrStart();
}

bool CSixChannelUnixQ::ReleaseChannel()
{
	return false;
}

void CSixChannelUnixQ::HandleCxError()
{
	Reset();
	CTimerFactory::St_GetInstance()->SetTimer(0, m_iTimerReconnect, this);
}

void CSixChannelUnixQ::Reset()
{
	TRACEPF_ERROR("", "CSixChannelUnixQ::Reset<0x%x>", this);

	MAPSTR2ROUTERUQ::iterator itConsumer;
	for (itConsumer = m_ConsumerMap.begin(); itConsumer != m_ConsumerMap.end(); itConsumer++ )
	{
		delete itConsumer->second;
	}
	m_ConsumerMap.clear();

	MAPSTR2ROUTERUQ::iterator itProducer;
	for (itProducer = m_ProducerMap.begin(); itProducer != m_ProducerMap.end(); itProducer++ )
	{
		delete itProducer->second;
	}
	m_ProducerMap.clear();

	m_eSTATE = STATE_NONE;
}

bool CSixChannelUnixQ::SendChannelMessage(CDynamicStruct* pDynData)
{
	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelUnixQ::SendChannelMessage<0x%x> dyn<0x%x> thr<0x%x> QueueSize %d", this, pDynData, m_pThreadTx->GetId(), m_pThreadTx->GetQueueSize());
	m_pThreadTx->SendThreadMessage(MSG_SEND_CHANNEL_MESSAGE, this, pDynData);
	return true;
}

void CSixChannelUnixQ::ThrSendMessage(NK_OBJPOINTER pData)
{
	CDynamicStruct* pMsg = (CDynamicStruct*)pData;

	if(m_eSTATE != STATE_OPENED)
	{
		TRACEPF_ERROR("", "CSixChannelUnixQ::ThrSendMessage<0x%x> Channel NOT OPENED... Discarding msg <0x%x>", this, pMsg);
		DYNFACTFREERES(pMsg);
		return;
	}

	CFwkString fstrDestiny;
	pMsg->GetString(_six_kHeader_Dest_Id, fstrDestiny);
	CFwkString fstrResourceId;
	pMsg->GetString(_six_kHeader_ResId, fstrResourceId);

	if(fstrDestiny.IsEmpty())
	{
		TRACEPF_ERROR(fstrResourceId, "CSixChannelUnixQ::ThrSendMessage<0x%x> Destiny empty. Can not send message... Discarding msg <0x%x>", this, pMsg);
		DYNFACTFREERES(pMsg);
		return;
	}

	if (!m_fstrUniqueName.IsEmpty())
		pMsg->AddString(_six_kHeader_Org_Id, m_fstrUniqueName);
	else
		pMsg->AddString(_six_kHeader_Org_Id, m_fstrGroupName);

	pMsg->AddInt(_six_kHeader_Org_Type, m_iModuleType);

	// Esto deberia eliminarse
	if(pMsg->IsParam(_six_kOper_Idx) == false)
		pMsg->AddInt(_six_kOper_Idx, 666);

	CFwkString sMsg;
	try
	{
		ParamsCodecGenericBuffer outputBuffer;
		pMsg->GetParamsCodec()->writeObjectBAS( outputBuffer );
		sMsg.FromBuffer( outputBuffer.buffer, outputBuffer.size );		
	}
	catch (...)
	{
		TRACEPF_ERROR(fstrResourceId, "CSixChannelUnixQ::ThrSendMessage<0x%x> Error writeObjectBAS. Can not send!!!... Discarding msg <0x%x>", this, pMsg);
		DYNFACTFREERES(pMsg);
		return;
	}

	CSixCommChannelModule::PrintMessage(pMsg, true);

	try
	{
		MAPSTR2ROUTERUQ::iterator i = m_ProducerMap.find (fstrDestiny);
		if(i != m_ProducerMap.end())
		{
			i->second->sendMsg(fstrDestiny.GetBuffer(), sMsg.GetBuffer(), sMsg.GetLength() + 1);
		}
		else
		{
			CRouterUnixQueue* pProducer = RegisterProducer(fstrDestiny);
			if (pProducer)
			{
				pProducer->sendMsg(fstrDestiny.GetBuffer(), sMsg.GetBuffer(), sMsg.GetLength() + 1);
				TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelUnixQ::ThrSendMessage<0x%x> dyn<0x%x> sent", this, pMsg);
			}
			else
			{
				TRACEPF_ERROR(fstrResourceId, "CSixChannelUnixQ::ThrSendMessage<0x%x> productor null. Can not send!!!... Discarding msg <0x%x>", this, pMsg);
			}
		}
	}
	catch (...)
	{
		TRACEPF_ERROR(fstrResourceId, "CSixChannelUnixQ::ThrSendMessage<0x%x> AMQ Error. Can not send!!!... Discarding msg <0x%x>", this, pMsg);
	}


	DYNFACTFREERES(pMsg);
}

void CSixChannelUnixQ::process(ParamsCodec *msg) throw( sixlabs::Exception)
{
	CDynamicStruct* pMsg = DYNFACTGETRES;
	pMsg->SetParamsCodec(msg);	
	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelUnixQ::process<0x%x> dyn<0x%x> thr<0x%x> QueueSize %d", this, pMsg, m_pThreadRx->GetId(), m_pThreadRx->GetQueueSize());
	m_pThreadRx->SendThreadMessage(MSG_ON_MESSAGE, this, pMsg);
}

void CSixChannelUnixQ::ThrOnMessage(CDynamicStruct* pMsg)
{
	CSixCommChannelModule::PrintMessage(pMsg, false);

	if(m_pCallback != 0)
		m_pCallback->ProcessCommChannelEvent(pMsg);
	else
	{
		CFwkString fstrResourceId;
		pMsg->GetString(_six_kHeader_ResId, fstrResourceId);
		TRACEPF_ERROR(fstrResourceId, "CSixChannelUnixQ::ThrOnMessage<0x%x> Message <0x%x> detected with NO CALLBACK!!!... Discarding", this, pMsg);
		DYNFACTFREERES(pMsg);
	}
}

CRouterUnixQueue* CSixChannelUnixQ::RegisterConsumer(CFwkString& fstrConsumer)
{
	TRACEPF_DEBUG("",  "CSixChannelUnixQ::RegisterConsumer<0x%x> - Queue %s", this, fstrConsumer.GetBuffer());

	CRouterUnixQueue *unixQueue = 0;

	MAPSTR2ROUTERUQ::iterator it;
	it = m_ConsumerMap.find( fstrConsumer.GetBuffer() );
	if ( it == m_ConsumerMap.end() )
	{
		try
		{
			unixQueue = new CRouterUnixQueue( "UnixQueue", fstrConsumer.AsInteger( 16 ), 1 );
		}
		catch ( ... )
		{
		}
		if ( unixQueue != 0 )
		{
			m_ConsumerMap[fstrConsumer] = unixQueue;
			unixQueue->registerCallBack( this );
		}
	}
	return unixQueue;
}

CRouterUnixQueue* CSixChannelUnixQ::RegisterProducer(CFwkString& fstrProducer)
{
	TRACEPF_DEBUG("",  "CSixChannelUnixQ::RegisterProducer<0x%x> - Queue %s", this, fstrProducer.GetBuffer());

	CRouterUnixQueue *unixQueue = 0;

	if(m_ProducerMap.find(fstrProducer.GetBuffer() ) == m_ProducerMap.end() )
	{
		try
		{
			unixQueue = new CRouterUnixQueue( "UnixQueue", fstrProducer.AsInteger( 16 ), 1, false );
		}
		catch ( ... )
		{
		}
		if ( unixQueue != 0 )
		{
			m_ProducerMap[fstrProducer] = unixQueue;
		}
	}

	return unixQueue;
}
