
#include "SixCommChannelModule.h"
#include "SixChannelAMQ.h"
#include "SixChannelUnixQ.h"
#include "SixChannelZMQ.h"

#include "SmallConfiguration/SmallConfiguration.h"
#include "FrameworkUtilities/DynamicStruct.h"
#include "Utilities/Tracing.h"
#include "SGenericParamFullNames.hpp"
#include "Templates/TFactory.h"

class CSixCommChannelModule::CCommChannelFactoriesDP
{
public:
	CCommChannelFactoriesDP(){};
	~CCommChannelFactoriesDP(){};

	 TFactory<CSixChannelAMQ, ISixCommChannel> ms_factChannelAMQ;
	 TFactory<CSixChannelUnixQ, ISixCommChannel> ms_factChannelUnixQ;
	 TFactory<CSixChannelZMQ, ISixCommChannel> ms_factChannelZMQ;
	 TFactory<CSixChannelZMQManager, ISixCommChannel> ms_factChannelZMQMan;
	 TFactory<CSixChannelZMQSNDR, ISixCommChannel> ms_factChannelZMQSender;
};

bool CSixCommChannelModule::ms_bPrintMessageHeader = false;

CSixCommChannelModule::CSixCommChannelModule(): m_factoriesDP(new CCommChannelFactoriesDP)
{
}

CSixCommChannelModule::~CSixCommChannelModule()
{
}

int CSixCommChannelModule::Init()
{
	ms_bPrintMessageHeader = false;
	VARIABLEBOOL(ms_bPrintMessageHeader, "TRACE", "PrintMessageHeader");

	CSixChannelAMQ::Initialize();
	CSixChannelUnixQ::Initialize();
	CSixChannelZMQ::Initialize();
	CSixChannelZMQManager::Initialize();
	CSixChannelZMQSNDR::Initialize();
	return 0;
}

int CSixCommChannelModule::Exit()
{

	return 0;
}

ISixCommChannel* CSixCommChannelModule::GetCommChannel(const char* strCxURI, const char* strGroup, const char* strIdentifier)
{
	CFwkString fstrURL = strCxURI;
	CFwkString fstrGroup = strGroup;
	CFwkString fstrIdentifier = strIdentifier;

	ISixCommChannel::CHANNEL_TYPE eChannelType = ISixCommChannel::SIX_CHANNEL_AMQ;

	size_t iIndex = fstrURL.Find("ZEROMQ");
	if(iIndex != 0) // AMQ o COLA UNIX
	{
		if(fstrGroup.CheckIsNumeric() && fstrIdentifier.CheckIsNumeric())
			eChannelType = ISixCommChannel::SIX_CHANNEL_UNIXQUEUE;
		else
			eChannelType = ISixCommChannel::SIX_CHANNEL_AMQ;
	}
	else
	{
		CFwkString fstrMode = fstrURL.Left(7);
		if(fstrMode == "ZEROMQ&" || fstrMode == "ZEROMQW") // compatibilidad para version original ZEROMQ&. Los valores son ahora ZEROMQW, ZEROMQM y ZEROMQS
			eChannelType = ISixCommChannel::SIX_CHANNEL_ZMQ;
		else if (fstrMode == "ZEROMQM")
			eChannelType = ISixCommChannel::SIX_CHANNEL_ZMQ_MANAGER;
		else if(fstrMode == "ZEROMQS")
			eChannelType = ISixCommChannel::SIX_CHANNEL_ZMQ_SENDER;
		else
		{
			TRACEPF_WARNING("", "CSixCommChannelModule::GetCommChannel UNKNOWN ZMQ channel type [%s]. Creating Manager by default!!!", fstrMode.GetBuffer());
			eChannelType = ISixCommChannel::SIX_CHANNEL_ZMQ_MANAGER;
		}
	}

	return (GetCommChannel(eChannelType));
}

ISixCommChannel* CSixCommChannelModule::GetCommChannel(ISixCommChannel::CHANNEL_TYPE eChannelType)
{
	ISixCommChannel* pChannel = 0;
	switch(eChannelType)
	{
		case ISixCommChannel::SIX_CHANNEL_AMQ:
			pChannel = m_factoriesDP->ms_factChannelAMQ.GetResource();
		break;

		case ISixCommChannel::SIX_CHANNEL_UNIXQUEUE:
			pChannel = m_factoriesDP->ms_factChannelUnixQ.GetResource();
		break;

		case ISixCommChannel::SIX_CHANNEL_ZMQ:
			pChannel = m_factoriesDP->ms_factChannelZMQ.GetResource();
		break;

		case ISixCommChannel::SIX_CHANNEL_ZMQ_MANAGER:
			pChannel = m_factoriesDP->ms_factChannelZMQMan.GetResource();
		break;

		case ISixCommChannel::SIX_CHANNEL_ZMQ_SENDER:
			pChannel = m_factoriesDP->ms_factChannelZMQSender.GetResource();
		break;

		case ISixCommChannel::SIX_CHANNEL_RAW_SOCKET:
		default:
			return 0;
	}

	pChannel->m_eChannelType = eChannelType;
	return pChannel;
}

void CSixCommChannelModule::FreeCommChannel(ISixCommChannel* pChannel)
{
	switch(pChannel->m_eChannelType)
	{
		case ISixCommChannel::SIX_CHANNEL_AMQ:
			m_factoriesDP->ms_factChannelAMQ.FreeResource(pChannel);
		break;

		case ISixCommChannel::SIX_CHANNEL_UNIXQUEUE:
			m_factoriesDP->ms_factChannelUnixQ.FreeResource(pChannel);
		break;

		case ISixCommChannel::SIX_CHANNEL_ZMQ:
			m_factoriesDP->ms_factChannelZMQ.FreeResource(pChannel);
		break;

		case ISixCommChannel::SIX_CHANNEL_ZMQ_MANAGER:
			m_factoriesDP->ms_factChannelZMQMan.FreeResource(pChannel);
		break;

		case ISixCommChannel::SIX_CHANNEL_ZMQ_SENDER:
			m_factoriesDP->ms_factChannelZMQSender.FreeResource(pChannel);
		break;

		case ISixCommChannel::SIX_CHANNEL_RAW_SOCKET:
		default:
			return;
	}
}

void CSixCommChannelModule::PrintMessage(CDynamicStruct* pMsg, bool bSend)
{
	if (CTracing::CanTrace(MASK_FWK_BASIC, TRACE_DEBUG) == true)
	{
		CFwkString fstrResourceId;
		int iDir = 0;
		if(!pMsg->GetInt(DRIVER_DIRECTION, iDir))
			pMsg->GetString(_six_kHeader_ResId, fstrResourceId);
		else
		{
			CFwkString fstrValue;
			pMsg->GetString(_six_kHeader_ResId, fstrValue);
			fstrResourceId.sprintf("%s(%d)", fstrValue.GetBuffer(), iDir);
		}
		int usMessage  = -1;
		pMsg->GetInt(_six_kOper_Type, usMessage);
		CFwkString fstrModule;
		pMsg->GetString(_six_kHeader_Dest_Id, fstrModule);

		int iSubType = 0;
		CFwkString fstrVal;
		if(pMsg->GetInt(_six_kOper_SubType, iSubType) == false)
			fstrVal.sprintf(!bSend?"<--\t%d\t%s\t%s":"-->\t%d\t%s\t%s", usMessage, fstrResourceId.GetBuffer(), fstrModule.GetBuffer());
		else
			fstrVal.sprintf(!bSend?"<--\t%d\t%s\t%s\t%d":"-->\t%d\t%s\t%s\t%d", usMessage, fstrResourceId.GetBuffer(), fstrModule.GetBuffer(), iSubType);

		pMsg->Print(fstrVal, MASK_FWK_BASIC, TRACE_DEBUG);
	}
	else
	{
		if(ms_bPrintMessageHeader)
		{
			CFwkString fstrResourceId;
			int iDir = 0;
			if(!pMsg->GetInt(DRIVER_DIRECTION, iDir))
				pMsg->GetString(_six_kHeader_ResId, fstrResourceId);
			else
			{
				CFwkString fstrValue;
				pMsg->GetString(_six_kHeader_ResId, fstrValue);
				fstrResourceId.sprintf("%s(%d)", fstrValue.GetBuffer(), iDir);
			}
			int usMessage  = -1;
			pMsg->GetInt(_six_kOper_Type, usMessage);
			CFwkString fstrModule;
			pMsg->GetString(_six_kHeader_Dest_Id, fstrModule);

			int iSubType = 0;
			CFwkString fstrVal;
			if(pMsg->GetInt(_six_kOper_SubType, iSubType) == false)
				fstrVal.sprintf(!bSend?"<--\t%d\t%s\t%s":"-->\t%d\t%s\t%s", usMessage, fstrResourceId.GetBuffer(), fstrModule.GetBuffer());
			else
				fstrVal.sprintf(!bSend?"<--\t%d\t%s\t%s\t%d":"-->\t%d\t%s\t%s\t%d", usMessage, fstrResourceId.GetBuffer(), fstrModule.GetBuffer(), iSubType);
			
			CFwkString fstrObjInfo;
			fstrObjInfo.sprintf("<0x%x>", pMsg);

			TRACEPF_ALWAYS(fstrObjInfo.GetBuffer(),  fstrVal.GetBuffer());
		}
	}
}