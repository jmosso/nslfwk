
#include "SixChannelZMQ.h"
#include "SixCommChannelModule.h"
#include "TimerModule/TimerFactory.h"
#include "ThreadModule/ThreadFactory.h"
#include "FrameworkUtilities/DynamicStruct.h"
#include "SmallConfiguration/SmallConfiguration.h"
#include "Utilities/Tracing.h"

#include "SGenericParamFullNames.hpp"
#include <paramsCodec.hpp>

#ifndef WIN32
#include <CZGlobal.hpp>
#include <CZWorker.hpp>
#include <CZManager.hpp>
#include <CZThreadSender.hpp>
#include <CZThreadReceiver.hpp>
#include <CZCallback.hpp>
#endif

NK_BEGIN_MESSAGE_MAP(CSixChannelZMQ, IThreadMessageCB)
	ON_NKMESSAGE(MSG_START, ThrStart);
	ON_NKMESSAGE_POINTER(MSG_SEND_CHANNEL_MESSAGE, ThrSendMessage);
	ON_NKMESSAGE_DYN(MSG_ON_MESSAGE, ThrOnMessage);
NK_END_MESSAGE_MAP()

void CSixChannelZMQ::Initialize()
{
	CZGlobal::initialize(10);
}

CSixChannelZMQ::CSixChannelZMQ(void)
{
	m_pZMQWorker = 0;
	Clean();
}

CSixChannelZMQ::~CSixChannelZMQ(void)
{

}

void CSixChannelZMQ::Clean()
{
	m_fstrURLConnection.Clear();
	m_fstrGroupName.Clear();
	m_fstrUniqueName.Clear();
	m_fstrDescription.Clear();
	m_iModuleType = -1;
}

bool CSixChannelZMQ::Start(CDynamicStruct* pDynStartInfo)
{
	pDynStartInfo->GetString("URL", m_fstrURLConnection);
	pDynStartInfo->GetString("NAME_GROUP", m_fstrGroupName);
	pDynStartInfo->GetString("NAME_UNIQUE", m_fstrUniqueName);
	pDynStartInfo->GetInt("MODULE_TYPE", m_iModuleType);
	pDynStartInfo->GetString("DESCRIPTION", m_fstrDescription);

	m_pThreadTx = CThreadFactory::St_GetInstance()->GetResource();
	m_pThreadTx->SetThreadName(CFwkString(1, "CSixChannelZMQTx<0x%x>", this));
	m_pThreadTx->Start();

	m_pThreadRx = CThreadFactory::St_GetInstance()->GetResource();
	m_pThreadRx->SetThreadName(CFwkString(1, "CSixChannelZMQRx<0x%x>", this));
	m_pThreadRx->Start();

	if(m_fstrURLConnection.IsEmpty())
	{
		TRACEPF_ERROR("", "CSixChannelZMQ::Start<0x%x> Can not create a CommChannel ZMQ without URL", this);
		return false;
	}

	m_pThreadTx->SendThreadMessage(MSG_START, this);
	return true;
}

void CSixChannelZMQ::ThrStart()
{
	TRACEPF_DEBUG("",  "CSixChannelZMQ::ThrStart<0x%x> OPENING channel [%s]", this, m_fstrURLConnection.GetBuffer());

	if(m_pZMQWorker != 0)
	{
		TRACEPF_ERROR("", "CSixChannelZMQ::ThrStart<0x%x> m_pZMQWorker NOT NULL!!! [%s]", this, m_fstrURLConnection.GetBuffer());
		return;
	}

	// ZEROMQ&tcp... | ZEROMQW&tcp... | ZEROMQM&tcp...
	size_t iTypeLimitOffset = m_fstrURLConnection.Find("&") + 1;

	size_t iIndex = m_fstrURLConnection.Find("&", iTypeLimitOffset);
	if(iIndex == J_MAX_INDEX)
	{
		TRACEPF_ERROR("", "CSixChannelZMQ::ThrStart<0x%x> Can not find connection parameters [%s]", this, m_fstrURLConnection.GetBuffer());
		return;
	}

	CFwkString strTargetConn = "";
	CFwkString strLocalConn = "";
	strTargetConn = m_fstrURLConnection.Mid(iTypeLimitOffset, iIndex - iTypeLimitOffset);
	strLocalConn = m_fstrURLConnection.Mid(iIndex + 1, m_fstrURLConnection.GetLength() - iIndex);
	if(m_fstrGroupName.IsEmpty() == true)
		m_fstrGroupName = strLocalConn;

	try
	{
		m_pZMQWorker = new CZWorker();
		m_pZMQWorker->setConfiguration((const char*)strTargetConn, (const char*)strLocalConn);
		m_pZMQWorker->setPrivateCallback(this);
		m_pZMQWorker->setServiceCallback(this);
	}
	catch(std::exception& e)
	{
		//delete(m_pZMQWorker);
		//m_pZMQWorker = 0;
		TRACEPF_ERROR("", "CSixChannelZMQ::ThrStart<0x%x> EXCEPTION starting connection [%s] msg: [%s]", this, m_fstrURLConnection.GetBuffer(), e.what());
		return;
	}

	TRACEPF_DEBUG("", "CSixChannelZMQ::ThrStart<0x%x> Channel OPENED [target: %s  local: %s]", this, strTargetConn.GetBuffer(), strLocalConn.GetBuffer());
	return;
}

bool CSixChannelZMQ::ReleaseChannel()
{
	return false;
}

void CSixChannelZMQ::Reset()
{
	TRACEPF_ERROR("", "CSixChannelZMQ::Reset<0x%x>", this);
}

bool CSixChannelZMQ::SendChannelMessage(CDynamicStruct* pDynData)
{
	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelZMQ::SendChannelMessage<0x%x> dyn<0x%x> thr<0x%x> QueueSize %d", this, pDynData, m_pThreadTx->GetId(), m_pThreadTx->GetQueueSize());
	m_pThreadTx->SendThreadMessage(MSG_SEND_CHANNEL_MESSAGE, this, pDynData);
	return true;
}

void CSixChannelZMQ::ThrSendMessage(NK_OBJPOINTER pData)
{
	CDynamicStruct* pMsg = (CDynamicStruct*)pData;

	if(m_pZMQWorker == 0)
	{
		TRACEPF_ERROR("", "CSixChannelZMQ::ThrSendMessage<0x%x> Channel NOT READY!!!... Discarding msg <0x%x>", this, pMsg);
		DYNFACTFREERES(pMsg);
		return;
	}

	if(m_fstrGroupName.IsEmpty() == false)
		pMsg->AddString(_six_kHeader_Org_Id, m_fstrGroupName);

	// Esto deberia eliminarse
	if(pMsg->IsParam(_six_kOper_Idx) == false)
		pMsg->AddInt(_six_kOper_Idx, 666);

	CSixCommChannelModule::PrintMessage(pMsg, true);

	ParamsCodec* pcToSend = pMsg->ChangeParamsCodec();
	m_pZMQWorker->sendMsg(pcToSend);

	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelZMQ::ThrSendMessage<0x%x> dyn<0x%x> sent", this, pMsg);

	DYNFACTFREERES(pMsg);
}

void CSixChannelZMQ::process(ParamsCodec *msg)
{
	CDynamicStruct* pMsg = DYNFACTGETRES;
	pMsg->SetParamsCodec(msg);

	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS,  "CSixChannelZMQ::process<0x%x> dyn<0x%x> thr<0x%x> QueueSize %d", this, pMsg, m_pThreadRx->GetId(), m_pThreadRx->GetQueueSize());
	m_pThreadRx->SendThreadMessage(MSG_ON_MESSAGE, this, pMsg);
}

void CSixChannelZMQ::ThrOnMessage(CDynamicStruct* pMsg)
{
	CSixCommChannelModule::PrintMessage(pMsg, false);

	if(m_pCallback != 0)
		m_pCallback->ProcessCommChannelEvent(pMsg);
	else
	{
		CFwkString fstrResourceId;
		pMsg->GetString(_six_kHeader_ResId, fstrResourceId);
		TRACEPF_ERROR(fstrResourceId, "CSixChannelZMQ::ThrOnMessage<0x%x> Message <0x%x> detected with NO CALLBACK!!!... Discarding", this, pMsg);
		DYNFACTFREERES(pMsg);
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

NK_BEGIN_MESSAGE_MAP(CSixChannelZMQManager, IThreadMessageCB)
	ON_NKMESSAGE(MSG_START, ThrStart);
	ON_NKMESSAGE_POINTER(MSG_SEND_CHANNEL_MESSAGE, ThrSendMessage);
	ON_NKMESSAGE_DYN(MSG_ON_MESSAGE, ThrOnMessage);
NK_END_MESSAGE_MAP()

void CSixChannelZMQManager::Initialize()
{
	CZGlobal::initialize(10);
}

CSixChannelZMQManager::CSixChannelZMQManager(void)
{
	m_pZMQManager = 0;
	Clean();
}

CSixChannelZMQManager::~CSixChannelZMQManager(void)
{

}

void CSixChannelZMQManager::Clean()
{
	m_fstrURLConnection.Clear();
	m_fstrGroupName.Clear();
	m_fstrUniqueName.Clear();
	m_fstrDescription.Clear();
	m_iModuleType = -1;
}

bool CSixChannelZMQManager::Start(CDynamicStruct* pDynStartInfo)
{
	pDynStartInfo->GetString("URL", m_fstrURLConnection);
	pDynStartInfo->GetString("NAME_GROUP", m_fstrGroupName);
	pDynStartInfo->GetString("NAME_UNIQUE", m_fstrUniqueName);
	pDynStartInfo->GetInt("MODULE_TYPE", m_iModuleType);
	pDynStartInfo->GetString("DESCRIPTION", m_fstrDescription);

	m_pThreadTx = CThreadFactory::St_GetInstance()->GetResource();
	m_pThreadTx->SetThreadName(CFwkString(1, "CSixChannelZMQManagerTx<0x%x>", this));
	m_pThreadTx->Start();

	m_pThreadRx = CThreadFactory::St_GetInstance()->GetResource();
	m_pThreadRx->SetThreadName(CFwkString(1, "CSixChannelZMQManagerRx<0x%x>", this));
	m_pThreadRx->Start();

	if(m_fstrURLConnection.IsEmpty())
	{
		TRACEPF_ERROR("", "CSixChannelZMQManager::Start<0x%x> Can not create a CommChannel ZMQ without URL", this);
		return false;
	}

	m_pThreadTx->SendThreadMessage(MSG_START, this);
	return true;
}

void CSixChannelZMQManager::ThrStart()
{
	TRACEPF_DEBUG("",  "CSixChannelZMQManager::ThrStart<0x%x> OPENING channel [%s]", this, m_fstrURLConnection.GetBuffer());

	if(m_pZMQManager != 0)
	{
		TRACEPF_ERROR("", "CSixChannelZMQManager::ThrStart<0x%x> m_pZMQManager NOT NULL!!! [%s]", this, m_fstrURLConnection.GetBuffer());
		return;
	}

	// ZEROMQ&tcp... | ZEROMQW&tcp... | ZEROMQM&tcp...
	size_t iTypeLimitOffset = m_fstrURLConnection.Find("&") + 1;

	size_t iIndex = m_fstrURLConnection.Find("&", iTypeLimitOffset);
	if(iIndex == J_MAX_INDEX)
	{
		TRACEPF_ERROR("", "CSixChannelZMQManager::ThrStart<0x%x> Can not find connection parameters [%s]", this, m_fstrURLConnection.GetBuffer());
		return;
	}

	m_fstrInfoListening = "";
	m_fstrInfoSending = "";
	m_fstrInfoSending = m_fstrURLConnection.Mid(iTypeLimitOffset, iIndex - iTypeLimitOffset);
	m_fstrInfoListening = m_fstrURLConnection.Mid(iIndex + 1, m_fstrURLConnection.GetLength() - iIndex);

	try
	{
		m_pZMQManager = new CZManager();
		m_pZMQManager->setConfiguration(m_fstrInfoListening.GetBuffer());
		m_pZMQManager->setCallback(this);
	}
	catch(std::exception& e)
	{
		//delete(m_pZMQManager);
		//m_pZMQManager = 0;
		TRACEPF_ERROR("", "CSixChannelZMQManager::ThrStart<0x%x> EXCEPTION starting connection [%s] msg: [%s]", this, m_fstrURLConnection.GetBuffer(), e.what());
		return;
	}

	TRACEPF_DEBUG("", "CSixChannelZMQManager::ThrStart<0x%x> Channel OPENED [target: %s  local: %s]", this, m_fstrInfoSending.GetBuffer(), m_fstrInfoListening.GetBuffer());
	return;
}

bool CSixChannelZMQManager::ReleaseChannel()
{
	return false;
}

void CSixChannelZMQManager::Reset()
{
	TRACEPF_ERROR("", "CSixChannelZMQManager::Reset<0x%x>", this);

}

bool CSixChannelZMQManager::SendChannelMessage(CDynamicStruct* pDynData)
{
	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelZMQManager::SendChannelMessage<0x%x> dyn<0x%x> thr<0x%x> QueueSize %d", this, pDynData, m_pThreadTx->GetId(), m_pThreadTx->GetQueueSize());
	m_pThreadTx->SendThreadMessage(MSG_SEND_CHANNEL_MESSAGE, this, pDynData);
	return true;
}

void CSixChannelZMQManager::ThrSendMessage(NK_OBJPOINTER pData)
{
	CDynamicStruct* pMsg = (CDynamicStruct*)pData;

	if(m_pZMQManager == 0)
	{
		TRACEPF_ERROR("", "CSixChannelZMQManager::ThrSendMessage<0x%x> Channel NOT READY!!!... Discarding msg <0x%x>", this, pMsg);
		DYNFACTFREERES(pMsg);
		return;
	}

	if(m_fstrGroupName.IsEmpty() == false)
		pMsg->AddString(_six_kHeader_Org_Id, m_fstrGroupName);

	// Esto deberia eliminarse
	if(pMsg->IsParam(_six_kOper_Idx) == false)
		pMsg->AddInt(_six_kOper_Idx, 666);

	CFwkString fstrDestiny;
	pMsg->GetString(_six_kHeader_Dest_Id, fstrDestiny);

	CSixCommChannelModule::PrintMessage(pMsg, true);

	ParamsCodec* pcToSend = pMsg->ChangeParamsCodec();
	m_pZMQManager->sendMsg(pcToSend, fstrDestiny.GetBuffer());

	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelZMQManager::ThrSendMessage<0x%x> dyn<0x%x> sent", this, pMsg);

	DYNFACTFREERES(pMsg);
}

void CSixChannelZMQManager::process(ParamsCodec *msg)
{
	CDynamicStruct* pMsg = DYNFACTGETRES;
	pMsg->SetParamsCodec(msg);

	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS,  "CSixChannelZMQManager::process<0x%x> dyn<0x%x> thr<0x%x> QueueSize %d", this, pMsg, m_pThreadRx->GetId(), m_pThreadRx->GetQueueSize());
	m_pThreadRx->SendThreadMessage(MSG_ON_MESSAGE, this, pMsg);
}

void CSixChannelZMQManager::ThrOnMessage(CDynamicStruct* pMsg)
{
	CSixCommChannelModule::PrintMessage(pMsg, false);

	if(m_pCallback != 0)
		m_pCallback->ProcessCommChannelEvent(pMsg);
	else
	{
		CFwkString fstrResourceId;
		pMsg->GetString(_six_kHeader_ResId, fstrResourceId);
		TRACEPF_ERROR(fstrResourceId, "CSixChannelZMQManager::ThrOnMessage<0x%x> Message <0x%x> detected with NO CALLBACK!!!... Discarding", this, pMsg);
		DYNFACTFREERES(pMsg);
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

NK_BEGIN_MESSAGE_MAP(CSixChannelZMQSNDR, IThreadMessageCB)
	ON_NKMESSAGE(MSG_START, ThrStart);
	ON_NKMESSAGE_POINTER(MSG_SEND_CHANNEL_MESSAGE, ThrSendMessage);
	ON_NKMESSAGE_DYN(MSG_ON_MESSAGE, ThrOnMessage);
NK_END_MESSAGE_MAP()

void CSixChannelZMQSNDR::Initialize()
{
	CZGlobal::initialize(10);
}

CSixChannelZMQSNDR::CSixChannelZMQSNDR(void)
{
	m_pZMQReceiver = 0;
	m_pZMQSender = 0;
	Clean();
}

CSixChannelZMQSNDR::~CSixChannelZMQSNDR(void)
{

}

void CSixChannelZMQSNDR::Clean()
{
	m_fstrURLConnection.Clear();
	m_fstrGroupName.Clear();
	m_fstrUniqueName.Clear();
	m_fstrDescription.Clear();
	m_iModuleType = -1;
}

bool CSixChannelZMQSNDR::Start(CDynamicStruct* pDynStartInfo)
{
	pDynStartInfo->GetString("URL", m_fstrURLConnection);
	pDynStartInfo->GetString("NAME_GROUP", m_fstrGroupName);
	pDynStartInfo->GetString("NAME_UNIQUE", m_fstrUniqueName);
	pDynStartInfo->GetInt("MODULE_TYPE", m_iModuleType);
	pDynStartInfo->GetString("DESCRIPTION", m_fstrDescription);

	m_pThreadTx = CThreadFactory::St_GetInstance()->GetResource();
	m_pThreadTx->SetThreadName(CFwkString(1, "CSixChannelZMQSNDRTx<0x%x>", this));
	m_pThreadTx->Start();

	m_pThreadRx = CThreadFactory::St_GetInstance()->GetResource();
	m_pThreadRx->SetThreadName(CFwkString(1, "CSixChannelZMQSNDRRx<0x%x>", this));
	m_pThreadRx->Start();

	if(m_fstrURLConnection.IsEmpty())
	{
		TRACEPF_ERROR("", "CSixChannelZMQSNDR::Start<0x%x> Can not create a CommChannel ZMQ without URL", this);
		return false;
	}

	m_pThreadTx->SendThreadMessage(MSG_START, this);
	return true;
}

void CSixChannelZMQSNDR::ThrStart()
{
	TRACEPF_DEBUG("",  "CSixChannelZMQSNDR::ThrStart<0x%x> OPENING channel [%s]", this, m_fstrURLConnection.GetBuffer());

	if(m_pZMQReceiver != 0 || m_pZMQSender != 0)
	{
		TRACEPF_ERROR("", "CSixChannelZMQSNDR::ThrStart<0x%x> m_pZMQReceiver or m_pZMQSender NOT NULL!!! [%s]", this, m_fstrURLConnection.GetBuffer());
		return;
	}


	// ZEROMQ&tcp... | ZEROMQW&tcp... | ZEROMQM&tcp...
	size_t iTypeLimitOffset = m_fstrURLConnection.Find("&") + 1;

	size_t iIndex = m_fstrURLConnection.Find("&", iTypeLimitOffset);
	if(iIndex == J_MAX_INDEX)
	{
		TRACEPF_ERROR("", "CSixChannelZMQSNDR::ThrStart<0x%x> Can not find connection parameters [%s]", this, m_fstrURLConnection.GetBuffer());
		return;
	}

	m_fstrInfoListening = "";
	m_fstrInfoSending = "";
	m_fstrInfoSending = m_fstrURLConnection.Mid(iTypeLimitOffset, iIndex - iTypeLimitOffset);
	m_fstrInfoListening = m_fstrURLConnection.Mid(iIndex + 1, m_fstrURLConnection.GetLength() - iIndex);

	int iTargtCxs = 0;

	try
	{

		// Socket para recibir respuestas
		m_pZMQReceiver = new CZThreadReceiver();
		m_pZMQReceiver->setCallback(this);
		m_pZMQReceiver->useGlobalContext();
		m_pZMQReceiver->setSocketType(ZMQ_PULL);
		m_pZMQReceiver->doBind();
		m_pZMQReceiver->setConnectionUri( m_fstrInfoListening.GetBuffer() );
		m_pZMQReceiver->setIdentity( m_fstrInfoListening.GetBuffer() );
		m_pZMQReceiver->start();

		// Conexiones para enviar datos

		m_pZMQSender = new CZThreadSender();
		m_pZMQSender->useGlobalContext();
		m_pZMQSender->setSocketType( ZMQ_PUSH );
		m_pZMQSender->doConnect();
		m_pZMQSender->setReplyAddress(m_fstrInfoListening.GetBuffer());

		size_t iLen = m_fstrInfoSending.GetLength();
		CFwkString fstrDestiny;
		if(iLen != 0)
		{
			char* strMultiDir = new char[iLen + 1];
			ACE_OS::strcpy(strMultiDir, m_fstrInfoSending.GetBuffer());
			char* token = 0;
			char* end_str;
			token = ACE_OS::strtok_r(strMultiDir, ",", &end_str);
			while(token != 0)
			{
				iTargtCxs++;
				fstrDestiny = token;
				m_pZMQSender->connectTo(fstrDestiny.GetBuffer());
				token = ACE_OS::strtok_r(NULL, ",", &end_str);
			}

			delete(strMultiDir);
		}
	}
	catch(std::exception& e)
	{
		//delete(m_pZMQReceiver);
		//m_pZMQReceiver = 0;
		TRACEPF_ERROR("", "CSixChannelZMQSNDR::ThrStart<0x%x> EXCEPTION starting connection [%s] msg: [%s]", this, m_fstrURLConnection.GetBuffer(), e.what());
		return;
	}

	TRACEPF_DEBUG("", "CSixChannelZMQSNDR::ThrStart<0x%x> Channel OPENED [targets(%d): %s  local: %s]", this, iTargtCxs, m_fstrInfoSending.GetBuffer(), m_fstrInfoListening.GetBuffer());
	return;
}

bool CSixChannelZMQSNDR::ReleaseChannel()
{
	return false;
}

void CSixChannelZMQSNDR::Reset()
{
	TRACEPF_ERROR("", "CSixChannelZMQSNDR::Reset<0x%x>", this);
}

bool CSixChannelZMQSNDR::SendChannelMessage(CDynamicStruct* pDynData)
{
	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelZMQSNDR::SendChannelMessage<0x%x> dyn<0x%x> thr<0x%x> QueueSize %d", this, pDynData, m_pThreadTx->GetId(), m_pThreadTx->GetQueueSize());
	m_pThreadTx->SendThreadMessage(MSG_SEND_CHANNEL_MESSAGE, this, pDynData);
	return true;
}

void CSixChannelZMQSNDR::ThrSendMessage(NK_OBJPOINTER pData)
{
	CDynamicStruct* pMsg = (CDynamicStruct*)pData;

	if(m_pZMQSender == 0)
	{
		TRACEPF_ERROR("", "CSixChannelZMQSNDR::ThrSendMessage<0x%x> Channel NOT READY!!!... Discarding msg <0x%x>", this, pMsg);
		DYNFACTFREERES(pMsg);
		return;
	}

	if(m_fstrGroupName.IsEmpty() == false)
		pMsg->AddString(_six_kHeader_Org_Id, m_fstrGroupName);

	// Esto deberia eliminarse
	if(pMsg->IsParam(_six_kOper_Idx) == false)
		pMsg->AddInt(_six_kOper_Idx, 666);

	CSixCommChannelModule::PrintMessage(pMsg, true);

	ParamsCodec* pcToSend = pMsg->ChangeParamsCodec();
	m_pZMQSender->sendMsg(pcToSend);

	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelZMQSNDR::ThrSendMessage<0x%x> dyn<0x%x> sent", this, pMsg);

	DYNFACTFREERES(pMsg);
}

void CSixChannelZMQSNDR::process(ParamsCodec *msg)
{
	CDynamicStruct* pMsg = DYNFACTGETRES;
	pMsg->SetParamsCodec(msg);

	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS,  "CSixChannelZMQSNDR::process<0x%x> dyn<0x%x> thr<0x%x> QueueSize %d", this, pMsg, m_pThreadRx->GetId(), m_pThreadRx->GetQueueSize());
	m_pThreadRx->SendThreadMessage(MSG_ON_MESSAGE, this, pMsg);
}

void CSixChannelZMQSNDR::ThrOnMessage(CDynamicStruct* pMsg)
{
	CSixCommChannelModule::PrintMessage(pMsg, false);

	if(m_pCallback != 0)
		m_pCallback->ProcessCommChannelEvent(pMsg);
	else
	{
		CFwkString fstrResourceId;
		pMsg->GetString(_six_kHeader_ResId, fstrResourceId);
		TRACEPF_ERROR(fstrResourceId, "CSixChannelZMQSNDR::ThrOnMessage<0x%x> Message <0x%x> detected with NO CALLBACK!!!... Discarding", this, pMsg);
		DYNFACTFREERES(pMsg);
	}
}
