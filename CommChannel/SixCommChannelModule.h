#pragma once

#include "Templates/TSingleton.h"
#include "CommChannel/ISixCommChannel.h"

class IThread;

class CSixCommChannelModule : public TSingleton<CSixCommChannelModule>
{
	class CCommChannelFactoriesDP;

public:
	CSixCommChannelModule(void);
	~CSixCommChannelModule(void);

	int Init();
	int Exit();

	ISixCommChannel* GetCommChannel(ISixCommChannel::CHANNEL_TYPE eChannelType);
	ISixCommChannel* GetCommChannel(const char* strCxURI, const char* strGroup, const char* strIdentifier);
	static void PrintMessage(CDynamicStruct* pMsg, bool bSend);

private:
	CCommChannelFactoriesDP* m_factoriesDP;
	void FreeCommChannel(ISixCommChannel* pChannel);

	static bool ms_bPrintMessageHeader;
};

