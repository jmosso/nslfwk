#pragma once
 
#include "ISixCommChannel.h"
#include "ThreadModule/IThreadMessageCB.h"
#include "TimerModule/ITimerCB.h"
#include "FrameworkUtilities/FwkString.h"

#include <CRouterCallBack.hpp>

#ifndef WIN32
#include <CRouterUnixQueue.hpp>
#else
#include "Exception.hpp"
class CRouterUnixQueue
{
public:
	CRouterUnixQueue(const char* strName, int a, int b, bool flag = true){}
	void sendMsg( std::string destination, const char *buf, size_t size ) throw( sixlabs::Exception ){};
	void registerCallBack( CRouterCallBack *pCoreBrainCallBack ) throw( sixlabs::Exception ){};
};
#endif

#include <map>

class IThread;
class CDynamicStruct;
class ParamsCodec;

class CSixChannelUnixQ : public ISixCommChannel
					   , public CRouterCallBack
					   , public IThreadMessageCB
					   , public ITimerCB
{
public:
	CSixChannelUnixQ(void);
	virtual ~CSixChannelUnixQ(void);

	static void Initialize();

private:
	static int m_iTimerReconnect;

private:

	enum eSTATE
	{
		STATE_NONE,
		STATE_OPENING,
		STATE_OPENED,
		STATE_CLOSING,
		STATE_CLOSED
	};

	enum eMESSAGES
	{
		MSG_START,
		MSG_SEND_CHANNEL_MESSAGE,
		MSG_TIME_OUT,
		MSG_ON_MESSAGE,
	};

	bool Start(CDynamicStruct* pDynStartInfo);
	bool ReleaseChannel();
	bool SendChannelMessage(CDynamicStruct* pDynData);
	void process(ParamsCodec *msg) throw( sixlabs::Exception);

	void ThrStart();
	void ThrSendMessage(NK_OBJPOINTER pData);
	void ThrOnMessage(CDynamicStruct* pMsg);
	void ThrTimeOut();
	void Reset();
	void HandleCxError();
	void Clean();

	void TimeOut(size_t iEventId, size_t iTimerId);

	CFwkString m_fstrGroupName;
	CFwkString m_fstrUniqueName;
	CFwkString m_fstrDescription;
	int	m_iModuleType;

	eSTATE m_eSTATE;
	IThread* m_pThreadTx;
	IThread* m_pThreadRx;

	CRouterUnixQueue* RegisterConsumer(CFwkString& fstrConsumer);
	CRouterUnixQueue* RegisterProducer(CFwkString& fstrProducer);

	typedef std::map<CFwkString, CRouterUnixQueue*> MAPSTR2ROUTERUQ;
	MAPSTR2ROUTERUQ m_ProducerMap;
	MAPSTR2ROUTERUQ m_ConsumerMap;

	NK_DECLARE_MESSAGE_MAP() 
};
