#pragma once
 
#include "ISixCommChannel.h"
#include "ThreadModule/IThreadMessageCB.h"
#include "FrameworkUtilities/FwkString.h"
#include <string>

class IThread;
class CDynamicStruct;
class ParamsCodec;

#ifdef WIN32

// Objetos dummmy para compilacion windows
class CZCallback
{
public:
    virtual void process( ParamsCodec *pc ) = 0;
};

class CZWorker
{
public:
	CZWorker(){};
	void setConfiguration(std::string remoteNetworkId, std::string localNetworkId){};

	void setPrivateCallback(CZCallback* cb){};
	void setServiceCallback(CZCallback* cb){};

	void sendMsg(ParamsCodec* pc){};
};

class CZManager
{
public:
	CZManager(){};
	void setConfiguration( std::string localNetworkId ){};
	void sendMsg( ParamsCodec *pc, std::string destination = "" ){};
	void setCallback( CZCallback *callback ){};
};

#define ZMQ_PULL 1
class CZThreadReceiver
{
public:
	CZThreadReceiver(){};
	void start(){};
	void setCallback( CZCallback *callback ){};

	void useGlobalContext(){};
	void setSocketType(int iVal){};
	void doBind(){};
	void setConnectionUri( std::string destination ){};
	void setIdentity( std::string identity ){};
};

#define ZMQ_PUSH 1
class CZThreadSender
{
public:
	CZThreadSender(){};
	void setReplyAddress( std::string replyAddress ){};
	void connectTo( std::string connectionUri ){};
	void sendMsg( ParamsCodec *pc, std::string destination = "" ){};
	void setCallback( CZCallback *callback ){};

	void useGlobalContext(){};
	void setSocketType(int iVal){};
	void doConnect(){};
};

class CZGlobal
{
public:
	static void initialize(int val){}
};
#else
#include <CZCallback.hpp>
class CZWorker;
class CZManager;
class CZThreadReceiver;
class CZThreadSender;
#endif

class CSixChannelZMQ : public ISixCommChannel
					   , public CZCallback
					   , public IThreadMessageCB
{
public:
	CSixChannelZMQ(void);
	virtual ~CSixChannelZMQ(void);

	static void Initialize();

private:

	enum eMESSAGES
	{
		MSG_START,
		MSG_SEND_CHANNEL_MESSAGE,
		MSG_ON_MESSAGE,
	};

	bool Start(CDynamicStruct* pDynStartInfo);
	bool ReleaseChannel();
	bool SendChannelMessage(CDynamicStruct* pDynData);
	void process(ParamsCodec *msg);

	void ThrStart();
	void ThrSendMessage(NK_OBJPOINTER pData);
	void ThrOnMessage(CDynamicStruct* pMsg);
	void Reset();
	void Clean();


	CFwkString m_fstrURLConnection;
	CFwkString m_fstrGroupName;
	CFwkString m_fstrUniqueName;
	CFwkString m_fstrDescription;
	int	m_iModuleType;

	IThread* m_pThreadTx;
	IThread* m_pThreadRx;
	CZWorker* m_pZMQWorker;


	NK_DECLARE_MESSAGE_MAP() 
};

class CSixChannelZMQManager : public ISixCommChannel
							, public CZCallback
							, public IThreadMessageCB
{
public:
	CSixChannelZMQManager(void);
	virtual ~CSixChannelZMQManager(void);

	static void Initialize();

private:

	enum eMESSAGES
	{
		MSG_START,
		MSG_SEND_CHANNEL_MESSAGE,
		MSG_ON_MESSAGE,
	};

	bool Start(CDynamicStruct* pDynStartInfo);
	bool ReleaseChannel();
	bool SendChannelMessage(CDynamicStruct* pDynData);
	void process(ParamsCodec *msg);

	void ThrStart();
	void ThrSendMessage(NK_OBJPOINTER pData);
	void ThrOnMessage(CDynamicStruct* pMsg);
	void Reset();
	void Clean();


	CFwkString m_fstrURLConnection;
	CFwkString m_fstrGroupName;
	CFwkString m_fstrUniqueName;
	CFwkString m_fstrDescription;
	int	m_iModuleType;

	IThread* m_pThreadTx;
	IThread* m_pThreadRx;
	CZManager* m_pZMQManager;
	CFwkString m_fstrInfoListening;
	CFwkString m_fstrInfoSending;

	NK_DECLARE_MESSAGE_MAP() 
};

class CSixChannelZMQSNDR : public ISixCommChannel
					     , public CZCallback
					     , public IThreadMessageCB
{
public:
	CSixChannelZMQSNDR(void);
	virtual ~CSixChannelZMQSNDR(void);

	static void Initialize();

private:

	enum eMESSAGES
	{
		MSG_START,
		MSG_SEND_CHANNEL_MESSAGE,
		MSG_ON_MESSAGE,
	};

	bool Start(CDynamicStruct* pDynStartInfo);
	bool ReleaseChannel();
	bool SendChannelMessage(CDynamicStruct* pDynData);
	void process(ParamsCodec *msg);

	void ThrStart();
	void ThrSendMessage(NK_OBJPOINTER pData);
	void ThrOnMessage(CDynamicStruct* pMsg);
	void Reset();
	void Clean();


	CFwkString m_fstrURLConnection;
	CFwkString m_fstrGroupName;
	CFwkString m_fstrUniqueName;
	CFwkString m_fstrDescription;
	int	m_iModuleType;

	CFwkString m_fstrInfoListening;
	CFwkString m_fstrInfoSending;

	IThread* m_pThreadTx;
	IThread* m_pThreadRx;
	CZThreadReceiver* m_pZMQReceiver;
	CZThreadSender*   m_pZMQSender;

	NK_DECLARE_MESSAGE_MAP() 
};