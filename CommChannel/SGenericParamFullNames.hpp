#pragma once

#define _six_kHeader_Org				"header.org"
#define _six_kHeader_Dest				"header.dest"
#define _six_kHeader_ResId				"header.resourceId"
#define _six_kHeader_Session			"header.session"

#define _six_kHeader_Org_Id				"header.org.id"
#define _six_kHeader_Org_Type			"header.org.type"
#define _six_kHeader_Org_SubType		"header.org.subtype"
#define _six_kHeader_Org_Desc			"header.org.desc"
#define _six_kHeader_Org_Format			"header.org.format"

#define _six_kHeader_Dest_Id			"header.dest.id"
#define _six_kHeader_Dest_Type			"header.dest.type"
#define _six_kHeader_Dest_SubType		"header.dest.subtype"
#define _six_kHeader_Dest_Desc			"header.dest.desc"
#define _six_kHeader_Dest_Format		"header.dest.format"

#define _six_kOperGroup					"oper"
#define _six_kOper_Type					"oper.type"
#define _six_kOper_SubType				"oper.subtype"
#define _six_kOper_Idx					"oper.idx"
#define _six_kOper_IdxRef				"oper.idxref"
#define _six_kOper_TypeRef				"oper.typeref"
#define _six_kOper_PrmType				"oper.prmtype"
#define _six_kOper_Desc					"oper.desc"
#define _six_kOper_Cause				"oper.cause"
						
#define _six_kType						"type"
#define _six_kSubType					"subtype"
#define _six_kIdx						"idx"
#define _six_kIdxRef					"idxref"
#define _six_kTypeRef					"typeref"
#define _six_kPrmType					"prmtype"
#define _six_kDesc						"desc"
#define _six_kCause						"cause"
								
#define _six_kDone_Cause				"done.cause"
#define _six_kError_Cause				"error.cause"
										
#define INNER_CAUSE						"cause"
#define OPERGROUP						"oper"
#define OPER_PROTOCOL_GROUP				"oper.param.protocol"
#define OPER_PROTOCOL_RELIABLE			"oper.param.protocol.sip.reliable"
#define OPERSUBTYPE						"oper.subtype"
#define OPER_BEARER_GROUP				"oper.Bearer"
#define END_GROUP						"oper.param.end"
#define END_MAIN_REASON					"oper.param.endcause"
#define END_PROTOCOL_REASON				"oper.param.protocolCause"
#define END_PROTOCOL_DESCRP				"oper.param.protocolCauseDesc"
#define INCOMING_MR_QUEUE				"oper.getresource.mrqueue"
#define OUTGOING_MR_QUEUE				"oper.done.mrqueue"
#define DRIVER_DIRECTION				"oper.Direction"

#define _six_kGetResource				1
#define _six_kFreeResource				2
#define _six_kDone						3
#define _six_kError						4
							
#define _six_kPrm_Event					1
#define _six_kPrm_Request				2
#define _six_kPrm_Response				3
#define _six_kPrm_Action				4
#define _six_kPrm_OperReq				5
#define _six_kPrm_OperEvent				6

#define _six_kModule_Control			1
#define _six_kModule_Sgnl				2
#define _six_kModule_Media				8
#define _six_kModule_Bas				10
#define _six_kModule_SIP				43

