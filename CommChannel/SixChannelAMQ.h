#pragma once
 
#include "ISixCommChannel.h"
#include "ThreadModule/IThreadMessageCB.h"
#include "TimerModule/ITimerCB.h"
#include "FrameworkUtilities/FwkString.h"

#include "cms/ExceptionListener.h"
#include "cms/MessageListener.h"
#include "cms/Connection.h"
#include "cms/Session.h"
#include "activemq/util/Config.h"

using namespace cms;

class IThread;
class CDynamicStruct;

class CSixChannelAMQ : public ISixCommChannel
					 , public ExceptionListener
					 , public MessageListener
					 , public IThreadMessageCB
					 , public ITimerCB
{
public:
	CSixChannelAMQ(void);
	virtual ~CSixChannelAMQ(void);

	static void Initialize();

private:
	static int m_iTimerReconnect;

private:

	enum eSTATE
	{
		STATE_NONE,
		STATE_OPENING,
		STATE_OPENED,
		STATE_CLOSING,
		STATE_CLOSED
	};


	enum eMESSAGES
	{
		MSG_START,
		MSG_SEND_CHANNEL_MESSAGE,
		MSG_ON_EXCEPTION,
		MSG_TIME_OUT,
		MSG_ON_MESSAGE,
	};

	bool Start(CDynamicStruct* pDynStartInfo);
	bool ReleaseChannel();
	bool SendChannelMessage(CDynamicStruct* pDynData);

	void ThrStart();
	void ThrSendMessage(NK_OBJPOINTER pData);
	void ThrOnMessage(NK_OBJPOINTER pData);
	void ThrOnException();
	void ThrTimeOut();
	void Reset();
	void HandleCxError();
	void Clean();

	virtual void onMessage(const Message* message);
	virtual void onException( const CMSException& ex AMQCPP_UNUSED);

	void TimeOut(size_t iEventId, size_t iTimerId);

	CFwkString m_fstrURLConnection;
	CFwkString m_fstrGroupName;
	CFwkString m_fstrUniqueName;
	CFwkString m_fstrDescription;
	int	m_iModuleType;
	int m_iUseTopic;

	eSTATE m_eSTATE;
	IThread* m_pThreadTx;
	IThread* m_pThreadRx;

	MessageConsumer* RegisterConsumer(CFwkString& fstrConsumer);
	MessageProducer* RegisterProducer(CFwkString& fstrProducer);

	typedef std::map<CFwkString, MessageProducer*> MAPSTRING_MESSAGEPRODUCER;
	typedef std::map<CFwkString, MessageConsumer*> MAPSTRING_MESSAGECONSUMER;
	MAPSTRING_MESSAGEPRODUCER m_ProducerMap;
	MAPSTRING_MESSAGECONSUMER m_ConsumerMap;

	bool m_bMultipleConsumer;
	Connection* m_Connection;
	Session* m_Session;


	NK_DECLARE_MESSAGE_MAP() 
};
