
#include "SixChannelAMQ.h"
#include "SixCommChannelModule.h"
#include "TimerModule/TimerFactory.h"
#include "ThreadModule/ThreadFactory.h"
#include "FrameworkUtilities/DynamicStruct.h"
#include "SmallConfiguration/SmallConfiguration.h"
#include "Utilities/Tracing.h"

#include "activemq/core/ActiveMQConnectionFactory.h"
#include "activemq/library/ActiveMQCPP.h"

#include "SGenericParamFullNames.hpp"
#include <paramsCodec.hpp>

using namespace activemq::core;
using namespace activemq::library;
using namespace std;

#define AMQ_DELIVERY_MODE		2
#define AMQ_DEFAULT_PRIORITY	4

NK_BEGIN_MESSAGE_MAP(CSixChannelAMQ, IThreadMessageCB)
	ON_NKMESSAGE(MSG_START, ThrStart);
	ON_NKMESSAGE(MSG_ON_EXCEPTION, ThrOnException);
	ON_NKMESSAGE(MSG_TIME_OUT, ThrTimeOut);
	ON_NKMESSAGE_POINTER(MSG_SEND_CHANNEL_MESSAGE, ThrSendMessage);
	ON_NKMESSAGE_POINTER(MSG_ON_MESSAGE, ThrOnMessage);
NK_END_MESSAGE_MAP()

int CSixChannelAMQ::m_iTimerReconnect = 10000;
void CSixChannelAMQ::Initialize()
{
	activemq::library::ActiveMQCPP::initializeLibrary(); 

	m_iTimerReconnect = 10000;
	VARIABLEINT(m_iTimerReconnect, "Configuracion", "BusTimeMonitor");
}

CSixChannelAMQ::CSixChannelAMQ(void)
{
	m_Session = 0;
	m_Connection = 0;
	Clean();
}

CSixChannelAMQ::~CSixChannelAMQ(void)
{

}

void CSixChannelAMQ::Clean()
{
	m_fstrURLConnection.Clear();
	m_fstrGroupName.Clear();
	m_fstrUniqueName.Clear();
	m_fstrDescription.Clear();
	m_iModuleType = -1;
	m_iUseTopic = 0;
	m_bMultipleConsumer = false;
	m_eSTATE = STATE_NONE;
}

bool CSixChannelAMQ::Start(CDynamicStruct* pDynStartInfo)
{
	pDynStartInfo->GetString("URL", m_fstrURLConnection);
	pDynStartInfo->GetString("NAME_GROUP", m_fstrGroupName);
	pDynStartInfo->GetString("NAME_UNIQUE", m_fstrUniqueName);
	pDynStartInfo->GetInt("MODULE_TYPE", m_iModuleType);
	pDynStartInfo->GetInt("USE_TOPIC", m_iUseTopic);
	pDynStartInfo->GetString("DESCRIPTION", m_fstrDescription);

	m_pThreadTx = CThreadFactory::St_GetInstance()->GetResource();
	m_pThreadTx->SetThreadName(CFwkString(1, "CSixChannelAMQTx<0x%x>", this));
	m_pThreadTx->Start();

	m_pThreadRx = CThreadFactory::St_GetInstance()->GetResource();
	m_pThreadRx->SetThreadName(CFwkString(1, "CSixChannelAMQRx<0x%x>", this));
	m_pThreadRx->Start();

	if(m_fstrURLConnection.IsEmpty() || m_fstrGroupName.IsEmpty())
	{
		TRACEPF_ERROR("", "CSixChannelAMQ::Start<0x%x> Can not create a CommChannelAMQ without URL and NAME_GROUP", this);
		return false;
	}

	m_pThreadTx->SendThreadMessage(MSG_START, this);

	m_eSTATE = STATE_OPENING;
	return true;
}

void CSixChannelAMQ::ThrStart()
{
	std::string brokerURI = string (m_fstrURLConnection.GetBuffer());
	m_Session = 0;
	m_Connection = 0;
	try
	{
		ActiveMQConnectionFactory* connectionFactory = new ActiveMQConnectionFactory (brokerURI);
		m_Connection = connectionFactory->createConnection();
		m_Connection->start();
		m_Connection->setExceptionListener (this);
		m_Session = m_Connection->createSession (Session::AUTO_ACKNOWLEDGE);
		delete connectionFactory;	

		MessageConsumer* pConsumer = RegisterConsumer(m_fstrGroupName);
		if (pConsumer == 0)
		{
			TRACEPF_ERROR("", "CSixChannelAMQ::ThrStart<0x%x> Can not register consumer group: %s", this, (const char*)m_fstrGroupName);
			HandleCxError();
			return;
		}

		if (m_fstrUniqueName.IsEmpty() == false)
		{
			MessageConsumer* pConsumerIdentifier = RegisterConsumer(m_fstrUniqueName);
			if (pConsumerIdentifier == 0)
			{
				TRACEPF_ERROR("", "CSixChannelAMQ::ThrStart<0x%x> Can not register consumer uniqueName: %s", this, (const char*)m_fstrUniqueName);
				HandleCxError();
				return;
			}
		}

		m_eSTATE = STATE_OPENED;
		TRACEPF_DEBUG("", "CSixChannelAMQ::ThrStart<0x%x> channel OPENED queues:[%s, %s] URL:[%s]", this, 
			(const char*)m_fstrGroupName, (const char*)m_fstrUniqueName, brokerURI.c_str());

	}
	catch (...)
	{
		TRACEPF_ERROR("", "CSixChannelAMQ::ThrStart<0x%x> Can not start connection with URL: [%s]", this, brokerURI.c_str());
		HandleCxError();
	}

	return;
}

void CSixChannelAMQ::TimeOut(size_t iEventId, size_t iTimerId)
{
	m_pThreadTx->SendThreadMessage(MSG_TIME_OUT, this);
}

void CSixChannelAMQ::ThrTimeOut()
{
	TRACEPF_WARNING("", "CSixChannelAMQ::ThrTimeOut<0x%x> Trying connection with URL: [%s]", this, m_fstrURLConnection.GetBuffer());
	ThrStart();
}

bool CSixChannelAMQ::ReleaseChannel()
{
	return false;
}

void CSixChannelAMQ::HandleCxError()
{
	Reset();
	CTimerFactory::St_GetInstance()->SetTimer(0, m_iTimerReconnect, this);
}

void CSixChannelAMQ::onException(const CMSException& ex AMQCPP_UNUSED)
{
	m_pThreadTx->SendThreadMessage(MSG_ON_EXCEPTION, this);
}

void CSixChannelAMQ::ThrOnException()
{
	TRACEPF_ERROR("", "CSixChannelAMQ::ThrOnException<0x%x> on URI [%s]", this, m_fstrURLConnection.GetBuffer());
	HandleCxError();
}

void CSixChannelAMQ::Reset()
{
	TRACEPF_ERROR("", "CSixChannelAMQ::Reset<0x%x>", this);

	MAPSTRING_MESSAGECONSUMER::iterator itConsumer;
	for (itConsumer = m_ConsumerMap.begin(); itConsumer != m_ConsumerMap.end(); itConsumer++ )
	{
		delete itConsumer->second;
	}
	m_ConsumerMap.clear();


	MAPSTRING_MESSAGEPRODUCER::iterator itProducer;
	for (itProducer = m_ProducerMap.begin(); itProducer != m_ProducerMap.end(); itProducer++ )
	{
		delete itProducer->second;
	}
	m_ProducerMap.clear();

	try
	{
		if (m_Session != 0)
			m_Session->close();
		if (m_Connection != 0)
			m_Connection->close();
	}
	catch (...)
	{
		TRACEPF_ERROR("", "CSixChannelAMQ::Reset<0x%x> Exception closing session and cx", this);
	}
	try
	{
		if (m_Session != 0)
			delete m_Session;
		// OJO: No se borra
		//if(m_Connection != 0) 
		//	delete m_Connection;
	}
	catch (...)
	{
		TRACEPF_ERROR("", "CSixChannelAMQ::Reset<0x%x> Exception Deleting session ", this);
	}

	m_Session = 0;
	m_Connection = 0;
	m_eSTATE = STATE_NONE;
}

bool CSixChannelAMQ::SendChannelMessage(CDynamicStruct* pDynData)
{
	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelAMQ::SendChannelMessage<0x%x> dyn<0x%x> thr<0x%x> QueueSize %d", this, pDynData, m_pThreadTx->GetId(), m_pThreadTx->GetQueueSize());
	m_pThreadTx->SendThreadMessage(MSG_SEND_CHANNEL_MESSAGE, this, pDynData);
	return true;
}

void CSixChannelAMQ::ThrSendMessage(NK_OBJPOINTER pData)
{
	CDynamicStruct* pMsg = (CDynamicStruct*)pData;

	if(m_eSTATE != STATE_OPENED)
	{
		TRACEPF_ERROR("", "CSixChannelAMQ::ThrSendMessage<0x%x> Channel NOT OPENED... Discarding msg <0x%x>", this, pMsg);
		DYNFACTFREERES(pMsg);
		return;
	}

	CFwkString fstrDestiny;
	pMsg->GetString(_six_kHeader_Dest_Id, fstrDestiny);
	CFwkString fstrResourceId;
	pMsg->GetString(_six_kHeader_ResId, fstrResourceId);

	if(fstrDestiny.IsEmpty())
	{
		TRACEPF_ERROR(fstrResourceId, "CSixChannelAMQ::ThrSendMessage<0x%x> Destiny empty. Can not send message... Discarding msg <0x%x>", this, pMsg);
		DYNFACTFREERES(pMsg);
		return;
	}

	if (!m_fstrUniqueName.IsEmpty())
		pMsg->AddString(_six_kHeader_Org_Id, m_fstrUniqueName);
	else
	{
		// Si hay multiple consumidor en esta misma conexion, el nombre del origen DEBE fijarse desde la capa de servicio
		if(m_bMultipleConsumer == false)
			pMsg->AddString(_six_kHeader_Org_Id, m_fstrGroupName);
	}

	pMsg->AddInt(_six_kHeader_Org_Type, m_iModuleType);

	// Esto deberia eliminarse
	if(pMsg->IsParam(_six_kOper_Idx) == false)
		pMsg->AddInt(_six_kOper_Idx, 666);

	int iTimeToLive = 0;
	if(pMsg->GetInt("SYS_TIME_TO_LIVE", iTimeToLive) == true)
		pMsg->DeleteParam("SYS_TIME_TO_LIVE");

	CFwkString sMsg;
	try
	{
		ParamsCodecGenericBuffer outputBuffer;
		pMsg->GetParamsCodec()->writeObjectBAS( outputBuffer );
		sMsg.FromBuffer( outputBuffer.buffer, outputBuffer.size );		
	}
	catch (...)
	{
		TRACEPF_ERROR(fstrResourceId, "CSixChannelAMQ::ThrSendMessage<0x%x> Error writeObjectBAS. Can not send!!!... Discarding msg <0x%x>", this, pMsg);
		DYNFACTFREERES(pMsg);
		return;
	}

	CSixCommChannelModule::PrintMessage(pMsg, true);

	try
	{
		TextMessage* message = m_Session->createTextMessage((string)sMsg.GetBuffer());
		message->setStringProperty( "JMSXGroupID",  fstrResourceId.GetBuffer());
		int usMessage  = -1;
		pMsg->GetInt(_six_kOper_Type, usMessage);
		if(usMessage == _six_kFreeResource)
			message->setIntProperty( "JMSXGroupSeq", -1);

		MAPSTRING_MESSAGEPRODUCER::iterator i = m_ProducerMap.find (fstrDestiny);
		if(i != m_ProducerMap.end())
		{
			i->second->send(message, AMQ_DELIVERY_MODE, AMQ_DEFAULT_PRIORITY, iTimeToLive);
		}
		else
		{
			MessageProducer* pProducer = RegisterProducer(fstrDestiny);
			if (pProducer)
			{
				pProducer->send(message, AMQ_DELIVERY_MODE, AMQ_DEFAULT_PRIORITY, iTimeToLive);
				TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelAMQ::ThrSendMessage<0x%x> dyn<0x%x> sent", this, pMsg);
			}
			else
			{
				TRACEPF_ERROR(fstrResourceId, "CSixChannelAMQ::ThrSendMessage<0x%x> productor null. Can not send!!!... Discarding msg <0x%x>", this, pMsg);
			}
		}
		delete message;
	}
	catch (...)
	{
		TRACEPF_ERROR(fstrResourceId, "CSixChannelAMQ::ThrSendMessage<0x%x> AMQ Error. Can not send!!!... Discarding msg <0x%x>", this, pMsg);
	}


	DYNFACTFREERES(pMsg);
}

void CSixChannelAMQ::onMessage(const Message* message)
{
	try
	{
		const TextMessage* textMessage = dynamic_cast<const TextMessage*>(message);
		if(textMessage != 0) 
		{
			ParamsCodecGenericBuffer inputBuffer( textMessage->getText().c_str(),textMessage->getText().length() + 1 );
			CDynamicStruct *pMsg = DYNFACTGETRES;
			pMsg->GetParamsCodec()->readObjectBAS(inputBuffer);
			TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelAMQ::onMessage<0x%x> dyn<0x%x> thr<0x%x> QueueSize %d", this, pMsg, m_pThreadRx->GetId(), m_pThreadRx->GetQueueSize());
			m_pThreadRx->SendThreadMessage(MSG_ON_MESSAGE, this, pMsg);
		}
		else
		{
			TRACEPF_ERROR("", "CSixChannelAMQ::onMessage<0x%x> Error Message type not supported. MUST be TEXT", this);
		}
	}
	catch (...)
	{
		TRACEPF_ERROR("", "CSixChannelAMQ::onMessage<0x%x> Error building paramscodec", this);
	}
}

void CSixChannelAMQ::ThrOnMessage(NK_OBJPOINTER pData)
{
	CDynamicStruct* pMsg = (CDynamicStruct*)pData;

	CSixCommChannelModule::PrintMessage(pMsg, false);

	if(m_pCallback != 0)
		m_pCallback->ProcessCommChannelEvent(pMsg);
	else
	{
		CFwkString fstrResourceId;
		pMsg->GetString(_six_kHeader_ResId, fstrResourceId);
		TRACEPF_ERROR(fstrResourceId, "CSixChannelAMQ::ThrOnMessage<0x%x> Message <0x%x> detected with NO CALLBACK!!!... Discarding", this, pMsg);
		DYNFACTFREERES(pMsg);
	}
}

MessageConsumer* CSixChannelAMQ::RegisterConsumer(CFwkString& fstrConsumer)
{
	TRACEPF_DEBUG("",  "CSixChannelAMQ::RegisterConsumer<0x%x> - Queue %s m_iUseTopic %d", this, fstrConsumer.GetBuffer(), m_iUseTopic);

	Destination* pDestination =  0;
	MessageConsumer* pConsumer = 0;

	try
	{
		if(fstrConsumer.Find(',') != J_MAX_INDEX)
		{
			int iCounter = 0;
			size_t iLen = fstrConsumer.GetLength();
			char* strConsumerList = new char[iLen + 1];
			ACE_OS::strcpy(strConsumerList, (const char*)fstrConsumer);

			char* token = 0;
			char* end_str;
			token = ACE_OS::strtok_r(strConsumerList, ", ", &end_str);
			while(token != 0 && ACE_OS::strcmp(token, "") != 0)
			{
				//m_sName = token;
				if(m_iUseTopic == 0)
					pDestination = m_Session->createQueue(token);
				else
					pDestination = m_Session->createTopic(token);

				pConsumer = m_Session->createConsumer(pDestination);
				pConsumer->setMessageListener(this);
				m_ConsumerMap[token] = pConsumer;
				delete pDestination;

				token = ACE_OS::strtok_r(NULL, ", ", &end_str);
				iCounter++;
			}
			delete(strConsumerList);
			if(iCounter > 1)
				m_bMultipleConsumer = true;
		}
		else
		{
			if(m_iUseTopic == 0)
				pDestination = m_Session->createQueue(fstrConsumer.GetBuffer());
			else
				pDestination = m_Session->createTopic(fstrConsumer.GetBuffer());

			pConsumer = m_Session->createConsumer(pDestination);
			pConsumer->setMessageListener(this);
			m_ConsumerMap[fstrConsumer] = pConsumer;
			delete pDestination;
		}
	}
	catch (...)
	{
		TRACEPF_ERROR("", "CSixChannelAMQ::RegisterConsumer<0x%x> Exception", this);
		return 0;
	}

	return pConsumer;
}

MessageProducer* CSixChannelAMQ::RegisterProducer(CFwkString& fstrProducer)
{
	TRACEPF_DEBUG("",  "CSixChannelAMQ::RegisterProducer<0x%x> - Queue %s m_iUseTopic %d", this, fstrProducer.GetBuffer(), m_iUseTopic);
	try
	{
		Destination* pDestination =  0;
		if(m_iUseTopic == 0)
			pDestination = m_Session->createQueue(fstrProducer.GetBuffer());
		else
			pDestination = m_Session->createTopic(fstrProducer.GetBuffer());

		MessageProducer* pProducer = m_Session->createProducer(pDestination);
		pProducer->setDeliveryMode (DeliveryMode::NON_PERSISTENT);
		m_ProducerMap[fstrProducer] = pProducer;
		delete pDestination;
		return pProducer;
	}
	catch (...)
	{
		TRACEPF_ERROR("", "CSixChannelAMQ::RegisterProducer<0x%x> Can not register producer", this);
	}

	return 0;
}
