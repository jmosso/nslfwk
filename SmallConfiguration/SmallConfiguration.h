#pragma once

#include "FrameworkUtilities/FwkString.h"
#include <map>

#define VARIABLECHAR CSmallConfiguration::VariableChar
#define VARIABLEINT CSmallConfiguration::VariableInt
#define VARIABLEBOOL CSmallConfiguration::Variablebool
#define SETVARIABLE CSmallConfiguration::setVariable
#define EXPORTCONFIGFILE CSmallConfiguration::ExportConfigFile
#define ENUMERATEVALUES CSmallConfiguration::enumerateValues
#define ENUMERATEVALUESDYN CSmallConfiguration::enumerateValuesDyn

class CDynamicStruct;
class ACE_Configuration;
class ACE_Configuration_Section_Key;

class CSmallConfiguration
{
public:
	CSmallConfiguration(void);
	~CSmallConfiguration(void);

	static bool InitConfiguration(const CFwkString& fstrConfigFilename);
	static bool EndConfiguration();
	static short LoadConfiguration( const CFwkString& fstrConfigFilename);

	static int ReloadConfiguration();

	static bool  VariableChar(CFwkString &OutVariable, const char *pModulo, const char *pVariable, const CFwkString & fstrDefaultVariable = "", bool setVariable = false);
	static bool  VariableChar(char* OutVariable, unsigned int sizeOutVariable, const char * modulo, const char * variable, const char * strDefaultVariable = "", bool setVariable = false); 

	
	static bool  VariableInt(long & OutVariable, const char *pModulo,const char *pVariable,const long& defaultVariable = 0, bool setVariable = false);
	static bool  VariableInt(unsigned long & OutVariable, const char *pModulo,const char *pVariable,const unsigned long & defaultVariable = 0, bool setVariable = false);
	static bool  VariableInt(int & OutVariable, const char *pModulo,const char *pVariable,const int & defaultVariable = 0, bool setVariable = false);
	static bool  VariableInt(unsigned int & OutVariable, const char *pModulo,const char *pVariable,const unsigned int & defaultVariable = 0, bool setVariable = false);
	static bool  VariableInt(short & OutVariable, const char *pModulo,const char *pVariable,const short & defaultVariable, bool setVariable = false);
	static bool  VariableInt(unsigned short & OutVariable, const char *pModulo,const char *pVariable,const unsigned short & defaultVariable = 0, bool setVariable = false);
	static bool  VariableInt(unsigned char & OutVariable, const char *pModulo,const char *pVariable,const unsigned char & defaultVariable = 0, bool setVariable = false);

	static bool  Variablebool(bool & OutVariable, const char *pModulo, const char *pVariable, const bool & defautVariable = 0, bool setVariable = false);
	static bool  SetVariable(const char * pModule, const char * pRoute, const char * pValue);
	static bool  SetVariable(const char * pModule, const char * pRoute, int nValor);


	static bool ExportConfigFile();


	static bool enumerateValues(const char* pModule, std::map<CFwkString, CFwkString>& mapVar);
	static bool enumerateValuesDyn(const char* pModule, CDynamicStruct* pDyn);

private:

	static bool SetDefaultChar(const CFwkString & fstrDefaultVariable, const char * pModulo, const char *pVariable);
	static bool SetDefaultInt(const long & defaultVariable, const char *pModulo, const char *pVariable);

	static bool getIsActive();
	static ACE_Configuration* m_pConfig;
	static ACE_Configuration_Section_Key* m_pRootKey;

	static CFwkString m_fstrConfigFileName;
	static bool m_bIsFileActive;
};
