
#include "SmallConfiguration.h"
#include "CommonHeaders/six_fwk_definitions.h"
#include "FrameworkUtilities/DynamicStruct.h"

#include "ace/Configuration.h"
#include "ace/Configuration_Import_Export.h"
#include "ace/Recursive_Thread_Mutex.h"

CFwkString CSmallConfiguration::m_fstrConfigFileName;
bool CSmallConfiguration::m_bIsFileActive = false;
ACE_Configuration* CSmallConfiguration::m_pConfig = 0;
ACE_Configuration_Section_Key* CSmallConfiguration::m_pRootKey = 0;

ACE_Recursive_Thread_Mutex g_configMutex;

CSmallConfiguration::CSmallConfiguration(void)
{
}

CSmallConfiguration::~CSmallConfiguration(void)
{
	if(m_pConfig)
		delete m_pConfig;
}

bool CSmallConfiguration::InitConfiguration(const CFwkString& fstrConfigFilename)
{
	ACE_Guard<ACE_Recursive_Thread_Mutex> mutex(g_configMutex);

	if(m_pConfig != 0 || m_pRootKey != 0)
		return false;

	m_pRootKey = new ACE_Configuration_Section_Key();

	ACE_Configuration_Heap *pTempConfig = new ACE_Configuration_Heap();
	if (-1 == pTempConfig->open())
	{
		delete pTempConfig;
		return false;
	}
	m_pConfig = pTempConfig;

	if(LoadConfiguration(fstrConfigFilename) == 0)
		m_bIsFileActive = true;

	return m_bIsFileActive;
}


short CSmallConfiguration::LoadConfiguration(const CFwkString & fstrConfigFilename)
{
	ACE_Guard<ACE_Recursive_Thread_Mutex> mutex(g_configMutex);

	if(!m_pConfig)
		return -1;

	if(fstrConfigFilename.IsEmpty())
	{
		printf("LoadConfiguration target file empty\n");
		return -2;
	}

	m_fstrConfigFileName = fstrConfigFilename;

	ACE_Ini_ImpExp importExport(*m_pConfig);
	int rc = importExport.import_config ( ACE_TEXT (m_fstrConfigFileName.GetBuffer()) );

	if(rc != 0)
	{
		printf("LoadConfiguration Can not open file %s\n", (const char*)m_fstrConfigFileName);
		return -3;
	}

	*m_pRootKey = m_pConfig->root_section(); 

	printf("LoadConfiguration success. Using file %s\n",(const char*)m_fstrConfigFileName);
	return 0;
}

int CSmallConfiguration::ReloadConfiguration()
{
	ACE_Guard<ACE_Recursive_Thread_Mutex> mutex(g_configMutex);

	printf("ReloadConfiguration target file [%s]\n", m_fstrConfigFileName.GetBuffer());

	if(m_pConfig != 0)
		delete(m_pConfig);
	if(m_pConfig != 0)
		delete(m_pRootKey);
	m_pConfig = 0;

	CFwkString strActualFile = m_fstrConfigFileName;
	if(InitConfiguration(strActualFile))
		return 0;
	
	return 1;
}


bool CSmallConfiguration::EndConfiguration()
{
	if(m_pConfig != 0)
		delete(m_pConfig);
	if(m_pRootKey != 0)
		delete(m_pRootKey);

	return true;
}

bool CSmallConfiguration::VariableChar(CFwkString &OutVariable, const char *pModulo, const char *pVariable,
										const CFwkString & fstrDefaultVariable, bool setVariable)
{
	ACE_Guard<ACE_Recursive_Thread_Mutex> mutex(g_configMutex);

	if (!m_pConfig)
		return false;

	ACE_Configuration::VALUETYPE originalType;
	ACE_Configuration_Section_Key section;

	if(m_pConfig->open_section(*m_pRootKey, pModulo, 1, section))
	{
		if(setVariable)
		{
			OutVariable = fstrDefaultVariable;
			SetDefaultChar(fstrDefaultVariable, pModulo, pVariable);
		}
		return false;
	}

	if (m_pConfig->find_value (section, pVariable, originalType) != 0)
	{
		if( setVariable )
		{
			OutVariable = fstrDefaultVariable;
			SetDefaultChar(fstrDefaultVariable, pModulo, pVariable);
		}
		return false;
	}

	switch(originalType)
	{
	case ACE_Configuration::STRING:
		{
			ACE_TString fromFileString;
			if (m_pConfig->get_string_value (section, pVariable, fromFileString) != 0)
			{
				return false;
			}
			OutVariable = fromFileString.c_str();
		}
		break;

	case ACE_Configuration::INTEGER:
		{
			int nValueRet = 0;
			if (m_pConfig->get_integer_value(section, pVariable, (u_int&)nValueRet) != 0)
			{
				return false;
			}
			OutVariable.sprintf("%d", nValueRet);
		}
		break;

	default:
		return false;
	}

	return true;
}


bool  CSmallConfiguration::VariableChar(char * OutVariable, unsigned int sizeOutVariable, const char * pModulo,
										const char * pVariable, const char * strDefaultVariable, bool setVariable)
{
 	CFwkString temp;
 	if(VariableChar(temp, pModulo, pVariable, strDefaultVariable, setVariable) )
 	{
 		if(temp.GetLength() < sizeOutVariable)
 			ACE_OS::strcpy(OutVariable, temp.GetBuffer());
 		else
 		{
 			ACE_OS::memcpy(OutVariable, temp.GetBuffer(), sizeOutVariable);
 			OutVariable[sizeOutVariable] = '\0';
 		}
 		return true;
 	}
	return false;
}

bool CSmallConfiguration::VariableInt(long & OutVariable, const char *pModulo, const char *pVariable,
									  const long & defaultVariable, bool setVariable)
{
	ACE_Guard<ACE_Recursive_Thread_Mutex> mutex(g_configMutex);

	if(!m_pConfig)
		return false;

	ACE_Configuration::VALUETYPE  originalType;
	ACE_Configuration_Section_Key section;

	if(m_pConfig->open_section (*m_pRootKey, pModulo, 1, section))
	{
		if(setVariable)
		{
			OutVariable = defaultVariable;
			SetDefaultInt(defaultVariable, pModulo, pVariable);
		}
		return false;
	}

	if(m_pConfig->find_value (section, pVariable, originalType) != 0)
	{
		if(setVariable)
		{
			OutVariable = defaultVariable;
			SetDefaultInt(defaultVariable,pModulo,pVariable);
		}
		return false;
	}

	switch(originalType)
	{
	case ACE_Configuration::STRING:
		{
			ACE_TString fromFileString;
			if (m_pConfig->get_string_value (section, pVariable, fromFileString) != 0)
			{
				return false;
			}
			int base = 10;
			if (fromFileString.find('x') != -1)
			{
				base = 16;
			}
			if (fromFileString.find("dword:") != -1)
			{
				size_t pos = fromFileString.find(":");
				fromFileString = fromFileString.substring( pos + 1 );
				base = 16;
			}
			OutVariable = (int)ACE_OS::strtol(fromFileString.c_str(), 0, base);
		}
		break;

	case ACE_Configuration::INTEGER:
		if (m_pConfig->get_integer_value(section, pVariable, (u_int&)OutVariable) != 0)
		{
			return false;
		}
	    break;

	default:
		return false;
	}

	return true;

}

bool CSmallConfiguration::VariableInt(unsigned long & OutVariable, const char *pModulo,
			const char *pVariable,const unsigned long & defaultVariable, bool setVariable)
{
	long iVar = OutVariable;
	bool nRet;
	nRet = VariableInt(iVar, pModulo, pVariable, defaultVariable, setVariable);
	OutVariable = iVar;
	return nRet;
}

bool CSmallConfiguration::VariableInt(int & OutVariable, const char *pModulo,
			const char *pVariable,const int & defaultVariable, bool setVariable)
{
	long iVar = OutVariable;
	bool nRet;
	nRet = VariableInt(iVar, pModulo, pVariable, defaultVariable, setVariable);
	OutVariable = iVar;
	return nRet;
}

bool CSmallConfiguration::VariableInt(unsigned int & OutVariable, const char *pModulo,
			const char *pVariable,const unsigned int & defaultVariable, bool setVariable)
{
	long iVar = OutVariable;
	bool nRet;
	nRet = VariableInt(iVar, pModulo, pVariable, defaultVariable, setVariable);
	OutVariable = iVar;
	return nRet;
}

bool CSmallConfiguration::VariableInt(unsigned char & OutVariable, const char *pModulo,
			const char *pVariable,const unsigned char & defaultVariable, bool setVariable)
{
	long iVar = OutVariable;
	bool nRet;
	nRet = VariableInt(iVar, pModulo, pVariable, defaultVariable, setVariable);
	OutVariable = (unsigned char)iVar;
	return nRet;
}

bool CSmallConfiguration::VariableInt(short & OutVariable, const char *pModulo,
			const char *pVariable,const short & defaultVariable, bool setVariable)
{
	long iVar = OutVariable;
	bool nRet;
	nRet = VariableInt(iVar, pModulo, pVariable, defaultVariable, setVariable);
	OutVariable = (short)iVar;
	return nRet;
}


bool CSmallConfiguration::VariableInt(unsigned short & OutVariable, const char *pModulo,
			const char *pVariable,const unsigned short & defaultVariable, bool setVariable)
{
	long iVar = OutVariable;
	bool nRet;
	nRet = VariableInt(iVar, pModulo, pVariable, defaultVariable, setVariable);
	OutVariable = (unsigned short)iVar;
	return nRet;
}

bool CSmallConfiguration::Variablebool(bool & OutVariable, const char *pModulo,
			 const char *pVariable,const bool & defaultVariable, bool setVariable)
{
	CFwkString fstrDefaultVariable;
	fstrDefaultVariable.sprintf("%d", defaultVariable);
	CFwkString temp;
	bool bRet = VariableChar(temp, pModulo, pVariable, fstrDefaultVariable, setVariable);
	if( !temp.IsEmpty() )
	{
		if(temp *= "false")
			OutVariable = false;
		else if(temp *= "0")
			OutVariable = false;
		else
			OutVariable = true;
	}
	return bRet;
}

bool CSmallConfiguration::SetVariable(const char * pModule, const char * pVariable, const char * pValue)
{
	ACE_Guard<ACE_Recursive_Thread_Mutex> mutex(g_configMutex);

	if (!m_pConfig)
		return false;

	ACE_Configuration_Section_Key section;

	if (m_pConfig->open_section(*m_pRootKey, ACE_TEXT (pModule), 1, section))
		return false;

	if (m_pConfig->set_string_value(section, ACE_TEXT (pVariable), ACE_TString (ACE_TEXT (pValue))))
		return false;

	return true;
}

bool CSmallConfiguration::SetVariable(const char * pModule, const char * pVariable, int nValue)
{
	ACE_Guard<ACE_Recursive_Thread_Mutex> mutex(g_configMutex);

	if (!m_pConfig)
		return false;

	ACE_Configuration_Section_Key section;

	if (m_pConfig->open_section (*m_pRootKey, ACE_TEXT (pModule), 1, section))
		return false;
 
	if (m_pConfig->set_integer_value (section, ACE_TEXT (pVariable), nValue))
		return false;

	return true;
}

bool CSmallConfiguration::SetDefaultChar(const CFwkString & fstrDefaultVariable, const char * pModulo, const char *pVariable)
{
	return SetVariable(pModulo, pVariable, fstrDefaultVariable);
}

bool CSmallConfiguration::SetDefaultInt(const long & defaultVariable, const char *pModulo,const char *pVariable)
{
	return SetVariable(pModulo, pVariable, defaultVariable);
}


bool CSmallConfiguration::ExportConfigFile( )
{
	return false;
}

bool CSmallConfiguration::enumerateValues(const char* pModule, std::map<CFwkString, CFwkString>& mapVar)
{
	ACE_Guard<ACE_Recursive_Thread_Mutex> mutex(g_configMutex);

	if(!m_pConfig)
		return true;

	ACE_Configuration_Section_Key section;

	if (m_pConfig->open_section(*m_pRootKey, pModule, 1, section))
		return false;

	ACE_TString aceVarName;
	ACE_TString aceVarValue;
	ACE_Configuration::VALUETYPE type;

	CFwkString varName;
	CFwkString varValue;

	bool bRes = false;
	int iIndex = 0;
	int iResult = m_pConfig->enumerate_values(section, iIndex, aceVarName, type);
	while(iResult == 0)
	{
		bRes = true;

		if(type == ACE_Configuration::STRING)
		{
			if(m_pConfig->get_string_value (section, aceVarName.c_str(), aceVarValue) != 0)
				continue;

		}	
		varName = aceVarName.c_str();
		varValue = aceVarValue.c_str();
		mapVar[varName] = varValue;
		iIndex++;
		iResult = m_pConfig->enumerate_values(section, iIndex, aceVarName, type);
	}
	
	return bRes;
}

bool CSmallConfiguration::enumerateValuesDyn(const char* pModule, CDynamicStruct* pDyn)
{
	ACE_Guard<ACE_Recursive_Thread_Mutex> mutex(g_configMutex);

	if(!m_pConfig)
		return true;

	ACE_Configuration_Section_Key section;

	if (m_pConfig->open_section (*m_pRootKey, pModule, 1, section))
		return false;

	ACE_TString aceVarName;
	ACE_TString aceVarValue;
	ACE_Configuration::VALUETYPE type;

	CFwkString varName;
	CFwkString varValue;
	unsigned int iValue = 0;
	bool bRes = false;
	int iIndex = 0;
	int iResult = m_pConfig->enumerate_values(section, iIndex, aceVarName, type);
	while(iResult == 0)
	{
		varName = aceVarName.c_str();
		varName.Replace("{", "[", true);
		varName.Replace("}", "]", true);
		bRes = true;
		switch(type)
		{
		case ACE_Configuration::STRING:
			if(m_pConfig->get_string_value(section, aceVarName.c_str(), aceVarValue) != 0)
				continue;	
			if (aceVarValue.find("int:") != -1)
			{
				size_t pos = aceVarValue.find(":");
				aceVarValue = aceVarValue.substring( pos + 1 );
				SIX_FWK_INT64 lVal = STR_TO_LL(aceVarValue.c_str(), 0, 10);
				pDyn->AddInt(varName, lVal);
			}
			else if(aceVarValue.find("buff:") != -1)
			{
				size_t pos = aceVarValue.find(":");
				aceVarValue = aceVarValue.substring( pos + 1 );
				varValue = aceVarValue.c_str();
				pDyn->AddBufferFromString(varName, varValue);
			}
			else
			{
				varValue = aceVarValue.c_str();
				pDyn->AddString(varName, varValue);
			}
			break;

		case ACE_Configuration::INTEGER:
			if(m_pConfig->get_integer_value(section, aceVarName.c_str(), iValue) != 0)
				continue;	
			pDyn->AddInt(varName, (const int)iValue);
			break;
		
		default:
			continue;
		}

		iIndex++;
		iResult = m_pConfig->enumerate_values(section, iIndex, aceVarName, type);
	}

	return bRes;
}



