
#include "JanusFileModule.h"
#include "JanusFile.h"

class CJanusFileModule::CJanusFileModuleDP
{
public:
	CJanusFileModuleDP(){};
	~CJanusFileModuleDP(){};

	 TFactory<CJanusFile, IJanusFile> m_factFile;
};

CJanusFileModule::CJanusFileModule(): m_factoryDP(new CJanusFileModuleDP)
{
	m_factoryDP->m_factFile.SetFactName("CJanusFileModule");
}

CJanusFileModule::~CJanusFileModule()
{
}

IJanusFile* CJanusFileModule::GetResource()
{
	return m_factoryDP->m_factFile.GetResource();
}

IJanusFile* CJanusFileModule::CreateInstance()
{
	return new CJanusFile;
}

bool CJanusFileModule::FreeResource(IJanusFile* pObj)
{
	pObj->Close();
	return m_factoryDP->m_factFile.FreeResource(pObj);
}	
