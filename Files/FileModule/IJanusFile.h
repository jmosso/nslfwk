#pragma once

#include <cstddef>
#include <fcntl.h>

class IJanusFile
{
public:
	IJanusFile(){;}
	virtual ~IJanusFile(){;}

	/// Modos de Acceso 
    enum OpenFlag
	{ 
#ifdef WIN32
		RDONLY = 0x0000,	///< El archivo se abre o crea como solo escritura.
        WRONLY = 0x0001,	///< El archivo se abre o crea como solo lectura.
        RDWR   = 0x0002,	///< El archivo se abre o crea para escritura o lectura.
		CREAT  = 0x0100,		///< El archivo se crea si no existe.
		TRUNK  = 0x0200,		///< open and truncate
		WPLUS  = 0x0202,		//Opens an empty file for both reading and writing. If the given file exists, its contents are destroyed.
		APPEND = 0x0008		// writes done at eof */
#else // linux				
		CREAT =	O_CREAT,
  		RDWR   = O_RDWR,
		WRONLY = O_WRONLY,
		RDONLY = O_RDONLY,
		TRUNK  = O_TRUNC,
		APPEND = O_APPEND,
//		BINARY = O_BINARY,
  		WPLUS  = TRUNK + RDWR
#endif //WIN32				
	};  

	enum MoveMode
	{
		BEGIN	= 0,	///< A partir del comienzo del archivo.
		CURRENT = 1,	///< A partir de la posici�n actual del apuntador.
		END		= 2		///< A partir del final del archivo.
	};

	virtual short Open(const char* strFileName, int iFlags = RDWR|CREAT) = 0;
	virtual short Close() = 0;
	virtual bool IsOpen() const = 0;

	virtual size_t Read(char * pBuffer, size_t iLength) const = 0;
	virtual size_t Write(const char * pBuffer, size_t iLength) = 0;
	virtual size_t Seek(size_t iOffSet, int iPos = CURRENT) = 0;
    virtual size_t GetLength() = 0;
};
