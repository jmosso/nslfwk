#pragma once

#include "IJanusFile.h"

class ACE_FILE_IO;
class ACE_FILE_Connector;
class CJanusFile : public IJanusFile
{
public:
	CJanusFile();
	virtual ~CJanusFile();

	short Close();
	short Open(const char* strFileName, int iFlags);
	bool IsOpen() const;

	size_t Read(char * pBuffer, size_t iLength) const;
	size_t Write(const char * pBuffer, size_t iLength);

	size_t Seek(size_t iOffSet, int iPos = CURRENT);
	size_t GetLength();

private:

	enum FileState
	{
		NONE,
		IDLE,
		ACTIVE
	}m_nState;

	int m_nOpenedFlag;

	ACE_FILE_IO* m_pACEFile;
	ACE_FILE_Connector* m_pACEConnector;
};
