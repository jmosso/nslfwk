# Create a library called "Hello" which includes the source file "hello.cxx".
# The extension is already found.  Any number of sources could be listed here.

SET( LIBNAME FileModule )


include_directories (
	${FRAMEWORK_ROOT}
	${ACE_INCLUDE_DIR}
	)
    
EXEC_PROGRAM( lsb_release ARGS -rs OUTPUT_VARIABLE RHEL_VERSION )
STRING( REGEX REPLACE "([0-9])..*" "\\1" RHEL_MAJOR_VERSION "${RHEL_VERSION}" )
IF( RHEL_MAJOR_VERSION EQUAL "6" )
   EXEC_PROGRAM( uname ARGS -m OUTPUT_VARIABLE ARCH )
   INCLUDE_DIRECTORIES( /usr/lib/${ARCH}-redhat-linux5E/include )
ENDIF( RHEL_MAJOR_VERSION EQUAL "6" )


add_library (${LIBNAME}
		JanusFile.cpp
        xDRFile.cpp
		JanusFileModule.cpp
		)

SET_TARGET_PROPERTIES( ${LIBNAME} PROPERTIES SKIP_BUILD_RPATH true)

SET_TARGET_PROPERTIES( ${LIBNAME} PROPERTIES VERSION ${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH} SOVERSION ${CPACK_PACKAGE_VERSION_MAJOR} )
		
INSTALL( TARGETS ${LIBNAME}
         DESTINATION ${SIXLABS_DIR}/lib/nslFwk )

INSTALL( FILES
		JanusFileModule.h
        IJanusFile.h
        xDRFile.h
        DESTINATION ${SIXLABS_DIR}/include/nslFwk/${LIBNAME} )
