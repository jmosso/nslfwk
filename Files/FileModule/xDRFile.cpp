
#include "xDRFile.h"
#include "Utilities/utilities.h"
#include "FrameworkUtilities/FwkTime.h"
#include "JanusFileModule.h"
#include "ace/OS_NS_time.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CxDRFile::CxDRFile()
{
	m_LogFile = 0;
	m_iActualSize = 0;
}

CxDRFile::~CxDRFile()
{
	if(CJanusFileModule::St_GetInstance())
	{	
		if (m_LogFile)
		{
			m_LogFile->Close();
			CJanusFileModule::St_GetInstance()->FreeResource(m_LogFile);
			m_LogFile = 0;
		}
	}
}

bool CxDRFile::InitFile(const char* strPrefifix, const char* strLocalPath, const char* strHeader, const char* strRootPath, size_t iMaxSize, const char* strExt)
{
	m_fstPrefix = strPrefifix;
	m_fstLocalPath = strLocalPath;
	m_fstHeader = strHeader;
	m_fstRootPath = strRootPath;
	m_iMaxSize = iMaxSize;
	m_fstExt = strExt;

	return(CreateNewFile());
}

bool CxDRFile::CreateNewFile()
{
	m_iActualSize = 0;
	CFwkString fstrName;

	time(&m_tomorrowDate);
	struct tm date;
	ACE_OS::localtime_r(&m_tomorrowDate, &date);
	m_tomorrowDate = m_tomorrowDate + 86400 - (date.tm_sec + date.tm_min*60 + date.tm_hour*3600);

	time_t now = time(NULL);
	char strFileDate[50];
	strftime(strFileDate, 50, "%Y_%m_%d", localtime(&now));

	fstrName.sprintf("%s/%s/%s", m_fstRootPath.GetBuffer(), m_fstLocalPath.GetBuffer(), strFileDate);
	if(CreatePathDirectory(fstrName) == false)
		return false;

	char strTime[50];
	strftime(strTime, 50, "%H_%M_%S", localtime(&now));

	CFwkString fstrFullPath;
	fstrFullPath.sprintf("%s/%s%s.%s", fstrName.GetBuffer(),  m_fstPrefix.GetBuffer(), strTime, m_fstExt.GetBuffer()); 
	
	IJanusFile* pFile = CJanusFileModule::St_GetInstance()->GetResource();
	
	if(pFile != 0) 
	{
		if(pFile->Open(fstrFullPath, IJanusFile::WPLUS  | IJanusFile::CREAT ) == 0)//quiero que se borre si existe
		{
			m_LogFile = pFile;
		}
		else
		{
			pFile->Close();
			CJanusFileModule::St_GetInstance()->FreeResource(pFile);
		}
	}


	if(m_fstHeader.IsEmpty() == false)
		Write(m_fstHeader, m_fstHeader.GetLength());

	return (m_LogFile != 0);
}

void CxDRFile::CloseFile()
{
	m_iActualSize = 0;
	if(m_LogFile)
	{
		m_LogFile->Close();
		CJanusFileModule::St_GetInstance()->FreeResource(m_LogFile);
	}
	m_LogFile = 0;

}

bool CxDRFile::Write(const char* strBuffer, size_t iLen)
{
	if(m_LogFile == 0)
		return false;

	if(strBuffer == 0)
		return false;

	m_iActualSize += iLen;

	time_t auxTime;
	time(&auxTime);

	if(auxTime > m_tomorrowDate || (m_iMaxSize != -1 && m_iActualSize > m_iMaxSize))
	{
		CloseFile();
		if(CreateNewFile() == false)
			return false;
	}

	m_LogFile->Write((const char*)strBuffer, iLen);
	return true;
}

