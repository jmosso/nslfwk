#pragma once

#include "Templates/TFactory.h"
#include "Templates/TSingleton.h"
#include "IJanusFile.h"

class CJanusFileModule : public TSingleton<CJanusFileModule>
{
	class CJanusFileModuleDP;

public:
	CJanusFileModule();
	virtual ~CJanusFileModule();

	IJanusFile* GetResource();
	bool FreeResource(IJanusFile* pObj);

private:
	CJanusFileModuleDP* m_factoryDP;
	IJanusFile* CreateInstance();
};
