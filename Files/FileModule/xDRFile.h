#pragma once
#include "FrameworkUtilities/FwkString.h"
#include <time.h>

class IJanusFile;
class CxDRFile  
{
public:
	CxDRFile();
	virtual ~CxDRFile();

	bool InitFile(const char* strPrefifix, const char* strLocalPath, const char* strHeader, const char* strRootPath , size_t iMaxSize, const char* strExt = "log");
	bool Write(const char* strBuffer, size_t iLen);
	void CloseFile();

private:

	CFwkString m_fstPrefix;
	CFwkString m_fstLocalPath;
	CFwkString m_fstRootPath;
	CFwkString m_fstHeader;
	CFwkString m_fstExt;
	size_t m_iMaxSize;
	time_t m_tomorrowDate;

	bool CreateNewFile();

	IJanusFile* m_LogFile;
	size_t m_iActualSize;
};

