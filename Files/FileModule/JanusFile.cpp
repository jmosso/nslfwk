
#include "JanusFile.h"
#include "ace/FILE_Connector.h"
#include "ace/FILE_IO.h"

CJanusFile::CJanusFile(): m_nState(NONE)
						, m_nOpenedFlag(0x1000)
						, m_pACEFile(new ACE_FILE_IO)
						, m_pACEConnector(new ACE_FILE_Connector)
{
}

CJanusFile::~CJanusFile()
{
	if(m_pACEFile != 0)
		delete(m_pACEFile);
	m_pACEFile = 0;
	if(m_pACEConnector != 0)
		delete(m_pACEConnector);
	m_pACEConnector = 0;
}

short CJanusFile::Open(const char* strFileName, int iFlags)
{
	m_nOpenedFlag = iFlags;

	short nRet;
	switch(m_nState)
	{
	case NONE:
	case IDLE:
		if(m_pACEConnector->connect(*m_pACEFile, ACE_FILE_Addr(strFileName), 0, 
									ACE_Addr::sap_any, 0, iFlags, ACE_DEFAULT_FILE_PERMS) == -1)
		{
			nRet = -1;
			break;
		}
		m_nState = ACTIVE;
		nRet = 0;
		break;

	case ACTIVE:
		nRet = 0;
		break;

	default:
		nRet = -1;
		break;
	}

	return nRet;
}

short CJanusFile::Close()
{
	m_nOpenedFlag = 0x1000;
	short nRet = 0;

	switch(m_nState)
	{
	case ACTIVE:
		if(m_pACEFile->close() == -1)
		{
			// manejar el error
			return -1;
		}

		m_nState = IDLE;
		nRet = 0;
		break;

	case IDLE:
	case NONE:
		nRet = 0;
		break;

	default:
		nRet = -1;
		break;
	}

	return nRet;
}

bool CJanusFile::IsOpen() const
{
	return (m_nState == ACTIVE);
}

size_t CJanusFile::Read(char* pBuffer, size_t iLength) const
{
	size_t nRet;
	switch(m_nState)
	{
	case ACTIVE:
		if((nRet = m_pACEFile->recv(pBuffer, iLength)) == -1)
		{
			// manejar el error
			return 0;
		}
		break;

	default:
		nRet = 0;
		break;
	}

	return nRet;
}

size_t CJanusFile::Write(const char * pBuffer, size_t iLength)
{
	size_t nRet;
	switch(m_nState)
	{
	case ACTIVE:
		if((nRet = m_pACEFile->send(pBuffer, iLength)) == -1)
		{
			// manejar el error
			return 0;
		}
		break;

	default:
		nRet = 0;
		break;
	}

	return nRet;
}

size_t CJanusFile::Seek(size_t iOffSet, int iPos)
{
	short nRet;

	switch(m_nState)
	{
	case ACTIVE:
		if(nRet = m_pACEFile->seek(iOffSet, iPos))
		{
			// manejar el error
			return 0;
		}

		break;

	default:
		nRet = 0;
		break;
	}

	return nRet;
}

size_t CJanusFile::GetLength()
{
	ACE_FILE_Info info;
	m_pACEFile->get_info(info);
	return info.size_;
}
