#----------------------- Determina el sistema operativo ------------------------
# First determine if we're using rh/rhel/fc
# *FIXME* CentOS and friends will *not* be recognized as RHEL
%define sb_disthandle  %(if [ -n "$(grep -i fedora /etc/redhat-release 2> /dev/null)" ]; then echo fc; else if [ -n "$(grep -i enterprise /etc/redhat-release 2> /dev/null)" ]; then echo el; else echo rh; fi; fi;)

# Determine release major for tagging
%define sb_distmajor   %(sed -e 's/.*release \\([0-9]\\).*/\\1/' < /etc/redhat-release)

# Complete dist identifier
%define dist	%{sb_disthandle}%{sb_distmajor}
#-------------------------------------------------------------------------------

#---------- Change __spec_install_port definition to avoid stripping -----------
%define __spec_install_post /usr/lib/rpm/brp-compress
#-------------------------------------------------------------------------------


#------------------ Definicion de Tags -----------------------------------------
Summary: asn1 Codec library used for Sixlabs products.
Name: @RPM_NAME@
Distribution: Linux Redhat, Inc
Version: 1.2.0
Release: 0.%{dist}
License: Commercial
Vendor: Sixlabs
URL: http://www.sixlabs.cl
Packager: %{packager}
Group: Development/Libraries
BuildRoot: %{_topdir}/BUILD/%{name}-%{version}-%{release}-root
Requires: activemq-cpp >= 3.1.2
BuildRequires: activemq-cpp-devel SIX_common-devel

%description
Library which provides codec to asn1.
#---------------------------------------------------------------------------------

%package devel

Requires: %{name} = %{version}
Group:    Development/Libraries
Summary:  Header files and static libraries for @TARGET_NAME@ library

%description devel
Header files you can use to develop this library with C++.


#------------------- Definicion de Script -----------------------------------------
%prep
rm -rf $RPM_BUILD_DIR/@RPM_NAME@
mkdir $RPM_BUILD_DIR/@RPM_NAME@

%build
cd $RPM_BUILD_DIR/@RPM_NAME@
cmake @RPM_CMAKE_ARGS@ $RPM_SOURCE_DIR/@RPM_NAME@
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/etc/ld.so.conf.d/
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/CommChannel
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/CommonHeaders
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Files
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Files/FileModule
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/FrameworkUtilities
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Gestion
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Gestion/GestionSNMP
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/HMPClient
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Interfaces
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/ModuloConsTablas
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/SigAS_IConnector
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/SmallConfiguration
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/SocketRPC
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/ThreadModule
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/TimerModule
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Utilities
mkdir -p $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates

echo "/opt/sixlabs/@LIB@/nslFwk" > $RPM_BUILD_ROOT/etc/ld.so.conf.d/nslFwk.@RPM_PROCESSOR_BITS@.conf

install -m 755 $RPM_BUILD_DIR/@RPM_NAME@/temp/CommChannel/libCommChannel.a $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk/
install -m 755 $RPM_BUILD_DIR/@RPM_NAME@/temp/FileModule/libFileModule.a $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk/
install -m 755 $RPM_BUILD_DIR/@RPM_NAME@/temp/FrameworkUtilities/libFrameworkUtilities.a $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk/
install -m 755 $RPM_BUILD_DIR/@RPM_NAME@/temp/GestionSNMP/libGestionSNMP.a $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk/
install -m 755 $RPM_BUILD_DIR/@RPM_NAME@/temp/HMPClient/libHMPClient.a $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk/
install -m 755 $RPM_BUILD_DIR/@RPM_NAME@/temp/ModuloConsTablas/libModuloConsTablas.a $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk/
install -m 755 $RPM_BUILD_DIR/@RPM_NAME@/temp/SigAS_IConnector/libSigAS_IConnector.a $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk/
install -m 755 $RPM_BUILD_DIR/@RPM_NAME@/temp/SmallConfiguration/libSmallConfiguration.a $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk/
install -m 755 $RPM_BUILD_DIR/@RPM_NAME@/temp/SocketRPC/libSocketRPC.a $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk/
install -m 755 $RPM_BUILD_DIR/@RPM_NAME@/temp/ThreadModule/libThreadModule.a $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk/
install -m 755 $RPM_BUILD_DIR/@RPM_NAME@/temp/TimerModule/libTimerModule.a $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk/
install -m 755 $RPM_BUILD_DIR/@RPM_NAME@/temp/Utilities/libUtilities.a $RPM_BUILD_ROOT/opt/sixlabs/@LIB@/nslFwk/

install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/CommChannel/ISixCommChannel.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/CommChannel
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/CommChannel/SixCommChannelModule.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/CommChannel
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/CommChannel/SGenericParamFullNames.hpp $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/CommChannel
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/CommonHeaders/six_fwk_definitions.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/CommonHeaders
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/CommonHeaders/payloadtype.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/CommonHeaders
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Files/FileModule/JanusFileModule.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Files/FileModule
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Files/FileModule/IJanusFile.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Files/FileModule
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Files/FileModule/xDRFile.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Files/FileModule
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/FrameworkUtilities/DynamicStruct.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/FrameworkUtilities
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/FrameworkUtilities/FwkBuffer.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/FrameworkUtilities
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/FrameworkUtilities/FwkRandom.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/FrameworkUtilities
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/FrameworkUtilities/FwkString.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/FrameworkUtilities
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/FrameworkUtilities/FwkStringFactory.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/FrameworkUtilities
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/FrameworkUtilities/FwkTime.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/FrameworkUtilities
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Gestion/GestionSNMP/IGestionSNMP.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Gestion/GestionSNMP

install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/HMPClient/SixHMPFactory.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/HMPClient
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/HMPClient/ISixHMPClient.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/HMPClient

install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Interfaces/IThreadFactory.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Interfaces
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/ModuloConsTablas/ModConsTabla.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/ModuloConsTablas
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/ModuloConsTablas/InterfazTab.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/ModuloConsTablas
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/SigAS_IConnector/ISAS_Connector.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/SigAS_IConnector
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/SigAS_IConnector/SAS_ConnFactory.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/SigAS_IConnector
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/SmallConfiguration/SmallConfiguration.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/SmallConfiguration
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/SocketRPC/SocketRPCModule.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/SocketRPC
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/SocketRPC/IRPCClient.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/SocketRPC
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/SocketRPC/IRPCServer.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/SocketRPC
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/ThreadModule/IThread.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/ThreadModule
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/ThreadModule/Thread.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/ThreadModule
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/ThreadModule/IThreadMessageCB.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/ThreadModule
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/ThreadModule/IThreadParams.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/ThreadModule
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/ThreadModule/ThreadFactory.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/ThreadModule
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/TimerModule/ITimerCB.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/TimerModule
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/TimerModule/TimerFactory.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/TimerModule
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Utilities/ThreadUtilities.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Utilities
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Utilities/Tracing.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Utilities
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Utilities/utilities.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Utilities
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/ITFactory.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TCommon.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TDrawer.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TFactory.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TFactoryNavigator.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TIdObjectMap.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TManageResourceFast.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TModule.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TModuleBase.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TModuleBusChannel.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TModuleBusChannelCB.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TModuleBusChannelSingleton.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TModuleBusChannelSingletonCB.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TSingleton.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates
install -m 644 $RPM_SOURCE_DIR/@RPM_NAME@/Templates/TStore.h $RPM_BUILD_ROOT/opt/sixlabs/include/nslFwk/Templates


%pre
if ! `grep "^sixlabs:" /etc/group > /dev/null`; #searchs for group 'sixlabs'
        then
        #create group
        echo "*** Creating group 'sixlabs' ***";
        /usr/sbin/groupadd sixlabs;
fi;

if ! `grep "^sixlabs:" /etc/passwd > /dev/null`; #searchs for username 'sixlabs'
        then
        #create user
        echo "*** Creating user 'sixlabs' ***";
        if ! `/usr/sbin/useradd -u 266 -d /opt/sixlabs -s /sbin/nologin -M sixlabs -g sixlabs 2> /dev/null` ; #create user with userId = 266
                then #creation failed
                #create user with no uid
                /usr/sbin/useradd -d /opt/sixlabs -s /sbin/nologin -M sixlabs -g sixlabs;
        fi;
fi;


%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%clean
rm -rf $RPM_BUILD_DIR/@RPM_NAME@
rm -rf $RPM_BUILD_ROOT

%files
%defattr(775,sixlabs,sixlabs,775)
# %dir /opt/sixlabs
# %dir /opt/sixlabs/@LIB@/nslFwk
/etc/ld.so.conf.d/nslFwk.@RPM_PROCESSOR_BITS@.conf
/opt/sixlabs/@LIB@/nslFwk/libCommChannel.a
/opt/sixlabs/@LIB@/nslFwk/libFileModule.a
/opt/sixlabs/@LIB@/nslFwk/libFrameworkUtilities.a
/opt/sixlabs/@LIB@/nslFwk/libGestionSNMP.a
/opt/sixlabs/@LIB@/nslFwk/libHMPClient.a
/opt/sixlabs/@LIB@/nslFwk/libModuloConsTablas.a
/opt/sixlabs/@LIB@/nslFwk/libSigAS_IConnector.a
/opt/sixlabs/@LIB@/nslFwk/libSmallConfiguration.a
/opt/sixlabs/@LIB@/nslFwk/libSocketRPC.a
/opt/sixlabs/@LIB@/nslFwk/libThreadModule.a
/opt/sixlabs/@LIB@/nslFwk/libTimerModule.a
/opt/sixlabs/@LIB@/nslFwk/libUtilities.a


%files devel
%defattr(775,sixlabs,sixlabs,775)
# %dir /opt/sixlabs
# %dir /opt/sixlabs/@LIB@/nslFwk
# %dir /opt/sixlabs/include/nslFwk
/etc/ld.so.conf.d/nslFwk.@RPM_PROCESSOR_BITS@.conf
/opt/sixlabs/@LIB@/nslFwk/libCommChannel.a
/opt/sixlabs/@LIB@/nslFwk/libFileModule.a
/opt/sixlabs/@LIB@/nslFwk/libFrameworkUtilities.a
/opt/sixlabs/@LIB@/nslFwk/libGestionSNMP.a
/opt/sixlabs/@LIB@/nslFwk/libHMPClient.a
/opt/sixlabs/@LIB@/nslFwk/libModuloConsTablas.a
/opt/sixlabs/@LIB@/nslFwk/libSigAS_IConnector.a
/opt/sixlabs/@LIB@/nslFwk/libSmallConfiguration.a
/opt/sixlabs/@LIB@/nslFwk/libSocketRPC.a
/opt/sixlabs/@LIB@/nslFwk/libThreadModule.a
/opt/sixlabs/@LIB@/nslFwk/libTimerModule.a
/opt/sixlabs/@LIB@/nslFwk/libUtilities.a


/opt/sixlabs/include/nslFwk/CommChannel/ISixCommChannel.h
/opt/sixlabs/include/nslFwk/CommChannel/SixCommChannelModule.h
/opt/sixlabs/include/nslFwk/CommChannel/SGenericParamFullNames.hpp
/opt/sixlabs/include/nslFwk/CommonHeaders/six_fwk_definitions.h
/opt/sixlabs/include/nslFwk/CommonHeaders/payloadtype.h
/opt/sixlabs/include/nslFwk/Files/FileModule/JanusFileModule.h
/opt/sixlabs/include/nslFwk/Files/FileModule/IJanusFile.h
/opt/sixlabs/include/nslFwk/Files/FileModule/xDRFile.h
/opt/sixlabs/include/nslFwk/FrameworkUtilities/DynamicStruct.h
/opt/sixlabs/include/nslFwk/FrameworkUtilities/FwkBuffer.h
/opt/sixlabs/include/nslFwk/FrameworkUtilities/FwkRandom.h
/opt/sixlabs/include/nslFwk/FrameworkUtilities/FwkString.h
/opt/sixlabs/include/nslFwk/FrameworkUtilities/FwkStringFactory.h
/opt/sixlabs/include/nslFwk/FrameworkUtilities/FwkTime.h
/opt/sixlabs/include/nslFwk/Gestion/GestionSNMP/IGestionSNMP.h

/opt/sixlabs/include/nslFwk/HMPClient/SixHMPFactory.h
/opt/sixlabs/include/nslFwk/HMPClient/ISixHMPClient.h

/opt/sixlabs/include/nslFwk/Interfaces/IThreadFactory.h
/opt/sixlabs/include/nslFwk/ModuloConsTablas/ModConsTabla.h
/opt/sixlabs/include/nslFwk/ModuloConsTablas/InterfazTab.h
/opt/sixlabs/include/nslFwk/SigAS_IConnector/ISAS_Connector.h
/opt/sixlabs/include/nslFwk/SigAS_IConnector/SAS_ConnFactory.h
/opt/sixlabs/include/nslFwk/SmallConfiguration/SmallConfiguration.h
/opt/sixlabs/include/nslFwk/SocketRPC/SocketRPCModule.h
/opt/sixlabs/include/nslFwk/SocketRPC/IRPCClient.h
/opt/sixlabs/include/nslFwk/SocketRPC/IRPCServer.h
/opt/sixlabs/include/nslFwk/ThreadModule/IThread.h
/opt/sixlabs/include/nslFwk/ThreadModule/Thread.h
/opt/sixlabs/include/nslFwk/ThreadModule/IThreadMessageCB.h
/opt/sixlabs/include/nslFwk/ThreadModule/IThreadParams.h
/opt/sixlabs/include/nslFwk/ThreadModule/ThreadFactory.h
/opt/sixlabs/include/nslFwk/TimerModule/ITimerCB.h
/opt/sixlabs/include/nslFwk/TimerModule/TimerFactory.h
/opt/sixlabs/include/nslFwk/Utilities/ThreadUtilities.h
/opt/sixlabs/include/nslFwk/Utilities/Tracing.h
/opt/sixlabs/include/nslFwk/Utilities/utilities.h

/opt/sixlabs/include/nslFwk/Templates/ITFactory.h
/opt/sixlabs/include/nslFwk/Templates/TCommon.h
/opt/sixlabs/include/nslFwk/Templates/TDrawer.h
/opt/sixlabs/include/nslFwk/Templates/TFactory.h
/opt/sixlabs/include/nslFwk/Templates/TFactoryNavigator.h
/opt/sixlabs/include/nslFwk/Templates/TIdObjectMap.h
/opt/sixlabs/include/nslFwk/Templates/TManageResourceFast.h
/opt/sixlabs/include/nslFwk/Templates/TModule.h
/opt/sixlabs/include/nslFwk/Templates/TModuleBase.h
/opt/sixlabs/include/nslFwk/Templates/TModuleBusChannel.h
/opt/sixlabs/include/nslFwk/Templates/TModuleBusChannelCB.h
/opt/sixlabs/include/nslFwk/Templates/TModuleBusChannelSingleton.h
/opt/sixlabs/include/nslFwk/Templates/TModuleBusChannelSingletonCB.h
/opt/sixlabs/include/nslFwk/Templates/TSingleton.h
/opt/sixlabs/include/nslFwk/Templates/TStore.h


%changelog

* Wed Dec 12 2018 John Mosso <john.mosso@sixbell.com>
- version 1.2.0
- Adicion libreria HMPClient

* Wed Dec 05 2018 John Mosso <john.mosso@sixbell.com>
- version 1.1.0
- Adicion de libreria SigAS_IConnector

* Mon Dec 03 2018 John Mosso <john.mosso@sixbell.com>
- version 1.0.2
- Cambios plantillas y ModConsTabla para soporte de integración con MR

* Thu Nov 22 2018 John Mosso <john.mosso@sixbell.com>
- version 1.0.1
- Cambio callback de hilos para manejar callback de herencia. 
- Adicion de archivo payloadtype.h


* Thu Oct 25 2018 John Mosso <john.mosso@sixbell.com>
- version 1.0.0
- Unificacion fwk controlnsl y mr


