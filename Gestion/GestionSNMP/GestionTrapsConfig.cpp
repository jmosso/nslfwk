
#include "GestionTrapsConfig.h"
#include "Utilities/Tracing.h"

CGestionTrapsConfig::CGestionTrapsConfig(void)
{
	m_pConfig = 0;
}

CGestionTrapsConfig::~CGestionTrapsConfig(void)
{
}

bool CGestionTrapsConfig::Init(const char* strFileName)
{
	ACE_Configuration_Heap *pTempConfig = 0;
	if(m_pConfig == 0)
	{
		pTempConfig = new ACE_Configuration_Heap();
		m_pConfig = pTempConfig;
	}

	if(m_pConfig != 0)
	{
		if (pTempConfig != 0 && !pTempConfig->open())
		{
		}

		// Imports
		ACE_Ini_ImpExp importExport (*m_pConfig);
		int iRet = importExport.import_config (ACE_TEXT (strFileName));

		if(iRet == 0)
		{
			m_RootKey = m_pConfig->root_section();			
			TRACEPF_DEBUG("", "CGestionTrapsConfig Usando archivo %s", strFileName);
			return true;
		}
		TRACEPF_ERROR("", "Cannot open %s", strFileName);
	}
	if(pTempConfig)
		delete pTempConfig;

	m_pConfig = 0;
	return false;
}

bool CGestionTrapsConfig::VariableChar(CFwkString &OutVariable,const char* pModulo, const char* pVariable)
{
	if (!m_pConfig)
		return false;		

	ACE_Configuration::VALUETYPE   originalType;
	ACE_Configuration_Section_Key section;

	if (m_pConfig->open_section (m_RootKey, pModulo, 1, section))
	{
		SIX_ASSERT(!"Section Not exist");
		TRACEPF_DEBUG("", "NO SE ENCONTRO el Modulo %s Variable %s",pModulo,pVariable);
		return false;
	}

	if (m_pConfig->find_value(section, pVariable, originalType) != 0)
	{
		//TRACEPF_DEBUG("", "NO SE ENCONTRO LA CLAVE Modulo %s Variable %s",pModulo,pVariable);
		return false;
	}

	switch(originalType)
	{
	case ACE_Configuration::STRING:
		{
			ACE_TString fromFileString;
			if (m_pConfig->get_string_value (section, pVariable, fromFileString) != 0)
				return false;

			OutVariable = fromFileString.c_str();
		}
		break;
	case ACE_Configuration::INTEGER:
		{
			int nValueRet = 0;
			if (m_pConfig->get_integer_value(section, pVariable,(u_int&)nValueRet) != 0)
				return false;

			OutVariable.sprintf("%d", nValueRet);
		}
		break;
	default:
		return false;
	}
	return true;
}

bool CGestionTrapsConfig::VariableInt(long& OutVariable, const char *pModulo, const char *pVariable)
{
	if (!m_pConfig)
		return false;		

	ACE_Configuration::VALUETYPE   originalType;
	ACE_Configuration_Section_Key section;

	if (m_pConfig->open_section (m_RootKey, pModulo, 1, section))
	{
		//TRACEPF_DEBUG("", "NO SE ENCONTRO LA CLAVE Modulo %s Variable %s",pModulo,pVariable);
		return false;
	}

	if (m_pConfig->find_value (section, pVariable, originalType) != 0)
	{
		//TRACEPF_DEBUG("", "NO SE ENCONTRO LA CLAVE Modulo %s Variable %s",pModulo,pVariable);
		return false;
	}
	switch(originalType)
	{
	case ACE_Configuration::STRING:
		{
			ACE_TString fromFileString;
			if (m_pConfig->get_string_value (section, pVariable, fromFileString) != 0)
				return false;

			int base = 10;
			if(fromFileString.find('x') != -1)
			{
				base = 16;				
			}
			OutVariable = (int)ACE_OS::strtol(fromFileString.c_str(), 0, base);
		}
		break;
	case ACE_Configuration::INTEGER:
		if (m_pConfig->get_integer_value(section, pVariable, (u_int&)OutVariable) != 0)
			return false;
		break;

	case ACE_Configuration::BINARY:
	default:
		return false;
	}
	return true;
}

bool CGestionTrapsConfig::BuildTrap(STRUCT_SNMP_EVENT& theEvent, const char* strKey, const char* strInfo, bool& bSend)
{
	theEvent.m_fstrFailure = strKey;
	theEvent.m_fstrText = strInfo;
	theEvent.m_iSpecificTrap = 19377;

	if(m_pConfig == 0)
		return true;

	long iValue = 0;
	CFwkString fstrValue;

	if(VariableInt(iValue, "TRAP_SPECIFIC", strKey) == true)
		theEvent.m_iSpecificTrap = iValue;

	if(VariableChar(fstrValue, "TRAP_FAILURE", strKey) == true)
	{
		if(fstrValue == "NULL")
			return false;

		theEvent.m_fstrFailure = fstrValue;
	}

	if(VariableInt(iValue, "TRAP_SEVERITY", strKey) == true)
		theEvent.m_sEventSeverity = (FWK_SNMP_EVENT_SEVERITY)((iValue >= 0 && iValue <= 5)?iValue:0);

	if(VariableInt(iValue, "TRAP_TYPE", strKey) == true)
		theEvent.m_sEventType = (FWK_SNMP_EVENT_TYPE)((iValue >= 0 && iValue <= 5)?iValue:0);

	if(VariableChar(fstrValue, "TRAP_LOCATION", strKey) == true)
		theEvent.m_fstrLocation = fstrValue;

	if(VariableChar(fstrValue, "TRAP_CAUSE", strKey) == true)
		theEvent.m_fstrCause = fstrValue;

	if(VariableChar(fstrValue, "TRAP_SOLUTION", strKey) == true)
		theEvent.m_fstrSolution = fstrValue;

	if(VariableChar(fstrValue, "TRAP_ACTION", strKey) == true)
		theEvent.m_fstrAction = fstrValue;

	if(theEvent.m_fstrText.IsEmpty() == true)
	{
		if(VariableChar(fstrValue, "TRAP_MORE_INFO", strKey) == true)
			theEvent.m_fstrText = fstrValue;
	}

	return true;
}
