
#pragma once

#include "FrameworkUtilities/FwkString.h"

enum FWK_SNMP_EVENT_TYPE
{
	FWK_SNMP_ET_INDETERMINADO,
	FWK_SNMP_ET_COMUNICACIONES,
	FWK_SNMP_ET_CALIDAD_SERVICIO,
	FWK_SNMP_ET_ERROR_PROCESAMIENTO,
	FWK_SNMP_ET_EQUIPO,
	FWK_SNMP_ET_ENTORNO
};

enum FWK_SNMP_EVENT_SEVERITY
{
	FWK_SNMP_ES_INDETERMINADO,
	FWK_SNMP_ES_ELIMINADO,
	FWK_SNMP_ES_AVISO,
	FWK_SNMP_ES_MENOR,
	FWK_SNMP_ES_MAYOR,
	FWK_SNMP_ES_CRITICA,
};

struct STRUCT_SNMP_EVENT
{
	int m_iSpecificTrap;

	FWK_SNMP_EVENT_SEVERITY m_sEventSeverity;
	FWK_SNMP_EVENT_TYPE m_sEventType;

	CFwkString m_fstrFailure;
	CFwkString m_fstrLocation;
	CFwkString m_fstrCause;
	CFwkString m_fstrSolution;
	CFwkString m_fstrAction;
	CFwkString m_fstrText;
	int		   m_iTries;

	STRUCT_SNMP_EVENT()
	{
		Clear();
	}

	void Clear()
	{
		m_iTries = 0;
		m_iSpecificTrap = 19377;
		m_sEventSeverity = FWK_SNMP_ES_INDETERMINADO;
		m_sEventType = FWK_SNMP_ET_INDETERMINADO;

		m_fstrFailure.Clear();
		m_fstrFailure.Clear();
		m_fstrLocation.Clear();
		m_fstrCause.Clear();
		m_fstrSolution.Clear();
		m_fstrAction.Clear();
		m_fstrText.Clear();
	}

	void CopyStrcut(const STRUCT_SNMP_EVENT* spEvent)
	{
		if(spEvent == 0)
			return;

		m_iTries = spEvent->m_iTries;
		m_iSpecificTrap = spEvent->m_iSpecificTrap;
		m_sEventSeverity = spEvent->m_sEventSeverity;
		m_sEventType = spEvent->m_sEventType;
		m_fstrFailure = spEvent->m_fstrFailure;
		m_fstrLocation = spEvent->m_fstrLocation;
		m_fstrCause = spEvent->m_fstrCause;
		m_fstrSolution = spEvent->m_fstrSolution;
		m_fstrAction = spEvent->m_fstrAction;
		m_fstrText = spEvent->m_fstrText;
	}

	STRUCT_SNMP_EVENT(const STRUCT_SNMP_EVENT& sEvent)
	{
		CopyStrcut(&sEvent);
	}

	STRUCT_SNMP_EVENT(const STRUCT_SNMP_EVENT* spEvent)
	{
		CopyStrcut(spEvent);
	}

	void operator = (const STRUCT_SNMP_EVENT& sEvent)
	{
		CopyStrcut(&sEvent);
	}

	void operator = (const STRUCT_SNMP_EVENT* spEvent)
	{
		CopyStrcut(spEvent);
	}
};

class IGestionSNMP  
{
public:

	IGestionSNMP();
	virtual ~IGestionSNMP();

	enum VERSION
	{
		SNMP_NO,
		SNMP_V1,
		SNMP_V2,
		SNMP_V3
	};

	static IGestionSNMP* Initialize(VERSION eVersion, const char* strAppName);
	void Delete();

	virtual int Init() = 0;
	virtual int Fini() = 0;
	virtual int Reload() = 0;
	virtual void SetRoot(const char* fstrRoot) = 0;
	virtual void SetAgent(const char* fstrAddress, const char* fstrComunity) = 0;
	virtual void EnableSNMPTrap(bool bEnableSNMPTrap) = 0;


	// Funciones SNMP

	virtual void SendSNMPTrap(
		FWK_SNMP_EVENT_SEVERITY eSeveridad,
		FWK_SNMP_EVENT_TYPE eTipoAlarma,
		int iSpecificTrap,
		const char* strFallaEspecifica,
		const char* strUbicacion,
		const char* strCausaProbable,
		const char* strSolucionPropuesta,
		const char* strAccionTomada,
		const char* strInfoAdicional
		) = 0;

	virtual void SendSNMPTrapEx(
		FWK_SNMP_EVENT_SEVERITY eSeveridad,
		FWK_SNMP_EVENT_TYPE eTipoAlarma,
		const char* strFallaEspecifica,
		const char* strUbicacion,
		const char* strCausaProbable,
		const char* strSolucionPropuesta,
		const char* strAccionTomada,
		const char* strInfoAdicional
		) = 0;
	
	virtual void SendSNMPTrapEx(
		STRUCT_SNMP_EVENT& sEvent,
		const char* strKey, 
		const char* InfoAditional, bool bSameThread = false) = 0;

	virtual void LoadAlarmMessageFile() = 0;

	static bool ReporteEvento(
								FWK_SNMP_EVENT_SEVERITY eSeveridad,
								FWK_SNMP_EVENT_TYPE eTipoAlarma,
								const char* strFallaEspecifica,
								const char* strUbicacion,
								const char* strCausaProbable,
								const char* strSolucionPropuesta,
								const char* strAccionTomada,
								const char* strInfoAdicional_format,...);

	static bool ReporteEventoSameThread(
								FWK_SNMP_EVENT_SEVERITY eSeveridad,
								FWK_SNMP_EVENT_TYPE eTipoAlarma,
								const char* strFallaEspecifica,
								const char* strUbicacion,
								const char* strCausaProbable,
								const char* strSolucionPropuesta,
								const char* strAccionTomada,
								const char* strInfoAdicional_format,...);

	static CFwkString ms_fstrAppName;

private:

	static IGestionSNMP* m_pGlobalTrapSender;
};

