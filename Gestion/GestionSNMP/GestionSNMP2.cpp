
#include "GestionSNMP2.h"

#ifdef WIN32
	#include "win32/net-snmp/net-snmp-config-win.h"
	#include "win32/net-snmp/net-snmp-includes-win.h"
#else
	#include "net-snmp/net-snmp-config.h"
	#include "net-snmp/net-snmp-includes.h"
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
oid             objid_sysuptime[] = { 1, 3, 6, 1, 2, 1, 1, 3, 0 };
oid             objid_snmptrapOID[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
oid             objid_snmptrapEnterprise[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

CGestionSNMP2::CGestionSNMP2()
{
	m_iSNMPVersion = SNMP_VERSION_2c;

}

CGestionSNMP2::~CGestionSNMP2()
{

}

void CGestionSNMP2::SendVSNMPTrap(STRUCT_SNMP_EVENT * pEvent)
{
	if(pEvent == 0)
		return;

	netsnmp_pdu    *pdu;
	oid             name[MAX_OID_LEN];
	size_t			name_length = MAX_OID_LEN;
	char			buffer[33];

    pdu = snmp_pdu_create(SNMP_MSG_TRAP2);

	unsigned long sysuptime = get_uptime();
	char csysuptime[20];
	sprintf(csysuptime, "%ld", sysuptime);

    snmp_add_var(pdu, objid_sysuptime, sizeof(objid_sysuptime) / sizeof(oid), 't', csysuptime);
	snmp_add_var(pdu, objid_snmptrapEnterprise, sizeof(objid_snmptrapEnterprise) / sizeof(oid), 'o', (const char*)m_fstrEnterpriseOID);
	
	CFwkString fstrTrapOID;
	// SpecificTrap: 0
	fstrTrapOID.sprintf("%s.%s.0", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
	if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
	{
		snmp_perror((const char*)m_fstrEnterpriseOID);
		return;
	}
	sprintf(buffer, "%d", pEvent->m_iSpecificTrap);
	if(snmp_add_var(pdu, name, name_length, 'i', buffer) != 0)
	{
		snmp_perror("SNMPv2-MIB::sysLocation.0");
		return;
	}

	// Falla
	if(m_lEnableFAILURE != 0)
	{
		fstrTrapOID.sprintf("%s.%s.1", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
		{
			snmp_perror((const char*)m_fstrEnterpriseOID);
			return;
		}
		if(snmp_add_var(pdu, name, name_length, 's', (const char*)pEvent->m_fstrFailure) != 0)
		{
			snmp_perror("SNMPv2-MIB::sysLocation.0");
			return;
		}
	}

	if(m_lEnableINFO != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.2", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;
		if(snmp_add_var(pdu, name, name_length, 's', pEvent->m_fstrText.GetBuffer()) != 0)
			return;
	}

	if(m_lEnableTYPE != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.3", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;

		sprintf(buffer, "%d", pEvent->m_sEventType);
		if(snmp_add_var(pdu, name, name_length, 'i', buffer) != 0)
			return;
	}

	// Severidad
	if(m_lEnableSEVERITY != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.4", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;

		sprintf(buffer, "%d", pEvent->m_sEventSeverity);
		if(snmp_add_var(pdu, name, name_length, 'i', buffer) != 0)
			return;
	}

	if(m_lEnableLOCATION != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.5", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;

		if(snmp_add_var(pdu, name, name_length, 's', pEvent->m_fstrLocation.GetBuffer()) != 0)
			return;
	}

	if(m_lEnableCAUSE != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.6", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;

		if(snmp_add_var(pdu, name, name_length, 's', pEvent->m_fstrCause.GetBuffer()) != 0)
			return;
	}

	if(m_lEnableSOLUTION != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.7", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;

		if(snmp_add_var(pdu, name, name_length, 's', pEvent->m_fstrSolution.GetBuffer()) != 0)
			return;
	}

	if(m_lEnableACTION != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.8", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;
		if(snmp_add_var(pdu, name, name_length, 's', pEvent->m_fstrAction.GetBuffer()) != 0)
			return;
	}

	BroadCastTrap(pdu);
}

