
#include "SmallConfiguration/SmallConfiguration.h"
#include "ThreadModule/ThreadFactory.h"
#include "Utilities/Tracing.h"
#include "SNMPBase.h"
#include "Files/FileModule/xDRFile.h"
#include "FrameworkUtilities/FwkTime.h"
#include "Utilities/utilities.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#define SNMP_TIME_OUT	1000
#define SNMP_RETRIES	3
#define DEFBUFFER		500

CxDRFile* CSNMPBase::ms_plogTraps = 0; 

NK_BEGIN_MESSAGE_MAP(CSNMPBase, IThreadMessageCB)
	ON_NKMESSAGE_POINTER(MSG_SEND_TRAP, ThrSendSNMPTrap);
	ON_NKMESSAGE(MSG_LOAD_SERVER_LIST, ThrLoadServerList);
NK_END_MESSAGE_MAP()

CSNMPBase::CSNMPBase()
{
	m_pThread = 0;
	m_bEnableSNMPTrap = true;
	m_strAgent[0] = '\0';
	m_strComunity[0] = '\0';
	m_strRoot[0] = '\0';

	m_lEnableSEVERITY = 1;
	m_lEnableTYPE = 1;
	m_lEnableFAILURE = 1;
	m_lEnableLOCATION = 1;
	m_lEnableCAUSE = 1;
	m_lEnableSOLUTION = 1;
	m_lEnableACTION = 1;
	m_lEnableINFO = 1;
	m_fstrEnterpriseOID = "1.3.6.1.4.1.19377";
	m_fstrTrapComunity = "public";
	m_fstrModuleOID = "1";
}

CSNMPBase::~CSNMPBase()
{

}

int CSNMPBase::Reload()
{
	LoadAlarmMessageFile();
	VARIABLECHAR(m_sSNMPServers, "Gestion", "SNMPServers");

	return 0;
}
void CSNMPBase::SetRoot(const char* fstrRoot)
{
	m_fstrMIBRoot = fstrRoot;
}

void CSNMPBase::SetAgent(const char* fstrAddress, const char* fstrComunity)
{
	memset(m_strAgent, 0, 16*sizeof(char));
	strncpy(m_strAgent, fstrAddress, 15);
	m_fstrAgent = m_strAgent;

	memset(m_strComunity, 0, 50*sizeof(char));
	strncpy(m_strComunity, fstrComunity, 49);
	m_fstrComunity = m_strComunity;
}

bool CSNMPBase::SendThreadMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER vParam)
{
	return (m_pThread->SendThreadMessage(usMessage, this, vParam));
}

int CSNMPBase::Init()
{
	m_pThread = CThreadFactory::St_GetInstance()->GetResource();
	if(m_pThread == 0)
		return -1;

	m_pThread->SetThreadName("CSNMPBase");
	m_pThread->Start();

	Reload();

	if(ms_plogTraps == 0)
	{
		bool bEnablexDRNamePrefix = false;
		VARIABLEBOOL(bEnablexDRNamePrefix, "Gestion", "EnablexDRNamePrefix", false);
		CFwkString fstrTRAPPrefix = "Trp";

		if(bEnablexDRNamePrefix == true)
			fstrTRAPPrefix.sprintf("%s_%s", (const char*)ms_fstrAppName, "Trp");


		int iMaxFileSize = -1;
		bool bRet = VARIABLEINT(iMaxFileSize, "Gestion", "Max_File_Size");
		if(bRet == false || iMaxFileSize < 100)
			iMaxFileSize = 5000;
		iMaxFileSize = iMaxFileSize * 1024;

		CFwkString fstrRootPath;
		fstrRootPath.Clear();
		bRet = VARIABLECHAR(fstrRootPath, "Gestion", "Logs_Path");
		if(bRet == false || fstrRootPath.IsEmpty() == true)
			fstrRootPath = "/op/sixlabs/var/log";		

		ms_plogTraps = new CxDRFile();
		ms_plogTraps->InitFile(fstrTRAPPrefix,"TRAPS", "Date,Failure,Severity,Type,Location,Cause,Solution,Action,Text\n", fstrRootPath, iMaxFileSize);
	}

	SendThreadMessage(MSG_LOAD_SERVER_LIST);

	return 0;
}

void CSNMPBase::ThrLoadServerList()
{
	SOCK_STARTUP;

    init_snmp("snmpapp");

	//netsnmp_session session;

    snmp_sess_init(&session);
    netsnmp_ds_set_int(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_DEFAULT_PORT, SNMP_TRAP_PORT);

	session.callback = NULL;
    session.callback_magic = NULL;
	session.community = (unsigned char*)(const char*)m_fstrTrapComunity;
	session.community_len = m_fstrTrapComunity.GetLength();
	session.version = m_iSNMPVersion;

	m_sSNMPServers.Replace(" ", "", true);
	if(m_sSNMPServers.IsEmpty() == true)
		return;

	size_t iLen = m_sSNMPServers.GetLength();

	if(m_sSNMPServers[iLen - 1] != ',')
		m_sSNMPServers += ',';

	size_t iStart = 0;
	size_t iEnd = m_sSNMPServers.Find(',');

	CFwkString sTemp;

	while(iEnd != J_MAX_INDEX)
	{
		sTemp = m_sSNMPServers.Mid(iStart, iEnd - iStart);
		if(sTemp.IsEmpty() == true)
		{
			iStart = iEnd + 1;
			iEnd = m_sSNMPServers.Find(',', iStart);	
			continue;
		}

		netsnmp_session* ss = (netsnmp_session*)FillSessionList(sTemp);
		mapName2session[sTemp] = ss;
		iStart = iEnd + 1;
		iEnd = m_sSNMPServers.Find(',', iStart);
	}

}

void* CSNMPBase::FillSessionList(CFwkString& fstrServer)
{
	if(fstrServer.IsEmpty() == true)
		return 0;

	size_t iPort = 0;
	CFwkString sPeer;
	CFwkString sPort;
	size_t iStart = 0;
	size_t iEnd = fstrServer.GetLength() - 1;

	iPort = fstrServer.Find(':', iStart);
	//netsnmp_session session;

	if(iPort != J_MAX_INDEX)
	{
		sPeer = fstrServer.Mid(0, iPort);
		session.peername = (char *)sPeer.GetBuffer();
		sPort = fstrServer.Mid(iPort + 1, iEnd);
		session.remote_port = sPort.AsInteger();
	}
	else
	{
		// no hubo
		sPeer = fstrServer;
		session.peername = (char *)sPeer.GetBuffer();
		session.remote_port = SNMP_TRAP_PORT;
	}

    netsnmp_session* ss = 0;
	ss = snmp_open(&session);
	if(ss == NULL)
	{
		TRACEPF_ERROR("", "CSNMPBase::FillSessionList Error snmp_open with %s ", session.peername);
		snmp_sess_perror("snmptrap", &session);
		SOCK_CLEANUP;
		return 0;
	}

	TRACEPF_DEBUG("",  "CSNMPBase::FillSessionList snmp_open success binding with %s ", session.peername);
	m_SNMPSessionList.insert(m_SNMPSessionList.end(), ss);
	return ss;
}

int CSNMPBase::Fini()
{
	for(std::list<NK_OBJPOINTER>::iterator i = m_SNMPSessionList.begin(); i != m_SNMPSessionList.end(); i++)
		snmp_close((netsnmp_session*)(*i));

    SOCK_CLEANUP;

	if(m_pThread)
	{
		m_pThread->Stop();
		CThreadFactory::St_GetInstance()->FreeResource(m_pThread);
		m_pThread = 0;
	}

	return 0;
}

void CSNMPBase::SendSNMPTrap(
								FWK_SNMP_EVENT_SEVERITY eSeveridad,
								FWK_SNMP_EVENT_TYPE eTipoAlarma,
								int iSpecificTrap,
								const char* strFallaEspecifica,
								const char* strUbicacion,
								const char* strCausaProbable,
								const char* strSolucionPropuesta,
								const char* strAccionTomada,
								const char* strInfoAdicional
								)

{
	STRUCT_SNMP_EVENT* pEvent = m_SNMPEventFactory.GetResource();

	pEvent->m_iSpecificTrap = iSpecificTrap;
	pEvent->m_sEventSeverity = eSeveridad;
	pEvent->m_sEventType = eTipoAlarma;
	pEvent->m_fstrFailure = strFallaEspecifica;
	pEvent->m_fstrLocation = strUbicacion;
	pEvent->m_fstrCause = strCausaProbable;
	pEvent->m_fstrSolution = strSolucionPropuesta;
	pEvent->m_fstrAction = strAccionTomada;
	pEvent->m_fstrText = strInfoAdicional;

	SendThreadMessage(MSG_SEND_TRAP, pEvent);
}

void CSNMPBase::SendSNMPTrapEx(STRUCT_SNMP_EVENT& sEvent, const char* strKey, const char* InfoAditional, bool bSameThread)
{
	char szBuff[DEFBUFFER];
	strcpy(szBuff, InfoAditional);

	bool bSend = true;

	// Si no se puede construir la alarma desde los archivos
	if(m_config.BuildTrap(sEvent, strKey, szBuff, bSend) == false)
		return;

	// Los campos obligatorios de SpecificTrap y Falla no existen o Falla es "NULL"
	if(bSend == false)
		return;

	STRUCT_SNMP_EVENT* pEvent = m_SNMPEventFactory.GetResource();
	*pEvent = sEvent;

	if(!bSameThread)
		SendThreadMessage(MSG_SEND_TRAP, pEvent);
	else
		ThrSendSNMPTrap(pEvent);
}

void CSNMPBase::SendSNMPTrapEx(
		FWK_SNMP_EVENT_SEVERITY eSeveridad,
		FWK_SNMP_EVENT_TYPE eTipoAlarma,
		const char* strFallaEspecifica,
		const char* strUbicacion,
		const char* strCausaProbable,
		const char* strSolucionPropuesta,
		const char* strAccionTomada,
		const char* strInfoAdicional
		)
{
	STRUCT_SNMP_EVENT sEvent;
	sEvent.m_sEventSeverity = eSeveridad;
	sEvent.m_sEventType = eTipoAlarma;
	sEvent.m_fstrAction = strAccionTomada;
	sEvent.m_fstrCause = strCausaProbable;
	sEvent.m_fstrLocation = strUbicacion;
	sEvent.m_fstrSolution = strSolucionPropuesta;
	SendSNMPTrapEx(sEvent, strFallaEspecifica, strInfoAdicional);
}

void CSNMPBase::ThrSendSNMPTrap(NK_OBJPOINTER vParam)
{
	STRUCT_SNMP_EVENT* pEvent = (STRUCT_SNMP_EVENT*)vParam;

	if(ms_plogTraps != 0 && pEvent != 0)
	{
		CFwkString fstrTrap;
		CFwkString fstrTime;
		FwkGetTime(fstrTime);
		fstrTrap.sprintf("%s,%s,%d,%d,%s,%s,%s,%s,%s\n", (const char*)fstrTime, (const char*)pEvent->m_fstrFailure, pEvent->m_sEventSeverity,
			pEvent->m_sEventType, (const char*)pEvent->m_fstrLocation, (const char*)pEvent->m_fstrCause,
			(const char*)pEvent->m_fstrSolution, (const char*)pEvent->m_fstrAction, (const char*)pEvent->m_fstrText);
		ms_plogTraps->Write(fstrTrap, fstrTrap.GetLength());
	}

	SendVSNMPTrap(pEvent);
	m_SNMPEventFactory.FreeResource(pEvent);
}

void CSNMPBase::LoadAlarmMessageFile()
{
	CFwkString fstrFileName = "TrapConfig.cfg";
	if(VARIABLECHAR(fstrFileName, "Gestion", "TrapConfig" ) == false)
		return;

	 m_config.Init(fstrFileName);
	//if( m_config.Init(fstrFileName) )
	//{
		m_config.VariableInt(m_lEnableSEVERITY, "TRAP_FIELDS", "SEVERITY");
		m_config.VariableInt(m_lEnableTYPE, "TRAP_FIELDS", "TYPE");
		m_config.VariableInt(m_lEnableFAILURE, "TRAP_FIELDS", "FAILURE");
		m_config.VariableInt(m_lEnableLOCATION, "TRAP_FIELDS", "LOCATION");
		m_config.VariableInt(m_lEnableCAUSE, "TRAP_FIELDS", "CAUSE");
		m_config.VariableInt(m_lEnableSOLUTION, "TRAP_FIELDS", "SOLUTION");
		m_config.VariableInt(m_lEnableACTION, "TRAP_FIELDS", "ACTION");
		m_config.VariableInt(m_lEnableINFO, "TRAP_FIELDS", "INFO");

		m_config.VariableChar(m_fstrTrapComunity, "TRAP_FIELDS", "COMUNITY");
		m_config.VariableChar(m_fstrEnterpriseOID, "TRAP_FIELDS", "ENTERPRISE_OID");
		m_config.VariableChar(m_fstrModuleOID, "TRAP_FIELDS", "MODULE_OID");
	//}
}

void CSNMPBase::BroadCastTrap(void* trapData)
{
	netsnmp_pdu    *pdu, *copy_pdu;
	pdu = (netsnmp_pdu*)trapData;

	MAPSTR2SESSION::iterator iter = mapName2session.begin();
	while(iter != mapName2session.end())
	{
		TRACEPF_DEBUG("",  "CSNMPBase::BroadCastTrap sending trap to %s ", (const char*)(*iter).first);
		netsnmp_session* theSession = (netsnmp_session*)((*iter).second);
		if(theSession == NULL)
		{
			CFwkString server = (*iter).first;
			TRACEPF_DEBUG("",  "CSNMPBase::BroadCastTrap session NULL try snmp_open with %s ", (const char*)server);
			netsnmp_session* ss = (netsnmp_session*)FillSessionList(server);
			(*iter).second = ss;
			if(ss == NULL)
			{
				iter++;
				continue;
			}
			theSession = ss;
		}

		copy_pdu = snmp_clone_pdu(pdu);
		int status = snmp_send(theSession, copy_pdu) == 0;
		if(status)
		{
			snmp_sess_perror("snmptrap", theSession);
			snmp_free_pdu(copy_pdu);
		}
		iter++;
	}
	snmp_free_pdu(pdu);

/*
	for(std::list<netsnmp_session *>::iterator i = m_SNMPSessionList.begin(); i != m_SNMPSessionList.end(); i++)
	{
		copy_pdu = snmp_clone_pdu(pdu);
		netsnmp_session* theSession = (*i);
		int status = snmp_send(theSession, copy_pdu) == 0;
		if(status)
		{
			snmp_sess_perror("snmptrap", (netsnmp_session*)(*i));
			snmp_free_pdu(copy_pdu);
		}
	}
	snmp_free_pdu(pdu);
*/

}