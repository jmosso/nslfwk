
#include "SmallConfiguration/SmallConfiguration.h"
#include "ThreadModule/ThreadFactory.h"
#include "Utilities/Tracing.h"
#include "GestionSNMP.h"

#ifdef WIN32
	#include "win32/net-snmp/net-snmp-config-win.h"
	#include "win32/net-snmp/net-snmp-includes-win.h"
#else
	#include "net-snmp/net-snmp-config.h"
	#include "net-snmp/net-snmp-includes.h"
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CGestionSNMP::CGestionSNMP()
{
	m_iSNMPVersion = SNMP_VERSION_1;
}

CGestionSNMP::~CGestionSNMP()
{

}

void CGestionSNMP::SendVSNMPTrap(STRUCT_SNMP_EVENT* pEvent)
{
	if(pEvent == 0)
		return;

    netsnmp_pdu    *pdu;
    in_addr_t      *pdu_in_addr_t;
    size_t          name_length;
    oid             name[MAX_OID_LEN];
	char			buffer [33];

	pdu = snmp_pdu_create(SNMP_MSG_TRAP);
	pdu_in_addr_t = (in_addr_t *) pdu->agent_addr;

	name_length = MAX_OID_LEN;

	CFwkString fstrFullEnterprise;
	fstrFullEnterprise.sprintf("%s.%s", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);

	if(!snmp_parse_oid((const char*)fstrFullEnterprise, name, &name_length))
		return;

	pdu->enterprise = (oid *) malloc(name_length * sizeof(oid));
	memcpy(pdu->enterprise, name, name_length * sizeof(oid));
	pdu->enterprise_length = name_length;

	*pdu_in_addr_t = get_myaddr();
	pdu->trap_type = 6;
	pdu->specific_type = pEvent->m_iSpecificTrap;
	pdu->time = get_uptime();

	CFwkString fstrTrapOID;

	// Falla
	if(m_lEnableFAILURE != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.1", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;

		if(snmp_add_var(pdu, name, name_length, 's', pEvent->m_fstrFailure.GetBuffer()) != 0)
			return;
	}

	if(m_lEnableINFO != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.2", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;
		if(snmp_add_var(pdu, name, name_length, 's', pEvent->m_fstrText.GetBuffer()) != 0)
			return;
	}

	// TIPO
	if(m_lEnableTYPE != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.3", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;

		sprintf(buffer, "%d", pEvent->m_sEventType);
		if(snmp_add_var(pdu, name, name_length, 'i', buffer) != 0)
			return;
	}
	// Severidad
	if(m_lEnableSEVERITY != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.4", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;

		sprintf(buffer, "%d", pEvent->m_sEventSeverity);
		if(snmp_add_var(pdu, name, name_length, 'i', buffer) != 0)
			return;
	}

	if(m_lEnableLOCATION != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.5", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;

		if(snmp_add_var(pdu, name, name_length, 's', pEvent->m_fstrLocation.GetBuffer()) != 0)
			return;
	}

	if(m_lEnableCAUSE != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.6", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;

		if(snmp_add_var(pdu, name, name_length, 's', pEvent->m_fstrCause.GetBuffer()) != 0)
			return;
	}

	if(m_lEnableSOLUTION != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.7", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;

		if(snmp_add_var(pdu, name, name_length, 's', pEvent->m_fstrSolution.GetBuffer()) != 0)
			return;
	}

	if(m_lEnableACTION != 0)
	{
		name_length = MAX_OID_LEN;
		fstrTrapOID.sprintf("%s.%s.8", (const char*)m_fstrEnterpriseOID, (const char*)m_fstrModuleOID);
		if(!snmp_parse_oid((const char*)fstrTrapOID, name, &name_length))
			return;
		if(snmp_add_var(pdu, name, name_length, 's', pEvent->m_fstrAction.GetBuffer()) != 0)
			return;
	}

	BroadCastTrap(pdu);	
}
