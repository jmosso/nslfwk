// SNMPBase.h: interface for the CSNMPBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SNMPBASE_H__47E23501_6FB9_4E2B_BE9E_C64A5A2503E7__INCLUDED_)
#define AFX_SNMPBASE_H__47E23501_6FB9_4E2B_BE9E_C64A5A2503E7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "Templates/TFactory.h"
#include "FrameworkUtilities/FwkString.h"
#include "ThreadModule/IThreadMessageCB.h"
#include <list>

#include "IGestionSNMP.h"
#include "GestionTrapsConfig.h"

#ifdef WIN32
	#include "win32/net-snmp/net-snmp-config-win.h"
	#include "win32/net-snmp/net-snmp-includes-win.h"
#else
	#include "net-snmp/net-snmp-config.h"
	#include "net-snmp/net-snmp-includes.h"
#endif

class IThread;
class CxDRFile;

class CSNMPBase : public IGestionSNMP
				, public IThreadMessageCB
{
public:
	CSNMPBase();
	virtual ~CSNMPBase();

	int Init();
	int Fini();
	int Reload();
	void SetRoot(const char* fstrRoot);
	void SetAgent(const char* fstrAddress, const char* fstrComunity);
	void EnableSNMPTrap(bool bEnable){m_bEnableSNMPTrap = bEnable;}

	char m_strAgent[16];
	char m_strComunity[50];
	char m_strRoot[255];

	int m_iSNMPVersion;

	typedef std::list<NK_OBJPOINTER> THELIST;
	THELIST m_SNMPSessionList;

	typedef std::map<CFwkString, NK_OBJPOINTER> MAPSTR2SESSION;
	MAPSTR2SESSION mapName2session;

protected:
	long	m_lEnableSEVERITY;
	long	m_lEnableTYPE;
	long	m_lEnableFAILURE;
	long	m_lEnableLOCATION;
	long	m_lEnableCAUSE;
	long	m_lEnableSOLUTION;
	long	m_lEnableACTION;
	long	m_lEnableINFO;
	CFwkString m_fstrEnterpriseOID;
	CFwkString m_fstrTrapComunity;
	CFwkString m_fstrModuleOID;
	void BroadCastTrap(void* trapData);

private:
	static CxDRFile* ms_plogTraps; 

	enum MESSAGES
	{
		MSG_SEND_TRAP,
		MSG_LOAD_SERVER_LIST,
	};

	int m_iSendDisableMask;

	void SendSNMPTrap(
		FWK_SNMP_EVENT_SEVERITY eSeveridad,
		FWK_SNMP_EVENT_TYPE eTipoAlarma,
		int iSpecificTrap,
		const char* strFallaEspecifica,
		const char* strUbicacion,
		const char* strCausaProbable,
		const char* strSolucionPropuesta,
		const char* strAccionTomada,
		const char* strInfoAdicional
		);

	void SendSNMPTrapEx(
		STRUCT_SNMP_EVENT& sEvent,
		const char* strKey, 
		const char* InfoAditional, bool bSameThread = false);

	void SendSNMPTrapEx(
		FWK_SNMP_EVENT_SEVERITY eSeveridad,
		FWK_SNMP_EVENT_TYPE eTipoAlarma,
		const char* strFallaEspecifica,
		const char* strUbicacion,
		const char* strCausaProbable,
		const char* strSolucionPropuesta,
		const char* strAccionTomada,
		const char* strInfoAdicional
		);
	

	bool SendThreadMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER vParam = 0);

	void* FillSessionList(CFwkString& fstrServer);
	void ThrSendSNMPTrap(NK_OBJPOINTER vParam);

	void ThrLoadServerList();

	virtual void SendVSNMPTrap(STRUCT_SNMP_EVENT* pEvent) = 0; 

	CFwkString m_fstrMIBRoot;
	CFwkString m_fstrAgent;
	CFwkString m_fstrComunity;
	CFwkString m_fstrEventTrapName;
	CFwkString m_fstrIPAddress;
	CFwkString m_sSNMPServers;

	IThread* m_pThread;
	bool	m_bEnableSNMPTrap;

	TFactory<STRUCT_SNMP_EVENT> m_SNMPEventFactory;


	void LoadAlarmMessageFile();
	
	CGestionTrapsConfig m_config;

	netsnmp_session session;

	NK_DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_SNMPBASE_H__47E23501_6FB9_4E2B_BE9E_C64A5A2503E7__INCLUDED_)
