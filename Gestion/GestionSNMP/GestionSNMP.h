// GestionSNMP.h: interface for the CGestionSNMP class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GESTIONSNMP_H__14637FC3_4A28_4FE8_9E3B_46B8F9A0FB22__INCLUDED_)
#define AFX_GESTIONSNMP_H__14637FC3_4A28_4FE8_9E3B_46B8F9A0FB22__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SNMPBase.h"

class CGestionSNMP : public CSNMPBase
{
public:
	CGestionSNMP();
	virtual ~CGestionSNMP();
	
	void SendVSNMPTrap(STRUCT_SNMP_EVENT* pEvent); 
};

#endif // !defined(AFX_GESTIONSNMP_H__14637FC3_4A28_4FE8_9E3B_46B8F9A0FB22__INCLUDED_)
