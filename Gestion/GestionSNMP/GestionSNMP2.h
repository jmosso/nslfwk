// GestionSNMP2.h: interface for the CGestionSNMP2 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GESTIONSNMP2_H__356E8FBA_44F5_4DE8_8E49_3DB8154E833A__INCLUDED_)
#define AFX_GESTIONSNMP2_H__356E8FBA_44F5_4DE8_8E49_3DB8154E833A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SNMPBase.h"

class CGestionSNMP2 : public CSNMPBase
{
public:
	CGestionSNMP2();
	virtual ~CGestionSNMP2();

	void SendVSNMPTrap(STRUCT_SNMP_EVENT* pEvent); 

};

#endif // !defined(AFX_GESTIONSNMP2_H__356E8FBA_44F5_4DE8_8E49_3DB8154E833A__INCLUDED_)
