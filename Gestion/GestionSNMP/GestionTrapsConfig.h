#pragma once

#include "IGestionSNMP.h"

#include "ace/Configuration_Import_Export.h"
#include "ace/Configuration.h"

class CGestionTrapsConfig
{
public:
	CGestionTrapsConfig(void);
	~CGestionTrapsConfig(void);

	bool Init(const char* strFileName);
	bool BuildTrap(STRUCT_SNMP_EVENT& theEvent, const char* strKey, const char* strInfo, bool& bSend);
	bool VariableChar(CFwkString &OutVariable, const char* pModulo,const char* pVariable);
	bool VariableInt(long& OutVariable,const char* pModulo,const char* pVariable);

private:
	CFwkString m_fstrFileName;
	ACE_Configuration * m_pConfig;
	ACE_Configuration_Section_Key m_RootKey;
};
