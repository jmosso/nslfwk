
#include "IGestionSNMP.h"
#include "GestionSNMP.h"
#include "GestionSNMP2.h"

#include "FrameworkUtilities/FwkString.h"
#include "Utilities/Tracing.h"
#include "SmallConfiguration/SmallConfiguration.h"
#include "Utilities/ThreadUtilities.h"

#include "ace/OS_NS_stdio.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
IGestionSNMP* IGestionSNMP::m_pGlobalTrapSender = 0;
CFwkString IGestionSNMP::ms_fstrAppName = "";

IGestionSNMP::IGestionSNMP()
{
}

IGestionSNMP::~IGestionSNMP()
{

}

IGestionSNMP* IGestionSNMP::Initialize(VERSION eVersion, const char* strAppName)
{
	ms_fstrAppName = strAppName;
	if(m_pGlobalTrapSender != 0)
		return m_pGlobalTrapSender;

	if(eVersion == SNMP_V1)
		m_pGlobalTrapSender = new CGestionSNMP();
	else if(eVersion == SNMP_V2)
		m_pGlobalTrapSender = new CGestionSNMP2();
	else
		m_pGlobalTrapSender = new CGestionSNMP();

	m_pGlobalTrapSender->Init();

	return(m_pGlobalTrapSender);
}

void IGestionSNMP::Delete()
{
	delete(this);
}

#define TRAP_BUFFER_LEN 16348
bool IGestionSNMP::ReporteEvento(
								FWK_SNMP_EVENT_SEVERITY eSeveridad,
								FWK_SNMP_EVENT_TYPE eTipoAlarma,
								const char* strFallaEspecifica,
								const char* strUbicacion,
								const char* strCausaProbable,
								const char* strSolucionPropuesta,
								const char* strAccionTomada,
								const char* strInfoAdicional_format,...)
{
	char szBuff[TRAP_BUFFER_LEN];
	va_list lista;
	va_start( lista, strInfoAdicional_format );
#if WIN32
	vsnprintf(szBuff, TRAP_BUFFER_LEN, strInfoAdicional_format, lista);
#else
	ACE_OS::vsnprintf(szBuff, TRAP_BUFFER_LEN, strInfoAdicional_format, lista);
#endif
	va_end( lista ); 

	TRACEPF_WARNING("", "TRAP EVENT:[%s]. Check TRAP files", strFallaEspecifica);

	STRUCT_SNMP_EVENT sEvent;
	sEvent.m_sEventType = eTipoAlarma;
	sEvent.m_sEventSeverity = eSeveridad;
	sEvent.m_fstrAction = strAccionTomada;
	sEvent.m_fstrCause = strCausaProbable;
	sEvent.m_fstrLocation = strUbicacion;
	sEvent.m_fstrSolution = strSolucionPropuesta;
	
	if(m_pGlobalTrapSender != 0)
		m_pGlobalTrapSender->SendSNMPTrapEx(sEvent, strFallaEspecifica, szBuff);

	return true;
}

bool IGestionSNMP::ReporteEventoSameThread(
							FWK_SNMP_EVENT_SEVERITY eSeveridad,
							FWK_SNMP_EVENT_TYPE eTipoAlarma,
							const char* strFallaEspecifica,
							const char* strUbicacion,
							const char* strCausaProbable,
							const char* strSolucionPropuesta,
							const char* strAccionTomada,
							const char* strInfoAdicional_format,...)
{
	char szBuff[TRAP_BUFFER_LEN];
	va_list lista;
	va_start( lista, strInfoAdicional_format );
#if WIN32
	vsnprintf(szBuff, TRAP_BUFFER_LEN, strInfoAdicional_format, lista);
#else
	ACE_OS::vsnprintf(szBuff, TRAP_BUFFER_LEN, strInfoAdicional_format, lista);
#endif
	va_end( lista ); 

	TRACEPF_WARNING("", "TRAP EVENT:[%s]. Check TRAP files", strFallaEspecifica);

	STRUCT_SNMP_EVENT sEvent;
	sEvent.m_sEventType = eTipoAlarma;
	sEvent.m_sEventSeverity = eSeveridad;
	sEvent.m_fstrAction = strAccionTomada;
	sEvent.m_fstrCause = strCausaProbable;
	sEvent.m_fstrLocation = strUbicacion;
	sEvent.m_fstrSolution = strSolucionPropuesta;
	
	if(m_pGlobalTrapSender != 0)
		m_pGlobalTrapSender->SendSNMPTrapEx(sEvent, strFallaEspecifica, szBuff, true);

	return true;
}