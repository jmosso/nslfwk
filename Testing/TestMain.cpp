
#include "versions/config.h"
#include "Utilities/Tracing.h"
#include "ThreadModule/ThreadFactory.h"
#include "HMPClient/SixHMPFactory.h"
#include "ace/OS.h"

#include "TestHMPClient.h"


void StartApplication(char* fileName);
void endApp(int signal);

int ACE_TMAIN(int argc, ACE_TCHAR* vars[])	
{
	signal( SIGTERM, endApp );
	signal( SIGINT,  endApp );
	signal( SIGSEGV, endApp );
	signal( SIGABRT, endApp );
	signal( SIGILL, endApp );
#ifndef WIN32	
	signal( SIGPIPE, endApp );
	signal( SIGBUS,  endApp );
#ifndef USE_SIX_LOG
	sixlabs::LogManager::instance()->set_flags( ACE_Log_Msg::STDERR );
	sixlabs::LogManager::instance()->clr_flags(ACE_Log_Msg::SYSLOG);
	set_log_generic(0);
#endif
#endif

	StartApplication(vars[1]);

	/**/
	CTestHMPClient testHmpClient;
	testHmpClient.StartTest();


	while (true)
	{
		ACE_OS::sleep(ACE_Time_Value(0, 1000 * 1000));
	}
	return 0;
}

void StartApplication(char* fileName)
{
	CTracing::SetProperties(1, 0xFFF, 5, "C:/Datos/Work/Code/Simulacion/Logs/nslTesting.log");
	CTracing::ActivateTurbo(false);
	CTracing::InitTrace();	

	TRACEPF_ALWAYS("", "Initializing %s %s", VERSION_BUILD, DATE_BUILD);

	CThreadFactory::St_CreateInstance();
	CDynamicStructFactory::St_CreateInstance();

	CSixHMPFactory::St_CreateInstance();
}

void endApp(int theSignal)
{
	switch( theSignal )
	{
	case SIGTERM:
	case SIGINT:
		TRACEFLUSH
		TRACEPF_ALWAYS("", "About to call exit()");
		TRACEFLUSH
		exit( 0 );
		break;

	default:
		TRACEFLUSH
#ifndef WIN32
        int pid = getpid();
        int tid = syscall( SYS_gettid );
		TRACEPF_ALWAYS("", "About to generate core for Process %s, PID = %d, Thread = %d, Signal = %d", appName, pid, tid, theSignal );
        char commandBuffer[1024];
        sprintf( commandBuffer, "gcore -o /opt/sixlabs/var/log/cores/core.%s.thread-%d.signal-%d %d", appName, tid, theSignal, pid );
        int ret = system( commandBuffer );
		TRACEPF_ALWAYS("", "generation of core for Process %s, PID = %d, Thread = %d, Signal = %d successful", appName, pid, tid, theSignal );
#endif

		TRACEPF_ALWAYS("",  "About to call exit()");
		TRACEFLUSH
		exit( 0 );
		break;
	}

	return;
}