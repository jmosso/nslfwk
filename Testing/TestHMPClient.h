#pragma once

#include "HMPClient/ISixHMPClient.h"

class ParamsCodec;
class CTestHMPClient : public ISixHMPClientEvent
{
public:
	CTestHMPClient();
	virtual ~CTestHMPClient();

	void StartTest();

private:

	void ProcessSixHMPClientEvent(ParamsCodec* pDynEvent);
	ISixHMPClient* m_pHMPClient;
};
