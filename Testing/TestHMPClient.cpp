
#include "TestHMPClient.h"
#include "HMPClient/SixHMPFactory.h"
#include "Utilities/Tracing.h"
#include "CommChannel/SGenericParamFullNames.hpp"

#include <paramsCodec.hpp>

CTestHMPClient::CTestHMPClient()
{
	m_pHMPClient = CSixHMPFactory::St_GetInstance()->GetResource();
	m_pHMPClient->SetEventCallback(this);
}

CTestHMPClient::~CTestHMPClient()
{
	if(m_pHMPClient != 0)
		CSixHMPFactory::St_GetInstance()->FreeResource(m_pHMPClient);
}

void CTestHMPClient::StartTest()
{
	TRACEPF_DEBUG("", "CTestHMPClient::StartTest<0x%x> hmpclient<0x%x>", this, m_pHMPClient);

	if(m_pHMPClient == 0)
		return;

	ParamsCodec* pPC = new ParamsCodec();
	pPC->setString("client.ipAddress", "127.0.0.1"); 
	pPC->setInteger("client.port", 30000); 
	pPC->setString("client.sdp", "v=0 ..."); 

	// hmp se encarga de borrar
	m_pHMPClient->GetBearer(pPC);
}

void CTestHMPClient::ProcessSixHMPClientEvent(ParamsCodec* pPC)
{
	// Aqui llega evento de callback del hmp... deber�a manejarse en el hilo del recurso!!! solo para ejemplo:
	int64_t iEvent = pPC->getInteger("event");

	switch(iEvent)
	{
	case GET_BEARER_DONE:
		{
			TRACEPF_DEBUG("", "ProcessSixHMPClientEvent<0x%x> event [GET_BEARER_DONE]. Invoke Play", this);
			// En este evento, el HMP devuelve ip, port y codec para usar en la respuesta 
			std::string strIp = pPC->getString("server.ipAddress"); 
			int64_t iPort = pPC->getInteger("server.port"); 
			// Con los parametros de conexion se debe rehacer el SDP!! 
			// El play se ordena desde el control!! 
			pPC->clear();
			pPC->setString("oper.FileName", "/opt/sixlabs/var/lib/mr/wav/welcome.wav"); 
			pPC->setInteger(_six_kOper_Type, ISixHMPClient::MR_PLAYMESSAGE);
			m_pHMPClient->PerformAction(pPC);
			return; // reusar pPC 
		}
		break;

	case ACTION_DONE:
		{
			int64_t iTypeRef = pPC->getInteger(_six_kOper_TypeRef); 
			switch(iTypeRef)
			{
			case ISixHMPClient::MR_PLAYMESSAGE:
				TRACEPF_DEBUG("", "ProcessSixHMPClientEvent<0x%x> event [PLAY_DONE]. Releasing", this);
				break;

			default:
				break;			
			}

			m_pHMPClient->ReleaseBearer();
		}
		break;

	case RELEASE_DONE:
		{
			TRACEPF_DEBUG("", "ProcessSixHMPClientEvent<0x%x> event [RELEASE_DONE]. EndTest!!!", this);
		}
		break;

	default:
		break;
	}

	delete(pPC);
}