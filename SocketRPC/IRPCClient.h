
#pragma once

#include "FrameworkUtilities/DynamicStruct.h"

class IRPCClientCB
{
public:

	virtual void ConnectionCB(int iErrorCode){};
	virtual void IncomingRPC(CDynamicStruct& dynData){};
};

class IRPCClient  
{
public:
	void Init(IRPCClientCB* pCB)
	{
		m_pCallback = pCB;
	}

	void ClearCB()
	{
		m_pCallback = 0;
	}

	virtual short Start(int iPort, const char* strServer) = 0;
	virtual short Stop() = 0;
	virtual short InvokeRPC(CDynamicStruct& dynData) = 0;
	virtual short FreeRequest() = 0;

	IRPCClientCB*	m_pCallback;
};

