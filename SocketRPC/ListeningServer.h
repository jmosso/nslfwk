
#pragma once

#include "ThreadModule/IThreadMessageCB.h"
#include "ace/SOCK_Acceptor.h"

class IRPCServerFactory;
class IRPCServer;
class IThread;

class CListeningServer : public IThreadMessageCB
{
public:
	CListeningServer();
	virtual ~CListeningServer();

	short	Start(int iPort, IRPCServerFactory* factory);
	short	Stop();
	void	FreeChildServer(IRPCServer* pChildServer);
	void	Clean();

private:
	enum MESSAGES
	{
		MSG_ACCEPT_CONN,
	};

	bool ProcessMessage(unsigned short usMessage, NK_OBJPOINTER iParam = 0);

	int					m_iPort;
	ACE_SOCK_Acceptor	m_asAcceptor;
	IThread*			m_pListenThread;
	IRPCServerFactory*	m_pRPCServerFactory;

	bool m_bContinue;
	void	ThrAcceptConnections();
};
