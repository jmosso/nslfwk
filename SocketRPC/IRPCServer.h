
#pragma once

#include "ThreadModule/IThreadMessageCB.h"
#include "FrameworkUtilities/DynamicStruct.h"

class CListeningServer;

class SurrogateThreadBC
{
public:
	virtual bool SurrogateProcessMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER iParam = 0) = 0;
};

class SurrogateThread : public IThreadMessageCB
{
public:
	SurrogateThread();

	void SetCallback(SurrogateThreadBC* pCallback){m_pCallback = pCallback;};

private:

	bool ProcessMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER iParam = 0)
	{
		if(m_pCallback != 0)
			m_pCallback->SurrogateProcessMessage(usMessage, iParam);
		return true;
	};

	SurrogateThreadBC* m_pCallback;
};

class IThread;
class IRPCServer : public SurrogateThreadBC
{
	class CStreamDP;

	friend class CListeningServer;

public:
	IRPCServer();
	virtual ~IRPCServer();

	virtual void IncomingRPC(CDynamicStruct& dynData){};
	virtual void InitServer(){};
	virtual void EndServer(){};

protected:
	short InvokeRPC(CDynamicStruct& dynData);
	void CloseServer();
	bool m_bRawServer;

private:

	enum MESSAGES
	{
		MSG_LISTEN,
		MSG_CLOSE_SERVER,
		MSG_RECEIVE_CB,
		MSG_INVOKE_RPC,
		MSG_SEND_CB,
	};

	bool SendThreadMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER iParam = 0);
	bool SurrogateProcessMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER iParam = 0);

	void ThrListen();
	bool ReceiveMsg();
	void ThrCloseServer();
	void ThrInvokeRPC(CDynamicStruct* dynData);


	short Init(NK_OBJPOINTER pStream, CListeningServer* pParent = 0);

	short ProcessSckMsg(int iSize);

	CStreamDP*				m_StreamDP;

	CListeningServer*		m_pParent;
	IThread*				m_pListenThread;
	bool					m_SplitMessage;
	CFwkString				m_fstrFullMsg;
	size_t					m_iFullMsgLen;
	size_t					m_iAcumLen;

	SurrogateThread			m_SurrThread;
	bool					m_bClosing;
};

class IRPCServerFactory
{
public:

	virtual IRPCServer* GetResource() = 0;
	virtual void FreeResource(IRPCServer* p) = 0;
};
