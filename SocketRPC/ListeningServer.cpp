
#include "ListeningServer.h"
#include "Utilities/Tracing.h"
#include "IRPCServer.h"
#include "SocketRPCModule.h"
#include "ThreadModule/ThreadFactory.h"
#include "ThreadModule/IThread.h"

#include "ace/Reactor.h"
#include "ace/SOCK_Stream.h"
#include "ace/SOCK_Connector.h"
#include "ace/SOCK_Acceptor.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CListeningServer::CListeningServer()
{
	Clean();
}

CListeningServer::~CListeningServer()
{

}

void CListeningServer::Clean()
{
	m_iPort = -1;
	m_pRPCServerFactory = 0;
	m_bContinue = false;
}

bool CListeningServer::ProcessMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER iParam)
{
	switch(usMessage)
	{
	case MSG_ACCEPT_CONN:
		ThrAcceptConnections();
		break;

	default:
		break;
	}

	return true;
}

short CListeningServer::Start(int iPort, IRPCServerFactory* factory)
{
	m_pRPCServerFactory = factory;
	if(m_pRPCServerFactory == 0)
	{
		TRACEPF_ERROR("", "CListeningServer::Start<0x%x> m_pRPCServerFactory NULL", this);
		return -1;
	}

	m_iPort = iPort;
	
	if(m_iPort < 0 || m_iPort > 65535)
	{
		TRACEPF_ERROR("", "CListeningServer::Start<0x%x> port %d out of range", this, iPort);
		return -1;
	}

	ACE_INET_Addr port_to_listen(m_iPort);
	if (m_asAcceptor.open(port_to_listen, 1) == -1)
	{
		TRACEPF_ERROR("", "CListeningServer::Start<0x%x> could not open port %d", this, iPort);
		return -1;
	}

	m_pListenThread = CThreadFactory::St_GetInstance()->GetResource();
	m_pListenThread->SetThreadName("CListeningServer");
	m_pListenThread->Start();
	m_pListenThread->SendThreadMessage(MSG_ACCEPT_CONN, this);

	return 0;
}

void CListeningServer::ThrAcceptConnections()
{
	TRACEPF_INFO("", "CListeningServer::ThrAcceptConnections<0x%x> listening at port %d factory <0x%x>", this, m_iPort, m_pRPCServerFactory);

	m_bContinue = true;
	while(m_bContinue)
	{
		ACE_SOCK_Stream stream;
		ACE_Time_Value timeout (ACE_DEFAULT_TIMEOUT);
		if (m_asAcceptor.accept(stream,NULL,&timeout) != -1)		
		{
			IRPCServer* pUserChildServer = m_pRPCServerFactory->GetResource();
			if(pUserChildServer == 0)
			{
				TRACEPF_ERROR("", "CListeningServer::ThrAcceptConnections<0x%x> child server NULL for incoming cx at port %d", this, m_iPort);
				return;
			}

			TRACEPF_DEBUG("",  "CListeningServer::ThrAcceptConnections<0x%x> Connection accepted at port %d childserver <0x%x>", this, m_iPort, pUserChildServer);

			pUserChildServer->Init(&stream, this);
			pUserChildServer->InitServer();
		}
	}
}

short CListeningServer::Stop()
{
	m_bContinue = false;
	TRACEPF_DEBUG("",  "CListeningServer::Stopping<0x%x>", this);
	m_asAcceptor.close () ;
	if(m_pListenThread)
	{
		m_pListenThread->Stop();
		CThreadFactory::St_GetInstance()->FreeResource(m_pListenThread);
		m_pListenThread = 0;
	}
	TRACEPF_DEBUG("",  "CListeningServer::Stopped<0x%x>", this);
	return 0;
}

void CListeningServer::FreeChildServer(IRPCServer* pChildServer)
{
	TRACEPF_INFO("",  "CListeningServer::FreeChildServer<0x%x> releasing child <0x%x> ", this, pChildServer);
	
	if(pChildServer == 0)
		return;
		
	pChildServer->EndServer();
	m_pRPCServerFactory->FreeResource(pChildServer);
}

