
#include "IRPCServer.h"
#include "ListeningServer.h"
#include "SocketRPCModule.h"
#include "Utilities/Tracing.h"
#include "paramsCodec.hpp"
#include "ThreadModule/ThreadFactory.h"
#include "ThreadModule/IThread.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

class IRPCServer::CStreamDP
{
public:
	CStreamDP(){};
	~CStreamDP(){};

	ACE_SOCK_Stream		m_asStream;
};

IRPCServer::IRPCServer():m_StreamDP(new CStreamDP)
{
	m_pParent = 0;
	m_pListenThread = 0;
	m_SurrThread.SetCallback(this);
	m_SplitMessage = false;
	m_iFullMsgLen = 0;
	m_iAcumLen = 0;
	m_bRawServer = false;
	m_bClosing = false;
}

IRPCServer::~IRPCServer()
{

}

SurrogateThread::SurrogateThread()
{
	m_pCallback = 0;
}

bool IRPCServer::SendThreadMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER iParam)
{
	if(CSocketRPCModule::St_GetInstance() == 0)
		return false;

	CSocketRPCModule::St_GetInstance()->SendThreadMessage(usMessage, &m_SurrThread, iParam);
	return true;
}

bool IRPCServer::SurrogateProcessMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER iParam)
{
	switch(usMessage)
	{
	case MSG_LISTEN:
		ThrListen();
		break;
	case MSG_CLOSE_SERVER:
		ThrCloseServer();
		break;
	case MSG_INVOKE_RPC:
		ThrInvokeRPC((CDynamicStruct*) iParam);
		DYNFACTFREERES((CDynamicStruct*) iParam);
		break;
	default:
		break;	
	}

	return true;
}

short IRPCServer::Init(NK_OBJPOINTER pStream, CListeningServer* pParent)
{
	TRACEPF_DEBUG("",  "IRPCServer::Init this <0x%x>", this);

	ACE_SOCK_Stream* p = (ACE_SOCK_Stream*)pStream;
	m_StreamDP->m_asStream = *p;
	m_pParent = pParent;
	m_bClosing = false;

	if(m_pListenThread == 0)
	{
		m_pListenThread = CThreadFactory::St_GetInstance()->GetResource();
		m_pListenThread->SetThreadName("IRPCServer");
		m_pListenThread->Start();
	}
	m_pListenThread->SendThreadMessage(MSG_LISTEN, &m_SurrThread);

	return 0;
}

void IRPCServer::ThrListen()
{
	m_SplitMessage = false;
	m_iFullMsgLen = 0;
	m_iAcumLen = 0;
	m_bClosing = false;
	bool bContinue = true;
	InitServer();
	while(bContinue)
	{
		if(ReceiveMsg() == false)
			bContinue = false;
	}
	TRACEPF_DEBUG("",  "IRPCServer::ThrListen this <0x%x> saliendo ciclo de escucha m_bClosing %d", this, m_bClosing);
	if(m_bClosing == false)
		CloseServer();
}

bool IRPCServer::ReceiveMsg()
{
	size_t lenRx = 0;
	iovec vec;
	size_t iStat = m_StreamDP->m_asStream.recvv(&vec);
	lenRx = vec.iov_len;
	if(iStat <= 0 || lenRx == 0) 
		return false;

	char* buffer = NULL;
	try 
	{
		if(vec.iov_base != 0)
		{
			buffer = new char[lenRx + 1];
			ACE_OS::memset(buffer, 0, lenRx + 1);
			ACE_OS::memcpy(buffer, vec.iov_base, lenRx);
			delete [] vec.iov_base;
		}
		else
		{
			TRACEPF_DEBUG("",  "IRPCServer::ReceiveMsg vec.iov_base null this <0x%x> ", this);
			return false;
		}
	}
	catch (...) 
	{
		TRACEPF_DEBUG("",  "IRPCServer::ReceiveMsg this <0x%x> ", this);
		return false;
	}

	if(m_bRawServer == false)
	{
		m_iAcumLen = m_iAcumLen + lenRx;

		if(m_SplitMessage == false)
		{
			size_t iLenHeader = 6;
			char* strLenInfo = new char[iLenHeader + 1];
			ACE_OS::memset(strLenInfo, 0, iLenHeader + 1);
			ACE_OS::memcpy(strLenInfo , buffer, iLenHeader);
			m_fstrFullMsg = (buffer + iLenHeader);

			delete[] buffer;

			int iMsgLen = ACE_OS::atoi(strLenInfo);
			delete[] strLenInfo;

			if(lenRx < iMsgLen)
			{
				// almacenar y esperar siguiente
				m_iFullMsgLen = iMsgLen;
				m_SplitMessage = true;
				return true;
			}
		}
		else
		{
			m_fstrFullMsg += buffer;
			delete[] buffer;
			if(m_iAcumLen < m_iFullMsgLen)
				return true;
		}


		m_iAcumLen = 0;
		m_iFullMsgLen = 0;
		m_SplitMessage = false;

		CDynamicStruct dynData;
	
		try
		{
			ParamsCodecGenericBuffer inputBuffer( m_fstrFullMsg, m_fstrFullMsg.GetLength() + 1 );
			dynData.GetParamsCodec()->readObjectBAS( inputBuffer );
			//dynData.readTextChar((const char*)m_fstrFullMsg);
		}
		catch(...)
		{
			TRACEPF_DEBUG("",  "IRPCServer::ReceiveMsg this <0x%x> Error reading message", this);
			return true;
		}

		//TRACEPF_DEBUG("",  "IRPCServer::ReceiveMsg report message this <0x%x> ", this);
		IncomingRPC(dynData);	
	}
	else
	{
		m_fstrFullMsg = buffer;
		delete[] buffer;

		size_t iMsgLen = m_fstrFullMsg.GetLength();

		CDynamicStruct dynData;

		dynData.AddString("Event.Name", "SYS_SOCKET_MSG");
		dynData.AddString("buffer", m_fstrFullMsg);
		dynData.AddInt("len", (int)iMsgLen);
		IncomingRPC(dynData);
	}


	return true;
}

void IRPCServer::CloseServer()
{
	TRACEPF_DEBUG("",  "IRPCServer::CloseServer this <0x%x> ", this);
	SendThreadMessage(MSG_CLOSE_SERVER);
}

void IRPCServer::ThrCloseServer()
{

	TRACEPF_DEBUG("",  "IRPCServer::ThrCloseServer this <0x%x> m_bClosing %d", this, m_bClosing);
	m_bClosing = true;

	if(m_pParent == 0)
		return;
	try
	{
		m_pParent->FreeChildServer(this);
	}
	catch(...)
	{
	}
	m_StreamDP->m_asStream.close();
}


short IRPCServer::ProcessSckMsg(int iSize)
{
	return 0;
}

short IRPCServer::InvokeRPC(CDynamicStruct& dynData)
{
	CDynamicStruct* pDyn = DYNFACTGETRES;
	*pDyn = dynData;
	SendThreadMessage(MSG_INVOKE_RPC, (NK_OBJPOINTER)pDyn);
	return 0;
}

void IRPCServer::ThrInvokeRPC(CDynamicStruct* dynData)
{
	if(m_bRawServer == false)
	{
		size_t fullsize = 0;
/*
		char* strData = new char[2001];

		try
		{
			bool bRes = dynData->writeTextChar(strData, 2000, fullsize);
			if(bRes == false)
			{
				delete []strData;
				strData = new char[fullsize + 1];
				dynData->writeTextChar(strData, fullsize, fullsize);
			}
		}
		catch(...)
		{
			TRACEPF_ERROR("", "IRPCServer::ThrInvokeRPC DYN INVALIDO");
			return;
		}
*/
		CFwkString m_fstrFullMsg;
		try
		{
			ParamsCodecGenericBuffer outputBuffer;
			dynData->GetParamsCodec()->writeObjectBAS( outputBuffer );
			m_fstrFullMsg.FromBuffer( outputBuffer.buffer, outputBuffer.size );
		}
		catch(...)
		{
			TRACEPF_ERROR("", "IRPCServer::ThrInvokeRPC Exception on writeObjectBAS");
			return;
		}

		fullsize = m_fstrFullMsg.GetLength();

		size_t iLenHeader = 6;
		CFwkString fstrLen;

		// Enviar caracter nulo
		fullsize = fullsize + 1;
		size_t iTotalLen = fullsize + iLenHeader;
		fstrLen.sprintf("%06d", iTotalLen);

		char* buffer = new char[iTotalLen + 1];

		ACE_OS::memcpy(buffer , (const char*)fstrLen, iLenHeader);
		ACE_OS::memcpy(buffer + iLenHeader, (const char*)m_fstrFullMsg, fullsize);
		//delete[]strData;

		size_t iStat = m_StreamDP->m_asStream.send_n((void*)buffer, iTotalLen);
		delete[]buffer;
		SIX_ASSERT(iStat == (iTotalLen));	
	}
	else
	{
		CFwkString fstrBuffer;
		if(dynData->GetString("buffer", fstrBuffer) == false)
			return;

		size_t iTotalLen = fstrBuffer.GetLength();
		size_t iLenEx = iTotalLen + 2;

		char* buffer = new char[iLenEx]; // incluir caracter nulo al final
		ACE_OS::memset(buffer, 0, iLenEx);
		ACE_OS::memcpy(buffer , (const char*)fstrBuffer, iTotalLen);

		size_t iStat = m_StreamDP->m_asStream.send_n((void*)buffer, iLenEx);
		delete[]buffer;
		SIX_ASSERT(iStat == (iTotalLen));	
	}


	return;
}
