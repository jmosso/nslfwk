
#pragma once

#include "Templates/TSingleton.h"
#include "ThreadModule/ThreadFactory.h"
#include "ThreadModule/IThread.h"
#include "IRPCClient.h"

class IRPCServer;
class IRPCServerFactory;
class CListeningServer;

class IThread;
class CSocketRPCModule : public TSingleton<CSocketRPCModule>   
{
	class CSocketRPCModuleDP;

public:
	CSocketRPCModule();
	virtual ~CSocketRPCModule();

	int Init();
	int Exit();

	short StartRPCServer(int iPort, IRPCServerFactory* factory);
	short StopRPCServer(int iPort);
	IRPCClient* GetRPCClient();
	void FreeRPCClient(IRPCClient* client);

	bool SendThreadMessage(SIX_FWK_MESSAGE usMessage, IThreadMessageCB* pObjSender, NK_OBJPOINTER iParam = 0);

private:
	
	IThread* m_pThread;

	CSocketRPCModuleDP* m_factoriesDP;

	typedef std::map<int, CListeningServer*> SERVERMAP;
	SERVERMAP::iterator m_theIterator;
	SERVERMAP m_MapServers;
};

