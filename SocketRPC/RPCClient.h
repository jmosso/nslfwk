
#pragma once

#include "IRPCClient.h"
#include "ThreadModule/IThreadMessageCB.h"
#include "ace/SOCK_Stream.h"
#include "ace/SOCK_Connector.h"
#include "ace/SOCK_Acceptor.h"

class IThread;

class CRPCClient : public IRPCClient, 
				   public IThreadMessageCB
{
public:
	CRPCClient();
	virtual ~CRPCClient();

private:

	bool SendThreadMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER iParam = 0);
	bool ProcessMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER iParam = 0);

	enum MESSAGES
	{
		MSG_START,
		MSG_STOP,
		MSG_CLOSECB,
		MSG_CONENCTION_CB,
		MSG_RECEIVE_CB,
		MSG_INVOKE_RPC,
		MSG_FREEREQUEST,
	};

	short Start(int iPort, const char* strServer);
	short Stop();
	short InvokeRPC(CDynamicStruct& dynData);
	short FreeRequest();

	void Clean();

	CFwkString  m_fstrServername;
	int			m_iPort;

	ACE_SOCK_Connector m_asConnector;
	ACE_SOCK_Stream m_asStream;

	void ThrStart();
	void ThrStop();
	void ThrInvokeRPC(CDynamicStruct* pDyn);
	void ThrFreeRequest();
	bool ReceiveMsg();

	// control de sincronizaci�n

	bool		m_bTaskInProgress;
	bool		m_bFreeRequest;
	IThread*	m_pListenThread;
	bool		m_SplitMessage;
	CFwkString	m_fstrFullMsg;
	int			m_iFullMsgLen;
	size_t		m_iAcumLen;
};

