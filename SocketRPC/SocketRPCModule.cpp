
#include "SocketRPCModule.h"
#include "RPCClient.h"
#include "ListeningServer.h"
#include "Utilities/Tracing.h"
#include "Templates/TFactory.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

class CSocketRPCModule::CSocketRPCModuleDP
{
public:
	CSocketRPCModuleDP(){};
	~CSocketRPCModuleDP(){};

	TFactory<CListeningServer> m_ListenigServerFactory;
	TFactory<CRPCClient, IRPCClient> m_RPCClientFactory;
};


CSocketRPCModule::CSocketRPCModule(): m_factoriesDP(new CSocketRPCModuleDP)
{
	m_factoriesDP->m_ListenigServerFactory.SetFactName("ListenigServerFactory");
	m_factoriesDP->m_RPCClientFactory.SetFactName("RPCClientFactory");
}

CSocketRPCModule::~CSocketRPCModule()
{

}

int CSocketRPCModule::Init()
{
	m_pThread = CThreadFactory::St_GetInstance()->GetResource();
	m_pThread->SetThreadName("CSocketRPCModule");
	m_pThread->Start();
	return 0;
}

int CSocketRPCModule::Exit()
{
	if(m_pThread != 0)
	{
		m_pThread->Stop();
		CThreadFactory::St_GetInstance()->FreeResource(m_pThread);
		m_pThread = 0;
	}

	return 0;
}

bool CSocketRPCModule::SendThreadMessage(SIX_FWK_MESSAGE usMessage, IThreadMessageCB* pObjSender, NK_OBJPOINTER vParam)
{
	if(m_pThread == 0)
		return false;
#ifdef _NK_MFC
	int iParam = (int)vParam;
#else
	NK_OBJPOINTER iParam = vParam;
#endif
	return(m_pThread->SendThreadMessage(usMessage, pObjSender, iParam));
}

short CSocketRPCModule::StartRPCServer(int iPort, IRPCServerFactory* factory)
{
	CListeningServer* pListeningServer = m_factoriesDP->m_ListenigServerFactory.GetResource();
	short sRet = pListeningServer->Start(iPort, factory);

	if(sRet == 0)
		m_MapServers[iPort] = pListeningServer;
	else
		m_factoriesDP->m_ListenigServerFactory.FreeResource(pListeningServer);

	return sRet;
}

short CSocketRPCModule::StopRPCServer(int iPort)
{
	m_theIterator = m_MapServers.find(iPort);
	if(m_theIterator == m_MapServers.end())
		return -1;

	CListeningServer* pListeningServer = (*m_theIterator).second;
	if(pListeningServer == 0)
		return -1;

	m_MapServers.erase(iPort);
	pListeningServer->Stop();
	m_factoriesDP->m_ListenigServerFactory.FreeResource(pListeningServer);

	return 0;
}

IRPCClient* CSocketRPCModule::GetRPCClient()
{
	IRPCClient* tmp = m_factoriesDP->m_RPCClientFactory.GetResource();
	return tmp;
}

void CSocketRPCModule::FreeRPCClient(IRPCClient* client)
{
	if(client == 0)
		return;

	m_factoriesDP->m_RPCClientFactory.FreeResource(client);
}
