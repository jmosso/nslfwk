
#include "RPCClient.h"
#include "SocketRPCModule.h"
#include "paramsCodec.hpp"
#include "ThreadModule/ThreadFactory.h"
#include "ThreadModule/IThread.h"
#include "Utilities/Tracing.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRPCClient::CRPCClient()
{
	m_pListenThread = 0;
	m_bTaskInProgress = false;
	m_bFreeRequest = false;
	m_SplitMessage = false;
	m_iFullMsgLen = 0;
	m_iAcumLen = 0;
}

CRPCClient::~CRPCClient()
{

}

void CRPCClient::Clean()
{
	m_fstrServername.Clear();
	m_iPort = -1;
}

bool CRPCClient::SendThreadMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER iParam)
{
	if(CSocketRPCModule::St_GetInstance() == 0)
		return false;

	CSocketRPCModule::St_GetInstance()->SendThreadMessage(usMessage, this, iParam);
	return true;
}

bool CRPCClient::ProcessMessage(SIX_FWK_MESSAGE usMessage, NK_OBJPOINTER iParam)
{
	switch(usMessage)
	{
	case MSG_START:
		ThrStart();
		break;

	case MSG_STOP:
		ThrStop();
		break;

	case MSG_INVOKE_RPC:
		ThrInvokeRPC((CDynamicStruct*)iParam);
		DYNFACTFREERES((CDynamicStruct*) iParam);
		break;
	case MSG_FREEREQUEST:
		ThrFreeRequest();
		break;

	default:
		break;
	}
	return true;
}


short CRPCClient::Start(int iPort, const char* strServer)
{
	m_iPort = iPort;
	m_fstrServername = strServer;
	if(m_iPort < 0 || m_iPort > 65535)
		return -1;

	if(m_fstrServername.IsEmpty() == true)
		return -1;
	m_bTaskInProgress = true;

	ACE_INET_Addr srvr(m_iPort, strServer);
	if (m_asConnector.connect (m_asStream, srvr) == -1)
	{
		try
		{
			if(m_pCallback != 0)
				m_pCallback->ConnectionCB(-1);
		}
		catch(...)
		{
		}
		return -1;
	}

	if(m_pListenThread == 0)
	{
		m_pListenThread = CThreadFactory::St_GetInstance()->GetResource();
		m_pListenThread->SetThreadName("CRPCClient");
		m_pListenThread->Start();
	}
	m_pListenThread->SendThreadMessage(MSG_START, this);
	return 0;
}

void CRPCClient::ThrStart()
{
	try
	{
		if(m_pCallback != 0)
			m_pCallback->ConnectionCB(0);
	}
	catch(...)
	{
	}

	m_SplitMessage = false;
	m_iFullMsgLen = 0;
	m_iAcumLen = 0;
	bool bContinue = true;
	while(bContinue)
	{
		if(ReceiveMsg() == false)
			bContinue = false;
	}

	try
	{
		if(m_pCallback != 0)
			m_pCallback->ConnectionCB(1);
	}
	catch(...)
	{
	}

	m_asStream.close();
	m_SplitMessage = false;
	m_iFullMsgLen = 0;
	m_iAcumLen = 0;

	return;
}

bool CRPCClient::ReceiveMsg()
{
	size_t lenRx = 0;
	iovec vec;
	size_t iStat = m_asStream.recvv(&vec);
	lenRx = vec.iov_len;
	if(iStat <= 0 || lenRx == 0) 
		return false;

	char* buffer = NULL;
	try 
	{
		if(vec.iov_base != 0)
		{
			buffer = new char[lenRx + 1];
			ACE_OS::memset(buffer, 0, lenRx + 1);
			ACE_OS::memcpy(buffer, vec.iov_base, lenRx);
			delete [] vec.iov_base;
		}
		else
		{
			TRACEPF_DEBUG("",  "CRPCClient::ReceiveMsg vec.iov_base null this <0x%x> ", this);
			return false;
		}
	}
	catch (...) 
	{
		TRACEPF_DEBUG("",  "CRPCClient::ReceiveMsg this <0x%x> ", this);
		return false;
	}

	m_iAcumLen = m_iAcumLen + lenRx;

	if(m_SplitMessage == false)
	{
		size_t iLenHeader = 6;
		char* strLenInfo = new char[iLenHeader + 1];
		ACE_OS::memset(strLenInfo, 0, iLenHeader + 1);
		ACE_OS::memcpy(strLenInfo , buffer, iLenHeader);
		m_fstrFullMsg = (buffer + iLenHeader);

		delete[] buffer;

		int iMsgLen = ACE_OS::atoi(strLenInfo);
		delete[] strLenInfo;
		if(lenRx < iMsgLen)
		{
			// almacenar y esperar siguiente
			m_iFullMsgLen = iMsgLen;
			m_SplitMessage = true;
			return true;
		}
	}
	else
	{
		m_fstrFullMsg += buffer;
		delete[] buffer;
		if(m_iAcumLen < m_iFullMsgLen)
			return true;
	}


	m_iAcumLen = 0;
	m_iFullMsgLen = 0;
	m_SplitMessage = false;

	CDynamicStruct dynData;
	
	try
	{
		ParamsCodecGenericBuffer inputBuffer( m_fstrFullMsg, m_fstrFullMsg.GetLength() + 1 );
		dynData.GetParamsCodec()->readObjectBAS( inputBuffer );
		//dynData.readTextChar((const char*)m_fstrFullMsg);
	}
	catch(...)
	{
		TRACEPF_DEBUG("",  "IRPCServer::ReceiveMsg this <0x%x> Error reading message", this);
		return true;
	}

	if(m_pCallback != 0)
		m_pCallback->IncomingRPC(dynData);

	return true;
}

short CRPCClient::Stop()
{
	SendThreadMessage(MSG_STOP);
	return 0;
}

void CRPCClient::ThrStop()
{
	m_asStream.close();
}

short CRPCClient::InvokeRPC(CDynamicStruct& dynData)
{
	CDynamicStruct* pDyn = DYNFACTGETRES;
	*pDyn = dynData;

	SendThreadMessage(MSG_INVOKE_RPC, (NK_OBJPOINTER)pDyn);
	return 0;
}

void CRPCClient::ThrInvokeRPC(CDynamicStruct* dynData)
{
	size_t fullsize = 0;
/*
	char* strData = new char[2001];
	try
	{
		bool bRes = dynData->writeTextChar(strData, 2000, fullsize);
		if(bRes == false)
		{
			delete []strData;
			strData = new char[fullsize + 1];
			dynData->writeTextChar(strData, fullsize, fullsize);
		}
	}
	catch(...)
	{
		delete []strData;
		TRACEPF_ERROR("", "IRPCServer::ThrInvokeRPC DYN INVALIDO");
		return;
	}
*/
	CFwkString m_fstrFullMsg;
	try
	{
		ParamsCodecGenericBuffer outputBuffer;
		dynData->GetParamsCodec()->writeObjectBAS( outputBuffer );
		m_fstrFullMsg.FromBuffer( outputBuffer.buffer, outputBuffer.size );
	}
	catch(...)
	{
		TRACEPF_ERROR("", "IRPCServer::ThrInvokeRPC Exception on writeObjectBAS");
		return;
	}
	fullsize = m_fstrFullMsg.GetLength();

	size_t iLenHeader = 6;
	CFwkString fstrLen;

	// Enviar caracter nulo
	fullsize = fullsize + 1;
	size_t iTotalLen = fullsize + iLenHeader;
	fstrLen.sprintf("%06d", iTotalLen);

	char* buffer = new char[iTotalLen + 1];

	ACE_OS::memcpy(buffer , (const char*)fstrLen, iLenHeader);
	ACE_OS::memcpy(buffer + iLenHeader, (const char*)m_fstrFullMsg, fullsize);
	//delete[]strData;

	size_t iStat = m_asStream.send_n((void*)buffer, iTotalLen);
	delete[]buffer;
	SIX_ASSERT(iStat == (iTotalLen));
}

short CRPCClient::FreeRequest()
{
	SendThreadMessage(MSG_FREEREQUEST);
	return 0;
}

void CRPCClient::ThrFreeRequest()
{
	if(m_bTaskInProgress)
	{
		ClearCB();
		m_asStream.close();
		CSocketRPCModule::St_GetInstance()->FreeRPCClient(this);
	}
}

