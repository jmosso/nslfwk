# nslFwk

Common framework for CONTROLNSL and MR. Use only as development framework at compiling time with static linking

Este framework consiste en un conjunto de cabeceras y librereas de enlace estático que se pueden usar para dar a su proyecto las siguientes capacidades:
- Manejo de log
- Uso de archivo de configuración
- Uso de hilos
- Uso de Timers
- Objetos de sincronía
- Manejo de cadenas, Buffers y DynamicStruct (a.k.a paramscodec)
- Manejo de archivos
- Manejo de archivos de mapeo llave-valor
- Envío de TRAPS SNMP v 1 y 2
- Transporte de paramscodec usando sockets
- Capacidad de mensajería paramscodec usando amq, colas unix o zmqX con interfaz única.
- Plantillas de desarrollo

# Prerequisitos

## Librerias Sixlabs
- EXCEPTION
- ASN1CODEC
- PARAMSCODEC
## Librerías de terceros
ACE
AMQ

# Compilando

El proceso de compilación se encarga de verificar la existencia de los pre requisitos.
Una salida típica de la compilación:

```
-- Release
-- EXCEPTION ------------------------------------------------
-- Found EXCEPTION_LIBRARY: /opt/sixlabs/lib64/libSIX_exception.so
-- Found EXCEPTION_INCLUDE_DIR: /opt/sixlabs/include
-- Found EXCEPTION_DEBUG_LIBRARY: /opt/sixlabs/lib64/libSIX_exception.so
-- Found EXCEPTION_ST_LIBRARY 
-- ASN1CODEC ------------------------------------------------
-- Found ASN1CODEC_LIBRARY: /opt/sixlabs/lib64/libSIX_asn1Codec.so
-- Found ASN1CODEC_INCLUDE_DIR: /opt/sixlabs/include
-- Found ASN1CODEC_DEBUG_LIBRARY: /opt/sixlabs/lib64/libSIX_asn1Codec.so
-- Found ASN1CODEC_ST_LIBRARY : 
-- PARAMSCODEC ------------------------------------------------
-- Found PARAMSCODEC_LIBRARY: /opt/sixlabs/lib64/libSIX_paramsCodec.so
-- Found PARAMSCODEC_INCLUDE_DIR: /opt/sixlabs/include
-- Found PARAMSCODEC_DEBUG_LIBRARY: /opt/sixlabs/lib64/libSIX_paramsCodec.so
-- Found PARAMSCODEC_ST_LIBRARY 
-- ACE ------------------------------------------------
-- Found ACE_LIBRARY: /usr/lib64/libACE.so
-- Found ACE_INCLUDE_DIR: /usr/include
-- Found ACE_LIBRARIES: /usr/include/lib
-- Found ACE_DEBUG_LIBRARY: /usr/lib64/libACE.so
-- AMQ ------------------------------------------------
-- Found AMQ_LIBRARY: /usr/lib64/libactivemq-cpp.so
-- Found AMQ_INCLUDE_DIR: /usr/include/activemq-cpp
-- Found AMQ_DEBUG_LIBRARY: /usr/lib64/libactivemq-cpp.so
-- Configuring done
-- Generating done
```

# Generación RPM
Use **gmake rpm** para generar los rpms de instalación


