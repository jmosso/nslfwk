#pragma once

#include "ISAS_Connector.h"
#include "ThreadModule/IThreadMessageCB.h"

#include <list>

class CSixChannelManager;
class IThread;
class CSAS_Connector : public ISAS_Connector
					 , public IThreadMessageCB
{
	friend class CSAS_ConnectorTopic;
public:
	CSAS_Connector(void);
	virtual ~CSAS_Connector(void){};

	void SetSixChannel(CSixChannelManager* pCM){m_pChannelManager = pCM;};
	bool MsgDetected(CDynamicStruct* pParam);
	void Initialize();

private:

	bool Request(CDynamicStruct* pParam);
	void ReleaseConnector();
	void SetCallback(ISAS_ConectorCB* pCallback);

	enum MESSAGES
	{
		INITIALIZE,
		REQUEST,
  		RELEASE,
    	MSG_DETECTED,
    	MSG_SET_CALLBACK,
	};

	CSixChannelManager* m_pChannelManager;
	void ThrRequest(NK_OBJPOINTER vParam);
	void ThrMsgDetected(NK_OBJPOINTER vParam);
	void ThrSetCallback(NK_OBJPOINTER vParam);
	void ThrInitialize();
	void ThrReleaseConnector();
	
	typedef std::list<CDynamicStruct*> LIST_MSGS;
	LIST_MSGS m_MsgStack;

	static int ms_iCounter;
	IThread* m_pThread;
	bool m_bInitialized;

	NK_DECLARE_MESSAGE_MAP()
};

class CSAS_ConnectorTopic : public CSAS_Connector
{
public:
	CSAS_ConnectorTopic(void);
	~CSAS_ConnectorTopic(void);

	virtual bool Request(CFwkString& fstrDestiny, CDynamicStruct* pParam, const char* strSessionID = 0);

};
