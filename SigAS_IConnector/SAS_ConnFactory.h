#pragma once

#include "Templates/TFactory.h"
#include "Templates/TSingleton.h"
#include "ThreadModule/IThreadMessageCB.h"
#include "SigAS_IConnector/ISAS_Connector.h"

class IThread;
class CDynamicStruct;
class CBusConnector;
class CSixChannelManager;

struct sCHANNEL_CONN_INFO
{
	sCHANNEL_CONN_INFO()
	{
		Clear();
	}

	void Clear()
	{
		BusServerIP.Clear();
		CtrlRegBusName.Clear();
		CtrlRegBusIdentifier.Clear();
		TargetQueueName.Clear();
		bEnableParsing = false;
	}

	void FillInfo(sCHANNEL_CONN_INFO& info)
	{
		BusServerIP = info.BusServerIP;
		CtrlRegBusName = info.CtrlRegBusName;
		CtrlRegBusIdentifier = info.CtrlRegBusIdentifier;
		TargetQueueName = info.TargetQueueName;
		bEnableParsing = info.bEnableParsing;
	}

	CFwkString CxAlias;
	CFwkString BusServerIP;
	CFwkString CtrlRegBusName;
	CFwkString CtrlRegBusIdentifier;
	CFwkString TargetQueueName;
	bool	bEnableParsing;
};

struct sSeqIdControl
{
	static TFactory<sSeqIdControl> g_SeqIdControlFactory;

	sSeqIdControl()
	{
		Clear();
	}

	void Clear()
	{
		iUseCount = 0;
		iCounter = 0;
	}

	int iUseCount;
	int iCounter;
};

class ACE_Recursive_Thread_Mutex;
class CSAS_ConnFactory : public TSingleton<CSAS_ConnFactory>
{
	friend class CSAS_Connector;
public:
	CSAS_ConnFactory(void);
	~CSAS_ConnFactory(void);

	int Init();
	int Exit();
	
	ISAS_Connector* GetClient(int iModuleType, CFwkString fstrUID, const CFwkString& fstrGroup, const CFwkString& fstrIP, const CFwkString& fstrIdentifier = "");
	ISAS_Connector* GetServer(int iModuleType, const CFwkString& fstrGroup, const CFwkString& fstrIP, const CFwkString& fstrIdentifier = "", const CFwkString& fstrCxAlias = "", bool bIsTopic = false);
	ISAS_Connector* GetServerSimple(int iModuleType, const CFwkString& fstrGroup, const CFwkString& fstrIP, const CFwkString& fstrIdentifier = "", bool bIsTopic = false);


	IThread* GetThread(int iSeq);
	bool GetChannelConnectionInfo(CFwkString& fstrAQMConnAlias, sCHANNEL_CONN_INFO& connInfo);

	static int GetSeqId(const CFwkString& fstrUID, int iCountInit = 0);
	static int ReleaseSeqId(const CFwkString& fstrUID);

private:

	CSixChannelManager* GetBusResource(int iModuleType, const CFwkString& fstrGroup, const CFwkString& fstrIP, const CFwkString& fstrIdentifier = "", bool bIsTopic = false);

	static ACE_Recursive_Thread_Mutex* ms_aceSEQIdSyncro;
	ACE_Recursive_Thread_Mutex* m_aceBusResSyncro;

	static int m_NumberOfThreads;
	IThread** m_pThreadArray;

	typedef std::map<CFwkString, CSixChannelManager*> MAPSTRINGTOVOID;
	MAPSTRINGTOVOID	m_mapChManager;

	typedef std::map<CFwkString, sSeqIdControl*> MAPSTRING2SEQID;
	static MAPSTRING2SEQID ms_MapUIDSeq;

	typedef std::map<CFwkString, sCHANNEL_CONN_INFO*> MAPSTR2CONNINFO;
	MAPSTR2CONNINFO m_mapConnInfo;
};
