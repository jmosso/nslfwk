
#include "SAS_ConnFactory.h"
#include "SixChannelManager.h"
#include "ThreadModule/ThreadFactory.h"
#include "ThreadModule/IThreadParams.h"
#include "SmallConfiguration/SmallConfiguration.h"
#include "Utilities/utilities.h"
#include "Utilities/Tracing.h"

#include "ace/OS.h"

int CSAS_ConnFactory::m_NumberOfThreads = 1;
ACE_Recursive_Thread_Mutex* CSAS_ConnFactory::ms_aceSEQIdSyncro = 0;

CSAS_ConnFactory::CSAS_ConnFactory(void)
{	
	ms_aceSEQIdSyncro = new ACE_Recursive_Thread_Mutex();
	m_aceBusResSyncro = new ACE_Recursive_Thread_Mutex();
}

CSAS_ConnFactory::~CSAS_ConnFactory(void)
{
	if(ms_aceSEQIdSyncro != 0)
		delete(ms_aceSEQIdSyncro);
	if(m_aceBusResSyncro != 0)
		delete(m_aceBusResSyncro);
}

int CSAS_ConnFactory::Init()
{
	if(VARIABLEINT(m_NumberOfThreads, "THREADS", "Connector") == false)
		m_NumberOfThreads = 1;

	m_pThreadArray = new IThread*[m_NumberOfThreads];
	for(int i = 0; i < m_NumberOfThreads; i++)
	{
		m_pThreadArray[i] = CThreadFactory::St_GetInstance()->GetResource();
		m_pThreadArray[i]->SetThreadName(CFwkString(1, "ConnFactory<0x%x>", m_pThreadArray[i]));
		m_pThreadArray[i]->Start();
	}

	sCHANNEL_CONN_INFO* pConnInfo = 0;

	char strAMQConnList[255];
	if(VARIABLECHAR(strAMQConnList, 255, "AMQ_PROPERTIES", "AMQAConnectionList") == true)
	{
		size_t iLen = ACE_OS::strlen(strAMQConnList);
		if(iLen == 0)
			return 0;

		char* token = 0;
		char* end_str;
		token = ACE_OS::strtok_r(strAMQConnList, ",", &end_str);
		int i = 0;
		while(token != 0)
		{
			pConnInfo = new sCHANNEL_CONN_INFO();
			pConnInfo->CxAlias.sprintf("AMQ_%s", token);
			VARIABLECHAR(pConnInfo->BusServerIP, pConnInfo->CxAlias, "BusServerIP");
			VARIABLECHAR(pConnInfo->CtrlRegBusName, pConnInfo->CxAlias, "CtrlRegBusName");
			VARIABLECHAR(pConnInfo->CtrlRegBusIdentifier, pConnInfo->CxAlias, "CtrlRegBusIdentifier");
			VARIABLECHAR(pConnInfo->TargetQueueName, pConnInfo->CxAlias, "TargetQueueName");

			m_mapConnInfo[token] = pConnInfo;

			i++;
			token = ACE_OS::strtok_r(NULL, ",", &end_str);
		}
	}

	if(VARIABLECHAR(strAMQConnList, 255, "AMQ_PROPERTIES", "AMQConnectionList") == true) // OJO!! esta al final debera ser la oficial. Si un servicio necesita una lista de conexiones
																								// debe estar en la configuracion del servicio
	{
		size_t iLen = ACE_OS::strlen(strAMQConnList);
		if(iLen == 0)
			return 0;

		char* token = 0;
		char* end_str;
		token = ACE_OS::strtok_r(strAMQConnList, ",", &end_str);
		int i = 0;
		while(token != 0)
		{
			pConnInfo = new sCHANNEL_CONN_INFO();
			pConnInfo->CxAlias.sprintf("AMQ_%s", token);
			VARIABLECHAR(pConnInfo->BusServerIP, pConnInfo->CxAlias, "BusServerIP");
			VARIABLECHAR(pConnInfo->CtrlRegBusName, pConnInfo->CxAlias, "CtrlRegBusName");
			VARIABLECHAR(pConnInfo->CtrlRegBusIdentifier, pConnInfo->CxAlias, "CtrlRegBusIdentifier");
			VARIABLECHAR(pConnInfo->TargetQueueName, pConnInfo->CxAlias, "TargetQueueName");

			m_mapConnInfo[token] = pConnInfo;

			i++;
			token = ACE_OS::strtok_r(NULL, ",", &end_str);
		}
	}

	strAMQConnList[0] = 0;
	if(VARIABLECHAR(strAMQConnList, 255, "NETConnections", "NETConnectionList") == true) 
	{
		size_t iLen = ACE_OS::strlen(strAMQConnList);
		if(iLen == 0)
			return 0;

		char* token = 0;
		char* end_str;
		token = ACE_OS::strtok_r(strAMQConnList, ",", &end_str);
		int i = 0;
		while(token != 0)
		{
			pConnInfo = new sCHANNEL_CONN_INFO();
			pConnInfo->CxAlias = token;
			VARIABLECHAR(pConnInfo->BusServerIP, pConnInfo->CxAlias, "BusServerIP");
			VARIABLECHAR(pConnInfo->CtrlRegBusName, pConnInfo->CxAlias, "CtrlRegBusName");
			VARIABLECHAR(pConnInfo->CtrlRegBusIdentifier, pConnInfo->CxAlias, "CtrlRegBusIdentifier");
			VARIABLECHAR(pConnInfo->TargetQueueName, pConnInfo->CxAlias, "TargetQueueName");
			VARIABLEBOOL(pConnInfo->bEnableParsing, pConnInfo->CxAlias, "EnableMAPParsing");

			m_mapConnInfo[token] = pConnInfo;

			i++;
			token = ACE_OS::strtok_r(NULL, ",", &end_str);
		}
	}

	// Adicionar al mapa de AMQConnInfo la conexion SMI de gestion por compatibilidad
	sCHANNEL_CONN_INFO connInfo;
	connInfo.CxAlias = "AMQ_SMI";
	if(GetChannelConnectionInfo(connInfo.CxAlias, connInfo) == false)
	{
		pConnInfo = new sCHANNEL_CONN_INFO();
		pConnInfo->CxAlias = "AMQ_SMI";
		VARIABLECHAR(pConnInfo->BusServerIP, pConnInfo->CxAlias, "BusServerIP");
		VARIABLECHAR(pConnInfo->CtrlRegBusName, pConnInfo->CxAlias, "CtrlRegBusName");
		VARIABLECHAR(pConnInfo->CtrlRegBusIdentifier, pConnInfo->CxAlias, "CtrlRegBusIdentifier");
		VARIABLECHAR(pConnInfo->TargetQueueName, pConnInfo->CxAlias, "TargetQueueName");
		m_mapConnInfo[pConnInfo->CxAlias] = pConnInfo;	
	}

	return 0;
}

bool CSAS_ConnFactory::GetChannelConnectionInfo(CFwkString& fstrAQMConnAlias, sCHANNEL_CONN_INFO& connInfo)
{
	MAPSTR2CONNINFO::iterator iter;
	iter = m_mapConnInfo.find(fstrAQMConnAlias);
	if(iter == m_mapConnInfo.end())
	{
		connInfo.Clear();
		return false;
	}
	sCHANNEL_CONN_INFO* pInfo = (*iter).second;
	connInfo.FillInfo(*pInfo);

	return true;
}


IThread* CSAS_ConnFactory::GetThread(int iSeq)
{
	return m_pThreadArray[iSeq%m_NumberOfThreads];
}

int CSAS_ConnFactory::Exit()
{
	for(int i = 0; i < m_NumberOfThreads; i++)
	{
		m_pThreadArray[i]->Stop();
		m_pThreadArray[i] = 0;
	}
	delete[] m_pThreadArray;

	return 0;
}

ISAS_Connector* CSAS_ConnFactory::GetClient(int iModuleType, CFwkString fstrUID, const CFwkString& fstrGroup, const CFwkString& fstrIP, const CFwkString& fstrIdentifier)
{
	if(fstrUID.IsEmpty())
	{
		FwkGetUUID(fstrUID);
		TRACEPF_WARNING("", "CSAS_ConnFactory::GetClient UID EMPTY!!!. Creatred: [%s]", fstrUID.GetBuffer());
	}

	CSixChannelManager* pBUSConn = GetBusResource(iModuleType, fstrGroup, fstrIP, fstrIdentifier, false);
	ISAS_Connector* tmp = pBUSConn->GetConnector(fstrUID);
	return tmp;
}

ISAS_Connector* CSAS_ConnFactory::GetServer(int iModuleType, const CFwkString& fstrGroup, const CFwkString& fstrIP, const CFwkString& fstrIdentifier, const CFwkString& fstrCxAlias, bool bIsTopic)
{
	CSixChannelManager* pBUSConn = GetBusResource(iModuleType, fstrGroup, fstrIP, fstrIdentifier, bIsTopic);

	if(pBUSConn->IsServer() == true)
	{
		TRACEPF_ERROR("", "CSAS_ConnFactory::GetServer server ALREAY created!!!. ONLY ONE OWNER allowed\nBusServerIP=[%s]\nCtrlRegBusName=[%s]", fstrIP.GetBuffer(), fstrGroup.GetBuffer());
		return 0;
	}
	
	pBUSConn->IsServer(true);
	pBUSConn->SetCxAlias(fstrCxAlias);
	CFwkString fstrUUID;
	FwkGetUUID(fstrUUID);
	ISAS_Connector* tmp = pBUSConn->GetConnector(fstrUUID);
	pBUSConn->SetOwnerCallback(tmp);
	return tmp;
}

ISAS_Connector* CSAS_ConnFactory::GetServerSimple(int iModuleType, const CFwkString& fstrGroup, const CFwkString& fstrIP, const CFwkString& fstrIdentifier, bool bIsTopic)
{
	CSixChannelManager* pBUSConn = GetBusResource(iModuleType, fstrGroup, fstrIP, fstrIdentifier, bIsTopic);
	if(pBUSConn == 0)
		return 0;
	
	// es server y ya existe. No se puede usar
	if(pBUSConn->IsServer() == true)
		return 0;

	// Ojo, el servidor simple es de TOPICO y en general habra uno solo (GESTION) se hace NEW del objeto.
	CSAS_ConnectorTopic* tmp = new CSAS_ConnectorTopic();
	pBUSConn->SetOwnerCallback(tmp);
	tmp->SetSixChannel(pBUSConn);

	return tmp;
}

CSixChannelManager* CSAS_ConnFactory::GetBusResource(int iModuleType, const CFwkString& fstrGroup, const CFwkString& fstrIP, const CFwkString& fstrIdentifier, bool bIsTopic)
{
	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard, *m_aceBusResSyncro, 0);

	CSixChannelManager* pBusConn = 0;
	CFwkString fstrKey;
	fstrKey.sprintf("%s_%s", fstrGroup.GetBuffer(), fstrIP.GetBuffer());

	MAPSTRINGTOVOID::iterator theIter;
	theIter = m_mapChManager.find(fstrKey);
	if(theIter == m_mapChManager.end())
	{
		CSixChannelManager* pConnChannel = new CSixChannelManager();
		m_mapChManager[fstrKey] = pConnChannel;
		pConnChannel->Init(iModuleType, fstrGroup, fstrIP, fstrIdentifier, bIsTopic);
		return pConnChannel;
	}

	pBusConn = theIter->second;
	return (pBusConn);
}


CSAS_ConnFactory::MAPSTRING2SEQID CSAS_ConnFactory::ms_MapUIDSeq;
TFactory<sSeqIdControl> sSeqIdControl::g_SeqIdControlFactory;

int CSAS_ConnFactory::GetSeqId(const CFwkString& fstrUID, int iCountInit)
{
	if(fstrUID.IsEmpty() == true)
	{
		TRACEPF_DEBUG("",  "CSAS_ConnFactory::GetSeqId UID VACIO");
		return 99;
	}

	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard, *ms_aceSEQIdSyncro, 0);

	MAPSTRING2SEQID::iterator iter = ms_MapUIDSeq.find(fstrUID);
	sSeqIdControl* tmpSeq = 0;
	if(iter == ms_MapUIDSeq.end())
	{
		tmpSeq = sSeqIdControl::g_SeqIdControlFactory.GetResource();
		tmpSeq->iCounter = iCountInit;
		ms_MapUIDSeq[fstrUID] = tmpSeq;
	}
	else
		tmpSeq = (*iter).second;

	tmpSeq->iUseCount++;
	tmpSeq->iCounter++;

	return (tmpSeq->iCounter);
}

int CSAS_ConnFactory::ReleaseSeqId(const CFwkString& fstrUID)
{
	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard, *ms_aceSEQIdSyncro, 0);

	MAPSTRING2SEQID::iterator iter = ms_MapUIDSeq.find(fstrUID);
	if(iter == ms_MapUIDSeq.end())
		return 0;

	sSeqIdControl* tmpSeq = (*iter).second;
	tmpSeq->iUseCount--;

	if(tmpSeq->iUseCount > 0)
		return 0;

	ms_MapUIDSeq.erase(iter);
	tmpSeq->Clear();
	sSeqIdControl::g_SeqIdControlFactory.FreeResource(tmpSeq);
	return 0;
}
