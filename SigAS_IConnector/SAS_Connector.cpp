
#include "SAS_Connector.h"
#include "SAS_ConnFactory.h"
#include "SixChannelManager.h"
#include "ThreadModule/IThread.h"
#include "FrameworkUtilities/DynamicStruct.h"
#include "CommChannel/SGenericParamFullNames.hpp"
#include "Utilities/Tracing.h"

int CSAS_Connector::ms_iCounter = 0;

NK_BEGIN_MESSAGE_MAP(CSAS_Connector, IThreadMessageCB)
	ON_NKMESSAGE(INITIALIZE, ThrInitialize);
	ON_NKMESSAGE_POINTER(REQUEST, ThrRequest);
	ON_NKMESSAGE_POINTER(MSG_DETECTED, ThrMsgDetected);
	ON_NKMESSAGE_POINTER(MSG_SET_CALLBACK, ThrSetCallback);
	ON_NKMESSAGE(RELEASE, ThrReleaseConnector);
NK_END_MESSAGE_MAP()

CSAS_Connector::CSAS_Connector(void)
{
	m_pChannelManager = 0;
	m_pCallback = 0;
	m_bInitialized = false;
	m_fstrUID.Clear();
	m_bAllowUIDOverwrite = false;
	m_pThread = CSAS_ConnFactory::St_GetInstance()->GetThread(ms_iCounter++);
}

void CSAS_Connector::Initialize()
{
	m_pThread->SendThreadMessage(INITIALIZE, this);
}

void CSAS_Connector::ThrInitialize()
{
	TRACEPF_DEBUG(m_fstrUID,  "Connector::ThrInitialize<0x%x>", this);
	m_bInitialized = true;
}

void CSAS_Connector::ReleaseConnector()
{
	m_pThread->SendThreadMessage(RELEASE, this);
}

void CSAS_Connector::ThrReleaseConnector()
{
	LIST_MSGS::iterator it;
	for(it = m_MsgStack.begin(); it != m_MsgStack.end(); it++)
	{
		DYNFACTFREERES(*it);
	}
	m_MsgStack.clear();
	m_bInitialized = false;
	m_bAllowUIDOverwrite = false;

	CFwkString fstrUIDTmp = m_fstrUID;
	ISAS_ConectorCB* cbTemp = m_pCallback;
	m_fstrUID.Clear();
	m_pCallback = 0;

	m_pChannelManager->ReleaseConnector(fstrUIDTmp, this);
	if(cbTemp != 0)
		cbTemp->ConnectorReleaseDone();

	TRACEPF_DEBUG(fstrUIDTmp, "Connector::ThrReleaseConnector<0x%x>", this);
}

void CSAS_Connector::SetCallback(ISAS_ConectorCB* pCallback)
{
	m_pThread->SendThreadMessage(MSG_SET_CALLBACK, this, pCallback);
}

void CSAS_Connector::ThrSetCallback(NK_OBJPOINTER vParam)
{
	m_pCallback = (ISAS_ConectorCB*)vParam;

	LIST_MSGS::iterator it;
	if(m_pCallback == 0)
	{
		TRACEPF_DEBUG(m_fstrUID, "Connector::ThrSetCallback<0x%x> NULO", this);
		for(it = m_MsgStack.begin(); it != m_MsgStack.end(); it++)
			DYNFACTFREERES(*it);

		m_MsgStack.clear();	
		return;
	}

	TRACEPF_DEBUG(m_fstrUID,  "Connector::ThrSetCallback<0x%x> cb<0x%x>", this, m_pCallback);
	
	if(m_MsgStack.empty() == true)
		return;

	CDynamicStruct* pDyn = 0;
	for(it = m_MsgStack.begin(); it != m_MsgStack.end(); it++)
	{
		pDyn = *it;
		if(m_pCallback != 0)
		{
			CFwkString fstrResourceId;
			pDyn->GetString(_six_kHeader_ResId, fstrResourceId);

			if(fstrResourceId != m_fstrUID)
			{
				TRACEPF_ERROR(m_fstrUID, "Connector::ThrSetCallback<0x%x> QUEUED MESSAGE WITH DIFFERENT UID [%s] ", this, (const char*)fstrResourceId);
				DYNFACTFREERES(pDyn);
				continue;
			}

			m_pCallback->IncomingRequest(pDyn);
		}
		else
			DYNFACTFREERES(pDyn);
	}
	m_MsgStack.clear();	
}

bool CSAS_Connector::Request(CDynamicStruct* pParam)
{
	m_pThread->SendThreadMessage(REQUEST, this, pParam);
	return true;
}

void CSAS_Connector::ThrRequest(NK_OBJPOINTER vParam)
{
	CDynamicStruct* pParam = (CDynamicStruct*)vParam;
	
	if(pParam == 0)
	{
		TRACEPF_ERROR(m_fstrUID, "Connector::ThrRequest<0x%x> pParam NULL", this);
		return;
	}

	TRACEPF_DEBUG(m_fstrUID,  "Connector::ThrRequest<0x%x> AllowUIDOverwrite[%d]", this, m_bAllowUIDOverwrite);
	if(m_bAllowUIDOverwrite == false)
		pParam->AddString(_six_kHeader_ResId, m_fstrUID);

	m_pChannelManager->SendMessage(pParam);
}


bool CSAS_Connector::MsgDetected(CDynamicStruct* pParam)
{
	// TRACEPF_DEBUG("", "CSAS_Connector::MsgDetected<0x%x> dyn<0x%x>", this, pParam);
	m_pThread->SendThreadMessage(MSG_DETECTED, this, pParam);
	return true;
}

void CSAS_Connector::ThrMsgDetected(NK_OBJPOINTER vParam)
{
	TRACEPF_DEBUG(m_fstrUID, "Connector::ThrMsgDetected<0x%x> m_pCallback <0x%x>", this, m_pCallback);

	CDynamicStruct* pParam = (CDynamicStruct*)vParam;

	if(m_pCallback != 0)
		m_pCallback->IncomingRequest(pParam);
	else
	{
		if(m_bInitialized == false)
		{
			TRACEPF_WARNING(m_fstrUID, "Connector::ThrMsgDetected<0x%x> NOT INITIALIZED", this);
			return;
		}
		// Si el objeto no tiene  callback, se encola la mensajería.
		m_MsgStack.push_back(pParam);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CSAS_ConnectorTopic::CSAS_ConnectorTopic(void)
{

}

CSAS_ConnectorTopic::~CSAS_ConnectorTopic(void)
{

}

bool CSAS_ConnectorTopic::Request(CFwkString& fstrDestiny, CDynamicStruct* pParam, const char* strSessionID )
{
	if(pParam == 0)
		return false;

	if(pParam->IsParam(_six_kHeader_ResId) == false)
		pParam->AddString(_six_kHeader_ResId, m_fstrUID);
		
	if(fstrDestiny.IsEmpty() == false)
		pParam->AddString(_six_kHeader_Dest_Id, fstrDestiny);
	
	m_pThread->SendThreadMessage(REQUEST, this, pParam);

	return true;
}

