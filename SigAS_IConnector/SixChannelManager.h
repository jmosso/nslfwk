#pragma once

#include "CommChannel/ISixCommChannel.h"
#include "ThreadModule/IThreadMessageCB.h"
#include "Templates/TFactory.h"
#include "SAS_Connector.h"

class IThread;
class ISixCommChannel;

class ACE_Recursive_Thread_Mutex;
class CSixChannelManager : public ISixCommChannelEvent
						 , public TFactory<CSAS_Connector, ISAS_Connector> 
						 , public IThreadMessageCB
{
public:
	CSixChannelManager();
	virtual ~CSixChannelManager();

	virtual ISAS_Connector* CreateInstance();
	virtual void Init(int iModuleType, const CFwkString& fstrGroup, const CFwkString& fstrURL, const CFwkString& fstrIdentifier, bool bIsTopic = false);
	virtual void Release();
	virtual bool SendMessage(CDynamicStruct *pParam = 0);

	ISAS_Connector* GetConnector(const CFwkString& fstrUUID);
	int ReleaseConnector(const CFwkString& fstrUUID, ISAS_Connector* p);
	
	void SetOwnerCallback(ISAS_Connector* p){m_pOwner = p;};
	void SetCxAlias(const CFwkString& fstrCxAlias){m_fstrCxAlias = fstrCxAlias;}
	void IsServer(bool b){m_bIsServer = b;}
	bool IsServer(){return m_bIsServer;}

	void ProcessCommChannelEvent(CDynamicStruct* pDynEvent);
	
protected:

	ISAS_Connector* m_pOwner;
	bool m_bIsServer;

private:

	enum eMESSAGES
	{
		MSG_ON_MESSAGE,
	};

	IThread* m_pThread;
	ISixCommChannel* m_pChannel;

	int ThrProcessCommChannelEvent(NK_OBJPOINTER pData);

	CFwkString m_fstrURLConnection;
	CFwkString m_fstrGroupName;
	CFwkString m_fstrUniqueName;
	CFwkString m_fstrDescription;
	CFwkString m_fstrCxAlias;
	int	m_iModuleType;
	int m_iUseTopic;

	typedef std::map<CFwkString, ISAS_Connector*> MAP_STR2CONN;
	MAP_STR2CONN m_mapUID2Connector;
	ACE_Recursive_Thread_Mutex* m_pSyncMap;


	NK_DECLARE_MESSAGE_MAP() 


};
