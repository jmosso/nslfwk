#pragma once

#include "FrameworkUtilities/FwkString.h"

class CDynamicStruct;
class ISAS_Connector;

class ISAS_ConectorCB
{
public:
	// Conector Cliente
	virtual bool IncomingRequest(CDynamicStruct* pParam) = 0;
	// Si el servidor reescribe este mensaje, DEBE retornar true si desea que el mensaje se encole en el conector
	virtual bool NewSession(CDynamicStruct* pParam, ISAS_Connector* pConnector){return true;};

	virtual void ConnectorReleaseDone(){};
};

class ISAS_Connector
{
public:

	// pParam DEBE SER un dynamic pedido a FACTORIA. EL conector se encarga de liberarlo!!!
	virtual bool Request(CDynamicStruct* pParam) = 0;
	virtual void ReleaseConnector() = 0;
	virtual void Initialize() = 0;

	virtual void SetCallback(ISAS_ConectorCB* pCallback){m_pCallback = pCallback;}
	ISAS_ConectorCB* GetCallback(){return m_pCallback;}
	void SetUID(const CFwkString& fstrUid){m_fstrUID = fstrUid;}
	const char* GetUID(){return (m_fstrUID);}
	virtual void AllowUIDOverwrite(bool allow){m_bAllowUIDOverwrite = allow;};

protected:
	CFwkString			m_fstrUID;
	ISAS_ConectorCB*	m_pCallback;
	bool				m_bAllowUIDOverwrite;
		
private:

};
