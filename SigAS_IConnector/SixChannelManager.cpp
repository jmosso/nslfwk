
#include "SixChannelManager.h"
#include "ThreadModule/ThreadFactory.h"
#include "CommChannel/SixCommChannelModule.h"
#include "CommChannel/SGenericParamFullNames.hpp"
#include "FrameworkUtilities/DynamicStruct.h"
#include "SAS_Connector.h"
#include "Utilities/Tracing.h"

NK_BEGIN_MESSAGE_MAP(CSixChannelManager, IThreadMessageCB)
	ON_NKMESSAGE_POINTER(MSG_ON_MESSAGE, ThrProcessCommChannelEvent);
NK_END_MESSAGE_MAP()

CSixChannelManager::CSixChannelManager()
{
	m_bIsServer = false;
	m_pOwner = 0;
	m_pChannel = 0;

	m_pSyncMap = new ACE_Recursive_Thread_Mutex();

	m_pThread = CThreadFactory::St_GetInstance()->GetResource();
	m_pThread->SetThreadName(CFwkString(1, "CSixChannelManager<0x%x>", this));
	m_pThread->Start();
}

CSixChannelManager::~CSixChannelManager()
{
	if(m_pSyncMap != 0)
		delete(m_pSyncMap);
}

ISAS_Connector* CSixChannelManager::CreateInstance()
{
	CSAS_Connector* pConnImp = new CSAS_Connector();
	pConnImp->SetSixChannel(this); // el channel se fija una unica vez cuando se crea. UN conector pertenece a un tipo de Manager
	return pConnImp;
}

void CSixChannelManager::Init(int iModuleType, const CFwkString& fstrGroup, const CFwkString& fstrURL, const CFwkString& fstrIdentifier, bool bIsTopic)
{
	size_t iIndex = fstrURL.Find("ZEROMQ");
	if(iIndex != 0) // AMQ o COLA UNIX
	{
		if(fstrGroup.CheckIsNumeric() && fstrIdentifier.CheckIsNumeric())
			m_pChannel = CSixCommChannelModule::St_GetInstance()->GetCommChannel(ISixCommChannel::SIX_CHANNEL_UNIXQUEUE);
		else
			m_pChannel = CSixCommChannelModule::St_GetInstance()->GetCommChannel(ISixCommChannel::SIX_CHANNEL_AMQ);
	}
	else
	{
		CFwkString fstrMode = fstrURL.Left(7);
		if(fstrMode == "ZEROMQ&" || fstrMode == "ZEROMQW") // compatibilidad para version original ZEROMQ&. Los valores son ahora ZEROMQW, ZEROMQM y ZEROMQS
			m_pChannel = CSixCommChannelModule::St_GetInstance()->GetCommChannel(ISixCommChannel::SIX_CHANNEL_ZMQ);
		else if (fstrMode == "ZEROMQM")
			m_pChannel = CSixCommChannelModule::St_GetInstance()->GetCommChannel(ISixCommChannel::SIX_CHANNEL_ZMQ_MANAGER);
		else if(fstrMode == "ZEROMQS")
			m_pChannel = CSixCommChannelModule::St_GetInstance()->GetCommChannel(ISixCommChannel::SIX_CHANNEL_ZMQ_SENDER);
		else
		{
			TRACEPF_WARNING("", "CSixChannelManager::Init UNKNOWN ZMQ channel type [%s]. Creating Manager by default!!!", fstrMode.GetBuffer());
			m_pChannel = CSixCommChannelModule::St_GetInstance()->GetCommChannel(ISixCommChannel::SIX_CHANNEL_ZMQ_MANAGER);
		}
	}

	CDynamicStruct dynConnInfo;
	dynConnInfo.AddString("URL", fstrURL);
	dynConnInfo.AddString("NAME_GROUP", fstrGroup);
	dynConnInfo.AddString("NAME_UNIQUE", fstrIdentifier);
	dynConnInfo.AddInt("MODULE_TYPE", iModuleType);
	dynConnInfo.AddInt("USE_TOPIC", bIsTopic);

	m_pChannel->SetEventCallback(this);
	m_pChannel->Start(&dynConnInfo);
}

void CSixChannelManager::Release()
{
}

bool CSixChannelManager::SendMessage(CDynamicStruct *pParam)
{
	m_pChannel->SendChannelMessage(pParam);
	return true;
}

void CSixChannelManager::ProcessCommChannelEvent(CDynamicStruct* pDynEvent)
{
	TRACEPF_COMPATIBLE("", MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelManager::ProcessCommChannelEvent<0x%x> dyn<0x%x> thr<0x%x> QueueSize %d", this, pDynEvent, m_pThread->GetId(), m_pThread->GetQueueSize());
	m_pThread->SendThreadMessage(MSG_ON_MESSAGE, this, pDynEvent);
}

int CSixChannelManager::ThrProcessCommChannelEvent(NK_OBJPOINTER pData)
{
	CDynamicStruct* pDynData = (CDynamicStruct*)pData;

	CFwkString fstrUUID;
	pDynData->GetString(_six_kHeader_ResId, fstrUUID);

	TRACEPF_COMPATIBLE(fstrUUID, MASK_THR_CHECKER, TRACE_ALWAYS, "CSixChannelManager::ThrProcessCommChannelEvent<0x%x> dyn<0x%x>", this, pDynData);

	// Analizar si es servidor simple o si se debe crear uid y lanzar algo...
	if(fstrUUID.IsEmpty() == true)
	{
		TRACEPF_ERROR("", "CSixChannelManager::ThrProcessCommChannelEvent resourceId empty. CAN NOT CONTINUE!!!");
		DYNFACTFREERES(pDynData);
		return 0;
	}

	CSAS_Connector* pConnImp = 0;
	ISAS_ConectorCB* pCallback = 0;

	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard, *m_pSyncMap, 0);

	MAP_STR2CONN::iterator it;
	it = m_mapUID2Connector.find(fstrUUID);
	if(it != m_mapUID2Connector.end()) // Encuentra conector. Invocar callback de mensaje
	{
		pConnImp = (CSAS_Connector*)it->second;
		pConnImp->MsgDetected(pDynData);
		return 0;
	}

	if(!m_fstrCxAlias.IsEmpty())
		pDynData->AddString("CX_ALIAS", m_fstrCxAlias);

	if(m_bIsServer)
	{
		pConnImp = (CSAS_Connector*)GetConnector(fstrUUID);

		pCallback = m_pOwner->GetCallback();
		bool bHandle = true;
		if(pCallback != 0)
			bHandle = pCallback->NewSession(pDynData, pConnImp);
		if(bHandle)
			pConnImp->MsgDetected(pDynData);
		return 0;
	}
	else
	{
		// Es un cliente al cual le llegan TODOS LOS mensajes sin discriminar por UID.
		// Se manda en el NewSession por consistencia SIN crear conector.
		// El DynamicStruct se debe usar y liberar en la reescritura del NewSession y DEBE retornar false
		bool bHandle = true;
		if(m_pOwner != 0)
		{
			pCallback = m_pOwner->GetCallback();
			if(pCallback != 0)
				bHandle = pCallback->NewSession(pDynData, 0);
		}
		if(bHandle)
		{
			TRACEPF_ERROR("", "CSixChannelManager::ThrProcessCommChannelEvent NO OWNER for UID %s", fstrUUID.GetBuffer());
			DYNFACTFREERES(pDynData);
		}	

		return 0;
	}

	return 0;
}

ISAS_Connector* CSixChannelManager::GetConnector(const CFwkString& fstrUUID)
{
	ISAS_Connector* pConn = GetResource();

	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard, *m_pSyncMap, 0);
	m_mapUID2Connector[fstrUUID] = pConn;

	pConn->SetUID(fstrUUID);
	pConn->Initialize();
	return (pConn);
}

int CSixChannelManager::ReleaseConnector(const CFwkString& fstrUUID, ISAS_Connector* p)
{
	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard, *m_pSyncMap, 0);

	MAP_STR2CONN::iterator it;
	it = m_mapUID2Connector.find(fstrUUID);
	if(it != m_mapUID2Connector.end()) 
	{
		if(it->second == p)
			m_mapUID2Connector.erase(fstrUUID);
		FreeResource(p);
	}

	return 0;
}
