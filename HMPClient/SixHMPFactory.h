#pragma once

#include "Templates/TFactory.h"
#include "Templates/TSingleton.h"
#include "ISixHMPClient.h"

class IThread;
class CSixHMPFactory : public TSingleton<CSixHMPFactory>
{
	class CSixHMPFactoryDP;
public:
	CSixHMPFactory();
	virtual ~CSixHMPFactory();

	int Init();
	int Exit();

	enum IMP_TYPE
	{
		REMOTE,
		LOCAL,
		SIMUL,
	};

	IThread* GetThread(int iSeq);

	ISixHMPClient* GetResource(IMP_TYPE type = REMOTE);
	bool FreeResource(ISixHMPClient* pObj, IMP_TYPE type = REMOTE);

protected:
	CSixHMPFactoryDP* m_factoryDP;

	static int ms_iMaxThreads;
	IThread** m_pThreadArray;
};
