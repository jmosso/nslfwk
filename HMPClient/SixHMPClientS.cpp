
#include "SixHMPClientS.h"
#include "SixHMPFactory.h"
#include "ThreadModule/IThread.h"
#include "Utilities/Tracing.h"
#include "CommChannel/SGenericParamFullNames.hpp"
#include "TimerModule/TimerFactory.h"

#include <paramsCodec.hpp>

NK_BEGIN_MESSAGE_MAP(CSixHMPClientS, IThreadMessageCB)
	ON_NKMESSAGE_POINTER(MSG_GET_BEARER, ThrGetBearer);
	ON_NKMESSAGE_POINTER(MSG_COMPLETE_BEARER, ThrCompleteBearer);
	ON_NKMESSAGE_POINTER(MSG_PERFORM_ACTION, ThrPerformAction);
	ON_NKMESSAGE(MSG_RELEASE_BEARER, ThrReleaseBearer);
	ON_NKMESSAGE_UINT(MSG_TIMEOUT, ThrTimeOut);
NK_END_MESSAGE_MAP()

int CSixHMPClientS::ms_iCounter = 0;
CSixHMPClientS::CSixHMPClientS(void)
{
	m_pCallback = 0;
	m_iSequenceId = ms_iCounter;
	ms_iCounter++;
	m_pThread = CSixHMPFactory::St_GetInstance()->GetThread(m_iSequenceId);
}

CSixHMPClientS::~CSixHMPClientS(void)
{

}

// OJO! Estas funciones deber�an implementarse en su propio hilo. 

bool CSixHMPClientS::GetBearer(ParamsCodec* pPC)
{
	return(m_pThread->SendThreadMessage(MSG_GET_BEARER, this, pPC));
}

void CSixHMPClientS::ThrGetBearer(NK_OBJPOINTER pData)
{
	ParamsCodec* pPC = (ParamsCodec*)pData;
	std::string strIp = pPC->getString("client.ipAddress"); 
	int64_t iPort = pPC->getInteger("client.port"); 
	// Lo ideal es que reciba el sdp para que se negocie en el server
	std::string strSDP = pPC->getString("client.sdp");
	m_fstrResourceId = pPC->getString(_six_kHeader_ResId);
	delete(pPC);

	TRACEPF_DEBUG(m_fstrResourceId, "ThrGetBearer ip[%s] port[%d] sdp[%s]", strIp.c_str(), iPort, strSDP.c_str());
	// Es la primera funci�n de interacci�n. Aqu� se deber�a realizar el proceso de conexion con el server HMP. 
	// Cuando el server haya realizado su tarea, responder� los respectivos par�metros. Esta lib debe invocar al callback:

	if(m_pCallback)
	{
		ParamsCodec* pPC = new ParamsCodec();
		pPC->setInteger("event", ISixHMPClientEvent::GET_BEARER_DONE);
		pPC->setString("server.ipAddress", "127.0.0.1");
		pPC->setInteger("server.port", 1234);
		pPC->setString("server.sdp", "v=0...");
		m_pCallback->ProcessSixHMPClientEvent(pPC);
	}
}

bool CSixHMPClientS::CompleteBearer(ParamsCodec* pPC)
{
	return(m_pThread->SendThreadMessage(MSG_COMPLETE_BEARER, this, pPC));
}

void CSixHMPClientS::ThrCompleteBearer(NK_OBJPOINTER pData)
{
	ParamsCodec* pPC = (ParamsCodec*)pData;
	std::string strIp = pPC->getString("client.ipAddress"); 
	int64_t iPort = pPC->getInteger("client.port"); 
	delete(pPC);

	//realizar el proceso de conexion con el server HMP. Cuando el proceso este listo debe retornar parametros de conexion del lado server. Invocar:

	if(m_pCallback)
	{
		ParamsCodec* pPC = new ParamsCodec();
		pPC->setInteger("event", ISixHMPClientEvent::COMPLETE_BEARER_DONE);
		pPC->setString("server.ipAddress", "ipServer");
		pPC->setInteger("server.port", 1234);
		m_pCallback->ProcessSixHMPClientEvent(pPC);
	}
}

bool CSixHMPClientS::PerformAction(ParamsCodec* pDynData)
{
	return (m_pThread->SendThreadMessage(MSG_PERFORM_ACTION, this, pDynData));
}

void CSixHMPClientS::ThrPerformAction(NK_OBJPOINTER pData)
{
	ParamsCodec* pPC = (ParamsCodec*)pData;
	int64_t iOperType = pPC->getInteger(_six_kOper_Type);

	switch(iOperType)
	{
	case MR_PLAYMESSAGE:
		{
			ParamsCodec* pPC = (ParamsCodec*)pData;
			std::string strPath = pPC->getString("oper.FileName"); 
			delete(pPC);

			TRACEPF_DEBUG(m_fstrResourceId, "ThrPerformAction MR_PLAYMESSAGE msg[%s]", strPath.c_str());
			// Usando la conexi�n ya establecida en GetBearer, realizar la petici�n de playMessage.
			// En ese momento, el server deberia enviar RTP hacia la ip/puerto con codec negociados. Cuando la reproducci�n termine, el server informar� via la conexi�n
			// el resultado de la reproducci�n. La lib debe invocar:

			CTimerFactory::St_GetInstance()->SetTimer(TIMER_PLAY_MESSAGE, 3000, this);		
		}
		break;

	default:
		break;	
	}
}

void CSixHMPClientS::TimeOut(size_t iEventId, size_t iTimerId)
{
	m_pThread->SendThreadMessage(MSG_TIMEOUT, this, (NK_OBJPOINTER)iEventId);
}

void CSixHMPClientS::ThrTimeOut(size_t iEventId)
{
	switch(iEventId)
	{
	case TIMER_PLAY_MESSAGE:
		{
			if(m_pCallback)
			{
				ParamsCodec* pPC = new ParamsCodec();
				pPC->setInteger("event", ISixHMPClientEvent::ACTION_DONE);
				pPC->setInteger(_six_kOper_TypeRef, MR_PLAYMESSAGE);
				pPC->setInteger("reason", 0); // reusar codigos de causa de terminacion de mensaje
				m_pCallback->ProcessSixHMPClientEvent(pPC);
			}		
		}
		break;

	default:
		break;	
	}
}

bool CSixHMPClientS::ReleaseBearer()
{
	return(m_pThread->SendThreadMessage(MSG_RELEASE_BEARER, this));
}

void CSixHMPClientS::ThrReleaseBearer()
{
	TRACEPF_DEBUG(m_fstrResourceId, "ThrReleaseBearer");

	Clean();
	// proceso de finalizacion.
	if(m_pCallback)
	{
		ParamsCodec* pPC = new ParamsCodec();
		pPC->setInteger("event", ISixHMPClientEvent::RELEASE_DONE);
		pPC->setInteger("reason", 0); // reusar codigos de causa de terminacion de mensaje
		m_pCallback->ProcessSixHMPClientEvent(pPC);
	}
}

void CSixHMPClientS::Clean()
{
	TRACEPF_DEBUG(m_fstrResourceId, "Clean");

	m_fstrResourceId.Clear();
}