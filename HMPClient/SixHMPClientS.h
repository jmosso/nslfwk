#pragma once
#include "ISixHMPClient.h"
#include "ThreadModule/IThreadMessageCB.h"
#include "FrameworkUtilities/FwkString.h"
#include "TimerModule/ITimerCB.h"

class IThread;
class CSixHMPClientS : public ISixHMPClient
					 , public IThreadMessageCB
					 , public ITimerCB
{
public:
	CSixHMPClientS();
	virtual ~CSixHMPClientS();

	bool GetBearer(ParamsCodec* pDynIn);
	bool CompleteBearer(ParamsCodec* pDynIn);
	bool UpdateBearer(ParamsCodec* pDynIn){return true;};
	bool PerformAction(ParamsCodec* pDynData);

	bool ReleaseBearer();

private:

	enum MESSAGES
	{
		MSG_GET_BEARER,
		MSG_COMPLETE_BEARER,
		MSG_UPDATE_BEARER,
		MSG_PERFORM_ACTION,
		MSG_RELEASE_BEARER,

		MSG_TIMEOUT,
	};

	enum TIMERS
	{
		TIMER_PLAY_MESSAGE = 1,

	};

	void ThrGetBearer(NK_OBJPOINTER pData);
	void ThrCompleteBearer(NK_OBJPOINTER pData);
	void ThrUpdateBearer(NK_OBJPOINTER pData);
	void ThrPerformAction(NK_OBJPOINTER pData);

	void ThrReleaseBearer();

	void TimeOut(size_t iEventId, size_t iTimerId);
	void ThrTimeOut(size_t iEventId);


	void Clean();

	IThread* m_pThread;

	int m_iSequenceId;
	static int ms_iCounter;
	CFwkString m_fstrResourceId;

	NK_DECLARE_MESSAGE_MAP()
};
