
#include "SixHMPFactory.h"
#include "SixHMPClientS.h"
#include "ThreadModule/ThreadFactory.h"
#include <paramsCodec.hpp>

class CSixHMPFactory::CSixHMPFactoryDP
{
public:
	CSixHMPFactoryDP(){};
	~CSixHMPFactoryDP(){};

	 TFactory<CSixHMPClientS, ISixHMPClient> m_factSimul;
};

// pedirlo a config
int CSixHMPFactory::ms_iMaxThreads = 5;
CSixHMPFactory::CSixHMPFactory(): m_factoryDP(new CSixHMPFactoryDP)
{
	m_factoryDP->m_factSimul.SetFactName("HMPFactorySimul");
}

CSixHMPFactory::~CSixHMPFactory()
{
}

int CSixHMPFactory::Init()
{
	m_pThreadArray = new IThread*[ms_iMaxThreads];
	for(int i = 0; i < ms_iMaxThreads; i++)
	{
		m_pThreadArray[i] = CThreadFactory::St_GetInstance()->GetResource();
		m_pThreadArray[i]->SetThreadName(CFwkString(1, "SixHMPFactory<0x%x>", m_pThreadArray[i]));
		m_pThreadArray[i]->Start();
	}

	return 0;
}

int CSixHMPFactory::Exit()
{
	for(int i = 0; i < ms_iMaxThreads; i++)
	{
		m_pThreadArray[i]->Stop();
		m_pThreadArray[i] = 0;
	}
	delete[] m_pThreadArray;

	return 0;
}

IThread* CSixHMPFactory::GetThread(int iSeq)
{
	return m_pThreadArray[iSeq%ms_iMaxThreads];
}


ISixHMPClient* CSixHMPFactory::GetResource(IMP_TYPE type)
{
	switch(type)
	{
	case REMOTE:
		return 0;
		break;

	case SIMUL:
		return m_factoryDP->m_factSimul.GetResource();
		break;

	default:
		return 0;
		break;
	}
}

bool CSixHMPFactory::FreeResource(ISixHMPClient* pObj, IMP_TYPE type)
{
	switch(type)
	{
	case REMOTE:
		return false;
		break;

	case SIMUL:
		return m_factoryDP->m_factSimul.FreeResource(pObj);
		break;

	default:
		return false;
		break;
	}
	
}	

