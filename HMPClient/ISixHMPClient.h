#pragma once

class ParamsCodec;

class ISixHMPClientEvent {
public:
    enum HMP_EVENTS {
		GET_BEARER_DONE,
		COMPLETE_BEARER_DONE,
		ACTION_DONE,
		RELEASE_DONE
	};

	virtual void ProcessSixHMPClientEvent(ParamsCodec* pDynEvent) = 0;
};

class ISixHMPClient {
public:
	virtual ~ISixHMPClient(){};

	// Valores de Mensajeria. NO deben cambiarse
    enum MR_METHODS {
		MR_GET_RESOURCE			   = 1,
		MR_FREE_RESOURCE		   = 2,

		MR_UPDATEBEARERINFORMATION = 1302,
		MR_PLAYMESSAGE             = 1303,
		MR_RECORDMESSAGE           = 1304,
		MR_COLLECTDIGITS           = 1305,
		MR_STOPMEDIA               = 1306,
		MR_SAYAS                   = 1307,
		MR_PLAYLIST                = 1308,
		MR_PLAYDTMF                = 1309,
		MR_STARTTONEDETECTION      = 1310,
		MR_STOPTONEDETECTION       = 1311,
		MR_PLAYTONE                = 1312,
		MR_TTS                     = 1313,
		MR_ASR                     = 1314,
		MR_FAXSEND                 = 1315,
		MR_FAXRECEIVE              = 1316,
		MR_BRIDGE                  = 1317,
		MR_CNFADDMBR               = 1319,
		MR_CNFRMMBR                = 1320,
		MR_CNFSPNDMBR              = 1321,
		MR_CNFRSMMBR               = 1322,
		MR_CNFMDFYMBR              = 1323,
		MR_CNFGETMBRPARM           = 1324,

		MR_RTP_CODEC               = 1329,
		MR_START_RECEIVE_DTMF      = 1330,
		MR_STOP_RECEIVE_DTMF       = 1331,
		MR_START_CALLPROGRESS      = 1332,
		MR_STOP_CALLPROGRESS       = 1333,
		MR_RECORD_CALL             = 1334,
		MR_STOP_RECORD_CALL        = 1335,
		MR_HOLD_CALL               = 1336,
		MR_UNHOLD_CALL             = 1337,
		MR_UNBRIDGE                = 1341,
		MR_NOTIFY				   = 1342,
		MR_COLLECTDIGITSCANCEL     = 1343,
		MR_GETBEARERINFORMATION    = 1344,

		MR_MANAGEMENT_INFO		   = 1400,

		MR_LAST_MSG                = 1499 //ultimo
	};

	virtual void SetEventCallback(ISixHMPClientEvent* p){m_pCallback = p;}

	/*
	Funcion para pedir un recurso. 
    CASO IN: lleva par�metros del origen, devuelve par�metros de conexi�n del
    server CASO OUT: NO lleva par�metros de origen, devuevle 1 o mas grupos de
    param�metros de conexi�n del server OJO! Podr�a recibir y devoler el SDP
    entero para que el server haga la negociaci�n seg�n sus capacidades?!!
	*/
	virtual bool GetBearer(ParamsCodec* pDynIn) = 0;

	/*
    Necesaria para el caso OUT: Una vez el sever HMP devuelve el grupo de
    capacidades (o el SDP ofrecido) y se ha negociado, se usa esta funcion para
    completar los datos del origen que a�n no se tienen. Podr�a recibir el SDP
    completo negociado para que actualice. NO DEVUELVE par�metros.
	*/
	virtual bool CompleteBearer(ParamsCodec* pDynIn) = 0;

	/*
    Evitar tener metodo por cada operacion. Cada implementacion decide cuales
    operaciones soporta o no
	*/
	virtual bool PerformAction(ParamsCodec* pDynData) = 0;

	/*
    Una vez completado el bearer, podr�a ser necesario cambiar los par�metros de
    conexi�n. Puede existir esta funcion como lo es actualmente, o se puede
    delegar al cliente la secuencia de eliminar y generar nuevamente el recurso.
	*/
    virtual bool UpdateBearer(ParamsCodec* pDynIn) = 0;
	/*
    Parametro adicional que se le puede pasar a la implementacion la cual sera
    responsable de la comprobacion de tipos.
	*/
	virtual void SetParam(void* vParam){}

    virtual bool ReleaseBearer() = 0;

protected:
	ISixHMPClientEvent* m_pCallback;
};
