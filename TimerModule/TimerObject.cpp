#include "TimerObject.h"
#include "ITimerCB.h"
#include "CommonHeaders/six_fwk_definitions.h"
#include "Utilities/Tracing.h"

ACE_Recursive_Thread_Mutex CTimerObject::ms_aceSyncMap;
size_t CTimerObject::ms_iTimeSequence = 100;
std::map<size_t, CTimerObject*> CTimerObject::ms_mapHandle2Timer;
TFactory<CTimerObject> CTimerObject::ms_factTimerObjs;

CTimerObject::CTimerObject(void)
{
	m_iEventId = 0;
	m_pCallback = 0;
  	m_bCicle = true;
  	m_bDisable = false;

	this->m_lTimer_id_ = TIMER_NOTSET;
}

CTimerObject::~CTimerObject(void)
{
	reactor(NULL);
}


void CTimerObject::ClearTimer()
{
	m_iEventId = 0;
	m_pCallback = 0;
	m_nTimerHandle = 0;
  	m_bCicle = true;
  	m_bDisable = false;
	Timer_id(CTimerObject::TIMER_CANCELLED);
}

void CTimerObject::OnTimer()
{
	///debido a que se llaman desde diferentes hilos esto puede ocurrir.
	if (this->m_lTimer_id_ == TIMER_CANCELLED)
		return;

	if(m_pCallback != 0)
		m_pCallback->TimeOut(m_iEventId, m_nTimerHandle);
}

int CTimerObject::handle_timeout( const ACE_Time_Value &tv, const void *arg)
{
	CTimerObject* tmp = CTimerObject::GetResourceFromHandle(m_nTimerHandle);
	if(tmp == 0)
		return 0;

	if(m_nTimerHandle != (size_t)arg)
	{
		TRACEPF_ERROR("", "CTimerObject::handle_timeout obj <0x%x> invalid handle", this);
		return 0;
	}

	OnTimer();
	localKillTimer();
	return 0;
}

int CTimerObject::handle_close( ACE_HANDLE handle, ACE_Reactor_Mask close_mask )
{
	return 0;
}

void CTimerObject::localKillTimer()
{
	ClearTimer();
	FreeResource(this);
}

CTimerObject* CTimerObject::GetResourceFromHandle(size_t nHandle)
{
	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard, ms_aceSyncMap, 0);
	//{
		CTimerObject* tmp = ms_mapHandle2Timer[nHandle];
		ms_mapHandle2Timer.erase(nHandle);
	//}

	return tmp;
}

CTimerObject * CTimerObject::GetResource()
{
	CTimerObject* tmp = ms_factTimerObjs.GetResource();

	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard, ms_aceSyncMap, 0);
	//{
		size_t iTimerHandle = ms_iTimeSequence++;
		tmp->TimerHandle(iTimerHandle);
		ms_mapHandle2Timer[iTimerHandle] = tmp;
	//}

	return tmp;
}

void CTimerObject::FreeResource(CTimerObject* pTimerObj)
{
	ms_factTimerObjs.FreeResource(pTimerObj);
}
