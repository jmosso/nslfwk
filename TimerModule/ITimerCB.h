#pragma once

#include <cstddef>

class ITimerCB
{
public:
	virtual void TimeOut(size_t iEventId, size_t iTimerId){};
};
