
#include "ReactorTimerImpl.h"
#include "CommonHeaders/six_fwk_definitions.h"
#include "Utilities/Tracing.h"
#include "ace/Trace.h"
#include "ace/Synch.h"
#include "ace/Reactor.h"

CReactorTimerImpl::CReactorTimerImpl(void)
{
	m_pReactor = NULL;
	m_pSemaphore = new ACE_Thread_Semaphore(0);
}

CReactorTimerImpl::~CReactorTimerImpl(void)
{
	if (m_pReactor)
	{
		delete m_pReactor;
		m_pReactor = NULL;
	}
	if (m_pSemaphore)
	{
		delete m_pSemaphore;
		m_pSemaphore = NULL;
	}
}

short CReactorTimerImpl::Init()
{
	this->activate();
	m_pSemaphore->acquire();
	return 0;
}

int CReactorTimerImpl::svc( void )
{
	m_pReactor = new ACE_Reactor;
	m_pReactor->open(512);
	m_pSemaphore->release();

	int status = m_pReactor->run_reactor_event_loop ();
	if(status == -1)
	{
		TRACEPF_ERROR("", "CReactorTimerImpl::svc returned with error!!!");
	}

	return 0;
}

short CReactorTimerImpl::Exit()
{
	TRACEPF_DEBUG("", "CReactorTimerImpl::Exit end_reactor_event_loop");
	m_pReactor->end_reactor_event_loop();
	this->wait();
	TRACEPF_DEBUG("", "CReactorTimerImpl::Exit AFTER wait");
	return 0;
}

size_t CReactorTimerImpl::SetTimer( size_t iEventId, size_t iElapse, ITimerCB* pCallback)
{
	CTimerObject* tmp = CTimerObject::GetResource();

	tmp->EventId(iEventId);
	tmp->Callback(pCallback);
	size_t iHandle = tmp->TimerHandle();

	tmp->Timer_id(m_pReactor->schedule_timer(tmp, (const void*)iHandle, ACE_Time_Value(iElapse/1000, (iElapse % 1000)*1000)));

	return iHandle;
}

int CReactorTimerImpl::SetTimer( int iEventId, int iElapse, ITimerCB* pCallback, bool bCicle,int nDelay )
{
	return SetTimer((size_t)iEventId, (size_t)iElapse, pCallback);
}

bool CReactorTimerImpl::KillTimer( size_t iTimerId )
{
	if(iTimerId <= 0)
		return false;

	CTimerObject* tmp = CTimerObject::GetResourceFromHandle(iTimerId);

	if(tmp == 0)
		return true;

	
	if (m_pReactor->cancel_timer(tmp->Timer_id(), 0, 0) != 1)
	{
		//tmp->handle_close(0,0);
	}

	tmp->localKillTimer();

	return true;
}

