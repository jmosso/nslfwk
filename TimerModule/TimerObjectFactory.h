#pragma once

/*
Copyright (c) 2001 - 2007  Sixbell Nekotec

Module Name: CTimerObjectFactory

Abstract:

This module create and manage timer objects.


Created:	[5/7/2007 lbolanos]
Author:		Leonardo Bola�os
Mail:		leonardo.bolanos@nekotectelecom.com

Revision History:


The copyright to the computer program(s) herein is the property of 
Sixbell Nekotec, Colombia. The program(s) may be used and/or copied  
only with the written permission of  Sixbell Nekotec or in accordance 
with the terms and conditions stipulated in the agreement/contract under 
which the  program(s) have been supplied.
*/
#include "TFactory.h"
#include "TSingleton.h"
#include "TimerObject.h"

class CTimerObjectFactory:	
	public TSingleton<CTimerObjectFactory>,
	public TFactory<CTimerObject>
{
public:
	CTimerObjectFactory(void);
	~CTimerObjectFactory(void);
};
