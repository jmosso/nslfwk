#pragma once

#include "Templates/TSingleton.h"

class ITimerCB;
class CReactorTimerImpl;

class CTimerFactory : public TSingleton<CTimerFactory>
{
public:
	CTimerFactory(void);
	~CTimerFactory(void);

	size_t SetTimer(size_t iEventId, size_t iElapse, ITimerCB* pCallback);
  int SetTimer(int iEventId, int iElapse, ITimerCB* pCallback, bool bCicle = true,int nDelay = 0);
	bool KillTimer(size_t iTimerId);

	int Init();
	int Exit();

private:

	CReactorTimerImpl *m_pReactor;
};
