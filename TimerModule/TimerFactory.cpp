
#include "TimerFactory.h"
#include "ReactorTimerImpl.h"

CTimerFactory::CTimerFactory(void)
{
	m_pReactor = NULL;
}

CTimerFactory::~CTimerFactory(void)
{
	if (m_pReactor)
		delete m_pReactor;
}

int CTimerFactory::Init()
{
	m_pReactor = new CReactorTimerImpl;
	m_pReactor->Init();
	return 0;
}

int CTimerFactory::Exit()
{
	if (m_pReactor)
	{
		m_pReactor->Exit();
	}
	
	if (m_pReactor)
	{
		delete m_pReactor;
		m_pReactor = NULL;
	}	

	return 0;
}

size_t CTimerFactory::SetTimer( size_t iEventId, size_t iElapse, ITimerCB* pCallback)
{
	if (m_pReactor)
	{
		return m_pReactor->SetTimer(iEventId, iElapse, pCallback);
	}
	return 0;
}

int CTimerFactory::SetTimer( int iEventId, int iElapse, ITimerCB* pCallback, bool bCicle,int nDelay)
{
	if (m_pReactor)
	{
		return m_pReactor->SetTimer(iEventId, iElapse, pCallback, bCicle,nDelay);
	}
	return 0;
}

bool CTimerFactory::KillTimer( size_t iTimerId )
{
	if (m_pReactor)
	{
		return m_pReactor->KillTimer(iTimerId);
	}
	return false;
}
