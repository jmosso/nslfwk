#pragma once
#include "ace/Event_Handler.h"
#include "Templates/TFactory.h"

class ITimerCB;

class CTimerObject : public ACE_Event_Handler
{
public:
	CTimerObject(void);
	~CTimerObject(void);
public:
	enum {
		TIMER_NOTSET = -1,
		TIMER_CANCELLED = -2,
		TIMER_FIRED = -3
	};

	size_t EventId() const { return m_iEventId; }
	void EventId(size_t val) { m_iEventId = val; }

	ITimerCB* Callback() const { return m_pCallback; }
	void Callback(ITimerCB* val) { m_pCallback = val; }

	size_t TimerHandle() const { return m_nTimerHandle; }
	void TimerHandle(size_t val) { m_nTimerHandle = val; }

	long Timer_id() const { return m_lTimer_id_; }
	void Timer_id(long val) { m_lTimer_id_ = val; }

	void ClearTimer();

	static CTimerObject * GetResourceFromHandle(size_t nHandle);
	static CTimerObject * GetResource();
	static void FreeResource(CTimerObject * pTimerObj);

	virtual int handle_close (ACE_HANDLE handle,ACE_Reactor_Mask close_mask);

	void localKillTimer();

  bool Cicle() const { return m_bCicle; }
  void Cicle(bool val) { m_bCicle = val; }
  bool Disable() const { return m_bDisable; }
  void Disable(bool val) { m_bDisable = val; }



private:

	virtual int handle_timeout(const ACE_Time_Value &tv, const void *arg = 0);

	// Event Identifier
	size_t		m_iEventId;
	ITimerCB*	m_pCallback;
	size_t		m_nTimerHandle;
	long		m_lTimer_id_;
  bool  m_bCicle;
  bool  m_bDisable;


	void OnTimer();	

	static ACE_Recursive_Thread_Mutex ms_aceSyncMap;
	static size_t ms_iTimeSequence;
	static std::map<size_t, CTimerObject*> ms_mapHandle2Timer;

	static TFactory<CTimerObject> ms_factTimerObjs;
};
