#pragma once

#include "ace/Task.h"
#include "TimerObject.h"
class ACE_Reactor;
class ITimerCB;
class ACE_Thread_Semaphore;

class CReactorTimerImpl : public ACE_Task<ACE_SYNCH>
{
public:
	CReactorTimerImpl(void);
	~CReactorTimerImpl(void);

	size_t SetTimer(size_t iEventId, size_t iElapse, ITimerCB* pCallback);
    int SetTimer(int iEventId, int iElapse, ITimerCB* pCallback, bool bCicle = true,int nDelay = 0);
	bool KillTimer(size_t iTimerId);

	short Init();
	short Exit();
	
	virtual int svc (void);

private:
	/// Manage the schedule of timers.
	ACE_Reactor *m_pReactor;
	ACE_Thread_Semaphore *m_pSemaphore;
};
