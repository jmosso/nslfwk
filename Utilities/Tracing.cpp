
#include "Tracing.h"

#include "utilities.h"
#include "versions/config.h"
#include "FrameworkUtilities/FwkTime.h"
#include "FrameworkUtilities/FwkString.h"

#include "ace/OS_NS_time.h"
#include "ace/Reactor.h"
#include "ace/OS_NS_stdio.h"
#include "ace/Recursive_Thread_Mutex.h"

bool CTracing::ms_bIncludeTime = true;
bool CTracing::ms_bIncludeDate = true;
bool CTracing::ms_bIncludeMicroSeconds = true;
bool CTracing::ms_bIncludeResourceId = true;
bool CTracing::ms_bIncludeFileName = false;
bool CTracing::ms_bIncludeFunction = false;
bool CTracing::ms_bIncludeLevel = true;
bool CTracing::ms_bTurbo = false;
bool CTracing::ms_bIncludeModule = true;
bool CTracing::ms_bIncludeThread = true;
bool CTracing::ms_bUseTimeFN = true;
size_t CTracing::ms_numMaxSizeFile = 100000000;
time_t CTracing::ms_tomorrowDate = -1;
int CTracing::ms_iAbleMask = 0;
int CTracing::ms_iSecuencia = 0;
int CTracing::ms_mask = 0xFF;
int CTracing::ms_level = 5;
size_t CTracing::ms_iActualSize = 0;
char CTracing::ms_fstrCurrentFile[1024];
char CTracing::ms_fstrFileName[256];
char CTracing::ms_fstrFilePath[1024];
char CTracing::ms_fstrAppName[128];

bool CTracing::ms_bIsActive = false;
FILE* CTracing::ms_pTracingFile = 0;

///////////////////////////////////////////////////////////////////

#define MAX_TRACE_BUFFER	16384
char pLevelsArray[10][5] = {"ALWA", "FERR", "ERRO", "WARN", "NORM", "DEBG", "DETL", "GDBG", "GDBG", "GDBG"};

ACE_Recursive_Thread_Mutex g_aceMutexTrace;

///////////////////////////////////////////////////////////////////
CTracing::CTracing(void)
{
}

CTracing::~CTracing(void)
{
}

void CTracing::SetProperties(int ableMask, 
							 int traceMask, 
							 int level, 
							 const char *pFNTrace, 
							 bool useTimeFN,
							 size_t inumMaxSizeFile,
							 bool includeTime,
							 bool includeDate,
							 bool includeResourceId,
							 bool includeFunction,
							 bool includeFileName,
							 bool includeMicroSeconds,
							 bool includeLevel,
							 bool includeModule,
							 bool bIncludeThread)
{
	ms_iAbleMask = ableMask;
	if(traceMask != 10)		// 10 habilita todo por herencia! de resto se maneja la mascara binaria
		ms_mask = traceMask;
	ms_level = level;

	ms_bUseTimeFN = useTimeFN;
	strcpy(ms_fstrFileName, pFNTrace);
	//ms_fstrFileName = pFNTrace;

	if(pFNTrace != 0)
	{
		CFwkString fstrPathFileTemp(pFNTrace);
#ifndef WIN32
		if(fstrPathFileTemp.Find("~") != J_MAX_INDEX)
		{
			fstrPathFileTemp.Replace("~",getenv("HOME"));
		}
#endif //WIN32

		fstrPathFileTemp.Replace("\\","/",true);
		strcpy(ms_fstrFilePath, fstrPathFileTemp.BeforeLast('/').GetBuffer());
		
		if(0 == ms_fstrFilePath[0])
			strcpy(ms_fstrFilePath, ".");
			
		CFwkString fstrFileNameTemp = fstrPathFileTemp.AfterLast('/');
		strcpy(ms_fstrFileName, fstrFileNameTemp.BeforeLast('.').GetBuffer());
	}

	ms_bIncludeTime = includeTime;
	ms_bIncludeDate = includeDate;
	ms_bIncludeResourceId = includeResourceId;
	ms_bIncludeFunction = includeFunction;
	ms_bIncludeFileName = includeFileName;
	ms_bIncludeMicroSeconds = includeMicroSeconds;
	ms_bIncludeLevel = includeLevel;
	ms_bIncludeModule = includeModule;
	ms_bIncludeThread = bIncludeThread;
	ms_numMaxSizeFile = inumMaxSizeFile;
}

void CTracing::SetAppName(const char* appName)
{
	strcpy(ms_fstrAppName, appName);
}

void CTracing::InitTrace()
{
#ifdef USE_SIX_LOG
		CFwkString logPath;
		logPath.sprintf("%s/%s", ms_fstrFilePath, ms_fstrAppName);
		sixlabs::LogManager::open( logPath.GetBuffer() );
		//sixlabs::LogManager::instance()->set_flags( ACE_Log_Msg::OSTREAM );

		switch(ms_level)
		{
		case 0:
		case 1:
		case 2:
		case 3:
			set_log_generic(LM_WARNING | LM_ERROR | LM_CRITICAL);
			break;
		case 4:
			set_log_info();
			break;
		case 5:
			set_log_debug();
			break;
		default:
			if(ms_level > 0)
				set_log_detail();
			else
				set_log_info();
			break;
		}
#endif	
	
	ms_bIsActive = ms_iAbleMask<=0?false:true;
	if(ms_bIsActive == true)
	{

		TRACEPF_ALWAYS("", "nslFwk-version-%s %s", VERSION_BUILD, DATE_BUILD);
		printf("nslFwk-version-%s %s\nTracing to %s\n", VERSION_BUILD, DATE_BUILD, ms_fstrCurrentFile);
	}
	else
	{
		if(ms_iAbleMask != -1) // -1 en silencio
			printf("nslFwk-version-%s %s\nTracing disable\n", VERSION_BUILD, DATE_BUILD);
	}
}

void CTracing::SetTraceLevel(int level, int mask)
{
	ms_mask = mask;
	ms_level = level;
}

void CTracing::GetTraceLevel(int& level, int& mask)
{
	mask = ms_mask;
	level = ms_level;
}

void CTracing::EndTrace()
{
}

size_t CTracing::Tracing(const char* file, int line, const char* function, const char* uid, int mask, int level, const char * format, ...)
{
#ifndef USE_SIX_LOG
	char szBuff[MAX_TRACE_BUFFER];
	size_t iBuffOffset = 0;

	CFwkString fstrTime;
	if(ms_bIncludeDate == false)
		FwkGetTime(fstrTime, "%H%M%S");
	else
	{
		FwkGetTime(fstrTime, "%Y%m%d|%H%M%S", true);
	}

	sprintf(szBuff, "%s|0x%x|%s|%s|", fstrTime.GetBuffer(), (int)ACE_OS::thr_self(), pLevelsArray[level], uid);
		
	iBuffOffset += strlen(szBuff);

	va_list theList;
	va_start(theList, format);

	ACE_OS::vsnprintf(szBuff  + iBuffOffset, MAX_TRACE_BUFFER - (iBuffOffset + 1), format, theList); 
	va_end(theList);

	size_t retValue = WriteToFile(szBuff);

	return retValue;
#else

	char szBuff[MAX_TRACE_BUFFER];
	size_t iBuffOffset = 0;

	va_list theList;
	va_start(theList, format);

	ACE_OS::vsnprintf(szBuff, MAX_TRACE_BUFFER, format, theList); 
	va_end(theList);

	switch(level)
	{
	case TRACE_ALWAYS:
		log_generic(LM_CRITICAL, "critical", uid, szBuff);
		break;
	case TRACE_FATALERROR:
	case TRACE_ERROR:
		log_generic(LM_ERROR, "error", uid, szBuff);
		break;
	case TRACE_WARNING:
		log_generic(LM_WARNING, "warning", uid, szBuff);
		break;
	case TRACE_NORMAL:
		log_generic(LM_INFO, "info", uid, szBuff);
		break;
	case TRACE_DEBUG:
		log_generic(LM_DEBUG, "debug", uid, szBuff);
		break;
	default:
		log_generic(LM_TRACE, "detail", uid, szBuff);
		break;
	}

	return 0;

#endif
}

size_t CTracing::Tracing(const char* uid, int level, const char * format, ...)
{
	char szBuff[MAX_TRACE_BUFFER];
	size_t iBuffOffset = 0;

	CFwkString fstrTime;
	if(ms_bIncludeDate == false)
		FwkGetTime(fstrTime, "%H%M%S");
	else
	{
		FwkGetTime(fstrTime, "%Y%m%d|%H%M%S", true);
	}

	sprintf(szBuff, "%s|0x%x|%s|%s|", fstrTime.GetBuffer(), (int)ACE_OS::thr_self(), pLevelsArray[level], uid);
		
	iBuffOffset += strlen(szBuff);

	va_list theList;
	va_start(theList, format);

	ACE_OS::vsnprintf(szBuff  + iBuffOffset, MAX_TRACE_BUFFER - (iBuffOffset + 1), format, theList); 
	va_end(theList);

	iBuffOffset = strlen(szBuff);
	strcpy(szBuff + iBuffOffset, "\n\0");

	size_t retValue = WriteToFile(szBuff);

	return retValue;
}

size_t CTracing::Tracing(std::string& uid, int level, const char * format, ...)
{
	char szBuff[MAX_TRACE_BUFFER];
	size_t iBuffOffset = 0;

	CFwkString fstrTime;
	if(ms_bIncludeDate == false)
		FwkGetTime(fstrTime, "%H%M%S");
	else
	{
		FwkGetTime(fstrTime, "%Y%m%d|%H%M%S", true);
	}

	sprintf(szBuff, "%s|0x%x|%s|%s|", fstrTime.GetBuffer(), (int)ACE_OS::thr_self(), pLevelsArray[level], uid.c_str());
		
	iBuffOffset += strlen(szBuff);

	va_list theList;
	va_start(theList, format);

	ACE_OS::vsnprintf(szBuff  + iBuffOffset, MAX_TRACE_BUFFER - (iBuffOffset + 1), format, theList); 
	va_end(theList);

	iBuffOffset = strlen(szBuff);
	strcpy(szBuff + iBuffOffset, "\n\0");

	size_t retValue = WriteToFile(szBuff);

	return retValue;
}

size_t CTracing::WriteToFile(const char* theBuffer)
{
	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard, g_aceMutexTrace, 0);

	time_t auxTime;
	time(&auxTime);

	if(auxTime > ms_tomorrowDate || ms_iActualSize > ms_numMaxSizeFile)
	{
		ms_iActualSize = 0;
		if(ms_pTracingFile)
		{
			fclose(ms_pTracingFile);
		}
		ms_pTracingFile = 0;
	}

	if(!ms_pTracingFile)
	{
		time(&ms_tomorrowDate);
		struct tm date;
		ACE_OS::localtime_r(&ms_tomorrowDate, &date);
		ms_tomorrowDate = ms_tomorrowDate + 86400 - (date.tm_sec + date.tm_min*60 + date.tm_hour*3600);

		ms_iSecuencia++;
		if(ms_bUseTimeFN == false)
			sprintf(ms_fstrCurrentFile, "%s/%s.%d.txt", ms_fstrFilePath, ms_fstrFileName, ms_iSecuencia);
		else
		{
			CFwkString fstrTime;
			FwkGetTime(fstrTime, "%Y%m%d.%H%M%S");
			sprintf(ms_fstrCurrentFile, "%s/%s-%s.%d.txt", ms_fstrFilePath, ms_fstrFileName, fstrTime.GetBuffer(), ms_iSecuencia);
		}

		ms_pTracingFile = fopen(ms_fstrCurrentFile, "w");
	}

	if(!ms_pTracingFile)
		return 0;

	size_t iBufferSize = strlen(theBuffer);
	fwrite(theBuffer, sizeof(char), iBufferSize, ms_pTracingFile);
	if(!ms_bTurbo)
		fflush(ms_pTracingFile);
	ms_iActualSize += iBufferSize;

	return iBufferSize;
}

int CTracing::FlushLog()
{
	ACE_GUARD_RETURN(ACE_Recursive_Thread_Mutex, guard, g_aceMutexTrace, 0);
	if(ms_pTracingFile)
		fflush(ms_pTracingFile);
	return 0;
}

