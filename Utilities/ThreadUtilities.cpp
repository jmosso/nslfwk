
#include "ThreadUtilities.h"
#include "ace/Recursive_Thread_Mutex.h"
#include "ace/Manual_Event.h"

class CSectionCritical::CSectionCriticalDP
{
public:
	CSectionCriticalDP(){};
	~CSectionCriticalDP(){};

	ACE_Recursive_Thread_Mutex mutexDP;
};

CSectionCritical::CSectionCritical(): m_DP(new CSectionCriticalDP)
{
}

CSectionCritical::~CSectionCritical()
{
    if(m_DP != 0)
        delete m_DP;

	m_DP = 0;
 }

void CSectionCritical::Lock()
{
	m_DP->mutexDP.acquire();
}
 
void CSectionCritical::Unlock()
{
	m_DP->mutexDP.release();
}
