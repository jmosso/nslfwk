#pragma once
#include "FrameworkUtilities/FwkString.h"

bool	CreatePathDirectory(const char* Directory);

unsigned int getSizeFile(const char *pPathFileName);
bool getDiskFreeSpace(const char *nameDisk, int &freeBytes,int &por);
bool deleteFile(const char * pPathDeleteFile,bool eraseIsReadOnly);
bool moveFile(const char* strPathSource, const char* strPathDestination);
bool copyFile(const char* strPathSource, const char* strPathDestination, bool bOverWrite);

bool encodeURL(const char* strIn, CFwkString& strOut);
bool decodeURL(const char* strIn, CFwkString& strOut);

bool FwkGetUUID(CFwkString& fstrUUID, const char* strPrefix = 0);

bool FwkGetStringInBtw(CFwkString& fstrOrigin, CFwkString& fstrTarget, const char* strChStart, const char* strChEnd);

bool FwkGetTime(CFwkString& fstrTime, const char* strFormat = 0, bool bIncludeMS = false, bool bIncludeTimeZone = false);
void SetTimeZone(const char* strTimeZone);
