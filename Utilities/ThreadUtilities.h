#pragma once

class CSectionCritical
{
	class CSectionCriticalDP;

public:
	CSectionCritical();
	~CSectionCritical();
 
	void Lock();
 	void Unlock();

private:
	CSectionCriticalDP* m_DP;
};
