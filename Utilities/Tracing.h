#pragma once

#include <stdio.h>
#include <time.h>
#include <string>

#ifdef USE_SIX_LOG

	#include "LogMsg.hpp"

	#define TRACEPF_ALWAYS(RESOURCEID, FMT, ...)	log_generic(LM_CRITICAL, "critical", RESOURCEID, FMT, ##__VA_ARGS__)	// log_critical
	#define TRACEPF_ERROR(RESOURCEID, FMT, ...)		log_generic(LM_ERROR, "error", RESOURCEID, FMT, ##__VA_ARGS__)			// log_error
	#define TRACEPF_WARNING(RESOURCEID, FMT, ...)	log_generic(LM_WARNING, "warning", RESOURCEID, FMT, ##__VA_ARGS__)		// log_warning
	#define TRACEPF_INFO(RESOURCEID, FMT, ...)		log_generic(LM_INFO, "info", RESOURCEID, FMT, ##__VA_ARGS__)			// log_info
	#define TRACEPF_DEBUG(RESOURCEID, FMT, ...)		log_generic(LM_DEBUG, "debug", RESOURCEID, FMT, ##__VA_ARGS__)			// log_debug
	#define TRACEPF_DETAIL(RESOURCEID, FMT, ...)	log_generic(LM_TRACE, "detail", RESOURCEID, FMT, ##__VA_ARGS__)			// log_detail

	#define TRACEPF_COMPATIBLE(uid, mask, level, format, ...)	\
		if(CTracing::CanTrace(mask, level))\
		switch(level)\
			{\
			case TRACE_ALWAYS:\
				log_generic(LM_CRITICAL, "critical", uid, format, ##__VA_ARGS__);\
				break;\
			case TRACE_FATALERROR:\
			case TRACE_ERROR:\
				log_generic(LM_ERROR, "error", uid, format, ##__VA_ARGS__);\
				break;\
			case TRACE_WARNING:\
				log_generic(LM_WARNING, "warning", uid, format, ##__VA_ARGS__);\
				break;\
			case TRACE_NORMAL:\
				log_generic(LM_INFO, "info", uid, format, ##__VA_ARGS__);\
				break;\
			case TRACE_DEBUG:\
				log_generic(LM_DEBUG, "debug", uid, format, ##__VA_ARGS__);\
				break;\
			case TRACE_DETAIL:\
				log_generic(LM_TRACE, "detail", uid, format, ##__VA_ARGS__);\
				break;\
			default:\
				log_generic(LM_INFO, "info", uid, format, ##__VA_ARGS__);\
				break;\
			}\

	#define TRACEFLUSH	log_flush()

#else // USE_SIX_LOG

	#define TRACEPF(uid, mask, level, format, ...)  if(CTracing::ms_bIsActive && CTracing::CanTrace(mask, level)) CTracing::Tracing(uid, level, format, ##__VA_ARGS__)

	#define TRACEPF_ALWAYS(RESOURCEID, FMT, ...)	if(CTracing::ms_bIsActive && CTracing::CanTrace(0xFFF, TRACE_ALWAYS)) CTracing::Tracing(RESOURCEID, TRACE_ALWAYS, FMT, ##__VA_ARGS__)
	#define TRACEPF_ERROR(RESOURCEID, FMT, ...)		if(CTracing::ms_bIsActive && CTracing::CanTrace(0xFFF, TRACE_ERROR)) CTracing::Tracing(RESOURCEID, TRACE_ERROR, FMT, ##__VA_ARGS__)	
	#define TRACEPF_WARNING(RESOURCEID, FMT, ...)	if(CTracing::ms_bIsActive && CTracing::CanTrace(0xFFF, TRACE_WARNING)) CTracing::Tracing(RESOURCEID, TRACE_WARNING, FMT, ##__VA_ARGS__)
	#define TRACEPF_INFO(RESOURCEID, FMT, ...)		if(CTracing::ms_bIsActive && CTracing::CanTrace(0xFFF, TRACE_NORMAL)) CTracing::Tracing(RESOURCEID, TRACE_NORMAL, FMT, ##__VA_ARGS__)
	#define TRACEPF_DEBUG(RESOURCEID, FMT, ...)		if(CTracing::ms_bIsActive && CTracing::CanTrace(0xFFF, TRACE_DEBUG)) CTracing::Tracing(RESOURCEID, TRACE_DEBUG, FMT, ##__VA_ARGS__)
	#define TRACEPF_DETAIL(RESOURCEID, FMT, ...)	if(CTracing::ms_bIsActive && CTracing::CanTrace(0xFFF, TRACE_DETAIL)) CTracing::Tracing(RESOURCEID, TRACE_DETAIL, FMT, ##__VA_ARGS__)
	#define TRACEPF_COMPATIBLE(uid, mask, level, format, ...)	if(CTracing::ms_bIsActive && CTracing::CanTrace(mask, level)) CTracing::Tracing(uid, level, format, ##__VA_ARGS__)	

	#define TRACEFLUSH CTracing::FlushLog();
#endif //USE_SIX_LOG

#ifdef _DEBUG
#define SIX_ASSERT  ACE_ASSERT
#else
#define SIX_ASSERT(x) if(!(x)) TRACEPF_WARNING( "", "ASSERT " #x " in line %d file %s\n" ,__LINE__,__FILE__);
#endif //_DEBUG

// Mascaras capas funcionales de la arquitectura
#define		MASK_ALWAYS					1023 // 11 mascaras
#define		MASK_FWK_INTERP_FUNC		1
#define		MASK_SERVICE				2
#define		MASK_SRV_SUPPORT			4
#define		MASK_CAPABILITIES			8
#define		MASK_CAP_SUPPORT			16
#define		MASK_INTERFACES				32
#define		MASK_FWK_UTILITIES			64
#define		MASK_FWK_BASIC				128
#define		MASK_THR_CHECKER			256

//niveles de trace
#define		TRACE_ALWAYS				0
#define		TRACE_FATALERROR			1
#define		TRACE_ERROR					2
#define		TRACE_WARNING				3
#define		TRACE_NORMAL				4
#define		TRACE_DEBUG					5
#define		TRACE_DETAIL				6

class CTracing
{
private:
	CTracing(void);
	~CTracing(void);

public:
	static size_t Tracing(const char* file, int line, const char* function, const char* uid, int mask, int level, const char * format, ...);
	static size_t Tracing(const char* uid, int level, const char * format, ...);
	static size_t Tracing(std::string& uid, int level, const char * format, ...);

	///Set the properties to trace
	static void SetProperties(
		int ableMask,
		int traceMask, 
		int level,
		const char *pFNTrace,
		bool useTimeFN = true,
		size_t inumMaxSizeFile = -1,
		bool includeTime = true,
		bool includeDate = true,
		bool includeResourceId = true,
		bool includeFunction = false,
		bool includeFileName = false,
		bool includeMicroSeconds = true,
		bool includeLevel = true,
		bool includeModule = false,
		bool bIncludeThread = true);

	static void SetTraceLevel(int level, int mask);
	static void GetTraceLevel(int& level, int& mask);


	static int FlushLog();
	static void ActivateTurbo(bool bTurbo){ms_bTurbo = bTurbo;};
	static void SetAppName(const char* appName);

	static void InitTrace();
	static void EndTrace();
	
	static bool ms_bIsActive;
	static inline bool CanTrace(int mask, int level = 0)
	{
		if((mask & ms_mask) == 0)
			return false;
		if(level > ms_level)
			return false;
		return true;
	}

private:

	static bool ms_bIncludeDate;
	static bool ms_bIncludeTime;
	static bool ms_bIncludeMicroSeconds;
	static bool ms_bIncludeFunction;
	static bool ms_bIncludeResourceId;
	static bool ms_bIncludeFileName;

	static bool ms_bIncludeLevel;
	static bool ms_bIncludeModule;
	static bool ms_bIncludeThread;
	static bool ms_bUseTimeFN;	
	static bool ms_bTurbo;
	static int ms_iAbleMask;
	static int ms_mask;
	static int ms_level;

	static char ms_fstrCurrentFile[1024];
	static char ms_fstrFileName[256];
	static char ms_fstrFilePath[1024];
	static char ms_fstrAppName[128];

	static FILE* ms_pTracingFile;
	static size_t ms_numMaxSizeFile;
	static int ms_iSecuencia;

	static time_t ms_tomorrowDate;

	static size_t ms_iActualSize;
	static size_t WriteToFile(const char* theBuffer);
};
