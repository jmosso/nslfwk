/* code file utilities.cpp
 * utilidades generales que no caben en una clase 
 * y se pueden usar desde multiples partes
 * contenido:
 *			CreatePathDirectory()
 */


/* CreatePathDirectory
 * metodo que permite crear directorios anidados
 * formas de uso:
 *				CreatePathDirectory(const char* Directory)
 * el formato de la cadena se ha probado con
 * c:\uno\dos\...\ene
 * c:\uno
 * \uno\dos\...\ene
 * uno\dos\...\ene
 * \uno
 * uno
 * return:
 *			true si la creacion fue exitosa
 *			false si fallo la creacion
 * remarks:
 * esta funcion permite crear un path de directorios al tiempo
 * los directorios se van creando en orden usando la funcion de 
 * win32 CreateDirectorio, si uno de los directorios ya exite, la funcion 
 * intenta crear el siguiente
 * recuerden que si definen la cadena el el codigo deben usar \\
 * 
 * cuando el path incluye c:\, d:\ se verifica la existencia del path usando
 * FindFirstFile(), si el path ya existe la funcion retorna 
 *
 * example:
 * CreatePathDirectory("c:\\hola\\que\\tal");
 * cualquier duda comunicarse con JNS
 */

#include "utilities.h"

#include "ace/OS_NS_time.h"
#include "ace/OS_NS_string.h"
#include "ace/Dirent.h"
#include "ace/OS_NS_sys_stat.h"
#include "ace/OS_NS_unistd.h"
#include "ace/UUID.h"

#ifndef WIN32
#include <uuid/uuid.h>
#endif

#include <fstream>
#include <iostream>

using namespace std;


bool FwkGetStringInBtw(CFwkString& fstrOrigin, CFwkString& fstrTarget, const char* strChStart, const char* strChEnd)
{
	size_t iOffset = 0;
	size_t iPos1 = fstrOrigin.Find(strChStart, iOffset);
			
	if(iPos1 == J_MAX_INDEX)
		iPos1 = 0;

	size_t iPos2 = fstrOrigin.Find(strChEnd, iPos1);
	if(iPos2 == J_MAX_INDEX)
		iPos2 = fstrOrigin.GetLength();

	if(iPos1 > iPos2)
	{
		fstrTarget = fstrOrigin;
		return false;
	}

	size_t len = 0;
	len = iPos2 - (iPos1 + 1);
	if(iPos1 != 0)
		iPos1++;
	else
		len++;

	fstrTarget = fstrOrigin.Mid(iPos1, len);
	return true;
}

bool FwkGetUUID(CFwkString& fstrUUID, const char* strPrefix)
{
#ifdef WIN32
	ACE_Utils::UUID uid;
	ACE_Utils::UUID_GENERATOR::instance()->generate_UUID(uid);
	if(strPrefix == 0)
		fstrUUID = uid.to_string()->c_str();
	else
		fstrUUID.sprintf("%s-%s", strPrefix, uid.to_string()->c_str());
	return true;
#else
	uuid_t uuid;
	uuid_generate_random(uuid);
	char uuidStr[37];
	uuid_unparse(uuid, uuidStr);

	if(strPrefix == 0)
		fstrUUID = uuidStr;
	else
		fstrUUID.sprintf("%s-%s", strPrefix, uuidStr);
#endif
}

bool CreatePathDirectory(const char* dir)
{
	CFwkString fstr = dir;
	fstr.Replace("\\", "/", true);
	char* Directory = new char[fstr.GetLength() + 1];
	strcpy(Directory, fstr);

	char *temp = new char[ACE_OS::strlen(Directory)+10];
	char *p,*q;
	int caso;

	int error = 0;
	char* end_str;
	/*
	HANDLE bu;
	WIN32_FIND_DATA nada;
	*/
	//busco un c:
	if(  ACE_OS::strchr(Directory, ':') != NULL)//existe la indicacion de un disco
	{	
		/*
		if((bu = FindFirstFile(Directory, &nada)) != INVALID_HANDLE_VALUE)
		{	
			FindClose(bu);
			delete[] temp;
			return true;
		}
		FindClose(bu);
		*/
#ifdef WIN32
		ACE_DIR *d = new ACE_DIR;
		d->directory_name_ = ACE_TEXT((ACE_TCHAR*)Directory);
		d->dirent_ = 0;
		d->started_reading_ = 0;
		ACE_DIRENT * ad = ACE_OS::readdir_emulation(d);
		if (ad)
		{
			delete[] temp;
			delete[] Directory;
			ACE_OS::free(ad);
			delete d;
			return true;
		}
		ACE_OS::free(ad);
		delete d;
#endif //WIN32

		ACE_OS::strcpy(temp, Directory);
		p = temp;
		p = ACE_OS::strchr(p,'/');
		if(!p)
		{
			delete[] temp;
			delete[] Directory;
			return false;
		}
		while(1)
		{
			p = ACE_OS::strchr(p+1,'/');
			if(p != NULL)
			{
				p[0] = '\0';
				if (!ACE_OS::mkdir(ACE_TEXT((ACE_TCHAR*)temp), ACE_DEFAULT_DIR_PERMS))
				//if(!CreateDirectory(temp, NULL))
				{
#ifdef WIN32
					error = GetLastError();
					if(error != ERROR_ALREADY_EXISTS)
					{
						delete[] temp;
						delete[] Directory;
						return false;
					}
#endif //WIN32
				}
				p[0] = '/';
			}
			else
			{
				//CreateDirectory(temp, NULL);
				ACE_OS::mkdir(ACE_TEXT((ACE_TCHAR*)temp), ACE_DEFAULT_DIR_PERMS);
				break;
			}
			
		}
	}
	else//sin c:
	{
		char *pos =0;
		ACE_OS::strcpy(temp, Directory);
		if(  (pos = ACE_OS::strchr(temp, '/')) != NULL)
		{
			if(pos == temp)//raya al inicio
			{
				//temp ++;//elimina primera raya
				pos++;
				//if(  ACE_OS::strchr(temp, '\\') != NULL)
				if(  ACE_OS::strchr(pos, '/') != NULL)
					caso = 1;//rayas
				else
					caso = 2;//un solo nombre
			}
			else
			{
				caso = 1;//rayas 
				
			}
		}
		else
			caso = 2;//un solo nombre
		
		
		switch(caso)
		{
		case 1:
			//q=strtok(temp,"\\");
			q = ACE_OS::strtok_r(pos,"/", &end_str);
			while(1)
			{
				if (!ACE_OS::mkdir(ACE_TEXT((ACE_TCHAR*)temp), ACE_DEFAULT_DIR_PERMS))
				//if(!CreateDirectory(temp, NULL))
				{
#ifdef WIN32
					error = GetLastError();
					if(error != ERROR_ALREADY_EXISTS)
					{
						delete[] temp;
						delete[] Directory;
						return false;
					}
#endif //WIN32
				}
				q = ACE_OS::strtok_r(NULL,"/", &end_str);
				if(q == NULL)
					break;
				*(q-1) = '/';
			}
			break;

		case 2:
			//if(!CreateDirectory(temp, NULL))
			if (!ACE_OS::mkdir(ACE_TEXT((ACE_TCHAR*)temp), ACE_DEFAULT_DIR_PERMS))
			{
#ifdef WIN32
				error = GetLastError();
				if(error != ERROR_ALREADY_EXISTS)
				{
					delete[] Directory;
					delete[] temp;
					return false;
				}
#endif //WIN32
			}
			break;
		}
	}

	delete[] temp;
	delete[] Directory;
	return true;
	
	
}

unsigned int getSizeFile(const char *pPathFileName)
{
	FILE* stream = ACE_OS::fopen(pPathFileName, "r");
	if(stream == 0)
{
	return 0;
}
    ACE_OS::fseek(stream, 0, SEEK_END);
	int iFakeSize = ACE_OS::ftell(stream);
	ACE_OS::fclose(stream);
	return iFakeSize;

}

bool getDiskFreeSpace(const char *nameDisk, int &freeBytes,int &por)
{
#ifdef WIN32

	unsigned __int64 i64FreeBytesToCaller = 0;
	unsigned __int64 i64TotalBytes = 0;
	unsigned __int64 i64FreeBytes = 0;


	if(GetDiskFreeSpaceEx(nameDisk,
                        (PULARGE_INTEGER)&i64FreeBytesToCaller,
						(PULARGE_INTEGER)&i64TotalBytes,
                        (PULARGE_INTEGER)&i64FreeBytes) == 0)
	{
		return false;
	}

	por = (int)((i64FreeBytes * 100)/i64TotalBytes);
	freeBytes = (int)(i64FreeBytes/1048576);
#endif //WIN32
	return true;
}

bool deleteFile(const char * pPathDeleteFile, bool eraseIsReadOnly)
{
#ifdef WIN32

	if(eraseIsReadOnly)
	{
		//int atri = GetFileAttributes(pPathDeleteFile);
		int atri = ACE_TEXT_GetFileAttributes(pPathDeleteFile);
		
		atri = atri & FILE_ATTRIBUTE_READONLY;
		if(atri==1)
		{
			if(SetFileAttributes(pPathDeleteFile,FILE_ATTRIBUTE_NORMAL|FILE_ATTRIBUTE_ARCHIVE) == 0)
			//if(ACE_TEXT_SetFileAttributes(pPathDeleteFile,FILE_ATTRIBUTE_NORMAL|FILE_ATTRIBUTE_ARCHIVE) == 0)
				return false;

		}
	}

/*	if(ACE_OS::unlink(ACE_TEXT((ACE_TCHAR*)pPathDeleteFile)) == 0)
		return true;
	else*/
#else
	if(remove(pPathDeleteFile) == 0)
	    return true;

#endif //WIN32
	return false;
}

bool moveFile(const char* strPathSource, const char* strPathDestination)
{
#ifdef WIN32

	//if(MoveFile(strPathSource, strPathDestination) != 0)
	int flags = MOVEFILE_COPY_ALLOWED | MOVEFILE_REPLACE_EXISTING;
	if (ACE_TEXT_MoveFileEx(ACE_TEXT((ACE_TCHAR*)strPathSource), ACE_TEXT((ACE_TCHAR*)strPathDestination), flags) != 0)
		return true;
	else
#else
	if(ACE_OS::rename(strPathSource, strPathDestination) == 0)
	    return true;
#endif //WIN32
		return false;
}

#define BUFF_SIZE 2048
bool copyFile(const char* strPathSource, const char* strPathDestination, bool bOverWrite)
{
#ifdef WIN32
	if(CopyFile(strPathSource, strPathDestination, !bOverWrite) != 0)
		return true;
		return false;
#else

	char buff[BUFF_SIZE];
	int readBytes = 1;

	std::ifstream inFileStream(strPathSource, ios::in|ios::binary);
	if(!inFileStream)
	{
		return false;
	}

	std::ifstream tmpStream(strPathDestination);
	if(tmpStream)
	{
		tmpStream.close();
		if(bOverWrite == false)
			return false;

		if(deleteFile(strPathDestination, true) == 0)
			return false;
	}

	ofstream outFileStream(strPathDestination, ios::out|ios::binary);
	if(!outFileStream)
	{
		return false;
	}

	while(readBytes != 0)
	{
		inFileStream.read((char*)buff, BUFF_SIZE);
		readBytes = inFileStream.gcount();
		outFileStream.write((char*)buff, readBytes);
	}
	return true;

#endif //WIN32


}

//converts char 255 to string "ff"
bool char2hex( char dec, CFwkString& strOut)
{
    char dig1 = (dec&0xF0)>>4;
    char dig2 = (dec&0x0F);
    if ( 0<= dig1 && dig1<= 9) dig1+=48;    //0,48inascii
    else if (10<= dig1 && dig1<=15) dig1+=97-10; //a,97inascii
    if ( 0<= dig2 && dig2<= 9) dig2+=48;
    else if (10<= dig2 && dig2<=15) dig2+=97-10;

    strOut += dig1;
    strOut += dig2;
    return true;
}

bool encodeURL(const char* strIn, CFwkString& strOut)
{
	strOut.Clear();
	const char* c = strIn;
	size_t max = ACE_OS::strlen(strIn);
    for(size_t i = 0; i < max; i++)
    {
        if ( (48 <= c[i] && c[i] <= 57) ||//0-9
             (65 <= c[i] && c[i] <= 90) ||//abc...xyz
             (97 <= c[i] && c[i] <= 122) || //ABC...XYZ
             (c[i]=='~' || c[i]=='!' || c[i]=='*' || 
			 c[i]=='(' || c[i]==')' || c[i]=='\'' ||
			 c[i]=='.' || c[i]=='_')
        )
        {
			strOut += c[i];
        }
        else
        {
            strOut += "%";
            char2hex(c[i], strOut);
}
    }

	return true;
}

bool decodeURL(const char* strIn, CFwkString& strOut)
{
	const char* p = strIn;
	char code[3] = {0};
	unsigned long ascii = 0;
	char* end = NULL;

	while(*p) 
	{
		if(*p == '%') 
		{
			memcpy(code, ++p, 2);
			ascii = strtoul(code, &end, 16);
			strOut += (char)ascii;
			p += 2;
		} 
		else if (*p == '+') 
		{
			strOut += ' ';
			p += 1;
		} 
		else 
		{
			strOut += *p++;
		}
	}

	return true;
}

CFwkString g_fstrTimeZone;

void SetTimeZone(const char* strTimeZone)
{
	// darle espacio
	g_fstrTimeZone.sprintf(" %s", strTimeZone);
}

bool FwkGetTime(CFwkString& fstrTime, const char* strFormat, bool bIncludeMS, bool bIncludeTimeZone)
{
	time_t now = time(NULL);
	char strTime[32];
	memset(strTime, 0, 32);

	struct tm date;
	if(strFormat == 0)
		strftime(strTime, 31, "%Y-%m-%d %H:%M:%S", ACE_OS::localtime_r(&now, &date));
	else
		strftime(strTime, 31, strFormat, ACE_OS::localtime_r(&now, &date));

	if(bIncludeMS == false)
		fstrTime = strTime;
	else
	{
	#ifdef WIN32
		static const __int64 scale = 10000000;
		__int64 timestamp;
		GetSystemTimeAsFileTime((LPFILETIME)&timestamp);
		fstrTime.sprintf("%s.%.6d", strTime, (long)(timestamp%scale/10));
	#else
		ACE_Time_Value time_of_day = ACE_OS::gettimeofday();		
		fstrTime.sprintf("%s.%.6d", strTime, time_of_day.usec());
	#endif
	}

	if(bIncludeTimeZone && !g_fstrTimeZone.IsEmpty())
		fstrTime += g_fstrTimeZone;

	return true;
}

