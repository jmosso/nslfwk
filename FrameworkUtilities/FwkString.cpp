#include "FwkString.h"

#include "ace/OS.h"
#include "ace/OS_NS_ctype.h"
#include "ace/OS_NS_stdio.h"

#define CSTRINGEX_SCANF_MAX_ARGS 10
#define ADD_ARGS_1		p[0]
#define ADD_ARGS_2		p[0], p[1]
#define ADD_ARGS_3		p[0], p[1], p[2]
#define ADD_ARGS_4		p[0], p[1], p[2], p[3]
#define ADD_ARGS_5		p[0], p[1], p[2], p[3], p[4]
#define ADD_ARGS_6		p[0], p[1], p[2], p[3], p[4], p[5]
#define ADD_ARGS_7		p[0], p[1], p[2], p[3], p[4], p[5], p[6]
#define ADD_ARGS_8		p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]
#define ADD_ARGS_9		p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8]
#define ADD_ARGS_10		p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8], p[9]

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
CFwkString::CFwkString()
{
	m_pCharBuffer = 0;
	m_iMaxSize = 0;
	SetMaxSize(1);
}
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
CFwkString::CFwkString(size_t iMaxSize)
{
	m_pCharBuffer = 0;
	m_iMaxSize = 0;
	SetMaxSize(iMaxSize);
}

//////////////////////////////////////////////////////////////////////
CFwkString::CFwkString(int type,const char * str, ...)
{
	m_pCharBuffer = 0;
	m_iMaxSize = 0;

	if(!str)
	{
		SetMaxSize(1);
		return;
	}
	va_list args;
	va_start(args, str);
	vsprintfNF(str, args);
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

CFwkString::CFwkString(const char* str)
{
	m_pCharBuffer = 0;
	m_iMaxSize = 0;
	CopyContents(str);
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

CFwkString::CFwkString(const char * cstr, size_t len)
{
	m_pCharBuffer = 0;
	m_iMaxSize = 0;

	SetMaxSize(len);
	if (m_pCharBuffer) {
		size_t cstrlen = 0;
		if(cstr)
		{
			cstrlen = ACE_OS::strlen(cstr);
			size_t min = cstrlen > m_iMaxSize?m_iMaxSize:cstrlen;
			if(min > 0)
				ACE_OS::memcpy(m_pCharBuffer, cstr, min);
		}
	}
}

CFwkString::CFwkString(void* pBuffer, size_t len)
{
	m_pCharBuffer = 0;
	m_iMaxSize = 0;
	SetMaxSize(len + 1);

	if(m_pCharBuffer) 
	{
		ACE_OS::memset(m_pCharBuffer, 0, len + 1);
		ACE_OS::memcpy(m_pCharBuffer, pBuffer, len);
	}
}


CFwkString::CFwkString(char c)
{
	m_pCharBuffer = 0;
	m_iMaxSize = 0;
	SetMaxSize(1);
	SetAt(0,c);
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
CFwkString::~CFwkString()
{
	if(m_pCharBuffer)
	{
		delete [] m_pCharBuffer;
		m_pCharBuffer = 0;
		m_iMaxSize = 0;
	}
}

char& CFwkString::operator [](size_t iIndex )
{

	if(iIndex >= m_iMaxSize)
	{
		SetMaxSize(iIndex + 1);
	}
	return m_pCharBuffer[iIndex];
}

bool CFwkString::IsEmpty() const
{
	if(m_pCharBuffer == NULL)
		return true;

	return GetLength() == 0;
}

bool CFwkString::CheckIsNumeric() const
{
	if(m_pCharBuffer == NULL)
		return true;

	size_t size = GetLength();
	for( size_t i = 0; i < size; i++ )
	{
		if (( m_pCharBuffer[i] < '0' ) || ( m_pCharBuffer[i] > '9' ) )
		{
			return false;
		}
	}
	return true;
}

bool CFwkString::GetStringInBtw(CFwkString& fstrTarget, const char* strChStart, const char* strChEnd)
{
	if(m_pCharBuffer == NULL)
		return false;

	size_t iOffset = 0;
	size_t iPos1 = Find(strChStart, iOffset);
			
	if(iPos1 == J_MAX_INDEX)
		iPos1 = 0;

	size_t iPos2 = Find(strChEnd, iPos1);
	if(iPos2 == J_MAX_INDEX)
		iPos2 = GetLength();

	if(iPos1 > iPos2)
	{
		fstrTarget = m_pCharBuffer;
		return false;
	}

	size_t len = 0;
	len = iPos2 - (iPos1 + 1);
	if(iPos1 != 0)
		iPos1++;
	else
		len++;

	fstrTarget = Mid(iPos1, len);
	return true;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
bool CFwkString::SetMaxSize (size_t newSize)
{
	if (newSize < 0) {
		return false;
	}
	size_t newsizebytes = newSize+1;
	size_t oldsizebytes = GetMaxSize();
	ACE_TCHAR * newArray;

	if (newSize > 0 && newSize <= oldsizebytes)
		return true;

	if ((newArray = new ACE_TCHAR[newsizebytes]) == NULL)
		return false;

	if (m_pCharBuffer != NULL)
		ACE_OS::memcpy(newArray, m_pCharBuffer, oldsizebytes < newsizebytes?oldsizebytes:newsizebytes);

	if (newsizebytes > oldsizebytes)
		ACE_OS::memset(newArray+oldsizebytes, 0, newsizebytes-oldsizebytes);

	if(m_pCharBuffer)
	{
		delete [] m_pCharBuffer;
		m_pCharBuffer = 0;
		m_iMaxSize = 0;
	}
	m_pCharBuffer = newArray;
	m_iMaxSize = newSize;

	return true;
}

bool CFwkString::ForceMinimumSize()
{
	size_t size = strlen(m_pCharBuffer);
	size_t oldsizebytes = GetMaxSize();
	if(oldsizebytes <= size)
		return false;

	size_t newsizebytes = size + 1;
	ACE_TCHAR * newArray;
	if ((newArray = new ACE_TCHAR[newsizebytes]) == NULL)
		return false;

	ACE_OS::memset(newArray, 0, newsizebytes);
	ACE_OS::memcpy(newArray, m_pCharBuffer, size);

	delete [] m_pCharBuffer;
	m_pCharBuffer = newArray;
	m_iMaxSize = size;

	return true;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
CFwkString & CFwkString::
operator += (const char* rhs)
{
	if(rhs == 0)
		return *this;

	size_t olen = GetLength();
	size_t alen = ACE_OS::strlen(rhs);
	SetMaxSize(olen+alen);
	ACE_OS::memcpy(m_pCharBuffer+olen, rhs, alen);
	m_pCharBuffer[olen+alen] = '\0';
	return *this;

}
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

CFwkString & CFwkString::
operator += (const char ch)
{
	char rhs[2];
	rhs[0] = ch;
	rhs[1] = '\0';
	return operator += (rhs);
}

CFwkString CFwkString::operator+(const char * rhs) const
{
	if(rhs == 0)
		return *this;

	size_t olen = GetLength();
	size_t alen = ACE_OS::strlen(rhs);
	CFwkString str;
	str.SetMaxSize(olen+alen);
	ACE_OS::memmove(str.m_pCharBuffer, m_pCharBuffer,  olen);
	ACE_OS::memcpy(str.m_pCharBuffer+olen, rhs, alen);
	return str;

}

CFwkString CFwkString::operator+(char ch) const
{
	size_t olen = GetLength();
	CFwkString str;
	str.SetMaxSize(olen+1);

	if(str.m_pCharBuffer && m_pCharBuffer)
	{
		ACE_OS::memmove(str.m_pCharBuffer, m_pCharBuffer, olen);
		str.m_pCharBuffer[olen] = ch;
	}
	return str;

}

CFwkString CFwkString::operator&(const char * cstr) const
{
	if (cstr == NULL)
		return *this;
	size_t alen = ACE_OS::strlen(cstr);
	if (alen == 1)
		return *this;
	size_t olen = GetLength();
	CFwkString str;
	int space = olen > 0 && m_pCharBuffer[olen-1]!=' ' && *cstr!=' ' ? 1 : 0;
	str.SetMaxSize(olen+alen+space);
	ACE_OS::memmove(str.m_pCharBuffer,  m_pCharBuffer,  olen);
	if (space != 0)
		str.m_pCharBuffer[olen] = ' ';
	ACE_OS::memcpy(str.m_pCharBuffer+olen+space, cstr, alen);
	return str;
}


CFwkString CFwkString::operator&(char c) const
{
	size_t olen = GetLength();
	CFwkString str;
	int space = olen > 0 && m_pCharBuffer[olen-1] != ' ' && c != ' ' ? 1 : 0;
	str.SetMaxSize(olen+1+space);
	ACE_OS::memmove(str.m_pCharBuffer,  m_pCharBuffer, olen);
	if (space != 0)
		str.m_pCharBuffer[olen] = ' ';
	str.m_pCharBuffer[olen+space] = c;
	return str;
}


CFwkString & CFwkString::operator&=(const char * cstr)
{
	if (cstr == NULL)
		return *this;

	size_t alen = ACE_OS::strlen(cstr);
	if (alen == 1)
		return *this;
	size_t olen = GetLength();
	int space = olen > 0 && m_pCharBuffer[olen-1]!=' ' && *cstr!=' ' ? 1 : 0;
	SetMaxSize(olen+alen+space);
	if (space != 0)
		m_pCharBuffer[olen] = ' ';
	ACE_OS::memcpy(m_pCharBuffer+olen+space, cstr, alen);
	return *this;
}


CFwkString & CFwkString::operator&=(char ch)
{
	size_t olen = GetLength();
	int space = olen > 0 && m_pCharBuffer[olen-1] != ' ' && ch != ' ' ? 1 : 0;
	SetMaxSize(olen+1+space);
	if (space != 0)
		m_pCharBuffer[olen] = ' ';
	m_pCharBuffer[olen+space] = ch;
	return *this;
}

bool CFwkString::operator*=(const char * cstr) const
{
	if (cstr == NULL)
		return IsEmpty();

	const char * pstr = m_pCharBuffer;
	while (*pstr != '\0' && *cstr != '\0') {
		if (ACE_OS::ace_toupper(*pstr) != ACE_OS::ace_toupper(*cstr))
			return false;
		pstr++;
		cstr++;
	}
	return *pstr == *cstr;
}
/*
bool CFwkString::CFwkStringCompare::operator()( const CFwkString name1, const CFwkString name2 ) const
{
	if( strcmp( name1.GetBuffer(), name2.GetBuffer() ) < 0 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

size_t CFwkString::CFwkStringCompare::operator()( const CFwkString& v ) const
{
	std::string strTmp( v.GetBuffer() );
	std::tr1::hash<std::string> myHash;
	return ( myHash )( strTmp );
}
*/
CFwkString CFwkString::ToLower() const
{
	CFwkString newStr(m_pCharBuffer);
	for (char *cpos = newStr.m_pCharBuffer; *cpos != '\0'; cpos++) {
		if (ACE_OS::ace_isupper(*cpos))
			*cpos = (char)ACE_OS::ace_tolower(*cpos);
	}
	return newStr;
}

void CFwkString::ToLowerThis() const
{
	for (char *cpos = m_pCharBuffer; *cpos != '\0'; cpos++) {
		if (ACE_OS::ace_isupper(*cpos))
			*cpos = (char)ACE_OS::ace_tolower(*cpos);
	}
}


CFwkString CFwkString::ToUpper() const
{
	CFwkString newStr(m_pCharBuffer);
	for (char *cpos = newStr.m_pCharBuffer; *cpos != '\0'; cpos++) {
		if (ACE_OS::ace_islower(*cpos))
			*cpos = (char)ACE_OS::ace_toupper(*cpos);
	}
	return newStr;
}

void CFwkString::ToUpperThis() const
{
	for (char *cpos = m_pCharBuffer; *cpos != '\0'; cpos++) {
		if (ACE_OS::ace_islower(*cpos))
			*cpos = (char)ACE_OS::ace_toupper(*cpos);
	}
}

CFwkString CFwkString::operator()(size_t start, size_t end) const
{
	if (end < start)
		return CFwkString();

	register size_t len = GetLength();
	if (start > len)
		return CFwkString();

	if (end >= len) {
		if (start == 0)
			return *this;
		end = len-1;
	}
	len = end - start + 1;

	return CFwkString(m_pCharBuffer+start, len);
}


CFwkString CFwkString::Left(size_t len) const
{
	if (len == 0)
		return CFwkString();

	if (len >= GetLength())
		return *this;

	return CFwkString(m_pCharBuffer, len);
}


CFwkString CFwkString::Right(size_t len) const
{
	if (len == 0)
		return CFwkString();

	size_t srclen = GetLength();
	if (len >= srclen)
		return *this;

	return CFwkString(m_pCharBuffer+srclen-len, len);
}


CFwkString CFwkString::Mid(size_t start, size_t len) const
{
	if (len == 0)
		return CFwkString();

	if (start+len < start) // Beware of wraparound
		return operator()(start, 1000000);
	else
		return operator()(start, start+len-1);
}

void CFwkString::Clear()
{
	if(m_pCharBuffer == NULL)
		return;
	size_t len = GetMaxSize();
	if(m_pCharBuffer)
		ACE_OS::memset(m_pCharBuffer,0,len);
}

SIX_FWK_INT64 CFwkString::AsInteger(unsigned base) const
{
	if(base == 10 && Find("x") != J_MAX_INDEX)
	{
		base = 16;
	}

	return STR_TO_LL(m_pCharBuffer, 0, base);
}

double CFwkString::AsReal() const
{
	return ACE_OS::strtod(m_pCharBuffer, 0);
}


CFwkString & CFwkString::sprintf(const char * fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	return vsprintf(fmt, args);
}

CFwkString & CFwkString::sprintfc(const char * fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	return vsprintf(fmt, args,true);
}


CFwkString & CFwkString::vsprintf(const char * fmt, va_list arg,bool concatenate)
{
	size_t len = concatenate ? GetLength() : 0;
	size_t size = 0;
	int iWC = 0;
	do 
	{
		size += 16384; // igual que MAX_TRACE_BUFFER
		SetMaxSize(size);
		iWC = ACE_OS::vsnprintf(m_pCharBuffer+len, size-len-1, fmt, arg);
	} while (iWC < 0 || (iWC >= (size-len-1) ) );

	ForceMinimumSize();
	return *this;
}

// para usar en el constructor. Formato de cadenas cortas
CFwkString & CFwkString::vsprintfNF(const char * fmt, va_list arg,bool concatenate)
{
	size_t len = concatenate ? GetLength() : 0;
	size_t size = 0;
	int iWC = 0;
	do 
	{
		size += 100; 
		SetMaxSize(size);
		iWC = ACE_OS::vsnprintf(m_pCharBuffer+len, size-len-1, fmt, arg);
	} while (iWC < 0 || (iWC >= (size-len-1) ) );

	return *this;
}


CFwkString jpsprintf(const char * fmt, ...)
{
	CFwkString str;
	va_list args;
	va_start(args, fmt);
	return str.vsprintf(fmt, args);
}


CFwkString jpvsprintf(const char * fmt, va_list arg)
{
	CFwkString str;
	return str.vsprintf(fmt, arg);
}

int CFwkString::InternalCompare(size_t offset, char c) const
{
	char ch = m_pCharBuffer[offset];
	if (ch < c)
		return -1;
	if (ch > c)
		return 1;
	return 0;
}


int CFwkString::InternalCompare(size_t offset, size_t length, const char * cstr) const
{
	if (offset == 0 && m_pCharBuffer == cstr)
		return 0;

	if (cstr == NULL)
		return IsEmpty() ? 0 : -1;

	int retval;
	if (length == J_MAX_INDEX)
		retval = ACE_OS::strcmp(m_pCharBuffer+offset, cstr);
	else
		retval = ACE_OS::strncmp(m_pCharBuffer+offset, cstr, length);

	if (retval < 0)
		return -1;

	if (retval > 0)
		return 1;

	return 0;
}

size_t CFwkString::Find(char ch, size_t offset) const
{
	register size_t len = GetLength();
	while (offset < len) {
		if (InternalCompare(offset, ch) == 0)
			return offset;
		offset++;
	}
	return J_MAX_INDEX;
}


size_t CFwkString::Find(const char * cstr, size_t offset) const
{
	if (cstr == NULL || *cstr == '\0')
		return J_MAX_INDEX;

	size_t len = GetLength();
	size_t clen = ACE_OS::strlen(cstr);
	if (clen > len)
		return J_MAX_INDEX;

	if (offset > len - clen)
		return J_MAX_INDEX;

	if (len - clen < 10) {
		while (offset+clen <= len) {
			if (InternalCompare(offset, clen, cstr) == 0)
				return offset;
			offset++;
		}
		return J_MAX_INDEX;
	}

	size_t strSum = 0;
	size_t cstrSum = 0;
	for (size_t i = 0; i < clen; i++) {
		strSum += ACE_OS::ace_toupper(m_pCharBuffer[offset+i]);
		cstrSum += ACE_OS::ace_toupper(cstr[i]);
	}

	// search for a matching substring
	while (offset+clen <= len) {
		if (strSum == cstrSum && InternalCompare(offset, clen, cstr) == 0)
			return offset;
		strSum += ACE_OS::ace_toupper(m_pCharBuffer[offset+clen]);
		strSum -= ACE_OS::ace_toupper(m_pCharBuffer[offset++]);
	}

	return J_MAX_INDEX;
}

size_t CFwkString::FindLast(char ch, size_t offset) const
{
	size_t len = GetLength();
	if (len == 0)
		return J_MAX_INDEX;
	if (offset >= len)
		offset = len-1;

	while (InternalCompare(offset, ch) != 0) {
		if (offset == 0)
			return J_MAX_INDEX;
		offset--;
	}

	return offset;
}


size_t CFwkString::FindLast(const char * cstr, size_t offset) const
{
	if (cstr == NULL || *cstr == '\0')
		return J_MAX_INDEX;

	size_t len = GetLength();
	size_t clen = ACE_OS::strlen(cstr);
	if (clen > len)
		return J_MAX_INDEX;

	if (offset == 0)
		return J_MAX_INDEX;

	if (offset > len - clen)
		offset = len - clen;

	size_t strSum = 0;
	size_t cstrSum = 0;
	for (size_t i = 0; i < clen; i++) {
		strSum += ACE_OS::ace_toupper(m_pCharBuffer[offset+i]);
		cstrSum += ACE_OS::ace_toupper(cstr[i]);
	}

	// search for a matching substring
	while (offset > 0) {
		if (strSum == cstrSum && InternalCompare(offset, clen, cstr) == 0)
			return offset;
		strSum += ACE_OS::ace_toupper(m_pCharBuffer[--offset]);
		strSum -= ACE_OS::ace_toupper(m_pCharBuffer[offset+clen]);
	}

	return J_MAX_INDEX;
}


size_t CFwkString::FindOneOf(const char * cset, size_t offset) const
{
	if (cset == NULL || *cset == '\0')
		return J_MAX_INDEX;

	size_t len = GetLength();
	while (offset < len) {
		const char * p = cset;
		while (*p != '\0') {
			if (InternalCompare(offset, *p) == 0)
				return offset;
			p++;
		}
		offset++;
	}
	return J_MAX_INDEX;
}

void CFwkString::Replace(const CFwkString & target,
						 const CFwkString & subs,
						 bool all, size_t offset)
{
	//MakeUnique();

	size_t tlen = target.GetLength();
	size_t slen = subs.GetLength();
	do {
		size_t pos = Find(target, offset);
		if (pos == J_MAX_INDEX)
			return;
		Splice(subs, pos, tlen);
		offset = pos + slen;
	} while (all);
}


void CFwkString::Splice(const char * cstr, size_t pos, size_t len)
{
	register size_t slen = GetLength();
	if (pos >= slen)
		operator+=(cstr);
	else {
		//MakeUnique();
		size_t clen = cstr != NULL ? ACE_OS::strlen(cstr) : 0;
		size_t newlen = slen-len+clen;
		if (clen > len)
			SetMaxSize(newlen);
		if (pos+len < slen)
			ACE_OS::memmove(m_pCharBuffer+pos+clen, m_pCharBuffer+pos+len, slen-pos-len+1);
		if (clen > 0)
			ACE_OS::memcpy(m_pCharBuffer+pos, cstr, clen);
		m_pCharBuffer[newlen] = '\0';
	}
}

size_t CFwkString::
ReverseFind(char ch) const
{
	if(m_pCharBuffer == 0)
		return J_MAX_INDEX;

	char* tmp = ACE_OS::strrchr(m_pCharBuffer, ch);
	if(tmp == 0)
		return J_MAX_INDEX;

	return(tmp - m_pCharBuffer);
}

bool CFwkString::
GetToken(size_t& iPosition, const char* strTerminator, CFwkString& fstrToken) const
{
	if(m_pCharBuffer == 0)
		return false;

	fstrToken = m_pCharBuffer + iPosition;
	size_t iLPos = J_MAX_INDEX;

	iLPos = Find(strTerminator, iPosition);

	if(iLPos == J_MAX_INDEX)
	{
		iPosition = J_MAX_INDEX;
		return false;
	}

	fstrToken.SetAt(iLPos - iPosition, 0);
	fstrToken.MakeMinimumSize();
	iPosition = iLPos + 1;

	return true;
}

bool CFwkString::GetTokenEx(size_t& iPosition, const char* strTerminator, CFwkString& fstrToken) const
{
	if(m_pCharBuffer == 0)
		return false;

	fstrToken = m_pCharBuffer + iPosition;
	size_t iLPos = J_MAX_INDEX;

	iLPos = Find(strTerminator, iPosition);

	if(iLPos == J_MAX_INDEX)
	{
		iPosition = J_MAX_INDEX;
		return false;
	}

	fstrToken.SetAt(iLPos - iPosition, 0);
	fstrToken.MakeMinimumSize();
	iPosition = iLPos + strlen(strTerminator);

	return true;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
void CFwkString::CopyContents(const CFwkString & rhs)
{
	CopyContents(rhs.m_pCharBuffer);
}
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
void CFwkString::CopyContents(const char * rhs)
{
	if(rhs != NULL && rhs == m_pCharBuffer)
		return;

	size_t size = 0;
	if(rhs)
		size = ACE_OS::strlen(rhs);
	SetMaxSize(size);
	ACE_OS::memset(m_pCharBuffer, 0, m_iMaxSize+1);
	ACE_OS::strncpy(m_pCharBuffer, rhs, size);
}

void CFwkString::FromBuffer(void* buffer, size_t iSize)
{
	if(buffer == NULL || iSize <= 0)
		return;

	SetMaxSize(iSize + 1);
	ACE_OS::memset(m_pCharBuffer, 0, iSize + 1);
	ACE_OS::memcpy(m_pCharBuffer, buffer, iSize);
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
short CFwkString::FillFormat(const char * fstrData, size_t uSize, size_t uAlign, char cFill)
{
	size_t uActSize = 0;
	size_t uPos = 0;

	if (uSize == 0)
	{
		CopyContents(fstrData);
		return 0;
	}

	uActSize = ACE_OS::strlen(fstrData);

	if (uActSize > uSize) 
	{
		//CopyContents(fstrData, uSize);
		CopyContents(fstrData);
		return 0;
	}

	if (m_iMaxSize < uSize)
		SetMaxSize(uSize);

	ACE_OS::memset(m_pCharBuffer, cFill, uSize);
	m_pCharBuffer[uSize] = '\0';
	//ACE_OS::memset(( m_pCharBuffer + uSize ), 0, (m_iMaxSize) );

	// left = 0
	if (uAlign == 0)
		uPos = 0;
	// rignt = 1;
	else 
		uPos = uSize - uActSize;

	ACE_OS::strncpy((m_pCharBuffer + uPos), fstrData, uActSize);

	return 0;

}
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
bool CFwkString::CopyContents(const char *rhs, size_t uSize)
{
	if(rhs != NULL && rhs == m_pCharBuffer)
		return false;

	size_t size = 0;
	if(rhs)
		size = ACE_OS::strlen(rhs);
	else 
		return false;
	if ( uSize > m_iMaxSize )
		SetMaxSize(uSize+1);

	ACE_OS::memset(m_pCharBuffer, 0, m_iMaxSize+1);
	ACE_OS::strncpy(m_pCharBuffer, rhs, uSize);
	return true;
}
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
short CFwkString::Format2Data(size_t uSize, size_t uAlign, char cFill)
{
	if(uSize <= 0)
		return 0;

	size_t uInitPos = 0;
	size_t uFinalPos = 0;
	size_t uActSize = GetUsedSize();

	if(uSize > uActSize)
		uSize = uActSize;

	//leff
	if (uAlign == 0)
	{
		uInitPos = uSize - 1;
	}
	//right
	else
	{
		uInitPos = 0;
	}
	uFinalPos = uInitPos;
	if (uActSize < uFinalPos)
	{
		uFinalPos = uActSize - 1;
	}
	while (m_pCharBuffer[uFinalPos] == cFill)
	{
		//left
		if(uAlign == 0)
			uFinalPos--;
		//right
		else
			uFinalPos++;
	}

	if (uAlign == 0)
		m_pCharBuffer[uFinalPos + 1] = '\0';
	else
		ACE_OS::strcpy(m_pCharBuffer, m_pCharBuffer + uFinalPos);


	return 0;
}
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

bool CFwkString::Scanf(int numArgs, const char * format, ...)
{
	int       i;
	int       numScanned;
	void      *argPointer[CSTRINGEX_SCANF_MAX_ARGS];
	va_list   arglist;

	if ( numArgs > CSTRINGEX_SCANF_MAX_ARGS )
	{
		// This function can only handle <CSTRINGEX_SCANF_MAX_ARGS> 
		//(10) arguments!
		return false;
	}

	va_start( arglist, format );
	{
		// Get all arguments from stack
		for ( i=0; i<numArgs; i++ )
		{
			argPointer[i] = va_arg( arglist, void * );
		}
	}
	va_end( arglist );

	// Call sscanf with correct no. of arguments
	numScanned = sscanfWrapper( numArgs, format, argPointer );

	return (numScanned == numArgs);
}

int CFwkString::sscanfWrapper( int numArgs, const char * format, void **p )
{
	switch ( numArgs )
	{
	case  0: return 0;
	case  1: return ::sscanf( m_pCharBuffer, format, ADD_ARGS_1  );
	case  2: return ::sscanf( m_pCharBuffer, format, ADD_ARGS_2  );
	case  3: return ::sscanf( m_pCharBuffer, format, ADD_ARGS_3  );
	case  4: return ::sscanf( m_pCharBuffer, format, ADD_ARGS_4  );
	case  5: return ::sscanf( m_pCharBuffer, format, ADD_ARGS_5  );
	case  6: return ::sscanf( m_pCharBuffer, format, ADD_ARGS_6  );
	case  7: return ::sscanf( m_pCharBuffer, format, ADD_ARGS_7  );
	case  8: return ::sscanf( m_pCharBuffer, format, ADD_ARGS_8  );
	case  9: return ::sscanf( m_pCharBuffer, format, ADD_ARGS_9  );
	case 10: return ::sscanf( m_pCharBuffer, format, ADD_ARGS_10 );
	}

	// When extending max. no. of arguments [CSTRINGEX_SCANF_MAX_ARGS],
	// this function must be updated!
	return 0;
}
