#pragma once

#include "Templates/TFactory.h"
#include "Templates/TSingleton.h"
#include "FrameworkUtilities/FwkString.h"
#include "FrameworkUtilities/FwkBuffer.h"

#ifdef _DEBUG
#define 	DYNDEBUG(pObj)	(pObj)->DebugInfo(__FILE__,__LINE__,__FUNCTION__)
#else
#define 	DYNDEBUG(pObj)
#endif

class ParamsCodec;
class ParamsCodecGenericBuffer;
namespace sixlabs
{
    class Exception;
}

typedef  sixlabs::Exception SC5Exception;

#ifdef WIN32
#include <stdint.h>
#endif //WIN32

#ifdef _DEBUG
#define 	DYNFACTGETRES	CDynamicStructFactory::St_GetInstance()->GetResource(__FILE__,__LINE__,__FUNCTION__)
#define		DYNFACTFREERES(x)	CDynamicStructFactory::St_GetInstance()->FreeResource(x,__FILE__,__LINE__,__FUNCTION__)
#else
#define 	DYNFACTGETRES	CDynamicStructFactory::St_GetInstance()->GetResource()
#define		DYNFACTFREERES	CDynamicStructFactory::St_GetInstance()->FreeResource
#endif


class CDynamicStruct
{
public:
	CDynamicStruct();
	CDynamicStruct(const CDynamicStruct & str);
	virtual ~CDynamicStruct();

	enum DYN_VAR_TYPE
	{
		DYN_PARAM_NONE = 0,
		DYN_PARAM_INTEGER = 1,
		DYN_PARAM_STRING,
		DYN_PARAM_BUFFER
	};

	bool writeTextChar(char* strOut, size_t size, size_t& fullSize);
	void readTextChar(const char* strIn);
	
	void readOrchestratorText(const CFwkString & in);

	bool AddString(const CFwkString & name,const CFwkString & value);
	bool AddInt(const CFwkString & name,const int intvalue);
	bool AddInt(const CFwkString & name,const size_t intvalue);
	//bool AddInt(const CFwkString & name,const int64_t intvalue);
	bool AddInt(const CFwkString & name,const SIX_FWK_INT64 intvalue);
	bool AddDynamicStruct(const CFwkString & name,CDynamicStruct * pDynamic);
	bool DeleteParam(const char * name);
	bool DeleteParamGroup(const char *groupName);
	bool GetString(const CFwkString & name,CFwkString & value) const;
	const char * GetStringPointer(const CFwkString & name) const;
	bool GetInt(const CFwkString & name,int &intvalue) const;
	bool GetInt(const CFwkString & name, SIX_FWK_INT64 &llValue) const;
	void Print(const CFwkString &fstrPre = "", int mask = 0, int level = 0);
	void Print(const CFwkString &fstrPre, void* pCaller, int mask = 0, int level = 0);
	void DebugInfo(const CFwkString & fstrFile, int nLine, const CFwkString & fstrFunction);

	bool GetBufferAsString(const CFwkString & name, CFwkString & value);
	bool AddBufferFromString(const CFwkString & name, CFwkString& value);

	CDynamicStruct & operator += (const CDynamicStruct& rhs);
	CDynamicStruct & operator = (const CDynamicStruct & buffer);

	void Clean();

	CDynamicStruct *GetGroupParams(const char *pstrGroupName) const;
	bool GetGroupParams(const char *pstrGroupName, CDynamicStruct* pDynTarget) const;
	bool CopyGroupParams(const char *pstrGroupName, CDynamicStruct* pDynTarget) const;
	bool CopyGroupParams(const char* strGroupName, CDynamicStruct* pDynTarget, const CFwkString& fstrTargetGroupName);
	bool CleanGroupParams(const char *pstrGroupName);

	CDynamicStruct *GetGroupParams(const char* file, int line, const char* function, const char *pstrGroupName) const;
	bool IsGroupParams(const char *pstrGroupName);
	bool IsParam(const char *pstrGroupName);
	bool getParamType(const char *pstrGroupName, DYN_VAR_TYPE& varType);

	void SetGroupParams(const char *pstrGroupName,CDynamicStruct *pDynamicParams = 0);

	bool SerializeToString(CFwkString& fstrTarget);
	bool BuildFromString(const CFwkString& fstrTarget, bool bClearPC = false);

	CDynamicStruct * clone();

	ParamsCodec * GetParamsCodec() {
		return m_pParamsCodec;
	}

	ParamsCodec * CloneParamsCodec();
	ParamsCodec * ChangeParamsCodec();
	ParamsCodec * ChangeParamsCodec(ParamsCodec * pParamsCodec) ;

	void ExchangeParamsCodec(CDynamicStruct* pToExchange);

	void SetParamsCodec(ParamsCodec * pParamsCodec) ;
	void SetParamsCodecBuffer(const char* strParamName, char* buffer, int len) ;

	int GetSize();

	void PrintToString(CFwkString &ToThisTring, const CFwkString &fstrPre = "");
	void PrintToString(CFwkString &ToThisTring, const char* strSepParam, const char* strSepValue);

	bool GetGenericBuffer(const char *pstrGroupName, ParamsCodecGenericBuffer* body);
	void AddParamsCodec(ParamsCodec paramcodec, const char *pstrGroupName);
	bool GetGroupParamsPC(const char *pstrGroupName, ParamsCodec*& newParamsCodec);
	void SetGenericBuffer(const char *pstrGroupName, ParamsCodecGenericBuffer& body);
  void writeObject( CFwkBuffer & out, int nSleepTime );
  void readObject( const CFwkBuffer & in, int nSleepTime );
  void writeText( CFwkString & out );


private:
	ParamsCodec * m_pParamsCodec;
	void URLDecode(char *dest, const char *src);
	ParamsCodec* ReleaseParamsCodec();

	int hexToBin(char *hex, char *bin, size_t size);
};

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

class CDynamicStructFactory : public TFactory<CDynamicStruct>
							, public TSingleton<CDynamicStructFactory>
{
public:
	CDynamicStructFactory()
	{
		m_fstrFactoryName = "CDynamicStructFactory";
	}

	virtual ~CDynamicStructFactory(){}

	virtual CDynamicStruct* GetResource();
	virtual CDynamicStruct* GetResource(const char* file, int line, const char* function);
	virtual bool FreeResource(CDynamicStruct* pObjectToFree);
	virtual bool FreeResource(CDynamicStruct* pObjectToFree, const char* file, int line, const char* function);

	static bool ms_bPrintBuffers;
	static bool ms_bUseNativePCPrint;

	inline CFwkString & GetVarSeparator() { return m_sep; }
	inline CFwkString & GetVarTerminator() { return m_ter; }
	inline CFwkString & GetTypeSeparator() { return m_ide; }

	void SetConfigParams(const char* strVarStringSep, const char* strVarStringTerm, const char* strTypeStringSep, bool bPrintPCBuffers, bool pUseNativePCPrint);

private :
	CFwkString m_sep;
	CFwkString m_ter;
	CFwkString m_ide;
};
