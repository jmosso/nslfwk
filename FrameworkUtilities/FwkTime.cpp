// FwkTime.cpp: implementation of the CFwkTime class.
//
//////////////////////////////////////////////////////////////////////
#include "FwkString.h"
#include "FwkTime.h"
//#include "string.h"
//#include <iomanip.h>
#define _OSUTIL_CXX

#ifndef WIN32	
#define P_LINUX 1
#endif //WIN32

#if defined(P_LINUX)
#ifndef _REENTRANT
#define _REENTRANT
#endif
#elif defined(P_SOLARIS) 
#define _POSIX_PTHREAD_SEMANTICS
#endif


#include <fcntl.h>
#ifdef P_VXWORKS
#include <sys/times.h>
#else
#include <time.h>
#ifndef WIN32
#include <sys/time.h>
#else
#include <Windows.h>
#endif //WIN32
#endif
#include <ctype.h>

#if defined(P_LINUX)

#include <mntent.h>
#include <sys/vfs.h>

#define P_HAS_READDIR_R

#if (__GNUC_MINOR__ < 7 && __GNUC__ < 3)
#include <localeinfo.h>
#else
#define P_USE_LANGINFO
#endif

#elif defined(P_FREEBSD) || defined(P_OPENBSD) || defined(P_NETBSD) || defined(P_MACOSX) || defined(P_MACOS)
#define P_USE_STRFTIME

#include <sys/param.h>
#include <sys/mount.h>

#elif defined(P_HPUX9) 
#define P_USE_LANGINFO

#elif defined(P_AIX)
#define P_USE_STRFTIME

#include <fstab.h>
#include <sys/stat.h>
#include <sys/statfs.h>

#elif defined(P_SOLARIS) 
#define P_HAS_READDIR_R
#define P_USE_LANGINFO
#include <sys/timeb.h>
#include <sys/statvfs.h>
#include <sys/mnttab.h>

#elif defined(P_SUN4)
#include <sys/timeb.h>

#elif defined(__BEOS__)
#define P_USE_STRFTIME

#elif defined(P_IRIX)
#define P_USE_LANGINFO
#include <sys/stat.h>
#include <sys/statfs.h>
#include <stdio.h>
#include <mntent.h>

#elif defined(P_VXWORKS)
#define P_USE_STRFTIME

#elif defined(P_RTEMS)
#define P_USE_STRFTIME
#include <time.h>
#include <stdio.h>
#define random() rand()
#define srandom(a) srand(a)

#elif defined(P_QNX)
#include <sys/dcmd_blk.h>
#include <sys/statvfs.h>
#define P_USE_STRFTIME
#endif

#ifdef P_USE_LANGINFO
#include <langinfo.h>
#endif

#define LINE_SIZE_STEP        100

#define DEFAULT_FILE_MODE        (S_IRUSR|S_IWUSR|S_IROTH|S_IRGRP)


#ifdef P_SUN4
extern "C" {
	int on_exit(void (*f)(void), caddr_t);
	int atexit(void (*f)(void))
	{
		return on_exit(f, 0);
	}
	static char *tzname[2] = { "STD", "DST" };
};
#endif


#define PABS(v) ((v) < 0 ? -(v) : (v))



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFwkTime::CFwkTime()
{
#ifdef WIN32	
  // Magic constant to convert epoch from 1601 to 1970
  static const __int64 delta = ((__int64)369*365+(369/4)-3)*24*60*60U;
  static const __int64 scale = 10000000;

  __int64 timestamp;

//#ifndef _WIN32_WCE
  GetSystemTimeAsFileTime((LPFILETIME)&timestamp);
/*#else
  SYSTEMTIME SystemTime;
  GetSystemTime(&SystemTime);
  SystemTimeToFileTime(&SystemTime, (LPFILETIME)&timestamp);
#endif	*/

  theTime = (time_t)(timestamp/scale - delta);
  microseconds = (long)(timestamp%scale/10);
#else
#ifdef P_VXWORKS
  struct timespec ts;
  clock_gettime(0,&ts);
  theTime = ts.tv_sec;
  microseconds = ts.tv_sec*10000 + ts.tv_nsec/100000L;
#else
  struct timeval tv;
  gettimeofday(&tv, NULL);
  theTime = tv.tv_sec;
  microseconds = tv.tv_usec;
#endif // P_VXWORKS
#endif //WIN32
}

CFwkTime::~CFwkTime()
{

}

CFwkString CFwkTime::AsString(
  const char * format,    /// Arbitrary format C string pointer for time.
  int zone          /// Time zone for the time.
) const
{

  bool is12hour = strchr(format, 'a') != NULL;

  CFwkString str;

  // the localtime call automatically adjusts for daylight savings time
  // so take this into account when converting non-local times
  if (zone == Local)
    zone = GetTimeZone();  // includes daylight savings time
  time_t realTime = theTime + zone*60;     // to correct timezone
  struct tm ts;
  struct tm * t = os_gmtime(&realTime, &ts);

  size_t repeatCount;
  CFwkString fstrTemp;
  
  while (*format != '\0') 
  {
	  repeatCount = 1;
	  switch (*format) 
	  {
      case 'a' :
		  while (*++format == 'a')
			  ;
		  if (t->tm_hour < 12)
			  str += GetTimeAM();
		  else
			  str += GetTimePM();
		  break;
		  
      case 'h' :
		  {
			  while (*++format == 'h')
				  repeatCount++;
			  
			  fstrTemp = CFwkString(0,"%d",is12hour ? (t->tm_hour+11)%12+1 : t->tm_hour);
			  for(size_t i = 0; i < (repeatCount - fstrTemp.GetLength()); i++)
			  {
				  CFwkString temp = fstrTemp;
				  fstrTemp = "0";
				  fstrTemp += temp;
			  }
			  
			  str += fstrTemp;
			  //str += CFwkString(0,"%d",is12hour ? (t->tm_hour+11)%12+1 : t->tm_hour);
		  }
		  break;
		  
      case 'm' :
		  {
			  while (*++format == 'm')
				  repeatCount++;

			  fstrTemp = CFwkString(0,"%d",t->tm_min);
			  for(size_t i = 0; i < (repeatCount - fstrTemp.GetLength()); i++)
			  {
				  CFwkString temp = fstrTemp;
				  fstrTemp = "0";
				  fstrTemp += temp;
			  }
			  str += fstrTemp;
			  //str += CFwkString(0,"%d",t->tm_min);
		  
		  }
		  break;
		  
      case 's' :
		  {
			  while (*++format == 's')
				  repeatCount++;

			  fstrTemp = CFwkString(0,"%d",t->tm_sec);
			  for(size_t i = 0; i < (repeatCount - fstrTemp.GetLength()); i++)
			  {
				  CFwkString temp = fstrTemp;
				  fstrTemp = "0";
				  fstrTemp += temp;
			  }
			  str += fstrTemp;

			  //str += CFwkString(0,"%d",t->tm_sec);
		  }
		  break;
		  
      case 'w' :
		  while (*++format == 'w')
			  repeatCount++;
		  if (repeatCount != 3 || *format != 'e')
			  str += GetDayName((Weekdays)t->tm_wday, repeatCount <= 3 ? Abbreviated : FullName);
		  else {
			  static const char * const EnglishDayName[] = {
				  "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
			  };
			  str += EnglishDayName[t->tm_wday];
			  format++;
		  }
		  break;
		  
      case 'M' :
		  {
			  while (*++format == 'M')
				  repeatCount++;
			  
			  fstrTemp.sprintf("%%-%d", repeatCount);
			  
			  if (repeatCount < 3)
			  {
				  //str.sprintf("%s%d", (const char*)fstrFormat, t->tm_mon+1);
				  fstrTemp = CFwkString(0,"%d", t->tm_mon+1);
				  //str += CFwkString(0,"%d", t->tm_mon+1);
				  for(size_t i = 0; i < (repeatCount - fstrTemp.GetLength()); i++)
				  {
					  CFwkString temp = fstrTemp;
					  fstrTemp = "0";
					  fstrTemp += temp;
				  }
				  str += fstrTemp;
			  }
			  else if (repeatCount > 3 || *format != 'E')
				  str += GetMonthName((Months)(t->tm_mon+1),
				  repeatCount == 3 ? Abbreviated : FullName);
			  else {
				  static const char * const EnglishMonthName[] = {
					  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
						  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
				  };
				  str += EnglishMonthName[t->tm_mon];
				  format++;
			  }
		  }
		  break;
		  
      case 'd' :
		  {
			  while (*++format == 'd')
				  repeatCount++;
			  
			  
			  fstrTemp = CFwkString(0,"%d", t->tm_mday);
			  //str += CFwkString(0,"%d", t->tm_mday);
			  for(size_t i = 0; i < (repeatCount - fstrTemp.GetLength()); i++)
			  {
				  CFwkString temp = fstrTemp;
				  fstrTemp = "0";
				  fstrTemp += temp;
			  }
			  str += fstrTemp;
		  }
		  break;
		  
      case 'y' :
		  {
			  while (*++format == 'y')
				  repeatCount++;
			  if (repeatCount <= 3)
				  fstrTemp = CFwkString(0,"%d",t->tm_year%100);
			  //str += CFwkString(0,"%d",t->tm_year%100);
			  else
				  fstrTemp = CFwkString(0,"%d",t->tm_year+1900);
			  //str += CFwkString(0,"%d",t->tm_year+1900);
			  for(size_t i = 0; i < (repeatCount - fstrTemp.GetLength()); i++)
			  {
				  CFwkString temp = fstrTemp;
				  fstrTemp = "0";
				  fstrTemp += temp;
			  }
			  str += fstrTemp;
		  }
		  break;
		  
      case 'z' :
      case 'Z' :
		  if (zone == 0) {
			  if (*format == 'Z')
				  str += 'Z';
			  else
				  str += "GMT";
		  }
		  else {
			  str += (zone < 0 ? '-' : '+');
			  zone = PABS(zone);
			  CFwkString strZone;
			  strZone.sprintf("%02d%02d", zone/60, zone%60);
			  str += strZone;
			  //str += CFwkString(0,"%d",zone/60) + CFwkString(0,"%d",zone%60);
		  }
		  while (toupper(*++format) == 'z')
			  ;
		  break;
		  
      case 'u' :
		  while (*++format == 'u')
			  repeatCount++;

		  switch (repeatCount) {
          case 1 :
			  str += ((microseconds+50000)/100000);
			  break;
          case 2 :
			  str += CFwkString(0,"%d",(microseconds+5000)/10000);
			  break;
          case 3 :
			  str += CFwkString(0,"%d",(microseconds+500)/1000);
			  break;
          default :
			  str += CFwkString(0,"%d", microseconds);
			  break;
		  }
		  break;
		  
		  case '\\' :
			  //format++;
			  // Escaped character, put straight through to output string
			  
		  default :
			  str += *format++;
    }
  }

  return str;
}

int CFwkTime::GetTimeZone(TimeZoneType type)
{
#ifdef WIN32
  TIME_ZONE_INFORMATION tz;
  if(GetTimeZoneInformation(&tz) == 0xffffffff)
  {
	//SIX_ASSERT(false);
  }
  if (type == DaylightSavings)
    tz.Bias += tz.DaylightBias;
  return -tz.Bias;
#else
#if defined(P_LINUX) || defined(P_SOLARIS) || defined (P_AIX) || defined(P_IRIX)
  long tz = -::timezone/60;
  if (type == StandardTime)
	  return tz;
  else
	  return tz + ::daylight*60;
#elif defined(P_FREEBSD) || defined(P_OPENBSD) || defined(P_NETBSD) || defined(P_MACOSX) || defined(P_MACOS) || defined(__BEOS__) || defined(P_QNX)
  time_t t;
  time(&t);
  struct tm ts;
  struct tm * tm = os_localtime(&t, &ts);
  int tz = tm->tm_gmtoff/60;
  if (type == StandardTime && tm->tm_isdst)
	  return tz-60;
  if (type != StandardTime && !tm->tm_isdst)
	  return tz + 60;
  return tz;
#elif defined(P_SUN4) 
  struct timeb tb;
  ftime(&tb);
  if (type == StandardTime || tb.dstflag == 0)
	  return -tb.timezone;
  else
	  return -tb.timezone + 60;
#else
  long tz = -::timezone/60;
  if (type == StandardTime)
	  return tz;
  else
	  return tz + ::daylight*60;
#endif  
#endif //WIN32
}

#ifndef WIN32
#ifdef P_PTHREADS
struct tm * os_localtime(const time_t * clock, struct tm * ts)
{
#ifdef P_MACOSX 
#warning LOCALTIME_R NOT DEFINED
	return ::localtime(clock);
#else
	return ::localtime_r(clock, ts);
#endif
}
#else
struct tm * os_localtime(const time_t * clock, struct tm * ts)
{
	return ::localtime_r(clock, ts);
}
#endif
#endif //WIN32

bool CFwkTime::IsDaylightSavings()
{
#ifdef WIN32
  TIME_ZONE_INFORMATION tz;
  DWORD result = GetTimeZoneInformation(&tz);
//  PAssertOS(result != 0xffffffff);
  return result == TIME_ZONE_ID_DAYLIGHT;
#else //linux  
  time_t theTime = ::time(NULL);
  struct tm ts;
  return os_localtime(&theTime, &ts)->tm_isdst != 0; 
#endif //WIN32
}

struct tm * CFwkTime::os_gmtime(const time_t * clock, struct tm * tb)
{
  struct tm * tp = ::gmtime(clock);
  if (tp != NULL)
    return tp;

  memset(tb, 0, sizeof(*tb));
  return tb;
}

#define PWIN32GetLocaleInfo GetLocaleInfo


CFwkString CFwkTime::GetTimeAM()
{
#ifdef WIN32
	CFwkString str;
	PWIN32GetLocaleInfo(GetUserDefaultLCID(), LOCALE_S1159, str.GetPointer(100), 99);
	str.MakeMinimumSize();
	return str;	
#else //linux
#if defined(P_USE_LANGINFO)
	return CFwkString(nl_langinfo(AM_STR));
#elif defined(P_USE_STRFTIME)
	char buf[30];
	struct tm t;
	memset(&t, 0, sizeof(t));
	t.tm_hour = 10;
	t.tm_min = 12;
	t.tm_sec = 11;
	strftime(buf, sizeof(buf), "%p", &t);
	return buf;
#else
#warning Using default AM string
	return "AM";
#endif
#endif //WIN32	

}


CFwkString CFwkTime::GetTimePM()
{
#ifdef WIN32
	CFwkString str;
	PWIN32GetLocaleInfo(GetUserDefaultLCID(), LOCALE_S2359, str.GetPointer(100), 99);
	str.MakeMinimumSize();
	return str;	
#else //linux
#if defined(P_USE_LANGINFO)
	return CFwkString(nl_langinfo(PM_STR));
#elif defined(P_USE_STRFTIME)
	char buf[30];
	struct tm t;
	memset(&t, 0, sizeof(t));
	t.tm_hour = 20;
	t.tm_min = 12;
	t.tm_sec = 11;
	strftime(buf, sizeof(buf), "%p", &t);
	return buf;
#else
#warning Using default PM string
	return "PM";
#endif	
#endif //WIN32	

}


CFwkString CFwkTime::GetDayName(Weekdays dayOfWeek, NameType type)
{
#ifdef WIN32
	CFwkString str;
  // Of course Sunday is 6 and Monday is 1...
	PWIN32GetLocaleInfo(GetUserDefaultLCID(), (dayOfWeek+6)%7 +
			(type == Abbreviated ? LOCALE_SABBREVDAYNAME1 : LOCALE_SDAYNAME1),
			 str.GetPointer(100), 99);
	str.MakeMinimumSize();
	return str;
#else //linux
#if defined(P_USE_LANGINFO)
	return CFwkString(
     (type == Abbreviated) ? nl_langinfo((nl_item)(ABDAY_1+(int)dayOfWeek)) :
			nl_langinfo((nl_item)(DAY_1+(int)dayOfWeek))
				  );

#elif defined(P_LINUX)
  return (type == Abbreviated) ? CFwkString(_time_info->abbrev_wkday[(int)dayOfWeek]) :
		  CFwkString(_time_info->full_wkday[(int)dayOfWeek]);

#elif defined(P_USE_STRFTIME)
  char buf[30];
  struct tm t;
  memset(&t, 0, sizeof(t));
  t.tm_wday = day;
  strftime(buf, sizeof(buf), type == Abbreviated ? "%a" : "%A", &t);
  return buf;
#else
#warning Using default day names
  static const char *defaultNames[] = {
	  "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
   "Saturday"
  };

  static const char *defaultAbbrev[] = {
	  "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
  };
  return (type == Abbreviated) ? CFwkString(defaultNames[(int)dayOfWeek]) :
		  CFwkString(defaultAbbrev[(int)dayOfWeek]);
#endif	
#endif //WIN32	
}


CFwkString CFwkTime::GetMonthName(Months month, NameType type)
{
#ifdef WIN32
	CFwkString str;
	PWIN32GetLocaleInfo(GetUserDefaultLCID(), month-1 +
			(type == Abbreviated ? LOCALE_SABBREVMONTHNAME1 : LOCALE_SMONTHNAME1),
			 str.GetPointer(100), 99);
	str.MakeMinimumSize();
	return str;		
#else //linux
#if defined(P_USE_LANGINFO)
	return CFwkString(
     (type == Abbreviated) ? nl_langinfo((nl_item)(ABMON_1+(int)month-1)) :
			nl_langinfo((nl_item)(MON_1+(int)month-1))
				  );
#elif defined(P_LINUX)
  return (type == Abbreviated) ? CFwkString(_time_info->abbrev_month[(int)month-1]) :
		  CFwkString(_time_info->full_month[(int)month-1]);
#elif defined(P_USE_STRFTIME)
  char buf[30];
  struct tm t;
  memset(&t, 0, sizeof(t));
  t.tm_mon = month-1;
  strftime(buf, sizeof(buf), type == Abbreviated ? "%b" : "%B", &t);
  return buf;
#else
#warning Using default monthnames
  static const char *defaultNames[] = {
	  "January", "February", "March", "April", "May", "June", "July", "August",
   "September", "October", "November", "December" };

   static const char *defaultAbbrev[] = {
	   "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
	"Sep", "Oct", "Nov", "Dec" };

  return (type == Abbreviated) ? CFwkString(defaultNames[(int)month-1]) :
		  CFwkString(defaultAbbrev[(int)month-1]);
#endif
#endif //WIN32	
}




