// FwkRandom.h: interface for the CFwkRandom class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FWKRANDOM_H__F4E425D8_346B_4086_A123_E91CE2815358__INCLUDED_)
#define AFX_FWKRANDOM_H__F4E425D8_346B_4086_A123_E91CE2815358__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#define M 397
#define MATRIX_A 0x9908b0df   /* constant vector a */
#define UPPER_MASK 0x80000000 /* most significant w-r bits */
#define LOWER_MASK 0x7fffffff /* least significant r bits */

/* Tempering parameters */   
#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)


class CFwkRandom  
{
public:
	CFwkRandom();
	virtual ~CFwkRandom();
	
	
	
    /**Construct the random number generator.
	This version allows the application to choose the seed, thus letting it
	get the same sequence of values on each run. Useful for debugging.
	*/
    CFwkRandom(
		unsigned int seed    /// New seed value, must not be zero
		)
	{
		SetSeed(seed);
	}
	
	
    /**Set the seed for the random number generator.
      */
    void SetSeed(
      unsigned int seed    /// New seed value, must not be zero
    );

    /**Get the next psuedo-random number in sequence.
       This generates one pseudorandom unsigned integer (32bit) which is
       uniformly distributed among 0 to 2^32-1 for each call.
      */
    unsigned Generate();

    /**Get the next psuedo-random number in sequence.
      */
    inline operator unsigned() { return Generate(); }


    /**Get the next psuedo-random number in sequence.
       This utilises a single system wide thread safe PRandom variable. All
       threads etc will share the same psuedo-random sequence.
      */
    static unsigned GetANumber();


  protected:
    enum {
      RandBits = 8, // I recommend 8 for crypto, 4 for simulations
      RandSize = 1<<RandBits
    };

    unsigned int randcnt;
    unsigned int randrsl[RandSize];
    unsigned int randmem[RandSize];
    unsigned int randa;
    unsigned int randb;
    unsigned int randc;

};

#endif // !defined(AFX_FWKRANDOM_H__F4E425D8_346B_4086_A123_E91CE2815358__INCLUDED_)
