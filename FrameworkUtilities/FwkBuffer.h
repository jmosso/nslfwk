#pragma once

#include "Templates/TFactory.h"
#include "Templates/TSingleton.h"

class CFwkString;

class CFwkBuffer
{
public:
	CFwkBuffer(void);
	CFwkBuffer(const char * pMem, int size);
public:
	~CFwkBuffer(void);

	short SetMaxSize(size_t iMaxSize);
	void Clear();

	CFwkBuffer & operator += (const CFwkBuffer & buffer);
	CFwkBuffer & operator = (const CFwkBuffer & buffer);
	CFwkBuffer & operator = (const char buffer[]);
	char & operator [] (size_t iIndex);

	inline size_t GetMaxSize()	const { return m_iMaxSize; }
	inline size_t GetUsedSize()	const { return m_iUsedSize; }
	inline char * GetBuffer() const { return m_pCharBuffer; }
	inline void SetUsedSize(size_t iUsedSize) { m_iUsedSize = iUsedSize; }

	int WriteHexDump(CFwkString & s);
	int ReadHexDump(CFwkString & s);
	CFwkBuffer& AddInt(int iIn);
	CFwkBuffer& AddMen(const char *refBuffer, int iSize);
	bool operator==(const CFwkBuffer& rBuff);
	bool CompBuff(char * chpBuff, int size);

	int SetBits(void * refInMen, int iBitMenSize, int iBitInitPos);
	int GetBits(void * refOutMen, int iBitMenSize, int iBitInitPos);


	int GetBitValue(char & octeto, int iBit);
	int SetBitValue(char & octeto, int iBit, int value);

	int GetSeqID(){return m_iSeqID;}

	short ReSize (size_t iMaxSize);

	bool SplitBuffer(int iIndex, size_t iSize, CFwkBuffer& buffer, bool& bLast, size_t& iStartPosition);
	bool AddHeader(const char* strHeader);
	bool AsString(CFwkString& fstr, size_t& iSize);

protected: 
	char * m_pCharBuffer;
	size_t m_iMaxSize;
	size_t m_iUsedSize;

	int m_iSeqID;
	static int ms_iCounter;


	static unsigned char chMasks1[8];
	static unsigned char chMasks0[8];
};

class CFwkBufferFactory:
	public TFactory<CFwkBuffer>,
	public TSingleton<CFwkBufferFactory>
{
public:
	CFwkBufferFactory()
	{
		m_fstrFactoryName = "CFwkBufferFactory";
	}
};
