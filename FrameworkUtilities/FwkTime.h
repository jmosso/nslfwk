// FwkTime.h: interface for the CFwkTime class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FWKTIME_H__BCD71AED_EE4B_4F82_A920_EE70636B4E17__INCLUDED_)
#define AFX_FWKTIME_H__BCD71AED_EE4B_4F82_A920_EE70636B4E17__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <time.h>

class CFwkTime  
{
public:

    enum {
      /// Universal Coordinated Time.
      UTC   = 0,
      /// Greenwich Mean Time, effectively UTC.
      GMT   = UTC,
      /// Local Time.
      Local = 9999
    };

	CFwkTime();
	virtual ~CFwkTime();

    CFwkString AsString(
      const char * formatPtr,    /// Arbitrary format C string pointer for time.
      int zone = Local           /// Time zone for the time.
    ) const;

  /**@name Time Zone configuration functions */
  //@{
    /**Get flag indicating daylight savings is current.
    
       @return
       TRUE if daylight savings time is active.
     */
    static bool IsDaylightSavings();
    /// Flag for time zone adjustment on daylight savings.
    enum TimeZoneType {
      StandardTime,
      DaylightSavings
    };


    /// Get the time zone offset in minutes.
	static inline int GetTimeZone() 
	  { return GetTimeZone(IsDaylightSavings() ? DaylightSavings : StandardTime); }
    /**Get the time zone offset in minutes.
       This is the number of minutes to add to UTC (previously known as GMT) to
       get the local time. The first form automatically adjusts for daylight
       savings time, whilst the second form returns the specified time.

       @return
       Number of minutes.
     */
    static int GetTimeZone(
       TimeZoneType type  /// Daylight saving or standard time.
    );


    /**Get the internationalised time AM string.
    
       @return
       string for AM.
     */
    static CFwkString GetTimeAM();

    /**Get the internationalised time PM string.
    
       @return
       string for PM.
     */
    static CFwkString GetTimePM();

    /// Days of the week.
    enum Weekdays {
      Sunday,
      Monday,
      Tuesday,
      Wednesday,
      Thursday,
      Friday,
      Saturday
    };
    /// Flag for returning language dependent string names.
    enum NameType {
      FullName,
      Abbreviated
    };

    /// Month codes.
    enum Months {
      January = 1,
      February,
      March,
      April,
      May,
      June,
      July,
      August,
      September,
      October,
      November,
      December
    };


    /**Get the internationalised month name string (1=Jan etc).
    
       @return
       string for month.
     */
    static CFwkString GetMonthName(
      Months month,             /// Code for month in year.
      NameType type = FullName  /// Flag for abbreviated or full name.
    );

    /**Get the internationalised day of week day name (0=Sun etc).
    
       @return
       string for week day.
     */
    static CFwkString GetDayName(
      Weekdays dayOfWeek,       /// Code for day of week.
      NameType type = FullName  /// Flag for abbreviated or full name.
    );


    static struct tm * os_gmtime(const time_t * clock, struct tm * t);

protected:
    // Member variables
    /// Number of seconds since 1 January 1970.
    time_t theTime;
    long   microseconds;

};

#endif // !defined(AFX_FWKTIME_H__BCD71AED_EE4B_4F82_A920_EE70636B4E17__INCLUDED_)
