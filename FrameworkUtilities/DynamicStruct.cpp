// DinamicStruct.cpp: implementation of the CDynamicStruct class.
//
//////////////////////////////////////////////////////////////////////
#pragma warning(disable:4786)

#include <deque>
#include "DynamicStruct.h"
#include "ace/OS_NS_unistd.h"
#include "ace/OS_NS_ctype.h"
#include "ace/OS_NS_stdio.h"
#include "paramsCodec.hpp"
#include "Utilities/Tracing.h"

#define LENGTH_LENGTH	2  //bytes
#define INT_LENGTH		4  //bytes

#define MAX_FILE_SIZE	600000 //bytes

#define SIZE_STEP	10000 //1000

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDynamicStruct::CDynamicStruct()
{
	m_pParamsCodec = new ParamsCodec;
}

CDynamicStruct::CDynamicStruct( const CDynamicStruct & str )
{
	m_pParamsCodec = new ParamsCodec;
	operator = ( str );
}

CDynamicStruct::~CDynamicStruct()
{
	if (m_pParamsCodec != NULL)
	{
		delete m_pParamsCodec;
	}
}

bool CDynamicStruct::AddString( const CFwkString & name, const CFwkString & value )
{
	try
	{
		m_pParamsCodec->setString( name, value );
	}
	catch ( SC5Exception &e )
	{
		TRACEPF_DEBUG("",  "/*************** SC5Exception AddString %s \n ",e.getString().c_str() );
		return false;
	}
	return true;
}

bool CDynamicStruct::AddInt(const CFwkString & name,const SIX_FWK_INT64 intvalue)
{
	try
	{
		m_pParamsCodec->setInteger( name, (int64_t)intvalue );
	}
	catch ( SC5Exception &e )
	{
		TRACEPF_DEBUG("",  "/*************** SC5Exception AddInt %s \n ",e.getString().c_str() );
		return false;
	}
	return true;
}

bool CDynamicStruct::AddInt( const CFwkString & name, const int intvalue )
{
	try
	{
		m_pParamsCodec->setInteger( name, intvalue );
	}
	catch ( SC5Exception &e )
	{
		TRACEPF_DEBUG("",  "/*************** SC5Exception AddInt %s \n ",e.getString().c_str() );
		return false;
	}
	return true;
}

bool CDynamicStruct::AddInt( const CFwkString & name, size_t intvalue )
{
	try
	{
		m_pParamsCodec->setInteger( name, (int)intvalue );
	}
	catch ( SC5Exception &e )
	{
		TRACEPF_DEBUG("",  "/*************** SC5Exception AddInt %s \n ",e.getString().c_str() );
		return false;
	}
	return true;
}

bool CDynamicStruct::AddDynamicStruct( const CFwkString & name, CDynamicStruct * pDynamic )
{
	try
	{
		m_pParamsCodec->addParamsCodec( *pDynamic->GetParamsCodec(), name );
	}
	catch ( SC5Exception &e )
	{
		TRACEPF_DEBUG("",  "/*************** SC5Exception AddDynamicStruct %s \n ",e.getString().c_str() );
		return false;
	}
	return true;
}

bool CDynamicStruct::GetString( const CFwkString & name, CFwkString & value ) const
{
	try
	{
		const char *pValue = m_pParamsCodec->getString( name );
		value.CopyContents( pValue, strlen( pValue ) );
	}
	catch (...)
	{
		return false;
	}
	return true;
}

const char * CDynamicStruct::GetStringPointer( const CFwkString & name ) const
{
	const char *pValue;
	try
	{
		pValue = m_pParamsCodec->getString( name );
	}
	catch (...)
	{
		pValue = 0;
	}
	return pValue;
}

bool CDynamicStruct::GetInt(const CFwkString & name, SIX_FWK_INT64 &llValue) const
{
	try
	{
		llValue = m_pParamsCodec->getInteger( name );
	}
	catch (...)
	{
		return false;
	}
	return true;
}

bool CDynamicStruct::GetInt( const CFwkString & name, int &intvalue ) const
{
	try
	{
		intvalue = m_pParamsCodec->getInteger( name );
	}
	catch (...)
	{
		return false;
	}
	return true;
}

CDynamicStruct *CDynamicStruct::GetGroupParams( const char *pstrGroupName )const
{
	CDynamicStruct *newParamsCodec = NULL;
	try
	{
		newParamsCodec = DYNFACTGETRES;
		newParamsCodec->SetParamsCodec( m_pParamsCodec->getParamsCodec( pstrGroupName ) );
	}
	catch (...)
	{
		if (newParamsCodec)
		{
			DYNFACTFREERES(newParamsCodec);
		}		
		return 0;
	}
	return newParamsCodec;
}

bool CDynamicStruct::GetGroupParams(const char *pstrGroupName, CDynamicStruct* pDynTarget)const
{
	CDynamicStruct* pDyn = this->GetGroupParams(pstrGroupName);
	*pDynTarget = *pDyn;
	DYNFACTFREERES(pDyn);
	return true;
}

bool CDynamicStruct::CleanGroupParams(const char *pstrGroupName)
{
	CDynamicStruct* pDyn = this->GetGroupParams(pstrGroupName);
	this->Clean();
	this->SetGroupParams(pstrGroupName, pDyn);
	DYNFACTFREERES(pDyn);
	return true;
}

bool CDynamicStruct::CopyGroupParams(const char *pstrGroupName, CDynamicStruct* pDynTarget) const
{
	CDynamicStruct* pDyn = this->GetGroupParams(pstrGroupName);
	pDynTarget->SetGroupParams(pstrGroupName, pDyn);
	DYNFACTFREERES(pDyn);
	return true;
}

bool CDynamicStruct::CopyGroupParams(const char* strGroupName, CDynamicStruct* pDynTarget, const CFwkString& fstrTargetGroupName)
{
	if(IsGroupParams(strGroupName) == false)
		return false;

	if(fstrTargetGroupName.IsEmpty() == true)
		return false;

	CDynamicStruct* pDyn = GetGroupParams(strGroupName);
	pDynTarget->SetGroupParams(fstrTargetGroupName, pDyn);
	DYNFACTFREERES(pDyn);
	return true;
}


bool CDynamicStruct::GetGroupParamsPC(const char *pstrGroupName, ParamsCodec*& newParamsCodec)
{
	return 	m_pParamsCodec->getParamsCodec( pstrGroupName, newParamsCodec );
}

CDynamicStruct* CDynamicStruct::GetGroupParams(const char* file, int line, const char* function, const char *pstrGroupName) const
{
	TRACEPF_DEBUG("",  "CDynamicStruct::GetGroupParams %s %s:%d %s", 
											pstrGroupName, file, line, function);
	return GetGroupParams(pstrGroupName);
}

bool CDynamicStruct::getParamType(const char *name, DYN_VAR_TYPE& varType)
{
	if(!m_pParamsCodec)
		return false;

	if(m_pParamsCodec->isParam(name) == false)
		return false;

	ParamsCodec::ParameterType type = m_pParamsCodec->getParameterType(name);
	switch(type)
	{
		case ParamsCodec::ParamsCodecValueChoiceInteger:
			varType = DYN_PARAM_INTEGER;
			break;

		case ParamsCodec::ParamsCodecValueChoiceString:
			varType = DYN_PARAM_STRING;
			break;

		case ParamsCodec::ParamsCodecValueChoiceBuffer:
			varType = DYN_PARAM_BUFFER;
			break;

		default:
			varType = DYN_PARAM_NONE;
			break;
	}

	return true;
}

bool CDynamicStruct::IsGroupParams( const char *pstrGroupName )
{
	try
	{
		return m_pParamsCodec->isParamGroup( pstrGroupName );
	}
	catch (...)
	{
		return false;
	}
}

bool CDynamicStruct::IsParam(const char *pstrGroupName)
{
	try
	{
		return m_pParamsCodec->isParam( pstrGroupName );
	}
	catch (...)
	{
		return false;
	}
}


bool CDynamicStruct::DeleteParam( const char * name )
{
	try
	{
		m_pParamsCodec->deleteParam( name );
	}
	catch (...)
	{
		return false;
	}
	return true;
}

bool CDynamicStruct::DeleteParamGroup( const char *groupName )
{
	try
	{
		m_pParamsCodec->deleteParamGroup(groupName);
	}
	catch (...)
	{
		return DeleteParam(groupName);
	}
	return true;
}

CDynamicStruct & CDynamicStruct::operator += ( const CDynamicStruct & rhs )
{
	CDynamicStruct& buffer = const_cast<CDynamicStruct&>( rhs );
	ParamsCodec * pTmp = buffer.GetParamsCodec();
	*m_pParamsCodec += *pTmp;
	return *this;
}

CDynamicStruct & CDynamicStruct::operator = ( const CDynamicStruct & data )
{
	CDynamicStruct& buffer = const_cast<CDynamicStruct&>( data );
	ParamsCodec * pTmp = buffer.GetParamsCodec();
	*m_pParamsCodec = *pTmp;
	return *this;
}

void CDynamicStruct::Clean()
{
	m_pParamsCodec->clear();
}

void CDynamicStruct::Print(const CFwkString &fstrPre, void* pCaller, int mask, int level)
{
	if(CTracing::CanTrace(mask, level) == true)
	{
		CFwkString fstrToDyn;
		PrintToString(fstrToDyn);
		CFwkString fstrObjInfo;
		fstrObjInfo.sprintf("<0x%x>", this);
		TRACEPF_COMPATIBLE(fstrObjInfo.GetBuffer(), mask, level, "%s\ncaller <0x%x>\n%s", (const char*)fstrPre, pCaller, (const char*)fstrToDyn);
	}
}

void CDynamicStruct::Print( const CFwkString &fstrPre, int mask, int level)
{
	if(CTracing::CanTrace(mask, level) == true)
	{
		CFwkString fstrToDyn;
		PrintToString(fstrToDyn);
		CFwkString fstrObjInfo;
		fstrObjInfo.sprintf("<0x%x>", this);
		TRACEPF_COMPATIBLE(fstrObjInfo.GetBuffer(), mask, level, "%s\n%s", (const char*)fstrPre, (const char*)fstrToDyn);
	}
}

void CDynamicStruct::SetGroupParams( const char *pstrGroupName, CDynamicStruct *pDynamicParams )
{
	try
	{
		m_pParamsCodec->addParamsCodec( *pDynamicParams->GetParamsCodec(), pstrGroupName );

	}
	catch ( SC5Exception &e )
	{
		TRACEPF_DEBUG("",  "/*************** SC5Exception SetGroupParams %s \n ",e.getString().c_str() );
	}
}

bool CDynamicStruct::GetGenericBuffer(const char* pstrGroupName, ParamsCodecGenericBuffer* body)
{
	try
	{
		*body = m_pParamsCodec->getBuffer(pstrGroupName);
		return true;
	}
	catch (...)
	{
		return false;
	}
}

void CDynamicStruct::SetGenericBuffer(const char *pstrGroupName, ParamsCodecGenericBuffer& body)
{
	try
	{
		m_pParamsCodec->setBuffer(pstrGroupName, body);
	}
	catch (...)
	{
		return;
	}
}

void CDynamicStruct::SetParamsCodecBuffer(const char* strParamName, char* buffer, int len)
{
	try
	{
		m_pParamsCodec->setBuffer(strParamName, buffer, len);
	}
	catch (...)
	{
		return;
	}
}

void CDynamicStruct::AddParamsCodec(ParamsCodec paramcodec, const char *pstrGroupName)
{
	try
	{
		m_pParamsCodec->addParamsCodec(paramcodec, pstrGroupName);
		return;
	}
	catch (...)
	{
		return;
	}
}


CDynamicStruct * CDynamicStruct::clone()
{
	CDynamicStruct * paramTmp = DYNFACTGETRES;
	if ( paramTmp )
	{
		*paramTmp = *this;
	}
	return paramTmp;
}

ParamsCodec* CDynamicStruct::CloneParamsCodec()
{
	ParamsCodec* pcCloned = new ParamsCodec;
	*pcCloned = *m_pParamsCodec;
	return pcCloned;
}

ParamsCodec * CDynamicStruct::ChangeParamsCodec()
{
	// dejar un pc vacio y devolver el actual
	ParamsCodec* pNewPC = new ParamsCodec;
	ParamsCodec* pSwitch = m_pParamsCodec;
	m_pParamsCodec = pNewPC;

	return pSwitch;
}

ParamsCodec * CDynamicStruct::ChangeParamsCodec( ParamsCodec * pParamsCodec )
{
	ParamsCodec * ret = 0;
	if(pParamsCodec != 0)
	{
		ret =  m_pParamsCodec;
		m_pParamsCodec = pParamsCodec;
	}

	return ret;
}

void CDynamicStruct::ExchangeParamsCodec(CDynamicStruct* pToExchange)
{
	if(pToExchange == 0)
		return;

	ParamsCodec* pSwitch = pToExchange->ReleaseParamsCodec();
	pToExchange->SetParamsCodec(m_pParamsCodec);
	m_pParamsCodec = pSwitch;
}

ParamsCodec* CDynamicStruct::ReleaseParamsCodec()
{
	ParamsCodec* pSwitch = m_pParamsCodec;
	m_pParamsCodec = NULL;
	return pSwitch;
}

int CDynamicStruct::GetSize()
{
	return m_pParamsCodec->size();
}

bool CDynamicStruct::AddBufferFromString(const CFwkString & name, CFwkString& value)
{
	if ( !m_pParamsCodec )
		return false;

	size_t iStringSize = value. GetMaxSize();
	char* argBuffer = new char[iStringSize + 1];
	int argSize = hexToBin(value.GetPointer(iStringSize), argBuffer, sizeof(argBuffer) );

	SetParamsCodecBuffer(name, argBuffer, argSize);
	delete[] argBuffer;

	return true;
}

bool CDynamicStruct::GetBufferAsString(const CFwkString & name, CFwkString & value)
{
	if ( !m_pParamsCodec )
		return false;

	if(m_pParamsCodec->isParam(name) == false)
		return false;

	ParamsCodec::ParameterType type = m_pParamsCodec->getParameterType(name);
	if(type != ParamsCodec::ParamsCodecValueChoiceBuffer)
		return false;

    ParamsCodecGenericBuffer *binaryBuffer = 0;
	m_pParamsCodec->getBuffer(name, binaryBuffer);

	std::string hex_str;
	for(size_t i = 0; i < binaryBuffer->size; i++)
	{
		unsigned int tmp = binaryBuffer->buffer[i] & 0xFF;
		char digit[4];
		sprintf(digit, "%02X ", tmp);
		hex_str.append(digit);
	}
	value = hex_str.c_str();
	return true;
}

void CDynamicStruct::PrintToString(CFwkString &ToThisTring, const char* strSepParam, const char* strSepValue)
{
	SIX_ASSERT( m_pParamsCodec );
	if(m_pParamsCodec == 0)
		return;

	CFwkString fstrData;
	bool bIsVal = false;
	m_pParamsCodec->restartIteration();
	while (!m_pParamsCodec->end() )
	{
		switch ( m_pParamsCodec->getParameterType() )
		{
			case ParamsCodec::ParamsCodecValueChoiceInteger:
				fstrData.sprintf( "%s%s%lld%s", m_pParamsCodec->getParameterName(), strSepValue, m_pParamsCodec->getInteger(m_pParamsCodec->getParameterName()), strSepParam);
				bIsVal = true;
				break;
			case ParamsCodec::ParamsCodecValueChoiceString:
				fstrData.sprintf( "%s%s%s%s", m_pParamsCodec->getParameterName(), strSepValue, m_pParamsCodec->getString(m_pParamsCodec->getParameterName()), strSepParam );
				bIsVal = true;
				break;

			default:
				m_pParamsCodec->next();
				continue;
				break;
		}

		ToThisTring += fstrData;
		m_pParamsCodec->next();
	}

	if(bIsVal == true)
		ToThisTring.SetAt(ToThisTring.GetLength() - 1, '\0'); // Quitar el ultimo caracter que tiene un separador adicional

	return;
}


void CDynamicStruct::PrintToString( CFwkString &ToThisTring, const CFwkString &fstrPre)
{
	SIX_ASSERT( m_pParamsCodec );
	if ( !m_pParamsCodec )
	{
		return;
	}

#ifdef WIN32
	CFwkString fstrData;
	m_pParamsCodec->restartIteration();
	while ( !m_pParamsCodec->end() )
	{
		switch ( m_pParamsCodec->getParameterType() )
		{
			case ParamsCodec::ParamsCodecValueChoiceInteger:
				fstrData.sprintf( "%s(%s) = %lld\n",
															m_pParamsCodec->getParameterName(),
															m_pParamsCodec->getParameterTypeStr(),
															m_pParamsCodec->getInteger( m_pParamsCodec->getParameterName() ) );
				break;
			case ParamsCodec::ParamsCodecValueChoiceString:
				fstrData.sprintf( "%s(%s) = %s\n",
															m_pParamsCodec->getParameterName(),
															m_pParamsCodec->getParameterTypeStr(),
															m_pParamsCodec->getString( m_pParamsCodec->getParameterName() ) );
				break;

			case ParamsCodec::ParamsCodecValueChoiceBuffer:
				{
					if(CDynamicStructFactory::ms_bPrintBuffers == false)
					{
						m_pParamsCodec->next();
						continue;
					}

    				ParamsCodecGenericBuffer *binaryBuffer = 0;
					m_pParamsCodec->getBuffer( m_pParamsCodec->getParameterName(), binaryBuffer);

					std::string hex_str;
					for(size_t i = 0; i < binaryBuffer->size; i++)
					{
						unsigned int tmp = binaryBuffer->buffer[i] & 0xFF;
						char digit[4];
						sprintf(digit, "%02X ", tmp);
						hex_str.append(digit);
					}

					fstrData.sprintf( "%s(%s) = %s\n",
																m_pParamsCodec->getParameterName(),
																m_pParamsCodec->getParameterTypeStr(),
																hex_str.c_str() );
				}
				break;

			default:
				break;
		}

		ToThisTring += fstrData;
		m_pParamsCodec->next();
	}
#else
	if(CDynamicStructFactory::ms_bUseNativePCPrint)
	{
		std::string textOut;
		m_pParamsCodec->print(textOut);
		ToThisTring = textOut.c_str();
	}
	else
	{
		CFwkString fstrData;
		m_pParamsCodec->restartIteration();
		while ( !m_pParamsCodec->end() )
		{
			switch ( m_pParamsCodec->getParameterType() )
			{
				case ParamsCodec::ParamsCodecValueChoiceInteger:
					fstrData.sprintf( "%s(%s) = %lld\n",
																m_pParamsCodec->getParameterName(),
																m_pParamsCodec->getParameterTypeStr(),
																m_pParamsCodec->getInteger( m_pParamsCodec->getParameterName() ) );
					break;
				case ParamsCodec::ParamsCodecValueChoiceString:
					fstrData.sprintf( "%s(%s) = %s\n",
																m_pParamsCodec->getParameterName(),
																m_pParamsCodec->getParameterTypeStr(),
																m_pParamsCodec->getString( m_pParamsCodec->getParameterName() ) );
					break;

				case ParamsCodec::ParamsCodecValueChoiceBuffer:
					{
						if(CDynamicStructFactory::ms_bPrintBuffers == false)
						{
							m_pParamsCodec->next();
							continue;
						}

    					ParamsCodecGenericBuffer *binaryBuffer = 0;
						m_pParamsCodec->getBuffer( m_pParamsCodec->getParameterName(), binaryBuffer);

						std::string hex_str;
						for(size_t i = 0; i < binaryBuffer->size; i++)
						{
							unsigned int tmp = binaryBuffer->buffer[i] & 0xFF;
							char digit[4];
							sprintf(digit, "%02X ", tmp);
							hex_str.append(digit);
						}

						fstrData.sprintf( "%s(%s) = %s\n",
																	m_pParamsCodec->getParameterName(),
																	m_pParamsCodec->getParameterTypeStr(),
																	hex_str.c_str() );
					}
					break;

				default:
					break;
			}

			ToThisTring += fstrData;
			m_pParamsCodec->next();
		}	
	}
#endif

}

bool CDynamicStruct::writeTextChar(char* strOut, size_t size, size_t& fullSize)
{
	ACE_OS::memset(strOut, 0, size);

	if ( !m_pParamsCodec )
	{
		fullSize = 0;
		return true;
	}

	CFwkString out;
	fullSize = 0;
	m_pParamsCodec->restartIteration();
	while ( !m_pParamsCodec->end() )
	{
		switch ( m_pParamsCodec->getParameterType() )
		{
			case ParamsCodec::ParamsCodecValueChoiceInteger:
				out.sprintf( "%s%sI%s%lld", //nombre&tipo:valor; // i
				m_pParamsCodec->getParameterName(), //nombre
				CDynamicStructFactory::St_GetInstance()->GetVarSeparator().GetBuffer(), //&
				CDynamicStructFactory::St_GetInstance()->GetTypeSeparator().GetBuffer(), // :
				m_pParamsCodec->getInteger( m_pParamsCodec->getParameterName() )); // valor

				out += CDynamicStructFactory::St_GetInstance()->GetVarTerminator().GetBuffer();
				break;
			case ParamsCodec::ParamsCodecValueChoiceString:
				out.sprintf( "%s%sS%s%s",
				m_pParamsCodec->getParameterName(),
				CDynamicStructFactory::St_GetInstance()->GetVarSeparator().GetBuffer(),
				CDynamicStructFactory::St_GetInstance()->GetTypeSeparator().GetBuffer(),
				m_pParamsCodec->getString( m_pParamsCodec->getParameterName() ));

				out += CDynamicStructFactory::St_GetInstance()->GetVarTerminator().GetBuffer();
				break;
			case ParamsCodec::ParamsCodecValueChoiceBuffer:
				break;
			default:
				break;
		}

		size_t localLen = out.GetLength();
		fullSize = fullSize + localLen;
		//if(fullSize >  (size - 1))
		if(fullSize >  size)
		{
			m_pParamsCodec->next();
			continue;
		}

		size_t outLen = ACE_OS::strlen(strOut);
		ACE_OS::memcpy(strOut + outLen, (const char*)out, localLen);
		m_pParamsCodec->next();
	}
	
	if(fullSize >  size)
		return false;

	return true;
}

void CDynamicStruct::readTextChar(const char* strIn)
{
	if(strIn == 0)
		return;

	size_t inLen = ACE_OS::strlen(strIn);
	if(inLen == 0)
		return;

	CFwkString fstrDynFull;
	CFwkString fstrType;
	CFwkString fstrName;
	CFwkString fstrValue;
	CFwkString m_ter = CDynamicStructFactory::St_GetInstance()->GetVarTerminator(); // ;
	CFwkString m_sep = CDynamicStructFactory::St_GetInstance()->GetVarSeparator();  // &
	CFwkString m_ide = CDynamicStructFactory::St_GetInstance()->GetTypeSeparator(); // :

	size_t scanned = 0;
	while(scanned <= inLen)
	{
		if(strIn[scanned] == '\\') // caracter de escape se debe tener en cuenta el siguiente
		{
			scanned++;
            fstrDynFull += strIn[scanned];
			scanned++;
			continue;
		}

		if(m_ter == strIn[scanned] || strIn[scanned] == 0) // encontrado terminador de variable
		{
			size_t posName = fstrDynFull.Find(m_sep);
			fstrName = fstrDynFull.Mid(0, posName);

			size_t posType = fstrDynFull.Find(m_ide, posName);
			fstrType = fstrDynFull.Mid(posName + 1, posType - (posName + 1));

			fstrValue = fstrDynFull.Right(fstrDynFull.GetLength() - (posType + 1));

			fstrDynFull.Clear();
			scanned++;

			if (fstrType == "I")
        		m_pParamsCodec->setInteger(fstrName, fstrValue.AsInteger());
			else
				m_pParamsCodec->setString(fstrName, fstrValue);
			continue;
		}
		fstrDynFull += strIn[scanned];
		scanned++;
	}
}

void CDynamicStruct::readOrchestratorText(const CFwkString & in)
{
	CFwkString sIn = in;
	sIn.Replace("\t", "", true);
	sIn.Replace("\n", "", true);
	sIn += CDynamicStructFactory::St_GetInstance()->GetVarTerminator().GetBuffer();
	size_t iPosition = 0;
	size_t iPos = 0;
	CFwkString sToken;
	CFwkString sName;
	CFwkString sValue;
	CFwkString sType;
	while (sIn.GetTokenEx(iPosition, CDynamicStructFactory::St_GetInstance()->GetVarTerminator().GetBuffer(), sToken))
	{
		iPos = 0;
		sToken.GetTokenEx(iPos, CDynamicStructFactory::St_GetInstance()->GetVarSeparator().GetBuffer(), sName);
		sToken.GetTokenEx(iPos, CDynamicStructFactory::St_GetInstance()->GetVarSeparator().GetBuffer(), sValue);
		sType = sValue.Mid(0, 1);
		sValue = sValue.Mid(CDynamicStructFactory::St_GetInstance()->GetTypeSeparator().GetLength() + 1, sValue.GetLength() - 2);
		if (sType == "I") 
		{
			long lTemp = sValue.AsInteger();
			if(lTemp != LONG_MAX) 
			{
				m_pParamsCodec->setInteger(sName, lTemp);
			} 
			else 
			{
				int64_t iTemp = sValue.AsInteger();
				m_pParamsCodec->setInteger(sName, iTemp);
			}
		}
		else 
		{
			//char out[sizeof sValue.GetBuffer()] = {0};
			size_t iLen = sValue.GetLength();
			char* out = new char[iLen + 1];
			memset(out, 0, iLen + 1);
			URLDecode(out, sValue.GetBuffer());
			sValue = out;
			delete(out);
			m_pParamsCodec->setString(sName, sValue);
		}
	}
}

bool CDynamicStruct::SerializeToString(CFwkString& fstrTarget)
{
	ParamsCodecGenericBuffer outputBuffer;

	int iSize = this->GetSize();
	if(iSize == 0)
		return false;

	try
	{
		this->GetParamsCodec()->writeObjectBAS( outputBuffer );
		fstrTarget.FromBuffer(outputBuffer.buffer, outputBuffer.size);	
	}
	catch(std::exception& ex)
	{
		TRACEPF_WARNING("", "CDynamicStruct::BuildFromString EXCEPTION en readObjectBAS\n%s", ex.what());
	}

	return true;
}

bool CDynamicStruct::BuildFromString(const CFwkString& fstrTarget, bool bClearPC)
{
	try
	{
		ParamsCodecGenericBuffer inputBuffer( (const char*)fstrTarget, fstrTarget.GetLength() + 1 );
		this->GetParamsCodec()->readObjectBAS( inputBuffer );
	}
	catch(std::exception& ex)
	{
		TRACEPF_WARNING("", "CDynamicStruct::BuildFromString EXCEPTION en readObjectBAS\n%s", ex.what());
	}
	return true;
}

void CDynamicStruct::SetParamsCodec( ParamsCodec * pParamsCodec )
{
	SIX_ASSERT(pParamsCodec);
	if (m_pParamsCodec != NULL)
	{
		delete m_pParamsCodec;
	}
	if (pParamsCodec)
	{
		m_pParamsCodec = pParamsCodec;
	}		

}

void CDynamicStruct::DebugInfo( const CFwkString & fstrFile, int nLine, const CFwkString & fstrFunction )
{
/*#ifdef _DEBUG
	AddString("Creation.FileName",fstrFile);
	AddInt("Creation.Line",nLine);
	AddString("Creation.Function",fstrFunction);
#endif //_DEBUG*/
}

void CDynamicStruct::URLDecode(char *dest, const char *src)
{
	const char *p = src;
	char code[3] = {0};
	unsigned long ascii = 0;
	char *end = NULL;

	
	while(*p) {
		if(*p == '%') {
			memcpy(code, ++p, 2);
			ascii = strtoul(code, &end, 16);
			*dest++ = (char)ascii;
			p += 2;
		} else if (*p == '+') {
			*dest++ = ' ';
			p += 1;
		} else {
			*dest++ = *p++;
		}
	}
}

int CDynamicStruct::hexToBin(char *hex, char *bin, size_t size)
{
    bool shouldExit = false;
    char *start = hex;
    char *tmp;
    int i = 0;

    do
    {
        if ( 0 == *start ) // fin de string
        {
            return i;
        }
        errno = 0;
        int val = strtol( start, &tmp, 16 );
        if ( 0 != errno)
        {
            shouldExit = true;
        }
        else
        {
            bin[i++] = val & 0xFF;
            start = tmp;
        }
    }
    while (!shouldExit);

    return i;
}

bool CDynamicStructFactory::ms_bPrintBuffers = true;
bool CDynamicStructFactory::ms_bUseNativePCPrint = false;

void CDynamicStructFactory::SetConfigParams(const char* strVarStringSep, const char* strVarStringTerm, const char* strTypeStringSep, 
											bool bPrintPCBuffers, bool pUseNativePCPrint)
{
	m_sep = strVarStringSep;
	m_ter = strVarStringTerm;
	m_ide = strTypeStringSep;
	ms_bPrintBuffers = bPrintPCBuffers;
	ms_bUseNativePCPrint = pUseNativePCPrint;
}

bool CDynamicStructFactory::FreeResource(CDynamicStruct* pObjectToFree)
{
	if(pObjectToFree == 0)
	{
		TRACEPF_ERROR("", "CDynamicStructFactory::FreeResource trying to free null object!");
		return false;
	}
	pObjectToFree->Clean();
	return TFactory<CDynamicStruct>::FreeResource(pObjectToFree);
}

bool CDynamicStructFactory::FreeResource(CDynamicStruct* pObjectToFree, const char* file, int line, const char* function)
{
	if(pObjectToFree == 0)
	{
		TRACEPF_ERROR("", "CDynamicStructFactory::FreeResource trying to free null object!");
		return false;
	}
	pObjectToFree->Clean();
	return TFactory<CDynamicStruct>::FreeResource(pObjectToFree);
}

CDynamicStruct* CDynamicStructFactory::GetResource()
{
	CDynamicStruct* tpm =  TFactory<CDynamicStruct>::GetResource();
	return tpm;
}

CDynamicStruct* CDynamicStructFactory::GetResource(const char* file, int line, const char* function)
{
	CDynamicStruct* tpm =  TFactory<CDynamicStruct>::GetResource();
	return tpm;
}

void CDynamicStruct::writeObject(CFwkBuffer& out, int nSleepTime) {
    try {
        ParamsCodecGenericBuffer* pencBuffer = new ParamsCodecGenericBuffer;
        m_pParamsCodec->writeObject(*pencBuffer);
        out.SetMaxSize(pencBuffer->size);
        out.SetUsedSize(pencBuffer->size);
        memcpy(out.GetBuffer(), pencBuffer->buffer, pencBuffer->size);
        delete pencBuffer;
        //#define		TRACE_FREE
        // 0x40000000
    } catch (sixlabs::Exception& e) {
        TRACEPF_ERROR("", "/*************** sixlabs::Exception writeObject %s",
                      e.errMsg());
    }
}

void CDynamicStruct::readObject(const CFwkBuffer& in, int nSleepTime) {
    try {
        ParamsCodecGenericBuffer encBuffer;
        encBuffer.setBuffer(in.GetBuffer(), in.GetUsedSize());
        m_pParamsCodec->readObject(encBuffer);
    } catch (sixlabs::Exception& e) {
        TRACEPF_ERROR("", "/*************** sixlabs::Exception readObject %s ",
                      e.errMsg());
    }
}

void CDynamicStruct::writeText(CFwkString& out) {
    if (0 != GetParamsCodec()) {
        ParamsCodecGenericBuffer outputBuffer;
        GetParamsCodec()->writeObjectBAS(outputBuffer);
        out.FromBuffer(outputBuffer.buffer, outputBuffer.size);
    }
}

//end file
