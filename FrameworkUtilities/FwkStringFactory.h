#pragma once

#include "FwkString.h"

class CFwkStringFactory:
	public TFactory<CFwkString>,
	public TSingleton<CFwkStringFactory>
{
};
