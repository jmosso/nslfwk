#pragma once

#include "CommonHeaders/six_fwk_definitions.h"
#include <stdio.h>
#include <string.h>

#define J_MAX_INDEX 0xffffffff

class CFwkString
{
public:
	CFwkString(void);

	//************************************
	// Method:    CFwkString
	// FullName:  CFwkString::CFwkString
	// Access:    public 
	// Description: Constructor that act like sprintf
	// Parameter: int type				Just for differentiation with other constructors.  = 0
	// Parameter: const char * strFormat
	// Parameter: ...
	//************************************
	CFwkString(int type,const char * strFormat, ...);
	CFwkString(size_t iMaxSize);
	CFwkString(const char * cstr, size_t len);
	CFwkString(void* pBuffer, size_t len);
	CFwkString(char c);
	CFwkString(const char* str);
	inline CFwkString(const CFwkString& rhs)
	{
		m_pCharBuffer = 0;
		m_iMaxSize = 0;
		CopyContents(rhs);
	}
	~CFwkString(void);


	//************************************
	// Method:    GetMaxSize
	// FullName:  CFwkString::GetMaxSize
	// Access:    public const 
	// Returns:   size_t
	// Qualifier: const
	// Description: Get the max size of characters that string can contain
	//************************************
	inline size_t GetMaxSize()	const
	{ 
		return m_iMaxSize; 
	}

	//************************************
	// Method:    MakeMinimumSize
	// FullName:  CFwkString::MakeMinimumSize
	// Access:    public 
	// Returns:   bool
	// Description: Make this string a minimum size for character contained
	//************************************
	inline bool MakeMinimumSize()
	{ 
		return SetMaxSize(GetLength());
	}

	bool ForceMinimumSize();

	//************************************
	// Method:    GetLength
	// FullName:  CFwkString::GetLength
	// Access:    public const 
	// Returns:   size_t
	// Qualifier: const
	//************************************
	inline size_t GetLength() const
	{
		if(m_pCharBuffer)
			return strlen(m_pCharBuffer);
		else
			return 0;
	}

	//************************************
	// Method:    SetMaxSize
	// FullName:  CFwkString::SetMaxSize
	// Access:    public 
	// Returns:   bool
	// Qualifier:
	// Parameter: size_t iMaxSize
	// Description: Set a max size of characters
	//************************************
	bool SetMaxSize(size_t iMaxSize);


	//************************************
	// Method:    GetUsedSize		Set the number of characters being used
	// FullName:  CFwkString::GetUsedSize
	// Access:    public const 
	// Returns:   size_t
	// Qualifier: const
	//************************************
	size_t GetUsedSize() const
	{
		return GetLength();
	}


	//************************************
	// Method:    GetBuffer   Get the pointer to the buffer
	// FullName:  CFwkString::GetBuffer
	// Access:    public const 
	// Returns:   const char*
	// Qualifier: const
	//************************************
	inline const char* GetBuffer() const
	{
		return m_pCharBuffer; 
	}


	//************************************
	// Method:    GetPointer		Get a pointer with a size
	// FullName:  CFwkString::GetPointer
	// Access:    public 
	// Returns:   char*
	// Qualifier:
	// Parameter: int size
	//************************************
	inline char* GetPointer(size_t size)
	{
		//SIX_ASSERT(size>0);
		if(size > 0)
			SetMaxSize(size);
		return m_pCharBuffer;
	}

	//************************************
	// Method:    IsEmpty				Ask the object if it is empty.
	// FullName:  CFwkString::IsEmpty
	// Access:    public const 
	// Returns:   bool		True if is empty.  False if contain characters.
	// Qualifier: const
	//************************************
	bool IsEmpty() const;

	//************************************
	// Method:    operator[]			Return the pointer character on iIndex position
	// FullName:  CFwkString::operator[]
	// Access:    public 
	// Returns:   char&
	// Qualifier:
	// Parameter: int iIndex
	//************************************
	char& operator [](size_t iIndex );

	//************************************
	// Method:    SetAt
	// FullName:  CFwkString::SetAt
	// Access:    public 
	// Returns:   void
	// Qualifier:
	// Parameter: int iIndex
	// Parameter: char ch
	//************************************

	inline void SetAt(size_t iIndex, char ch)
	{
		if((iIndex < 0) || (iIndex >= m_iMaxSize))
			return;

		if(m_pCharBuffer == 0)
			return;

		m_pCharBuffer[iIndex] = ch;
	}

	//************************************
	// Method:    Clear				Clear the string with NULL character.
	// FullName:  CFwkString::Clear
	// Access:    public 
	// Returns:   void
	// Qualifier:
	//************************************
	void Clear();

	//************************************
	// Method:    Find				Find a first character on the string
	// FullName:  CFwkString::Find
	// Access:    public const 
	// Returns:   int				Position where the function find the character
	// Qualifier: const
	// Parameter: char ch
	// Parameter: int iStart 
	//************************************
	size_t Find(char ch, size_t iStart = 0)const;
	size_t Find(const char* str2Find, size_t iStart = 0)const;


	//************************************
	// Method:    ReverseFind				Find a first character from the end on the string.
	// FullName:  CFwkString::ReverseFind
	// Access:    public const 
	// Returns:   int						Position where the function find the character.
	// Qualifier: const
	// Parameter: char ch
	//************************************
	size_t ReverseFind(char ch)const;


	//************************************
	// Method:    GetToken					Return a string that is between tokens.  Este método tiene un error, favor no usarlo
	// FullName:  CFwkString::GetToken
	// Access:    public const 
	// Returns:   bool
	// Qualifier: const
	// Parameter: int& iPosition
	// Parameter: const char* strTerminator
	// Parameter: CFwkString& fstrToken
	//************************************
	bool GetToken(size_t& iPosition, const char* strTerminator, CFwkString& fstrToken) const;

	//************************************
	// Method:    GetToken, obtains tokens taking into account the strTerminato length
	// FullName:  CFwkString::GetToken
	// Access:    public const 
	// Returns:   bool
	// Qualifier: const
	// Parameter: int& iPosition
	// Parameter: const char* strTerminator
	// Parameter: CFwkString& fstrToken
	//************************************
	bool GetTokenEx(size_t& iPosition, const char* strTerminator, CFwkString& fstrToken) const;

	void FromBuffer(void* buffer, size_t iSize);


	bool CheckIsNumeric() const;

	bool GetStringInBtw(CFwkString& fstrTarget, const char* strChStart, const char* strChEnd);

	//************************************
	// Method:    operator const char*			Return the character pointer
	// FullName:  CFwkString::operator=
	// Access:    public 
	// Returns:   CFwkString &
	// Qualifier:
	// Parameter: const char* rhs
	//************************************
	operator const char*( ) const
	{
		return m_pCharBuffer;
	}

	//************************************
	// Method:    operator=					Get the string equal to parameter
	// FullName:  CFwkString::operator=
	// Access:    public 
	// Returns:   CFwkString &
	// Qualifier:
	// Parameter: const char* rhs
	//************************************
	inline CFwkString & operator = (const char* rhs)
	{
		CopyContents(rhs);
		return *this;
	}
	inline CFwkString & operator = (const CFwkString& rhs)
	{
		CopyContents((const char*)rhs.m_pCharBuffer);
		return *this;
	}
	inline CFwkString & operator = (const char ch)
	{
		CopyContents(CFwkString(ch));
		return *this;
	}

	//************************************
	// Method:    operator+=				Add string parameter to this string
	// FullName:  CFwkString::operator+=
	// Access:    public 
	// Returns:   CFwkString &
	// Qualifier:
	// Parameter: const CFwkString& rhs
	//************************************
	inline CFwkString &operator += (const CFwkString& rhs)
	{
		return operator+=((const char*)rhs);
	};
	CFwkString & operator += (const char* rhs);
	CFwkString & operator += (const char ch);

	//************************************
	// Method:    operator+					Add this string to parameter string and return a new String
	// FullName:  CFwkString::operator+
	// Access:    public const 
	// Returns:   CFwkString
	// Qualifier: const
	// Parameter: const CFwkString & str
	//************************************
	inline CFwkString operator+(const CFwkString & str) const
	{
		return operator+((const char *)str);
	}
	CFwkString operator+(const char * rhs) const;
	CFwkString operator+(char ch) const;

	//************************************
	// Method:    operator!					Verify if the string is empty
	// FullName:  CFwkString::operator!
	// Access:    public const 
	// Returns:   bool
	// Qualifier: const
	//************************************
	inline bool operator!() const
	{ return !IsEmpty(); }


	//************************************
	// Method:    operator==				Return if this string is equal to parameter string
	// FullName:  CFwkString::operator==
	// Access:    public const 
	// Returns:   bool
	// Qualifier: const
	// Parameter: const char * cstr
	//************************************
	inline bool operator==(const char * cstr) const
	{
		return InternalCompare(0, J_MAX_INDEX, cstr) == 0;
	}
	inline bool operator == (const CFwkString& rhs) const
	{
		return operator == ((const char * )rhs);
	}
/*
    class CFwkStringCompare
    {

    public:
        size_t operator()( const CFwkString& v ) const;
        bool operator()( const CFwkString name1, const CFwkString name2 ) const;
    };
*/
	//************************************
	// Method:    operator!=				Return if this string is different to parameter string
	// FullName:  CFwkString::operator!=
	// Access:    public const 
	// Returns:   bool
	// Qualifier: const
	// Parameter: const char * cstr
	//************************************
	inline bool operator!=(const char * cstr) const
	{ return InternalCompare(0, J_MAX_INDEX, cstr) != 0; }

	inline bool operator != (const CFwkString& rhs) const
	{return operator != ((const char * )rhs);}

	inline bool operator<(const char * cstr) const
	{ return InternalCompare(0, J_MAX_INDEX, cstr) == -1; }
	inline bool operator < (const CFwkString& rhs) const
	{return operator < ((const char*)rhs);}	

	inline bool operator>(const char * cstr) const
	{ return InternalCompare(0, J_MAX_INDEX, cstr) == 1; }
	inline bool operator > (const CFwkString& rhs) const
	{return operator > ((const char*)rhs);}

	inline bool operator<=(const char * cstr) const
	{ return InternalCompare(0, J_MAX_INDEX, cstr) != 1; }
	inline bool operator <= (const CFwkString& rhs) const
	{return operator <= ((const char*)rhs);}

	inline bool operator>=(const char * cstr) const
	{ return InternalCompare(0, J_MAX_INDEX, cstr) != -1; }
	inline bool operator >= (const CFwkString& rhs) const
	{return operator >= ((const char*)rhs);}



	/**Concatenate two strings to produce a third. The original strings are
	not modified, an entirely new unique reference to a string is created.

	@return
	new string with concatenation of the object and parameter.
	*/
	inline CFwkString operator&(const CFwkString & str) const
	{ return operator&(str); }

	/**Concatenate a C string to a CFwkString to produce a third. The original
	string is not modified, an entirely new unique reference to a string
	is created. The #cstr# parameter is typically a literal
	string, eg:
	\begin{verbatim}
	myStr = aStr & "fred";
	\end{verbatim}

	This function differs from operator+ in that it assures there is at
	least one space between the strings. Exactly one space is added if
	there is not a space at the end of the first or beggining of the last
	string.

	@return
	new string with concatenation of the object and parameter.
	*/
	CFwkString operator&(const char * cstr ) const;
	/**Concatenate a single character to a CFwkString to produce a third. The
	original string is not modified, an entirely new unique reference to a
	string is created. The #ch# parameter is typically a
	literal, eg:
	\begin{verbatim}
	myStr = aStr & '!';
	\end{verbatim}

	This function differes from operator+ in that it assures there is at
	least one space between the strings. Exactly one space is added if
	there is not a space at the end of the first or beggining of the last
	string.

	@return
	new string with concatenation of the object and parameter.
	*/
	CFwkString operator&(
		char ch   /// Character to concatenate.
		) const;

	/**Concatenate a CFwkString to a C string to produce a third. The original
	string is not modified, an entirely new unique reference to a string
	is created. The #cstr# parameter is typically a literal
	string, eg:
	\begin{verbatim}
	myStr = "fred" & aStr;
	\end{verbatim}

	This function differes from operator+ in that it assures there is at
	least one space between the strings. Exactly one space is added if
	there is not a space at the end of the first or beggining of the last
	string.

	@return
	new string with concatenation of the object and parameter.
	*/
	friend inline CFwkString operator&(const char * cstr, const CFwkString & str)
	{ return CFwkString(cstr) & str; }

	/**Concatenate a CFwkString to a single character to produce a third. The
	original string is not modified, an entirely new unique reference to a
	string is created. The #c# parameter is typically a literal,
	eg:
	\begin{verbatim}
	myStr = '!' & aStr;
	\end{verbatim}

	This function differes from #operator+# in that it assures there is at
	least one space between the strings. Exactly one space is added if
	there is not a space at the end of the first or beggining of the last
	string.

	@return
	new string with concatenation of the object and parameter.
	*/
	friend inline CFwkString operator&(char c, const CFwkString & str)
	{ return CFwkString(c) & str; }


	/**Concatenate a string to another string, modifiying that string.

	@return
	reference to string that was concatenated to.
	*/
	inline CFwkString & operator&=(const CFwkString & str)
	{ return operator&=(str); }


	/**Concatenate a C string to a CFwkString, modifiying that string. The
	#cstr# parameter is typically a literal string, eg:
	\begin{verbatim}
	myStr &= "fred";
	\end{verbatim}

	This function differes from operator+ in that it assures there is at
	least one space between the strings. Exactly one space is added if
	there is not a space at the end of the first or beggining of the last
	string.

	@return
	reference to string that was concatenated to.
	*/
	CFwkString & operator&=(
		const char * cstr  /// C string to concatenate.
		);


	/**Concatenate a character to a CFwkString, modifiying that string. The
	#ch# parameter is typically a literal string, eg:
	\begin{verbatim}
	myStr &= '!';
	\end{verbatim}

	This function differes from operator+ in that it assures there is at
	least one space between the strings. Exactly one space is added if
	there is not a space at the end of the first or beggining of the last
	string.

	@return
	reference to string that was concatenated to.
	*/
	CFwkString & operator&=(
		char ch  /// Character to concatenate.
		);
	//@}


	/**@name Comparison operators */
	//@{
	/**Compare two strings using case insensitive comparison.

	@return
	TRUE if equal.
	*/
	inline bool operator*=(const CFwkString & str) const
	{ return operator*=((const char *)str); }


	/**Compare a CFwkString to a C string using a case insensitive compare
	function. The #cstr# parameter is typically a literal string,
	eg:
	\begin{verbatim}
	if (myStr == "fred")
	\end{verbatim}

	@return
	TRUE if equal.
	*/
	bool operator*=(const char * cstr ) const;


	/**Create a string consisting of all characters from the source string
	with all upper case letters converted to lower case. The original
	string is not changed and a new unique reference to a string is
	returned.

	@return
	string with upper case converted to lower case.
	*/
	CFwkString ToLower() const;
	void ToLowerThis() const;

	/**Create a string consisting of all characters from the source string
	with all lower case letters converted to upper case. The original
	string is not changed and a new unique reference to a string is
	returned.

	@return
	string with lower case converted to upper case.
	*/
	CFwkString ToUpper() const;
	void ToUpperThis() const;
	/**Extract a portion of the string into a new string. The original string
	is not changed and a new unique reference to a string is returned.

	A substring from the beginning of the string for the number of
	characters specified is extracted.

	If #len# is greater than the length of the string then all
	characters to the end of the string are returned.

	If #len# is zero then an empty string is returned.

	@return
	substring of the source string.
	*/
	CFwkString Left(
		size_t len   /// Number of characters to extract.
		) const;

	/**Extract a portion of the string into a new string. The original string
	is not changed and a new unique reference to a string is returned.

	A substring from the end of the string for the number of characters
	specified is extracted.

	If #len# is greater than the length of the string then all
	characters to the beginning of the string are returned.

	If #len# is zero then an empty string is returned.

	@return
	substring of the source string.
	*/
	CFwkString Right(
		size_t len   /// Number of characters to extract.
		) const;

	/**Extract a portion of the string into a new string. The original string
	is not changed and a new unique reference to a string is returned.

	A substring from the #start# position for the number of
	characters specified is extracted.

	If #len# is greater than the length of the string from the
	#start# position then all characters to the end of the
	string are returned.

	If #start# is greater than the length of the string or
	#len# is zero then an empty string is returned.

	@return
	substring of the source string.
	*/
	CFwkString Mid(
		size_t start,             /// Starting position of the substring.
		size_t len   /// Number of characters to extract.
		) const;

	/**Extract a portion of the string into a new string. The original string
	is not changed and a new unique reference to a string is returned.

	The substring is returned inclusive of the characters at the
	#start# and #end# positions.

	If the #end# position is greater than the length of the
	string then all characters from the #start# up to the end of
	the string are returned.

	If #start# is greater than the length of the string or
	#end# is before #start# then an empty string is
	returned.

	@return
	substring of the source string.
	*/
	CFwkString operator()(
		size_t start,  /// Starting position of the substring.
		size_t end     /// Ending position of the substring.
		) const;


	/**Convert the string to an integer value using the specified number base.
	All characters up to the first illegal character for the number base are
	converted. Case is not significant for bases greater than 10.

	The number base may only be from 2 to 36 and the function will assert
	if it is not in this range.

	This function uses the standard C library #strtol()# function.

	@return
	integer value for the string.
	*/
	SIX_FWK_INT64 AsInteger(
		unsigned base = 10    /// Number base to convert the string in.
		) const;


	double AsReal() const;

	/**put a formatted output to the string. This is identical to the
	standard C library #sprintf()# function, but appends its
	output to the string.

	This function makes the assumption that there is less the 1000
	characters of formatted output. The function will assert if this occurs.

	Note that this function will break the current instance from multiple
	references to the string. A new string buffer is allocated and the data
	from the old string buffer copied to it.

	@return
	reference to the current string object.
	*/
	CFwkString & sprintf(
		const char * cfmt,   /// C string for output format.
		...                  /// Extra parameters for #sprintf()# call.
		);

	/**Concatenate a formatted output to the string. This is identical to the
	standard C library #sprintf()# function, but appends its
	output to the string.

	This function makes the assumption that there is less the 1000
	characters of formatted output. The function will assert if this occurs.

	Note that this function will break the current instance from multiple
	references to the string. A new string buffer is allocated and the data
	from the old string buffer copied to it.

	@return
	reference to the current string object.
	*/
	CFwkString & sprintfc(
		const char * cfmt,   /// C string for output format.
		...                  /// Extra parameters for #sprintf()# call.
		);

	/** Concatenate a formatted output to the string. */
	CFwkString & vsprintf(
		const CFwkString & fmt, /// String for output format.
		va_list args         /// Extra parameters for #sprintf()# call.

		);
	/**Concatenate a formatted output to the string. This is identical to the
	standard C library #vsprintf()# function, but appends its
	output to the string.

	This function makes the assumption that there is less the 1000
	characters of formatted output. The function will assert if this occurs.

	Note that this function will break the current instance from multiple
	references to the string. A new string buffer is allocated and the data
	from the old string buffer copied to it.

	@return
	reference to the current string object.
	*/
	CFwkString & vsprintf(
		const char * cfmt,   /// C string for output format.
		va_list args,         /// Extra parameters for #sprintf()# call.
		bool concatenate = false
		);

	CFwkString & vsprintfNF(
		const char * cfmt,   /// C string for output format.
		va_list args,         /// Extra parameters for #sprintf()# call.
		bool concatenate = false
		);

	/** Produce formatted output as a string. */
	friend CFwkString jpvsprintf(
		const char * cfmt,   /// C string for output format.
		va_list args         /// Extra parameters for #sprintf()# call.
		);
	/**Produce formatted output as a string. This is identical to the standard
	C library #vsprintf()# function, but sends its output to a
	#CFwkString#.

	This function makes the assumption that there is less the 1000
	characters of formatted output. The function will assert if this occurs.

	Note that this function will break the current instance from multiple
	references to the string. A new string buffer is allocated and the data
	from the old string buffer copied to it.

	@return
	reference to the current string object.
	*/
	friend CFwkString jpvsprintf(
		const CFwkString & fmt, /// String for output format.
		va_list args         /// Extra parameters for #sprintf()# call.
		);


	/**Produce formatted output as a string. This is identical to the standard
	C library #sprintf()# function, but sends its output to a
	#CFwkString#.

	This function makes the assumption that there is less the 1000
	characters of formatted output. The function will assert if this occurs.

	Note that this function will break the current instance from multiple
	references to the string. A new string buffer is allocated and the data
	from the old string buffer copied to it.

	@return
	reference to the current string object.
	*/
	friend CFwkString jpsprintf(
		const char * cfmt,   /// C string for output format.
		...                  /// Extra parameters for #sprintf()# call.
		);



	/** Locate the position of the last matching character. */
	size_t FindLast(
		char ch,                     /// Character to search for in string.
		size_t offset = J_MAX_INDEX  /// Offset into string to begin search.
		) const;

	/** Locate the position of the last matching substring. */
	inline size_t FindLast(const CFwkString & str, size_t offset= J_MAX_INDEX) const
	{ 
		return FindLast((const char *)str, offset);
	}

	/**Locate the position of the last matching substring.
	Locate the position within the string of the last matching character or
	substring. The search will begin at the character offset provided,
	moving backward through the string.

	If #offset# is beyond the length of the string, then the
	search begins at the end of the string. If #offset# is zero
	then the function always returns #J_MAX_INDEX#.

	The matching will be for identical character or string. If a search
	ignoring case is required then the string should be converted to a
	#PCaselessString# before the search is made.

	@return
	position of character or substring in the string, or J_MAX_INDEX if the
	character or substring is not in the string.
	*/
	size_t FindLast(const char * cstr,		/// C string to search for in string.
		size_t offset = J_MAX_INDEX		/// Offset into string to begin search.
		) const;

	/** Locate the position of one of the characters in the set. */
	inline size_t Find(const CFwkString & str, size_t offset) const
	{ 
		return Find((const char *)str, offset);
	}

	/**Locate the position of one of the characters in the set.
	The search will begin at the character offset provided.

	If #offset# is beyond the length of the string, then the
	function will always return #J_MAX_INDEX#.

	The matching will be for identical character or string. If a search
	ignoring case is required then the string should be converted to a
	#PCaselessString# before the search is made.

	@return
	position of character in the string, or J_MAX_INDEX if no characters
	from the set are in the string.
	*/
	inline size_t FindOneOf(const CFwkString & str, size_t offset) const
	{ 
		return FindOneOf((const char *)str, offset);
	}

	/**Locate the position of one of the characters in the set.
	The search will begin at the character offset provided.

	If #offset# is beyond the length of the string, then the
	function will always return #J_MAX_INDEX#.

	The matching will be for identical character or string. If a search
	ignoring case is required then the string should be converted to a
	#PCaselessString# before the search is made.

	@return
	position of character in the string, or J_MAX_INDEX if no characters
	from the set are in the string.
	*/
	size_t FindOneOf(
		const char * cset,    /// C string of characters to search for in string.
		size_t offset = 0     /// Offset into string to begin search.
		) const;



	/**Locate the substring within the string and replace it with the specifed
	substring. The search will begin at the character offset provided.

	If #offset# is beyond the length of the string, then the
	function will do nothing.

	The matching will be for identical character or string. If a search
	ignoring case is required then the string should be converted to a
	#PCaselessString# before the search is made.
	*/
	void Replace(
		const CFwkString & target,   /// String to be replaced in string.
		const CFwkString & subs,     /// String to do replace in string.
		bool all = false,         /// Replace all occurrences of string.
		size_t offset = 0         /// Offset into string to begin search.
		);

	/**Splice the string into the current string at the specified position. The
	specified number of bytes are removed from the string.

	Note that this function will break the current instance from multiple
	references to the string. A new string buffer is allocated and the data
	from the old string buffer copied to it.
	*/
	inline void Splice(const CFwkString & str, size_t pos, size_t len)
	{ 
		Splice((const char *)str, pos, len);
	}


	/**Splice the string into the current string at the specified position. The
	specified number of bytes are removed from the string.

	Note that this function will break the current instance from multiple
	references to the string. A new string buffer is allocated and the data
	from the old string buffer copied to it.
	*/
	void Splice(
		const char * cstr,    /// Substring to insert.
		size_t pos,           /// Position in string to insert the substring.
		size_t len = 0        /// Length of section to remove.
		);

	bool Scanf(
		int numArgs, 
		const char * format, 
		... 
		);

	// get all characters before the first occurence of ch
	// (returns the whole string if ch not found)
	CFwkString BeforeFirst(char ch) const
	{
		size_t iPos = Find(ch);
		if ( iPos == J_MAX_INDEX )
			iPos = GetLength();
		return Left(iPos);
	}
	/// get all characters before the last occurence of ch
	/// (returns empty string if ch not found)
	CFwkString BeforeLast(char ch) const
	{
		CFwkString str;
		size_t iPos = FindLast(ch);
		if ( iPos != J_MAX_INDEX && iPos != 0 )
			str = Left(iPos);

		return str;
	}

	/// get all characters after the first occurence of ch
	/// (returns empty string if ch not found)
	CFwkString AfterFirst(char ch) const
	{
		CFwkString str;
		size_t iPos = Find(ch);
		if ( iPos != J_MAX_INDEX )
			str = GetBuffer() + iPos + 1;

		return str;
	}

	// get all characters after the last occurence of ch
	// (returns the whole string if ch not found)
	CFwkString AfterLast(char ch) const
	{
		CFwkString str;
		size_t iPos = FindLast(ch);
		if ( iPos == J_MAX_INDEX )
			str = *this;
		else
			str = GetBuffer() + iPos + 1;

		return str;
	}

	// string concatenation
	// in place concatenation
	/*
	Concatenate and return the result. Note that the left to right
	associativity of << allows to write things like "str << str1 << str2
	<< ..." (unlike with +=)
	*/
	// string += string
	CFwkString& operator<<(const CFwkString& s)
	{
		(*this) += s;
		return *this;
	}
	// string += C string
	CFwkString& operator<<(const char *psz)
	{
		(*this) += psz;
		return *this;
	}
	// string += char
	CFwkString& operator<<(char ch) { 
		(*this) += ch; 
		return *this; 
	}

	// stream-like functions
	// insert an int into string
	CFwkString& operator<<(int i)
	{ sprintfc("%d",i);
	return (*this); }
	// insert an unsigned int into string
	CFwkString& operator<<(unsigned int ui)
	{ sprintfc("%u",ui);
	return (*this); }
	// insert a long into string
	CFwkString& operator<<(long l)
	{ sprintfc("%ld",l);
	return (*this); }
	// insert an unsigned long into string
	CFwkString& operator<<(unsigned long ul)
	{ sprintfc("%lu",ul);
	return (*this); }
	// insert a float into string
	CFwkString& operator<<(float f)
	{ sprintfc("%f",f);
	return (*this); }
	// insert a double into string
	CFwkString& operator<<(double d)
	{ sprintfc("%g",d);
	return (*this); }

	short Format2Data(size_t uSize, size_t uAlign, char cFill);
	bool CopyContents(const char * rhs, size_t uSize);
	short FillFormat(const char *, size_t uSize, size_t uAlign, char cFill);

protected: 

	virtual int InternalCompare(
		size_t offset,      // Offset into string to compare.
		char c              // Character to compare against.
		) const;
	virtual int InternalCompare(
		size_t offset,      // Offset into string to compare.
		size_t length,      // Number of characters to compare.
		const char * cstr   // C string to compare against.
		) const;

	int sscanfWrapper(
		int numArgs, 
		const char * format, 
		void **p 
		);
	void CopyContents(const CFwkString & cont);
	void CopyContents(const char * cont);

	char *m_pCharBuffer;
	size_t m_iMaxSize;

private:
	//ni idea

};

//end file


