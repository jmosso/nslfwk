
#include "FwkBuffer.h"
#include "FwkString.h"

unsigned char CFwkBuffer::chMasks1[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };
unsigned char CFwkBuffer::chMasks0[8] = {0xfe, 0xfd, 0xfb, 0xf7, 0xef, 0xdf, 0xbf, 0x7f };

int CFwkBuffer::ms_iCounter = 0;

CFwkBuffer::CFwkBuffer(void)
{
	m_pCharBuffer = 0;
	m_iMaxSize = 0;
	m_iUsedSize = 0;
	m_iSeqID = ++ms_iCounter;
}

CFwkBuffer::~CFwkBuffer(void)
{
	if(m_pCharBuffer)
	{
		delete [] m_pCharBuffer;
		m_pCharBuffer = 0;
	}
}

CFwkBuffer::CFwkBuffer(const char * pMem, int size) 
{
	m_pCharBuffer = 0;
	m_iMaxSize = 0;
	m_iUsedSize = 0;
	SetMaxSize (size);
	memcpy(GetBuffer(), pMem, GetUsedSize());
}

short CFwkBuffer::SetMaxSize (size_t iMaxSize)
{
	if(m_iMaxSize >= iMaxSize)
	{
		m_iUsedSize = iMaxSize;
		memset(m_pCharBuffer, 0, m_iMaxSize);		
	}
	else
	{
		m_iMaxSize = iMaxSize;

		if(m_pCharBuffer)
			delete [] m_pCharBuffer;

		m_pCharBuffer = new char[m_iMaxSize];
		memset(m_pCharBuffer, 0, m_iMaxSize);
		m_iUsedSize = m_iMaxSize;
	}

	return 0;
}

CFwkBuffer & CFwkBuffer::operator += (const CFwkBuffer & buffer)
{
	if(buffer.GetUsedSize() == 0)
		return *this;

	if(m_iUsedSize + buffer.GetUsedSize() > m_iMaxSize)
	{
		m_iMaxSize = m_iUsedSize + buffer.GetUsedSize();
		char * pTempBuffer = new char[m_iMaxSize];
		memcpy(pTempBuffer, m_pCharBuffer, m_iUsedSize);
		memcpy(pTempBuffer + m_iUsedSize, buffer.GetBuffer(), buffer.GetUsedSize());

		if(m_pCharBuffer)
			delete [] m_pCharBuffer;

		m_pCharBuffer = pTempBuffer;
		m_iUsedSize += buffer.GetUsedSize();
	}
	else
	{
		memcpy(m_pCharBuffer + m_iUsedSize, buffer.GetBuffer(), buffer.GetUsedSize());
		m_iUsedSize += buffer.GetUsedSize();
	}

	return *this;
}

CFwkBuffer & CFwkBuffer::operator = (const CFwkBuffer & buffer)
{
	if(m_iMaxSize < buffer.GetUsedSize())
	{
		m_iMaxSize = buffer.GetUsedSize();
		char * pTempBuffer = new char[m_iMaxSize];
		memcpy(pTempBuffer, buffer.GetBuffer(), buffer.GetUsedSize());

		if(m_pCharBuffer)
			delete [] m_pCharBuffer;
		m_iUsedSize = buffer.GetUsedSize();
		m_pCharBuffer = pTempBuffer;
	}
	else
	{
		if(m_pCharBuffer && buffer.GetBuffer())
		{
			memcpy(m_pCharBuffer, buffer.GetBuffer(), buffer.GetUsedSize());
		}
		m_iUsedSize = buffer.GetUsedSize();
	}
	return *this;
}

void CFwkBuffer::Clear()
{
	if(!m_pCharBuffer)
		return;

	memset(m_pCharBuffer, 0, m_iUsedSize);
	m_iUsedSize = 0;
}

char & CFwkBuffer::operator [] (size_t iIndex)
{
	if(iIndex > m_iMaxSize)
		// esto esta mal
		return m_pCharBuffer[0];

	return m_pCharBuffer[iIndex];
}
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
bool CFwkBuffer::operator==(const CFwkBuffer& rBuff)
{
	size_t iCont = 0;

	if (m_iUsedSize != rBuff.GetUsedSize())
		return false;

	while(iCont<m_iUsedSize )
	{
		if ((m_pCharBuffer[iCont]^rBuff.GetBuffer()[iCont]))
			return false;

		iCont++;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
CFwkBuffer & CFwkBuffer::operator = (const char buffer[])
{
	size_t bufSize = sizeof(buffer);

	if(m_iMaxSize < bufSize)
	{
		m_iMaxSize = bufSize;
		char * pTempBuffer = new char[bufSize];
		memcpy(pTempBuffer, buffer, bufSize);

		if(m_pCharBuffer)
			delete [] m_pCharBuffer;
		m_iUsedSize = bufSize;
		m_pCharBuffer = pTempBuffer;
	}
	else
	{
		if(m_pCharBuffer && bufSize)
		{
			memcpy(m_pCharBuffer, buffer, bufSize);
		}
		m_iUsedSize = bufSize;
	}
	return *this;
}
//////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
CFwkBuffer& CFwkBuffer::AddInt(int iIn)
{
	char cTmp = (char)iIn;
	int iSize = 1; // SOLO SE PEUDE COPIAR EL PRIMER BYTE DEL ENTERO
	if(iSize == 0 || iSize > 4)
		return *this;

	if(m_iUsedSize + iSize > m_iMaxSize)
	{
		m_iMaxSize = m_iUsedSize + iSize;
		char * pTempBuffer = new char[m_iMaxSize];
		memcpy(pTempBuffer, m_pCharBuffer, m_iUsedSize);
		memcpy(pTempBuffer + m_iUsedSize, &cTmp, iSize);

		if(m_pCharBuffer)
			delete [] m_pCharBuffer;

		m_pCharBuffer = pTempBuffer;
		m_iUsedSize += iSize;
	}
	else
	{
		memcpy(m_pCharBuffer + m_iUsedSize, &cTmp, iSize);
		m_iUsedSize += iSize;
	}

	return *this;
}
//////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
CFwkBuffer& CFwkBuffer::AddMen(const char *refBuffer, int iSize)
{
	if(iSize == 0)
		return *this;

	if(m_iUsedSize + iSize > m_iMaxSize)
	{
		m_iMaxSize = m_iUsedSize + iSize;
		char * pTempBuffer = new char[m_iMaxSize];
		memcpy(pTempBuffer, m_pCharBuffer, m_iUsedSize);
		memcpy(pTempBuffer + m_iUsedSize, refBuffer, iSize);

		if(m_pCharBuffer)
			delete [] m_pCharBuffer;

		m_pCharBuffer = pTempBuffer;
		m_iUsedSize += iSize;
	}
	else
	{
		memcpy(m_pCharBuffer + m_iUsedSize, refBuffer, iSize);
		m_iUsedSize += iSize;
	}

	return *this;

}
//////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
bool CFwkBuffer::SplitBuffer(int iIndex, size_t iSize, CFwkBuffer& buffer, bool& bLast, size_t& iStartPosition)
{
	if(iSize < 1)
		return false;

	size_t iStartCpy = 0;
	size_t iPosUntilCpy = 0;
	iStartCpy = iIndex * iSize;

	if(iStartCpy > m_iUsedSize)
		return false;

	iPosUntilCpy = iStartCpy + iSize;

	bLast = false;
	if(iPosUntilCpy >= m_iUsedSize)
	{
		iSize = m_iUsedSize - iStartCpy;
		bLast = true;
	}

	buffer.SetMaxSize(iSize);
	iStartPosition = iStartCpy;
	memcpy(buffer.GetBuffer(), m_pCharBuffer + iStartCpy, iSize);

	return true;
}

bool CFwkBuffer::AddHeader(const char* strHeader)
{
	size_t iHeaderLen = strlen(strHeader);
	if(iHeaderLen == 0)
		return true;

	m_iMaxSize = m_iUsedSize + iHeaderLen;
	char * pTempBuffer = new char[m_iMaxSize];
	
	memcpy(pTempBuffer, strHeader, iHeaderLen);
	memcpy(pTempBuffer + iHeaderLen, m_pCharBuffer, m_iUsedSize);
	
	if(m_pCharBuffer)
		delete [] m_pCharBuffer;
	
	m_pCharBuffer = pTempBuffer;
	m_iUsedSize += m_iMaxSize;

	return true;
}


//////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
bool CFwkBuffer::CompBuff(char * chpBuff, int size)
{
	size_t iCont = 0;

	if (m_iUsedSize != size)
		return false;

	while(iCont<m_iUsedSize )
	{
		if ((m_pCharBuffer[iCont]^chpBuff[iCont]))
			return false;

		iCont++;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
short CFwkBuffer::ReSize (size_t iMaxSize)
{
	char * tmpBuff = m_pCharBuffer;

	if(m_iMaxSize >= iMaxSize)
	{
		return 0;
	}

	else
	{
		m_iMaxSize = iMaxSize;
		m_pCharBuffer = new char[m_iMaxSize];
		memset(m_pCharBuffer, 0, m_iMaxSize);

		if(tmpBuff)
		{
			memcpy(m_pCharBuffer, tmpBuff, m_iUsedSize);
			delete tmpBuff;
		}
	}
	return 0;
}
//////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
int CFwkBuffer::SetBits(void * refInMen, int iBitMenSize, int iBitInitPos)
{
	size_t iTotalSize;
	int iInnerOctCount = (iBitInitPos/ 8);
	int iInnerBitCount  = (iBitInitPos % 8)-1;
	int iExtOctCount = 0;
	int iExtBitCount = 0;
	int iTotalBitCount = 0;
	int iSet;
	char * chpInMen = (char *) refInMen;


	//MIRAR TAMA� MEMORIA A VER SI SE ESTA AGREGANDO.
	if ((iBitInitPos +  iBitMenSize ) % 8)
		iTotalSize	 = ((iBitInitPos +  iBitMenSize)/8) + 1;
	else 
		iTotalSize	 = ((iBitInitPos +  iBitMenSize)/8);
	ReSize(iTotalSize);

	if(m_iUsedSize < iTotalSize)
	{
		m_iUsedSize = iTotalSize;
	}

	for (iTotalBitCount = 0; iTotalBitCount < iBitMenSize ; 1)
	{
		while (iInnerBitCount < 8  && iTotalBitCount < iBitMenSize)
		{
			if ( GetBitValue(chpInMen[iExtOctCount], iExtBitCount) )
				iSet = 1;
			else
				iSet = 0;

			SetBitValue(m_pCharBuffer[iInnerOctCount], iInnerBitCount, iSet);

			iInnerBitCount++;			
			iExtBitCount ++;
			if (iExtBitCount>= 8)
			{
				iExtBitCount= 0;
				iExtOctCount++;
			}
			iTotalBitCount ++;
		}
		iInnerBitCount = 0;			
		iInnerOctCount++;
//		iExtBitCount = 0;
	}
	return 0;
}
//////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
int CFwkBuffer::GetBits(void * refOutMen, int iMenSize, int iBitInitPos)
{
	return 0;

}
//////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
int CFwkBuffer::GetBitValue(char & octeto, int iBit)
{
	if (octeto & chMasks1[iBit])	
		return 1;
	return 0;
}
//////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
int CFwkBuffer::SetBitValue(char & octeto, int iBit, int value)
{
	if (value)
		octeto = octeto | chMasks1[iBit];
	else 
		octeto = octeto & chMasks0[iBit];
	return 0;
}
//////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

int CFwkBuffer::WriteHexDump(CFwkString & s)
{
	char c;
	//EIMG se requieren 3 chars por cada octeto almacenado (FF )
	s.SetMaxSize(3 * GetUsedSize());

	for(size_t i = 0; i < GetUsedSize(); i++)
	{
		c = m_pCharBuffer[i];

		switch((c & 0xF0) >> 4)
		{
		case 0x0:
			s += '0';
			break;

		case 0x1:
			s += '1';
			break;

		case 0x2:
			s += '2';
			break;

		case 0x3:
			s += '3';
			break;

		case 0x4:
			s += '4';
			break;

		case 0x5:
			s += '5';
			break;

		case 0x6:
			s += '6';
			break;

		case 0x7:
			s += '7';
			break;

		case 0x8:
			s += '8';
			break;

		case 0x9:
			s += '9';
			break;

		case 0xA:
			s += 'A';
			break;

		case 0xB:
			s += 'B';
			break;

		case 0xC:
			s += 'C';
			break;

		case 0xD:
			s += 'D';
			break;

		case 0xE:
			s += 'E';
			break;

		case 0xF:
			s += 'F';
			break;
		}

		switch(c & 0x0F)
		{
		case 0x0:
			s += '0';
			break;

		case 0x1:
			s += '1';
			break;

		case 0x2:
			s += '2';
			break;

		case 0x3:
			s += '3';
			break;

		case 0x4:
			s += '4';
			break;

		case 0x5:
			s += '5';
			break;

		case 0x6:
			s += '6';
			break;

		case 0x7:
			s += '7';
			break;

		case 0x8:
			s += '8';
			break;

		case 0x9:
			s += '9';
			break;

		case 0xA:
			s += 'A';
			break;

		case 0xB:
			s += 'B';
			break;

		case 0xC:
			s += 'C';
			break;

		case 0xD:
			s += 'D';
			break;

		case 0xE:
			s += 'E';
			break;

		case 0xF:
			s += 'F';
			break;
		}
	
		s += ' ';
	}

	return 0;
}

int CFwkBuffer::ReadHexDump(CFwkString & s)
{
	//EIMG asumo un dump formado como la salida de WriteHexDump, es decir, un octeto por cada 3 chars
	SetMaxSize((s.GetLength() / 3) + 1);
	bool bPair = false;
	m_iUsedSize = 0;
	char c;

	for(size_t i = 0; i < s.GetLength(); i++)
	{
		switch(s[i])
		{
			case '0':
				c = 0x0;
				break;

			case '1':
				c = 0x1;
				break;

			case '2':
				c = 0x2;
				break;

			case '3':
				c = 0x3;
				break;

			case '4':
				c = 0x4;
				break;

			case '5':
				c = 0x5;
				break;

			case '6':
				c = 0x6;
				break;

			case '7':
				c = 0x7;
				break;

			case '8':
				c = 0x8;
				break;

			case '9':
				c = 0x9;
				break;

			case 'a':
			case 'A':
				c = 0xA;
				break;

			case 'b':
			case 'B':
				c = 0xB;
				break;

			case 'c':
			case 'C':
				c = 0xC;
				break;

			case 'd':
			case 'D':
				c = 0xD;
				break;

			case 'e':
			case 'E':
				c = 0xE;
				break;

			case 'f':
			case 'F':
				c = 0xF;
				break;

			default:
				continue;
		}

		if(bPair)
			m_pCharBuffer[m_iUsedSize++] |= c;
		else
			m_pCharBuffer[m_iUsedSize] |= (c << 4);

		bPair = !bPair;
	}

	return 0;
}

bool CFwkBuffer::AsString(CFwkString& fstr, size_t& iSize)
{
	size_t iLen = GetUsedSize();
	if(iLen == 0)
		return false;

	char* p = fstr.GetPointer(iLen + 2);
	if(p == 0)
		return false;

	memcpy(p, m_pCharBuffer, iLen);
	p[iLen] = 0;
	iSize = iLen;
	return true;
}
